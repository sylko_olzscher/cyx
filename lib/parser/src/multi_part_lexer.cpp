﻿/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/multi_part_lexer.h>
#include <cyx/http/content_disposition.hpp>
#include <cyx/http/parser/content_disposition_parser.h>
#include <cyx/http/parser/content_type_parser.h>
#include <cyx/http/intrinsics/factory/mime_factory.h>
#include <cyx/http/intrinsics/factory/path_factory.h>
#include <cyx/http/intrinsics/factory/query_factory.h>
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/factory/buffer_factory.h>
#include <cyy/intrinsics/factory/string_factory.h>
#include <cyy/intrinsics/factory/boolean_factory.h>
#include <cyy/intrinsics/factory/numeric_factory.h>
#include <cyy/io/format/bytes_format.h>
#include <boost/algorithm/string/replace.hpp>

namespace cyx
{
	multi_part_lexer::multi_part_lexer(std::function<void(cyy::vector_t&&)> cb
		, logger_type& l
		, std::uint32_t& content_size
		, request& req)
	: state_(chunk_boundary)
		, cb_(cb)
		, logger_(l)
		, upload_()
		, is_ws_()
		, is_cr_()
		, is_nl_()
		, boundary_()
		, chunck_()
		, content_size_(content_size)
		, request_(req)
		, upload_size_(0)
		, progress_(0)
	{}

	void multi_part_lexer::reset(std::string const& boundary)
	{
#ifdef _DEBUG
		CYY_LOG_TRACE(logger_, "reset multi-part lexer");
#endif

		//
		//	reset temporary data
		//
		clear(upload_);

		//CYY_LOG_TRACE(logger_, "boundary: "
		//	<< boundary);
		boundary_ = boundary;

		//
		//	reset upload size
		//
		upload_size_ = 0;
		chunck_.clear();

	}


	http::lexer_state_enum multi_part_lexer::parse(char c)
	{
		//
		//	update total upload size
		//
		upload_size_++;

		if (upload_size_ > content_size_)
		{
			CYY_LOG_ERROR(logger_, "more data received then specified "
				<< cyy::bytes(upload_size_)
				<< " - "
				<< cyy::bytes(content_size_)
				<< " = "
				<< (upload_.data_.size() - content_size_)
				<< " bytes");

		}

		std::uint32_t progress = ((100u * upload_size_) / content_size_);
		if (progress != progress_)
		{
			progress_ = progress;

			//
			//	producing too much output garbage
			//
#ifdef _DEBUG
			//CYY_LOG_TRACE(logger_, "upload "
			//	<< upload_size_
			//	<< " / "
			//	<< content_size_
			//	<< " = "
			//	<< progress_
			//	<< "% " );
#endif

			cb_(cyy::generate_invoke("http.upload.progress"
				, cyy::factory(upload_size_)
				, cyy::factory(content_size_)
				, cyy::factory(progress_)
			));

		}

		//	manage upload state 
		switch (state_)
		{
		case chunk_boundary:

			if (upload_size_ < 4)
			{
				BOOST_ASSERT_MSG(c == '-', "not a boundary");
			}

			//	store boundary in buffer
			chunck_.push_back(c);

			//	ignore trailing "-" signs (hyphens)
			if (boost::algorithm::equals(chunck_, boundary_))
			{
				state_ = chunk_boundary_cr;
			}
			break;

		case chunk_boundary_cr:
			switch (c)
			{
			case '-':
				state_ = chunk_boundary_end;
				break;
			default:
				if (!is_cr_(c))
				{
					CYY_LOG_ERROR(logger_, "CR expected");
				}
				state_ = chunk_boundary_nl;
				break;
			}
			break;

		case chunk_boundary_end:
			//	gracefull end of upload
			if (c != '-')
			{
				CYY_LOG_ERROR(logger_, "- expected");
			}
			else
			{
				if (upload_size_ + 2 == content_size_)
				{
					CYY_LOG_TRACE(logger_, "end of upload "
						<< to_str(request_.uri_.path_));

					//	send HTTP response
					cb_(cyy::generate_invoke("http.upload.complete"
						, cyy::true_factory()	//	OK
						, cyy::factory(upload_size_)
						, cyy::factory(request_.uri_.path_)
						, cyy::factory(request_.uri_.query_)
						, request_.uri_.fragment_
					));

				}
				else
				{
					CYY_LOG_WARNING(logger_, "upload incomplete");

					//	send HTTP response
					cb_(cyy::generate_invoke("http.upload.complete"
						, cyy::false_factory()	//	error
						, cyy::factory(upload_size_)
						, cyy::factory(request_.uri_.path_)
						, cyy::factory(request_.uri_.query_)
						, request_.uri_.fragment_
					));
				}
			}
			state_ = chunk_boundary;
			return http::method_start;

		case chunk_boundary_nl:
			if (!is_nl_(c))
			{
				CYY_LOG_ERROR(logger_, "NL expected");
			}

			//	create new part
			state_ = chunk_header;
			chunck_.clear();
			break;

		case chunk_header:
			if (is_cr_(c))
			{

				//	header line complete
				if (chunck_.empty())
				{
					state_ = chunk_data_start;
				}
				else
				{
					CYY_LOG_TRACE(logger_, "chunk line: "
						<< chunck_);

					state_ = chunk_header_nl;
				}
			}
			else
			{

				//	read header into buffer
				chunck_.push_back(c);
			}
			break;

		case chunk_header_nl:
			if (boost::algorithm::starts_with(chunck_, "Content-Disposition:"))
			{
				
				boost::algorithm::replace_first(chunck_, "Content-Disposition:", "");
				content_disposition cd;
				bool b = parse_content_disposition(chunck_, cd);
				if (b)	
				{
					//	get variable name
					const std::string var_name = lookup_param(cd.params_, "name");
					CYY_LOG_TRACE(logger_, "variable name: "
						<< var_name);

					//	get filename
					const std::string filename = lookup_filename(cd.params_);
					if (!filename.empty())
					{
						CYY_LOG_TRACE(logger_, "upload file: "
							<< filename);
					}

					switch (cd.type_)
					{
					case rfc2183::cdv_inline:		
						//	displayed automatically, [RFC2183]
						CYY_LOG_WARNING(logger_, "inlines not supported yet: "
							<< chunck_);
						break;
					case rfc2183::cdv_attachment: 
						//	user controlled display, [RFC2183]
						CYY_LOG_WARNING(logger_, "attachments not supported yet: "
							<< chunck_);
						break;
					case rfc2183::cdv_form_data: 
						//	process as form response, [RFC7578]
						upload_.meta_.assign(cd.params_.begin(), cd.params_.end());
						break;

					default:
						break;
						//	unknown
						CYY_LOG_ERROR(logger_, "unknown content type: "
							<< chunck_);
					}
				}
				else
				{
					CYY_LOG_ERROR(logger_, "parsing content disposition failed: "
						<< chunck_);

				}
			}
			else if (boost::algorithm::starts_with(chunck_, "Content-Type:"))
			{
				//	get MIME type
				boost::algorithm::replace_first(chunck_, "Content-Type:", "");
				const bool b = get_http_mime_type(chunck_, upload_.type_);
				if (!b)
				{
					CYY_LOG_ERROR(logger_, "parsing content type failed: "
						<< chunck_);

				}
				state_ = chunk_boundary_nl;
			}
			else
			{
				CYY_LOG_ERROR(logger_, "unknown chunk attribute: "
					<< chunck_);
			}

			if (is_cr_(c))
			{
				CYY_LOG_ERROR(logger_, "CR expected");
			}

			//	next state
			state_ = chunk_header;
			chunck_.clear();
			break;

		case chunk_data_start:
			if (is_cr_(c))
			{
				CYY_LOG_ERROR(logger_, "CR expected");
			}
			state_ = chunk_data;
			break;

		case chunk_data:
		{
			//	detect boundary
			if (c == '-')
			{
				chunck_.clear();
				chunck_.push_back(c);
				state_ = chunk_esc1;
			}
			else 
			{

				//	save data into memory
				//	or write on disk
				upload_.data_.push_back(c);
			}

			//	upload is running
		}
		break;

		case chunk_esc1:
			chunck_.push_back(c);
			if (c == '-')
			{
				state_ = chunk_esc2;
			}
			else
			{
				BOOST_ASSERT_MSG(chunck_.size() == 2, "wrong chunk size");
				upload_.data_.insert(upload_.data_.end(), chunck_.begin(), chunck_.end());
				state_ = chunk_data;
			}
			break;

		case chunk_esc2:
			chunck_.push_back(c);
			if (c == '-')
			{
				state_ = chunk_esc3;
			}
			else
			{
				BOOST_ASSERT_MSG(chunck_.size() == 3, "wrong chunk size");
				upload_.data_.insert(upload_.data_.end(), chunck_.begin(), chunck_.end());
				state_ = chunk_data;
			}
			break;

		case chunk_esc3:
			chunck_.push_back(c);
			if (chunck_.size() == boundary_.size())
			{
				if (boost::algorithm::starts_with(chunck_, boundary_))
				{
					std::string var_name = lookup_param(upload_.meta_, "name");

					CYY_LOG_INFO(logger_, "upload of ["
						<< var_name
						<< "] completed");

					std::string filename = lookup_filename(upload_.meta_);

					if (filename.empty() && (upload_.data_.size() > 2))
					{
						//	remove <CR><NL> tail
						upload_.data_.pop_back();
						upload_.data_.pop_back();

						CYY_LOG_TRACE(logger_, var_name
							<< " = "
							<< cyy::to_str(upload_.data_));

					}

					//
					//	invoke callback
					//

					cb_(cyy::generate_invoke("http.upload.data"
						, cyy::factory(std::move(var_name))
						, cyy::factory(std::move(filename))
						, cyy::factory(upload_.type_)
						, cyy::factory(std::move(upload_.data_))
						, cyy::factory(request_.uri_.path_)
					));

					//	next boundary
					state_ = chunk_boundary_cr;
				}
				else
				{
					//	restore data
					upload_.data_.insert(upload_.data_.end(), chunck_.begin(), chunck_.end());
					//	continue read data stream
					state_ = chunk_data;
				}
			}
			break;

		default:
			CYY_LOG_FATAL(logger_, "illegal upload state: "
				<< state_);
			break;
		}

		//
		//	stay in multipart state
		//
		return http::multipart;
	}

	std::string multi_part_lexer::lookup_filename(param_container_t const& phrases)
	{
		//	The parameters "filename" and "filename*" differ only in that "filename*" uses the encoding 
		//	defined in RFC 5987. When both "filename" and "filename*" are present in a single header field value, 
		//	"filename*" is preferred over "filename" when both are present and understood.

		const std::string filename = lookup_param(phrases, "filename*");
		return (filename.empty())
			? lookup_param(phrases, "filename")
			: filename
			;
	}


}
