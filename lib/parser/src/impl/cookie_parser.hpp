/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_COOKIE_PARSER_HPP
#define CYX_HTTP_COOKIE_PARSER_HPP


#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/cookie_parser.h>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/std_pair.hpp>

namespace cyx
{

	/**
	*	According to http://tools.ietf.org/html/rfc2965
	*	HTTP Cookie
	*	syntax:
	*	@code
	The syntax for the header is:

	cookie          =  "Cookie:" cookie-version 1*((";" | ",") cookie-value)
	cookie-value    =  NAME "=" VALUE [";" path] [";" domain] [";" port]
	cookie-version  =  "$Version" "=" value
	NAME            =  attr
	VALUE           =  value
	path            =  "$Path" "=" value
	domain          =  "$Domain" "=" value
	port            =  "$Port" [ "=" <"> value <"> ]
	*	@endcode
	*
	*	examples:
	*	@code
	Cookie: $Version="1"; name=newvalue; expires=date; path=/; domain=.example.org
	Cookie: __utma=21769261.3385744493344380400.1226516557.1254512919.1257273134.452
	*	@endcode
	*/
	template <typename Iterator>
	cookie_parser < Iterator > ::cookie_parser()
		: cookie_parser::base_type(cookie_)
	{
			//	namespace sp = boost::spirit;

			cookie_
				%= pair_ % ';'
				;

			pair_
				%= *boost::spirit::ascii::blank >> key_ >> *boost::spirit::ascii::blank >> '=' >> value_
				;

			key_
				%= +(boost::spirit::ascii::char_ - boost::spirit::ascii::char_(" \t\";="))
				;

			value_
				= r_quoted_string[boost::spirit::_val = boost::spirit::_1]
				| r_token[boost::spirit::_val = boost::spirit::_1]
				;

			r_token
				%= *(boost::spirit::ascii::char_ - boost::spirit::ascii::char_("\t $;\""))
				;

	}


}	//	cyx

#endif	//	CYX_HTTP_COOKIE_PARSER_HPP
