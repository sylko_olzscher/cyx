/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_FORM_PARSER_HPP
#define CYX_HTTP_FORM_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/form_parser.h>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace cyx
{

	template <typename Iterator>
	form_parser<Iterator>::form_parser()
		: form_parser::base_type(r_start)
	{
		r_start
			= *r_sep >> r_pair >> *(r_sep >> r_pair)
			;

		r_pair
			= r_key >> -('=' >> -r_value)	//	optional value
			;

		r_value
			= +((boost::spirit::ascii::char_ - boost::spirit::ascii::char_("%&# \"\t")) | r_esc_)
			;

		r_sep
			= +(boost::spirit::qi::lit(';') | '&')
			;
	}


}	//	cyx


#endif	//	CYX_HTTP_FORM_PARSER_HPP
