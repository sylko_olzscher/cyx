/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_CONTENT_TYPE_PARSER_HPP
#define CYX_HTTP_CONTENT_TYPE_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/content_type_parser.h>

#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/adapted/struct.hpp>

namespace cyx	
{
	//	examples:
	//	multipart/form-data; boundary=----WebKitFormBoundaryBStJolBqHji2FoKK
	template <typename Iterator>
	mime_content_type_parser< Iterator > ::mime_content_type_parser()
		: mime_content_type_parser::base_type(content_type_header)
	{
		content_type_header
			= *boost::spirit::qi::lit(' ')
			>> part
			>> '/'
			>> sub_part
			>> *r_phrase
			;

		part
			= r_token
			| extension_token
			;

		sub_part
			= r_token
			| extension_token
			;

		r_phrase
			= boost::spirit::qi::lit(';')
			>> +ws_
			>> r_attr
			>> '='
			>> value
			>> *ws_
			;

		ws_
			= r_white_space
			| r_line_sep
			| r_comment
			;

		r_attr = r_token.alias();

		value
			= +(r_qp | boost::spirit::qi::char_(" -~"))
			;

		r_token
			= +(boost::spirit::qi::alnum | boost::spirit::qi::char_("-"))
			;

		extension_token
			= boost::spirit::qi::char_("Xx")
			>> boost::spirit::qi::lit('-')
			>> r_token
			;
	}
}	//	cyx


BOOST_FUSION_ADAPT_STRUCT(
	cyx::mime_content_type,
	(std::string, type_)
	(std::string, sub_type_)
	(cyx::param_container_t, phrases_)
	)

#endif	//	CYX_HTTP_CONTENT_TYPE_PARSER_HPP
