/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#ifndef CYX_HTTP_AUTH_PARSER_HPP
#define CYX_HTTP_AUTH_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <cyx/http/parser/auth_parser.h>
#include <cyy/crypto/base64.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace cyx
{
	namespace 
	{	//	anonymous

		std::string decode(std::string const& s)
		{
			std::string result;
			auto bi = std::back_inserter(result);
			//	decode
			cyy::crypto::base64::decode<>(s, bi);
			return result;
		}

	}	//	anonymous

		/**
		*	Example:
		*	@code
		Authorization: Digest username="Mufasa",
		realm="testrealm@host.com",
		nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
		uri="/htdocs/",
		algorithm=MD5
		response="72ab5c8b48cf7513264cd9c93fc7a1ec",
		opaque="5ccc069c403ebaf9f0171e9517f40e41",
		qop=auth,
		nc=00000001,
		cnonce="773bf5c409960bf7"
		*	@endcode
		*/
	template <typename Iterator>
	auth_parser< Iterator >::auth_parser()
		: auth_parser::base_type(r_start)
	{
		r_start
			= boost::spirit::qi::lit("Basic")[boost::phoenix::bind(&authentification::scheme_, boost::spirit::_val) = auth::BASIC]
			>> *boost::spirit::qi::lit(' ')
			>> r_basic[boost::phoenix::bind(&authentification::credentials_, boost::spirit::_val) = boost::spirit::qi::_1]

			| boost::spirit::qi::lit("Digest")[boost::phoenix::bind(&authentification::scheme_, boost::spirit::_val) = auth::DIGEST]
			>> *((*boost::spirit::qi::blank >>
			(r_username[boost::phoenix::bind(&authentification::user_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_realm[boost::phoenix::bind(&authentification::realm_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_nonce[boost::phoenix::bind(&authentification::nonce_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_uri[boost::phoenix::bind(&authentification::uri_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_algo[boost::phoenix::bind(&authentification::algorithm_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_response[boost::phoenix::bind(&authentification::response_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_opaque[boost::phoenix::bind(&authentification::opaque_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_qop[boost::phoenix::bind(&authentification::qop_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_nc[boost::phoenix::bind(&authentification::nc_, boost::spirit::_val) = boost::spirit::qi::_1]
				| r_cnonce[boost::phoenix::bind(&authentification::cnonce_, boost::spirit::_val) = boost::spirit::qi::_1]
				)) % boost::spirit::qi::lit(",")
				)

			;

		//	Basic d2lraTpwZWRpYQ==
		r_basic
			= r_token[boost::spirit::_val = boost::phoenix::bind(&decode, boost::spirit::qi::_1)]
			;

		//	username="Mufasa"
		r_username
			= boost::spirit::qi::lit("username=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	realm="testrealm@host.com"
		r_realm
			= boost::spirit::qi::lit("realm=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093"
		r_nonce
			= boost::spirit::qi::lit("nonce=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//cnonce="773bf5c409960bf7"
		r_cnonce
			= boost::spirit::qi::lit("cnonce=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	algorithm=MD5
		r_algo
			= boost::spirit::qi::lit("algorithm=")
			>> (r_token[boost::spirit::_val = boost::spirit::qi::_1] | r_quote[boost::spirit::_val = boost::spirit::qi::_1])
			;

		//	uri="/digest-auth.html"
		r_uri
			= boost::spirit::qi::lit("uri=")
			>> r_uri_string[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	response="72ab5c8b48cf7513264cd9c93fc7a1ec"
		r_response
			= boost::spirit::qi::lit("response=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	opaque="5ccc069c403ebaf9f0171e9517f40e41"
		r_opaque
			= boost::spirit::qi::lit("opaque=")
			>> r_quote[boost::spirit::_val = boost::spirit::qi::_1]
			;

		//	qop=auth
		r_qop
			= boost::spirit::qi::lit("qop=")
			>> ((boost::spirit::qi::lit("auth") | boost::spirit::qi::lit("\"auth\""))[boost::spirit::_val = authentification::AUTH]
				| (boost::spirit::qi::lit("auth-int") | boost::spirit::qi::lit("\"auth-int\""))[boost::spirit::_val = authentification::AUTH_INT]
				| (boost::spirit::qi::lit("auth-conf") | boost::spirit::qi::lit("\"auth-conf\""))[boost::spirit::_val = authentification::AUTH_CONF]
				| (boost::spirit::qi::lit("token") | boost::spirit::qi::lit("\"token\""))[boost::spirit::_val = authentification::TOKEN]
				)
			;

		r_nc
			= boost::spirit::qi::lit("nc=") >> boost::spirit::hex[boost::spirit::_val = boost::spirit::qi::_1]
			;

	}
}	//	cyx


#endif	//	CYX_HTTP_AUTH_PARSER_HPP
