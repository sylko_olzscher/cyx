/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#ifndef CYX_HTTP_HEADER_PARSER_HPP
#define CYX_HTTP_HEADER_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <cyx/http/parser/header_parser.h>
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <boost/spirit/home/support/attributes.hpp>	//	transform_attribute
#include <boost/spirit/include/phoenix.hpp>	//	enable assignment of values like cyy::object

namespace boost		
{
    namespace spirit	
	{
        namespace traits	
		{

            template <>
            struct transform_attribute< cyy::object, std::string, boost::spirit::qi::domain >
            {
                typedef std::string type;
                static type pre(cyy::object& v)
                {
//                    std::cerr << "pre(std::string)" << std::endl;
                    return type();
                }
                static void post(cyy::object& val, type const& attr)
                {
//                    std::cerr << "post(" << attr << ")" << std::endl;
                    val = cyy::factory(attr);
                }
                static void fail(cyy::object&)
                {}
            };

            /**
             * Define a customization point with transform_attribute<>.
             *
             * @see http://boost-spirit.com/home/2010/02/08/how-to-adapt-templates-as-a-fusion-sequence/
             */
            template <>
            struct transform_attribute< cyy::param_t, boost::fusion::vector < std::string, cyy::object >, boost::spirit::qi::domain >
            {
				typedef boost::fusion::vector < std::string, cyy::object > type;
                static type pre(cyy::param_t& param)
                {
                    //std::cerr << "pre(cyy::param_t)" << std::endl;
					return type("", cyy::object());
				}
                static void post(cyy::param_t& param, type const& attr)
                {
					const std::string key = boost::fusion::front(attr);
					//std::cerr << "post(" << key << ")" << std::endl;
					const cyy::object value = boost::fusion::back(attr);
					//param = cyy::param_t(key, value);
					param = std::make_pair(key, value);
				}
                static void fail(cyy::param_t&)
                {}
            };

			/**
			 * Define a customization point with transform_attribute<> for
			 * boost::fusion::vector<std::string, std::vector<std::string>>
			 */
			template <>
			struct transform_attribute< cyy::param_t, boost::fusion::vector<std::string, std::vector<std::string>>, boost::spirit::qi::domain >
			{
				typedef boost::fusion::vector<std::string, std::vector<std::string>> type;
				static type pre(cyy::param_t& param)
				{
					//std::cerr << "pre(std::vector<std::string>)" << std::endl;
					return type();
				}
				static void post(cyy::param_t& param, type const& attr)
				{
					const std::string key = boost::fusion::front(attr);
					//std::cerr << "post(" << key << ", std::vector<std::string>)" << std::endl;
					const std::vector<std::string> vec = boost::fusion::back(attr);
					param = cyy::param_t(key, cyy::vector_factory(vec));
				}
				static void fail(cyy::param_t&)
				{}
			};

        }	//	traits
    }	//	spirit
}	//	boost

namespace cyx
{

    template <typename Iterator>
    header_parser<Iterator>::header_parser()
        : header_parser::base_type(r_start)
    {

        namespace sp = boost::spirit;

        r_start
            = r_uireq
            | r_dnt
            | r_pair.alias()
            ;

        r_pair
			= r_connection
			| r_length	//	Content-Length
			| r_ws_version	//	Sec-WebSocket-Version
			| r_ws_protocols
			| r_encodings
            | r_param   //	transform_attribute<>(...)
            ;

        //
        //	r_param synthesizes an attribute
        //	of type boost::fusion::vector< std::string, object>
        //
        r_param
            = r_key
            >> sp::omit[*sp::ascii::blank]
            >> r_value
            ;

        r_value
            = r_string	//	transform_attribute<>(...)
            ;

		//	read until end of line
        r_string
            %= *(boost::spirit::qi::ascii::char_ - '\r')
            ;

		//	accept a comma-separated list of values
        r_connection
			= boost::spirit::ascii::string("Connection") >> r_def >> r_list
            ;

        r_uireq	//	https://www.w3.org/TR/upgrade-insecure-requests/#preference
            = boost::spirit::lit("Upgrade-Insecure-Requests: 1")[boost::spirit::_val = cyy::make_parameter("Upgrade-Insecure-Requests", cyy::true_factory())]
            ;

		r_dnt	//	do not track
            = boost::spirit::ascii::no_case[boost::spirit::lit("DNT: 1")][boost::spirit::_val = cyy::make_parameter("DNT", cyy::true_factory())]      
            | boost::spirit::ascii::no_case[boost::spirit::lit("DNT: 0")][boost::spirit::_val = cyy::make_parameter("DNT", cyy::false_factory())]
			;

        //
        //	r_length synthesizes an attribute
        //	of type boost::fusion::vector< std::string, object>
        //
		r_length
			= boost::spirit::ascii::string("Content-Length") >> r_def >> r_int
			;

		//
		//	example:
		//	Sec-WebSocket-Version: 13
		//	boost::fusion::vector<std::string, cyy::object>
		//
		r_ws_version
			= boost::spirit::ascii::string("Sec-WebSocket-Version") >> r_def >> r_int
			;

		//
		//	example:
		//	Accept-Encoding: gzip, deflate, sdch
		//	boost::fusion::vector<std::string, std::vector<std::string>>
		//
		r_encodings
			= boost::spirit::ascii::string("Accept-Encoding") >> r_def >> r_list
			;

		//
		//	example:
		//	Sec-WebSocket-Protocol: soap, xmpp
		//	boost::fusion::vector<std::string, std::vector<std::string>>
		//
		r_ws_protocols
			= boost::spirit::ascii::string("Sec-WebSocket-Protocol") >> r_def >> r_list
			;

		//
		//	std::vector<std::string>
		//
		r_list
			%= r_token % r_sep
			;

		r_sep
			= +(boost::spirit::lit(',') | boost::spirit::lit(' '))
			;

		r_def
			= boost::spirit::lit(':') >> *boost::spirit::lit(' ')
			;

	}



}	//	cyx



#endif	//	s
