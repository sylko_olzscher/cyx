/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_PATH_PARSER_HPP
#define CYX_HTTP_PATH_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/path_parser.h>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace cyx
{

    template <typename Iterator>
    uri_char_parser< Iterator > ::uri_char_parser()
        : uri_char_parser::base_type(r_start)
    {
        //	A resource path contains a '/' at least.
        r_start
            = r_char[boost::spirit::_val = boost::spirit::_1]
            | r_esc[boost::spirit::_val = boost::spirit::_1]
            //  treat '+' as SPACE
            | boost::spirit::lit('+')[boost::spirit::_val = ' ']
            ;

        r_char
            %= (boost::spirit::ascii::char_ - boost::spirit::ascii::char_(" ?+%#\"\t\r\n"))
            ;

    }

    /**
     *	The query string syntax
     *	is not generically defined, but is commonly organized as a sequence of <key>=<value> pairs
     *	separated by a semicolon or separated by an ampersand, for example:
     *	@code
     Semicolon: key1=value1;key2=value2;key3=value3
     Ampersand: key1=value1&key2=value2&key3=value3
     *	@endcode
     *
	 *	Spaces not allowed.
	 *
     *	 Within a query component, the characters ";", "/", "?", ":", "@",
     *   "&", "=", "+", ",", and "$" are reserved.
     */
    template <typename Iterator>
    query_parser<Iterator>::query_parser()
        : query_parser::base_type(r_start)
    {
            r_start
                = r_pair
                >> *((boost::spirit::qi::lit(';') | '&') >> r_pair)
                ;

            r_pair
                %= r_key >> -('=' >> -r_val)	//	value is optional
                ;

			r_val
				= +((boost::spirit::ascii::char_ - boost::spirit::ascii::char_("%;&# \"\t")) | r_esc)
                ;
    }

    template <typename Iterator>
    path_parser< Iterator > ::path_parser()
        : path_parser::base_type(r_start)	{

            //	A resource path contains a '/' at least.
            r_start
                %= separator_
                >> -segments_
                >> *separator_
                ;

			//
			//	adds new segements to the vector (path)
			//
            segments_
                %= segment_ % separator_
                ;

            segment_
                //	an uri char is none of this: " ?+%#\"\t\r\n"
                %= +(uri_char_ - boost::spirit::ascii::char_("//\"{}"))
                ;

            separator_
                = boost::spirit::lit('/') | boost::spirit::lit('\\')
                ;
    }

    template <typename Iterator>
    path_string_parser< Iterator > ::path_string_parser()
        : path_string_parser::base_type(r_start) {

            r_start
                %= boost::spirit::lexeme['"' >> path_p >> '"']
                ;
    }


}	//	cyx


#endif	//	CYX_HTTP_PATH_PARSER_HPP
