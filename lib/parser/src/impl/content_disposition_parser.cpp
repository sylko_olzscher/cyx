/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "content_disposition_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>

namespace cyx	
{

	template struct content_disposition_parser<std::string::const_iterator>;
	template struct content_disposition_parser<cyy::buffer_t::const_iterator>;

	bool parse_content_disposition(std::string const& inp, content_disposition& result)
	{
		static content_disposition_parser< std::string::const_iterator >	g;
		std::string::const_iterator first = inp.begin();
		std::string::const_iterator last = inp.end();
		return boost::spirit::qi::parse(first, last, g, result);
	}

	bool parse_content_disposition(cyy::buffer_t const& inp, content_disposition& result)
	{
		static content_disposition_parser< cyy::buffer_t::const_iterator >	g;
		cyy::buffer_t::const_iterator first = inp.begin();
		cyy::buffer_t::const_iterator last = inp.end();
		return boost::spirit::qi::parse(first, last, g, result);
	}

}	//	cyx
