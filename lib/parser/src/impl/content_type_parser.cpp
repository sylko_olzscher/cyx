/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "content_type_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>

namespace cyx	
{

	template struct mime_content_type_parser<std::string::const_iterator>;
	template struct mime_content_type_parser<cyy::buffer_t::const_iterator>;

	bool get_http_mime_type(std::string const& inp, mime_content_type& result)	
	{

		static mime_content_type_parser< std::string::const_iterator >	g;
		std::string::const_iterator first = inp.begin();
		std::string::const_iterator last = inp.end();
		return boost::spirit::qi::parse(first, last, g, result);
	}

	bool get_http_mime_type(cyy::buffer_t const& inp, mime_content_type& result)	
	{

		static mime_content_type_parser< cyy::buffer_t::const_iterator >	g;
		cyy::buffer_t::const_iterator first = inp.begin();
		cyy::buffer_t::const_iterator last = inp.end();
		return boost::spirit::qi::parse(first, last, g, result);
	}


}	//	cyx
