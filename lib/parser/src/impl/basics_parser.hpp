/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#ifndef CYX_HTTP_BASICS_PARSER_HPP
#define CYX_HTTP_BASICS_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <cyx/http/parser/basics_parser.h>
#include <boost/spirit/include/phoenix.hpp>

namespace cyx
{
	template <typename Iterator>
	escape_parser<Iterator>::escape_parser()
		: escape_parser::base_type(r_start)
	{
		//  this statement requires to include the phoenix library
		r_start
			= ('%' >> r_hex)[boost::spirit::_val = boost::spirit::_1]
			//= ('%' >> r_hex >> r_hex)[boost::spirit::_val = (boost::spirit::_1 * 0x10) + boost::spirit::_2]
			;
	}

    template <typename Iterator>
	line_separator<Iterator>::line_separator()
        : line_separator::base_type(r_start)
    {

        r_start
            = boost::spirit::qi::lexeme[boost::spirit::qi::lit("\015\012")]
            ;
    }

	template <typename Iterator>
	white_space<Iterator>::white_space()
		: white_space::base_type(r_start)
	{
		//	same as boost::spirit::ascii::blank
		r_start
			= boost::spirit::qi::lit(' ') 
			| boost::spirit::qi::lit('\t')
			;
	}

	template <typename Iterator>
	printable<Iterator>::printable()
		: printable::base_type(r_start)
	{

		r_start
			= boost::spirit::qi::char_(" -~")
			;
	}

	namespace
	{	//	*anonymous*
		struct esc_symbols : boost::spirit::qi::symbols<char const, char const>
		{
			/**
			 * same escape symbols as in C/C++
			 * HTTP supports some addiotional characters.
			 */
			esc_symbols()
			{
				add
					//	C/C++
					("\\a", '\a')
					("\\b", '\b')
					("\\f", '\f')
					("\\n", '\n')
					("\\r", '\r')
					("\\t", '\t')
					("\\v", '\v')
					("\\\\", '\\')
					("\\\'", '\'')
					("\\\"", '\"')

					//	rfc822
					("\\(", '(')
					("\\)", ')')
					("\\<", '<')
					("\\>", '>')
					("\\@", '@')
					("\\;", ';')
					("\\,", ',')
					("\\.", '.')
					("\\[", '[')
					("\\]", ']')
					;
			}
		}	r_quoted;
	}	//	*anonymous*

	template <typename Iterator>
	quoted_pair<Iterator>::quoted_pair()
		: quoted_pair::base_type(r_start)
	{
		r_start
			= r_quoted
			;
	}

	template <typename Iterator>
	header_field<Iterator>::header_field()
		: header_field::base_type(r_start)
	{
		r_start
			//= +(boost::spirit::qi::char_(" -~") - boost::spirit::qi::char_(":"))
			= +(r_printable - boost::spirit::qi::char_(":")) >> ':'
			;
	}

	template <typename Iterator>
	header_field_strict<Iterator>::header_field_strict()
		: header_field_strict::base_type(r_start)
	{
		r_start
			= +(r_printable - boost::spirit::qi::char_("()<>@,;:\\/[]?={} ")) >> ':'
			;
	}

	template <typename Iterator>
	utf8_filename<Iterator>::utf8_filename()
		: utf8_filename::base_type(r_start)
	{
		//	utf-8 encoded file name (filename*=utf-8''%e2%82%ac%20rates)

		r_start
			= boost::spirit::qi::lit("utf-8''")
			>> r_name
			;

		r_name
			= +(boost::spirit::qi::alnum | r_esc_)
			;

	}

	template <typename Iterator>
	quoted_string<Iterator>::quoted_string()
		: quoted_string::base_type(r_start)
	{
		//	quoted string(filename = "EURO rates";)

		r_start
			= boost::spirit::qi::lexeme['"' >> r_name >> '"'];
			//%= boost::spirit::qi::lexeme['"' >> r_name >> '"'];
		;

		r_name
			= *(r_esc_ | (boost::spirit::ascii::char_ - '"'))
			;

	}

	template <typename Iterator>
	comment<Iterator>::comment()
		: comment::base_type(r_start)
	{
		r_start
			= boost::spirit::qi::lit('(') >> r_string >> boost::spirit::qi::lit(')')
			;

		r_string
			= boost::spirit::qi::lit('(') >> r_string.alias() >> boost::spirit::qi::lit(')')
			| r_ctext
			//| *r_qp
			;

		//	any printable character except '(' and ')'
		r_ctext
			= boost::spirit::qi::hold[*(r_printable - boost::spirit::qi::char_("()"))]
			;

	}

}	//	cyx


#endif	//	CYX_HTTP_BASICS_PARSER_HPP
