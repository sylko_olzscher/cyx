/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_METHOD_PARSER_HPP
#define CYX_HTTP_METHOD_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/http.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_statement.hpp>

namespace cyx
{
    namespace
    {	//	*anonymous*
        struct method_parser : boost::spirit::qi::symbols<char, methods::method>
        {
            method_parser()
            {
                /**
                 *	Only lower case entries are valid. See remark below.
                 */
                add
                    ("head", methods::Head)
                    ("get", methods::Get)
                    ("post", methods::Post)
                    ("put", methods::Put)
                    ("delete", methods::Delete)
                    ("trace", methods::Trace)
                    ("options", methods::Options)
                    ("connect", methods::Connect)
					("copy", methods::Copy)
					("move", methods::Move)
					("lock", methods::Lock)
					("unlock", methods::Unlock)
					("mkcol", methods::MkCol)
					("propfind", methods::Propfind)
					("proppatch", methods::Proppatch)
                    ;

            }
        }	method_symbols;
    }	//	*anonymous*

    /**
    *	@note There is a stupid behaviour that symbols with the same case as in the
    *	method symbol table does NOT match. e.g. POST != "POST". This bug is ugly
    *	and not solved yet.
    */
    template < typename Iterator >
    bool get_http_method(Iterator first, Iterator last, methods::method& m)
    {
        const bool r = boost::spirit::qi::parse(first, last,
            (boost::spirit::ascii::no_case[method_symbols[boost::phoenix::ref(m) = boost::spirit::_1]]));

        return (first != last)
            ? false // fail if we did not get a full match
            : r
            ;
    }


}	//	cyx
#endif	//	CYX_HTTP_METHOD_PARSER_HPP

