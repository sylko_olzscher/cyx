/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#ifndef CYX_HTTP_URL_PARSER_HPP
#define CYX_HTTP_URL_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <cyx/http/parser/url_parser.h>
#include <boost/fusion/adapted.hpp>

namespace cyx
{

    template <typename Iterator>
    url_parser<Iterator>::url_parser()
        : url_parser::base_type(r_start)
    {

        namespace sp = boost::spirit;

        r_start
            %= path_ 
            >> -('?' >> r_query)
            >> -('#' >> r_frag)
            ;

        //	A resource path contains a '/' at least.
        path_
            %= r_path_
            ;

        r_frag
            %= *r_char_	//	dummy
            ;

    }

    template <typename Iterator>
    url_parser_string<Iterator>::url_parser_string()
        : url_parser_string::base_type(r_start)
    {
        namespace sp = boost::spirit;

        r_start
            = boost::spirit::qi::lit('"')
            >> r_url
            >> boost::spirit::qi::lit('"')
            ;
    }

}	//	cyx

BOOST_FUSION_ADAPT_STRUCT(
    cyx::uri,
    (cyx::path_t, path_)		//	[0]
    (cyx::query_t, query_)      //	[1]
    (std::string, fragment_)	//	[2]
    )

#endif	//	CYX_HTTP_URL_PARSER_HPP
