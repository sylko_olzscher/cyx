/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#include "auth_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <string>

namespace cyx
{
	template struct auth_parser<std::string::const_iterator>;
	template struct auth_parser<cyy::buffer_t::const_iterator>;
	
	std::pair<authentification, bool>	parse_auth(std::string const& inp)
	{
		static auth_parser< std::string::const_iterator >	g;
		std::string::const_iterator first = inp.begin();
		std::string::const_iterator last = inp.end();
		authentification a;
		//return std::make_pair(a, boost::spirit::qi::parse(first, last, g, a));
		return (boost::spirit::qi::parse(first, last, g, a))
			? std::make_pair(a, true)
			: std::make_pair(a, false)
			;

	}

}	//	cyx
