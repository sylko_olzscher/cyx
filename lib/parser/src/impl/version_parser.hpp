/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_VERSION_PARSER_HPP
#define CYX_HTTP_VERSION_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/http.h>

#include <cyy/intrinsics/version.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx
{


    /**
     *	Compare this to cyy::grammar::version_parser
     */
    template < typename Iterator >
    bool get_http_version(Iterator first, Iterator last, cyy::version& v)
    {

        bool r = boost::spirit::qi::phrase_parse(first, last,
            (
            boost::spirit::ascii::no_case[boost::spirit::lit("HTTP/")]
            >> (boost::spirit::qi::ushort_
            >> '.'
            >> boost::spirit::qi::ushort_)[boost::phoenix::ref(v) = (boost::spirit::_1 * 0x10000) + boost::spirit::_2]
            )
            ,
            boost::spirit::ascii::space);

        return (first != last)
            ? false // fail if we did not get a full match
            : r
            ;
    }


}	//	cyx


#endif	//	CYX_HTTP_VERSION_PARSER_HPP
