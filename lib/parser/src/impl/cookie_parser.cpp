/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/

#include "cookie_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>

namespace cyx
{
	template struct cookie_parser<std::string::const_iterator>;
	template struct cookie_parser<cyy::buffer_t::const_iterator>;

}	//	cyx
