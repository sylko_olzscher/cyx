/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "form_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <string>

namespace cyx
{

    template struct form_parser<std::string::const_iterator>;
    template struct form_parser<cyy::buffer_t::const_iterator>;

	bool parse_form(cyy::buffer_t const& input, query_t& result)
	{
		static form_parser< cyy::buffer_t::const_iterator >	g;
		cyy::buffer_t::const_iterator first = input.begin();
		cyy::buffer_t::const_iterator last = input.end();
		return boost::spirit::qi::parse(first, last, g, result);

	}

}	//	cyx


