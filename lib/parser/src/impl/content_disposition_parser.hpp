/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_CONTENT_DISPOSITION_PARSER_HPP
#define CYX_HTTP_CONTENT_DISPOSITION_PARSER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/content_disposition_parser.h>

#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/adapted/struct.hpp>

namespace cyx	
{
	namespace
	{	//	*anonymous*
		struct cdv_parser
			: boost::spirit::qi::symbols<char, rfc2183::values>
		{
			cdv_parser()
			{
				/**
				*	Only lower case entries are valid. See remark below.
				*/
				add
					("inline", rfc2183::cdv_inline)
					("attachment", rfc2183::cdv_attachment)
					("form-data", rfc2183::cdv_form_data)
					("signal", rfc2183::cdv_signal)
					("alert", rfc2183::cdv_alert)
					("icon", rfc2183::cdv_icon)
					("render", rfc2183::cdv_render)
					("recipient-list-history", rfc2183::cdv_recipient_list_history)
					("session", rfc2183::cdv_session)
					("aib", rfc2183::cdv_aib)
					("early-session", rfc2183::cdv_early_session)
					("recipient-list", rfc2183::cdv_recipient_list)
					("notification", rfc2183::cdv_notification)
					("by-reference", rfc2183::cdv_by_reference)
					("info-package", rfc2183::cdv_info_package)
					("recording-session", rfc2183::cdv_recording_session)
					;

			}
		}	content_disposition_values;
	}	//	*anonymous*

	template <typename Iterator>
	content_disposition_parser< Iterator > ::content_disposition_parser()
		: content_disposition_parser::base_type(r_start)
	{
		r_start
			= *r_ws
			>> content_disposition_values
			>> *r_phrase
			;

		r_phrase
			%= boost::spirit::qi::lit(';')
			>> +r_ws
			>> r_token	//.alias()
			>> '='
			>> r_value
			>> *r_ws
			;

		r_token
			= +(boost::spirit::qi::alnum | boost::spirit::qi::char_("-*"))
			;

		r_value
			= r_ut8_filename	//	utf-8''%e2%82%ac%20rates
			| r_quoted_string	//	"demo.jpg"
			| r_token
			;

	}
}	//	cyx

BOOST_FUSION_ADAPT_STRUCT(
	cyx::content_disposition,
	(cyx::rfc2183::values, type_)
	(cyx::param_container_t, params_)
	)

#endif	//	CYX_HTTP_CONTENT_DISPOSITION_PARSER_HPP
