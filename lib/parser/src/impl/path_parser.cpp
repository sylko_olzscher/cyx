/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "path_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <string>

namespace cyx
{

    template struct uri_char_parser<std::string::const_iterator>;
    template struct uri_char_parser<cyy::buffer_t::const_iterator>;

    template struct query_parser<std::string::const_iterator>;
    template struct query_parser<cyy::buffer_t::const_iterator>;

    template struct path_parser<std::string::const_iterator>;
    template struct path_parser<cyy::buffer_t::const_iterator>;

    template struct path_string_parser<std::string::const_iterator>;
    template struct path_string_parser<cyy::buffer_t::const_iterator>;


}	//	cyx


