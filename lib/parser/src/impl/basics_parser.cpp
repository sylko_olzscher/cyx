/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#include "basics_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <string>

namespace cyx
{
	template struct escape_parser<std::string::const_iterator>;
	template struct escape_parser<cyy::buffer_t::const_iterator>;
	
	template struct line_separator<std::string::const_iterator>;
    template struct line_separator<cyy::buffer_t::const_iterator>;

	template struct white_space<std::string::const_iterator>;
	template struct white_space<cyy::buffer_t::const_iterator>;

	template struct printable<std::string::const_iterator>;
	template struct printable<cyy::buffer_t::const_iterator>;

	template struct quoted_pair<std::string::const_iterator>;
	template struct quoted_pair<cyy::buffer_t::const_iterator>;

	template struct header_field<std::string::const_iterator>;
	template struct header_field<cyy::buffer_t::const_iterator>;

	template struct header_field_strict<std::string::const_iterator>;
	template struct header_field_strict<cyy::buffer_t::const_iterator>;

	template struct utf8_filename<std::string::const_iterator>;
	template struct utf8_filename<cyy::buffer_t::const_iterator>;

	template struct quoted_string<std::string::const_iterator>;
	template struct quoted_string<cyy::buffer_t::const_iterator>;

	template struct comment<std::string::const_iterator>;
	template struct comment<cyy::buffer_t::const_iterator>;
}	//	cyx
