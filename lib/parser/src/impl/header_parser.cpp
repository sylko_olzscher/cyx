/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#include "header_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <string>

namespace cyx
{
    template struct header_parser<std::string::const_iterator>;
    template struct header_parser<cyy::buffer_t::const_iterator>;

    bool get_http_header(cyy::buffer_t const& inp, cyy::param_t& param)
    {
        static header_parser< cyy::buffer_t::const_iterator >	g;
        cyy::buffer_t::const_iterator first = inp.begin();
        cyy::buffer_t::const_iterator last = inp.end();
        return boost::spirit::qi::parse(first, last, g, param);
    }
}	//	cyx
