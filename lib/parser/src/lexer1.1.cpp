﻿/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/lexer1.1.h>
#include "impl/method_parser.hpp"
#include "impl/version_parser.hpp"
#include <cyx/http/parser/url_parser.h>
#include <cyx/http/parser/header_parser.h>
#include <cyx/http/parser/content_type_parser.h>
#include <cyx/http/parser/form_parser.h>
#include <cyx/http/intrinsics/factory/path_factory.h>
#include <cyx/http/intrinsics/factory/query_factory.h>
#include <cyy/io/format/version_format.h>
#include <cyy/io.h>
#include <cyy/vm/code_generator.h>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/intrinsics/factory/buffer_factory.h>
#include <boost/algorithm/string/replace.hpp>

namespace cyx
{
	lexer::lexer(std::function<void(cyy::vector_t&&)> cb, logger_type& l)
		: state_(http::method_start)
		, cb_(cb)
		, logger_(l)
		, is_ws_()
		, is_cr_()
		, is_nl_()
		, request_()
		, content_size_(0)
		, reader_(request_.params_)
		, buffer_()
		, form_lexer_(cb, l, content_size_, request_)
		, ws_lexer_(cb, l, buffer_)
	{}

	lexer::lexer(cyy::vm_dispatcher& vm, std::function<void(cyy::vector_t&&)> cb, logger_type& l)
		: state_(http::method_start)
		, cb_(cb)
		, logger_(l)
		, is_ws_()
		, is_cr_()
		, is_nl_()
		, request_()
		, content_size_(0)
		, reader_(request_.params_)
		, buffer_()
		, form_lexer_(cb, l, content_size_, request_)
		, ws_lexer_(cb, l, buffer_)
	{
		//
		//	define lexer/parser methods
		//

		//vm.async_run(cyy::register_function("open.websocket", 0, [this](cyy::context& ctx) {

		//	//
		//	//	switch into websocket mode
		//	//
		//	CYY_LOG_TRACE(logger_, "switch into websocket mode");
		//	state_ = http::ws;
		//	buffer_.clear();
		//}));

	}

	lexer::~lexer()
	{

	}

	std::string lexer::state() const
	{
		return http::name(state_);
	}

	void lexer::parse(char c)
	{
		switch (state_)
		{
		case http::method_start:
			//	skip trailing white spaces
			if (!is_ws_(c)) 	
			{
                clear(request_);	//	reset request object
 				buffer_.clear();
				content_size_ = 0;
				form_lexer_.reset("");
				//clear(upload_);
				state_ = http::method_char;
				buffer_.push_back(c);
			}
			break;

		case http::method_char:
			if (is_ws_(c))		
			{
				//	parse HTTP method
                parse_method();
 				state_ = http::method_done;
			}
			else 	
			{
				buffer_.push_back(c);
			}
			break;

		case http::method_done:
			if (!is_ws_(c))		
			{
				state_ = http::uri_char;
				buffer_.clear();
				buffer_.push_back(c);
			}
			break;

		case http::uri_char:
			if (is_ws_(c))		
			{
				state_ = http::uri_done;
			}
			else
			{
				buffer_.push_back(c);
			}
			break;

		case http::uri_done:
			if (!is_ws_(c)) 	
			{
                parse_url();
				state_ = http::version_char;
				buffer_.clear();
				buffer_.push_back(c);
			}
			break;

		case http::version_char:
			if (is_cr_(c)) 	
			{
				state_ = http::version_cr;
			}
			else	
			{
				buffer_.push_back(c);
			}
			break;

		case http::version_cr:
			if (is_nl_(c)) 	
			{
				state_ = http::version_done;
			}
			break;

		case http::version_done:
			if (is_cr_(c)) 	
			{
				//	no HTTP header
				state_ = http::headers_cr;
			}
			else	
			{
				//	parse HTTP version
                parse_version();
				state_ = http::header_char;
				buffer_.clear();
				buffer_.push_back(c);
			}
			break;

		case http::header_char:
			if (is_cr_(c)) 	
			{
				state_ = http::header_cr;
			}
			else 	
			{
				buffer_.push_back(c);
			}
			break;

		case http::header_cr:
			if (is_nl_(c)) 	
			{
				state_ = http::header_nl;
			}
			break;

		case http::header_nl:
			if (is_ws_(c))
			{
				/*
				*	HTTP/1.1 header field values can be folded onto multiple lines if the
				*	continuation line begins with a space or horizontal tab. All linear
				*	white space, including folding, has the same semantics as SP. A
				*	recipient MAY replace any linear white space with a single SP before
				*	interpreting the field value or forwarding the message downstream.
				*	@code
				LWS            = [CRLF] 1*( SP | HT )
				*	@endcode
				*
				*	@see http://tools.ietf.org/html/rfc2616#section-2.2
				*/
				state_ = http::header_char;
			}
			else if (is_cr_(c)) 	
			{
				//	parse last header attribute
                parse_header();
				state_ = http::headers_cr;
			}
			else	
			{
				//	parse header attribute
                parse_header();
				state_ = http::header_char;
				buffer_.clear();
				buffer_.push_back(c);
			}
			break;

		case http::headers_cr:
			if (is_nl_(c)) 		
			{
				//	header done
 				switch (request_.method_)
 				{
 				case methods::Post:
 					//	depends from media type
					state_ = calculate_post_state();
						
					//	clear buffer
					buffer_.clear();
					break;
				case methods::Get:
					state_ = http::method_start;
					invoke_get();
					break;
				case methods::Head:
					state_ = http::method_start;
					invoke_head();
					break;
				case methods::Connect:
					state_ = http::method_start;
					invoke_connect();
					break;
				case methods::Options:
					state_ = http::method_start;
					invoke_options();
					break;
				case methods::Propfind:
					state_ = http::method_start;
					invoke_propfind();
					break;
				case methods::Delete:
					state_ = http::method_start;
					invoke_delete();
					break;
				case methods::Put:
				case methods::Proppatch:
				default:
					CYY_LOG_ERROR(logger_, "HTTP method "
						<< methods::name(request_.method_)
						<< " not implemented yet");
					state_ = http::method_start;
					break;
				}
 			}
			break;

		case http::content_xml:
			buffer_.push_back(c);
 			if (buffer_.size() == content_size_)	
 			{
				CYY_LOG_TRACE(logger_, "XML data complete");
					
				//
				//	if your data starts with 0x3c, 0x3f it's UTF-8 encoded
				//
					
				cb_(cyy::generate_invoke("http.post.xml"
				, request_.version_
				, cyy::factory(request_.uri_.path_)
				, cyy::factory(request_.uri_.query_)
				, request_.uri_.fragment_
				, cyy::factory(request_.params_)
				, cyy::factory(buffer_)
				));
					
 				state_ = http::method_start;
 			}
			break;
		case http::content_json:
			buffer_.push_back(c);
 			if (buffer_.size() == content_size_)	
 			{
				CYY_LOG_TRACE(logger_, "JSON data complete");
					
				cb_(cyy::generate_invoke("http.post.json"
				, request_.version_
				, cyy::factory(request_.uri_.path_)
				, cyy::factory(request_.uri_.query_)
				, request_.uri_.fragment_
				, cyy::factory(request_.params_)
				, cyy::factory(buffer_)
				));

				state_ = http::method_start;
 			}
			break;

		case http::content_urlencoded:
 			buffer_.push_back(c);
 			if (buffer_.size() == content_size_)	
 			{
				parse_form();
 				state_ = http::method_start;
 			}
			break;

		case http::multipart:
			state_ = form_lexer_.parse(c);
			break;
				
		case http::ws:
			ws_lexer_.parse(c);
			break;

		default:
			CYY_LOG_FATAL(logger_, "illegal parser state");
			break;
		}
	}
	
    /**
     *	Parse HTTP method
     */
    void lexer::parse_method()
    {
        if (!get_http_method(buffer_.begin()
            , buffer_.end()
            , request_.method_))
        {
            CYY_LOG_WARNING(logger_, "no valid HTTP method: "
                << std::string(buffer_.begin(), buffer_.end()));

            //	assumption
            request_.method_ = methods::Get;
        }
        else
        {
            CYY_LOG_TRACE(logger_, "method: "
                << methods::name(request_.method_));
        }
    }

    void lexer::parse_version()
    {
        if (!get_http_version(buffer_.begin()
            , buffer_.end()
            , request_.version_))
        {

            CYY_LOG_FATAL(logger_, "HTTP version parse error");
            request_.version_ = cyy::version(1, 1);
        }
        else
        {
			if (request_.version_ != cyy::version(1, 1))
			{
				CYY_LOG_INFO(logger_, "version: "
					<< request_.version_);
			}

        }
    }

    void lexer::parse_url()
    {
		//	handle special case HTTP OPTIONS:
		//	OPTIONS * HTTP/1.1

		if (!buffer_.empty() && (buffer_[0] == '*'))
		{
			CYY_LOG_INFO(logger_, "OPTIONS: "
			<< std::string(buffer_.begin(), buffer_.end()));

			request_.uri_ = uri();
		}
        else if (!get_http_version(buffer_, request_.uri_))
        {
            CYY_LOG_FATAL(logger_, "HTTP URI parse error");
            request_.uri_ = uri();
        }
        else
        {
            CYY_LOG_TRACE(logger_, "uri: "
                << to_str(request_.uri_.path_));

        }
    }

    void lexer::parse_header()
    {
         //CYY_LOG_TRACE(logger_, "header: "
         //    << std::string(buffer_.begin(), buffer_.end()));

        cyy::param_t param;
        if (!get_http_header(buffer_, param))
        {
            CYY_LOG_ERROR(logger_, "parsing attribute "
                << std::string(buffer_.begin(), buffer_.end())
                << " failed");

        }
        else
        {
#ifdef __DEBUG
            CYY_LOG_TRACE(logger_, "attribute: "
                << param.first
                << ": "
                << cyy::to_literal(param.second));
#endif

			//
			//	insert into parameter list
			//
			auto pos = request_.params_.find(param.first);
			if (pos != request_.params_.end())
			{
				CYY_LOG_WARNING(logger_, "duplicate attribute name "
					<< param.first);

				//
				//	append values
				//

				//(*pos).second += ";";
				//(*pos).second += param.second;
			}
			else
			{
				//
				//	insert new parameter
				//
				request_.params_.emplace(param.first, param.second);
				//request_.params_[param.first] = param.second;
			}
        }
    }
    
	void lexer::parse_form()
	{
		//
		//	The cyy:: prefix is required to distinguish between class and global method 
		//
		if (!cyx::parse_form(buffer_, request_.form_))
		{
			CYY_LOG_ERROR(logger_, "parsing x-www-form-urlencoded data "
				<< std::string(buffer_.begin(), buffer_.end())
				<< " failed");

		}
		else
		{
			CYY_LOG_TRACE(logger_, request_.form_.size()
				<< " x-www-form-urlencoded encoded values parsed");

			cb_(cyy::generate_invoke("http.post.form"
				, request_.version_
				, cyy::factory(request_.uri_.path_)
				, cyy::factory(request_.uri_.query_)
				, request_.uri_.fragment_
				, cyy::factory(request_.params_)
				, cyy::factory(request_.form_)
			));

		}

	}

	http::lexer_state_enum lexer::calculate_post_state()
	{
		const std::string ct = reader_.get_string("Content-Type");
//             CYY_LOG_TRACE(logger_, "Content-Type = "
//                 << ct);
	
		mime_content_type type;
		if (!get_http_mime_type(ct, type))
		{
			CYY_LOG_ERROR(logger_, "parsing MIME content type failed with: "
				<< ct);
		}
		else 
		{
			//
			//	get content size (this is an optimization)
			//
			content_size_ = reader_.get_numeral("Content-Length", content_size_);
			//	64MB == 67108864 Bytes
			//	1GB == 1073741824 Bytes
			//	2GB == 2147483648 Bytes
			if (content_size_ > 67108864)	
			{
				CYY_LOG_WARNING(logger_, (content_size_ / (1024UL * 1024UL))
					<< "MB upload expected");
			}
			else if (content_size_ == 0)
			{
				CYY_LOG_WARNING(logger_, "no content");
			}
			
			//
			//	prepare read buffer
			//
			buffer_.reserve(content_size_);
			
			if (is_matching(type, "application", "x-www-form-urlencoded"))	
			{
				CYY_LOG_TRACE(logger_, "read application/x-www-form-urlencoded "
				<< content_size_
				<< " bytes");
				return 	http::content_urlencoded;
			}
			else if (is_matching(type, "application", "xml"))	
			{
				CYY_LOG_TRACE(logger_, "read application/xml "
				<< content_size_
				<< " bytes");
				return 	http::content_xml;
			}
			else if (is_matching(type, "application", "json"))	
			{
				CYY_LOG_TRACE(logger_, "read application/json "
				<< content_size_
				<< " bytes");
				return 	http::content_json;
			}
			else if (is_matching(type, "multipart", "form-data"))	
			{
				CYY_LOG_TRACE(logger_, "read multipart/form-data "
				<< content_size_
				<< " bytes");
				
				if (type.phrases_.empty())
				{
					CYY_LOG_FATAL(logger_, "multipart/form-data without a boundary: "
						<< ct);
					
					//boundary_.clear();
					form_lexer_.reset("");
				}
				else 
				{			
					form_lexer_.reset(get_boundary(type.phrases_));
				}
				return http::multipart;
			}
			else 
			{
				CYY_LOG_WARNING(logger_, "unknown MIME content type: "
					<< ct);
				
			}
		}
			
		return http::method_start;
	}    

	void lexer::invoke_get()
	{
		if (is_websocket(request_.params_))
		{
			//
			//	switch into websocket mode
			//
			CYY_LOG_TRACE(logger_, "switch into websocket mode");
			state_ = http::ws;

			//
			//	prepare websocket lexer
			//
			ws_lexer_.set_version(reader_.get_numeral<int>("Sec-WebSocket-Version", 0));
			ws_lexer_.set_key(reader_.get_string("Sec-WebSocket-Key"));

			cb_(cyy::generate_invoke("http.open.websocket"
				, request_.version_
				, cyy::factory(request_.uri_.path_)
				, cyy::factory(request_.uri_.query_)
				, request_.uri_.fragment_
				, cyy::factory(request_.params_)
			));

			buffer_.clear();

		}
		else
		{
			cb_(cyy::generate_invoke("http.get"
				, request_.version_
				, cyy::factory(request_.uri_.path_)
				, cyy::factory(request_.uri_.query_)
				, request_.uri_.fragment_
				, cyy::factory(request_.params_)
			));
		}
	}
	
	void lexer::invoke_head()
	{
		cb_(cyy::generate_invoke("http.head"
			, request_.version_
			, cyy::factory(request_.uri_.path_)
			, cyy::factory(request_.uri_.query_)
			, request_.uri_.fragment_
			, cyy::factory(request_.params_)
		));
	}

	void lexer::invoke_delete()
	{
		cb_(cyy::generate_invoke("http.delete"
			, request_.version_
			, cyy::factory(request_.uri_.path_)
			, cyy::factory(request_.uri_.query_)
			, request_.uri_.fragment_
			, cyy::factory(request_.params_)
		));
	}

	/**
	 * example:
	 * CONNECT  126mx03.mxmail.netease.com:25 HTTP/1.0<CR><NL><CR><NL>
	 */
	void lexer::invoke_connect()
	{
		cb_(cyy::generate_invoke("http.connect"
			, request_.version_
			, cyy::factory(request_.uri_.path_)
			, cyy::factory(request_.uri_.query_)
			, request_.uri_.fragment_
			, cyy::factory(request_.params_)
		));
	}

	/**
	 * example:
	 * OPTIONS * HTTP/1.1
     * Host: example.bank.com
     * Upgrade: TLS/1.0
     * Connection: Upgrade
	*/
	void lexer::invoke_options()
	{
		cb_(cyy::generate_invoke("http.options"
			, request_.version_
			, cyy::factory(request_.uri_.path_)
			, cyy::factory(request_.uri_.query_)
			, request_.uri_.fragment_
			, cyy::factory(request_.params_)
		));
	}

	void lexer::invoke_propfind()
	{
		cb_(cyy::generate_invoke("http.propfind"
			, request_.version_
			, cyy::factory(request_.uri_.path_)
			, cyy::factory(request_.uri_.query_)
			, request_.uri_.fragment_
			, cyy::factory(request_.params_)
		));

	}

}
