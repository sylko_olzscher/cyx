﻿/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/websocket_lexer.h>
#include <cyx/http/websocket.h>
#include <cyy/crypto/base64.hpp>
#include <cyy/io/format/buffer_format.h>
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/factory/string_factory.h>
#include <cyy/intrinsics/factory/buffer_factory.h>
#include <cyy/util/byte_swap.h>
#include <algorithm>

namespace cyx
{
	websocket_lexer::websocket_lexer(std::function<void(cyy::vector_t&&)> cb, logger_type& l, cyy::buffer_t& buffer)
		: state_(ws_open)
		, cb_(cb)
		, logger_(l)
		, buffer_(buffer)
		, mask_(false)
		, size_(0)
		, code_(ws::close)
		, scrambler_()
	{}

	void websocket_lexer::set_version(int v)
	{
		if (v != 13)
		{
			CYY_LOG_WARNING(logger_, "ws version: " << v);
		}
	}

	void websocket_lexer::set_key(std::string const& key)
	{
		//	typcally 16 bytes
		const auto buffer = cyy::crypto::decode_base64(key);
		CYY_LOG_TRACE(logger_, "ws key: " 
			<< key 
			<< ", " 
			<< cyy::buffer_format_hex(buffer)
			//<< ", "
			//<< cyy::crypto::base64::encode(buffer.begin(), buffer.end())
		);
	}

	void websocket_lexer::parse(char c)
	{
		switch (state_)
		{
		case ws_open:
			buffer_.push_back(c);
			if (buffer_.size() == 2)
			{
				//	 the most significant bit is the leftmost in the ABNF
				//	example:
				//	[0000]  81 8d 1b fa 3d 5b 73 9f  51 37 74 d6 1d 2c 74 88  ....=[s. Q7t..,t.
				//	[0010]  51 3f 3a                                          Q?:
				//	‭1000 0001 1000 1101‬

				//	see https://github.com/vinniefalco/Beast/blob/master/include/beast/websocket/detail/frame.hpp
				//	see https://tools.ietf.org/html/rfc6455#section-5.7

				rfc6455_header_overlay h;
				h.overlay_[0] = buffer_[0];
				h.overlay_[1] = buffer_[1];

				//
				//	collect frame meta data
				//
				mask_ = (h.header_.mask == 1);
				size_ = h.header_.length;
				code_ = static_cast<ws::opcode>(h.header_.opcode);

				BOOST_ASSERT(mask_ == ((buffer_.at(1) & 0x80) != 0));
				BOOST_ASSERT((buffer_.at(1) & 0x7F) == size_);
				BOOST_ASSERT((buffer_.at(0) & 0x0F) == code_);

#ifdef _DEBUG
				CYY_LOG_TRACE(logger_, ws::name(code_));
#endif

				//
				//	reset mask
				//
				scrambler_.reset(0);

				//
				//	clear buffer
				//
				buffer_.clear();

				//
				//	calculate next state
				//

				//	Extended payload length continued, if payload len > 125
				if (size_ == 126)
				{
					//	the following 2 bytes interpreted as a 16
					//	bit unsigned integer are the payload length
					state_ = ws_length_16bit;
				}
				else if (size_ == 127)
				{
					//	the following 8 bytes interpreted as a 64 - bit unsigned integer(the
					//	most significant bit MUST be 0) are the payload length
					state_ = ws_length_64bit;
				}
				else if (mask_)
				{
					state_ = ws_mask;
				}
				else
				{
					state_ = ws_data;
				}
			}
			break;

		case ws_length_16bit:
			buffer_.push_back(c);
			if (buffer_.size() == 2)
			{
				size_ = cyy::swap_num(cyy::to_number<std::uint16_t>(buffer_, 0));
				buffer_.clear();

				CYY_LOG_TRACE(logger_, "ws frame length "
					<< size_);

				if (mask_)
				{
					state_ = ws_mask;
				}
				else
				{
					state_ = ws_data;
				}
			}
			break;

		case ws_length_64bit:
			buffer_.push_back(c);
			if (buffer_.size() == 4)
			{
				size_ = cyy::swap_num(cyy::to_number<std::uint64_t>(buffer_, 0));
				buffer_.clear();

				CYY_LOG_TRACE(logger_, "ws frame length "
					<< size_);

				if (mask_)
				{
					state_ = ws_mask;
				}
				else
				{
					state_ = ws_data;
				}
			}
			break;

		case ws_mask:
			buffer_.push_back(c);
			if (buffer_.size() == 4)
			{
				//
				//	prepare de-scrambler with mask
				//
				scrambler::key_type	mask;
				std::copy(buffer_.begin(), buffer_.end(), mask.begin());
				scrambler_ = mask;

				//
				//	empty buffer
				//
				buffer_.clear();

				if (size_ == 0)
				{
					//
					//	call session method 
					//
					msg_complete();
				}
				else
				{
					//
					//	update lexer state
					//
					state_ = ws_data;

					//
					//	prepare buffer for incoming data
					//
					buffer_.reserve(size_);
				}
			}
			break;

		case ws_data:
			buffer_.push_back(scrambler_[c]);
			if (buffer_.size() == size_)
			{
				msg_complete();
			}
			break;

		default:
			break;
		}
	}

	void websocket_lexer::msg_complete()
	{
		if (code_ == ws::text)
		{
			const std::string text(buffer_.begin(), buffer_.end());
			CYY_LOG_TRACE(logger_, "ws data: "
				<< text);

			cb_(cyy::generate_invoke("ws.data.text"
				, cyy::factory(text)
			));

		}
		else if (code_ == ws::binary)
		{
			cb_(cyy::generate_invoke("ws.data.binary"
				, cyy::factory(buffer_)
			));
		}
		else if (code_ == ws::close)
		{
			cb_(cyy::generate_invoke("ws.close"
				, cyy::factory(buffer_)
			));
		}
		else if (code_ == ws::ping)
		{
			cb_(cyy::generate_invoke("ws.ping"
				, cyy::factory(buffer_)
			));
		}
		else if (code_ == ws::pong)
		{
			cb_(cyy::generate_invoke("ws.pong"
				, cyy::factory(buffer_)
			));
		}

		//	data complete
		state_ = ws_open;
		buffer_.clear();

	}


}
