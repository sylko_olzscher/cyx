# top level files
set (parser_lib)

set (parser_lib_cpp

	lib/parser/src/lexer1.1.cpp
	lib/parser/src/multi_part_lexer.cpp
	lib/parser/src/websocket_lexer.cpp
)

set (parser_lib_h

	src/main/include/cyx/http/lexer_states.h
	src/main/include/cyx/http/lexer1.1.h
	src/main/include/cyx/http/multi_part_lexer.h
	src/main/include/cyx/http/websocket_lexer.h

)

set (parser_lib_parser
    lib/parser/src/impl/method_parser.hpp
    lib/parser/src/impl/version_parser.hpp

    src/main/include/cyx/http/parser/basics_parser.h
    lib/parser/src/impl/basics_parser.hpp
    lib/parser/src/impl/basics_parser.cpp

    src/main/include/cyx/http/parser/path_parser.h
    lib/parser/src/impl/path_parser.hpp
    lib/parser/src/impl/path_parser.cpp

    src/main/include/cyx/http/parser/url_parser.h
    lib/parser/src/impl/url_parser.hpp
    lib/parser/src/impl/url_parser.cpp

    src/main/include/cyx/http/parser/header_parser.h
    lib/parser/src/impl/header_parser.hpp
    lib/parser/src/impl/header_parser.cpp

    src/main/include/cyx/http/parser/cookie_parser.h
    lib/parser/src/impl/cookie_parser.hpp
    lib/parser/src/impl/cookie_parser.cpp
    
    src/main/include/cyx/http/parser/content_type_parser.h
    lib/parser/src/impl/content_type_parser.hpp
    lib/parser/src/impl/content_type_parser.cpp

    src/main/include/cyx/http/parser/content_disposition_parser.h
    lib/parser/src/impl/content_disposition_parser.hpp
    lib/parser/src/impl/content_disposition_parser.cpp

    src/main/include/cyx/http/parser/form_parser.h
    lib/parser/src/impl/form_parser.hpp
    lib/parser/src/impl/form_parser.cpp

    src/main/include/cyx/http/parser/auth_parser.h
    lib/parser/src/impl/auth_parser.hpp
    lib/parser/src/impl/auth_parser.cpp

)
	

source_group("parsers" FILES ${parser_lib_parser})


# define the master program
set (parser_lib
  ${parser_lib_cpp}
  ${parser_lib_parser}
)
