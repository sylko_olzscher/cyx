# top level files
set (http_lib)

set (http_lib_cpp

	lib/http/src/path.cpp
	lib/http/src/serializer1.1.cpp
	lib/http/src/request.cpp
	lib/http/src/response.cpp
	lib/http/src/multi_part.cpp
	lib/shared/resource/favicon.hpp
)

set (http_lib_h

	src/main/include/cyx/http/http.h
	src/main/include/cyx/http/path.h
	src/main/include/cyx/http/uri.hpp
	src/main/include/cyx/http/content_type.hpp
	src/main/include/cyx/http/content_disposition.hpp
	src/main/include/cyx/http/auth_type.hpp
	src/main/include/cyx/http/serializer1.1.h
	src/main/include/cyx/http/request.h
	src/main/include/cyx/http/response.h
	src/main/include/cyx/http/multi_part.h
	src/main/include/cyx/http/websocket.h

	src/main/include/cyx/http/type_codes.h
	src/main/include/cyx/http/intrinsics/type_traits.hpp
)

set (http_lib_factory
	src/main/include/cyx/http/intrinsics/factory/path_factory.h
	src/main/include/cyx/http/intrinsics/factory/query_factory.h
	src/main/include/cyx/http/intrinsics/factory/method_factory.h
	src/main/include/cyx/http/intrinsics/factory/status_factory.h
	src/main/include/cyx/http/intrinsics/factory/mime_factory.h
	lib/http/src/factory/path_factory.cpp 
	lib/http/src/factory/query_factory.cpp 
	lib/http/src/factory/method_factory.cpp 
	lib/http/src/factory/status_factory.cpp 
	lib/http/src/factory/mime_factory.cpp 
)

set (http_lib_io
	src/main/include/cyx/http/intrinsics/io/io.h
	src/main/include/cyx/http/intrinsics/io/disk_file.h
	lib/http/src/io/io.cpp 
	lib/http/src/io/disk_file.cpp 
)

set (http_lib_db
	src/main/include/cyx/db/db.h
	lib/http/src/db/db.cpp 
)

#source_group("parsers" FILES ${http_lib_parser})
source_group("factory" FILES ${http_lib_factory})
source_group("io" FILES ${http_lib_io})
source_group("db" FILES ${http_lib_db})


# define the master program
set (http_lib
  ${http_lib_cpp}
  ${http_lib_h}
  ${http_lib_factory}
  ${http_lib_io}
  ${http_lib_db}
)
