/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/multi_part.h>


namespace cyx
{


    //	--- multipart/form-data

    multi_part::multi_part()
		: size_(0)
		, data_()
		, meta_()
		, type_()
	{}


    void clear(multi_part& req)
    {
        multi_part r;
        std::swap(req, r);
    }

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::multi_part>(cyx::multi_part& rl, cyx::multi_part& rr)
    {
		//std::swap(rl.state_, rr.state_);
		std::swap(rl.size_, rr.size_);
		std::swap(rl.data_, rr.data_);
 		std::swap(rl.meta_, rr.meta_);
		std::swap(rl.type_, rr.type_);
	}

}
