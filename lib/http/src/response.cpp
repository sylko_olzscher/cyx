/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/version_info.h>
#include <cyx/http/response.h>
#include <cyx/http/auth_type.hpp>
#include <cyy/intrinsics/factory/string_factory.h>
#include <cyy/intrinsics/factory/numeric_factory.h>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/digest_format.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/crypto/sha1.h>
#include <cyy/crypto/base64.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace cyx
{


    //	--- HTTP response

	response::response()
        : version_(1, 1)
		, status_(codes::OK)
		, params_()
		, body_()
	{}

	response::response(codes::status code)
		: version_(1, 1)
		, status_(code)
		, params_()
		, body_()
	{}

	response::response(codes::status code, std::string&& body)
		: version_(1, 1)
		, status_(code)
		, params_()
		, body_(std::move(body))
	{
		content_length(params_, body_.size());
		etag(params_, cyy::md5_hash(body_));
	}

    void clear(response& req)
    {
		response r;
        std::swap(req, r);
    }

	void powered_by(cyy::param_map_t& h, std::string const& engine)
	{
		h["X-Powered-By"] = cyy::factory(engine);
	}

	void powered_by(cyy::param_map_t& h)
	{
		powered_by(h, CYX_ENGINE);
	}

	void content_length(cyy::param_map_t& h, std::size_t l)
	{
		h["Content-Length"] = cyy::numeric_factory_cast<std::uint64_t>(l);
	}

	void content_type(cyy::param_map_t& h, std::string const& type)
	{
		h["Content-Type"] = cyy::factory(type);
	}

	void content_type(cyy::param_map_t& h, mime_content_type const& type)
	{
		h["Content-Type"] = cyy::factory(to_str(type));
	}

	void last_modified(cyy::param_map_t& h, std::chrono::system_clock::time_point const& tp)
	{
		h["Last-Modified"] = cyy::factory(cyy::rfc1123_format(tp));
	}

	void location(cyy::param_map_t& h, std::string const& loc)
	{
		h["Location"] = cyy::factory(loc);
	}

	void etag(cyy::param_map_t& h, cyy::crypto::digest_md5::value_type const& tag)
	{
		h["ETag"] = cyy::factory(cyy::to_string(tag));
	}

	void accept_websocket(cyy::param_map_t& h
		, std::string const& key
		, std::string const& protocol)
	{

		h["Upgrade"] = cyy::string_factory("websocket");
		h["Connection"] = cyy::string_factory("Upgrade");
		h["Sec-WebSocket-Accept"] = cyy::factory(calculate_websocket_hash(key));
		h["Sec-WebSocket-Protocol"] = cyy::factory(protocol);
	}

	std::string calculate_websocket_hash(std::string const& key)
	{
//		const std::string hash_value = key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
		const std::string hash_value = key + cyy::uuid_uppercase_format(MAGIC_WEBSOCKET_KEY);
		const cyy::crypto::digest_sha1::value_type digest = cyy::sha1_hash(hash_value);
		return cyy::crypto::encode_base64(digest);
	}

	void basic_auth(cyy::param_map_t& h, std::string const& realm)
	{
		h["WWW-Authenticate"] = cyy::factory("Basic realm=\"" + realm + "\"");
	}

	void digest_auth(cyy::param_map_t& h
		, std::string const& realm
		, std::string const& algo
		, std::string const& nonce
		, std::string const opaque)
	{
		//WWW - Authenticate: Digest
		//	realm = "http-auth@example.org",
		//	qop = "auth, auth-int",
		//	algorithm = SHA - 256,
		//	nonce = "7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v",
		//	opaque = "FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"

		std::stringstream ss;
		ss
			<< "Digest realm=\""
			<< realm
			<< '"'

			<< ", qop=\""
			<< "auth,auth-int"	//	authentication with integrity protection
			<< '"'

			<< ", algorithm=\""
			<< algo	//	MD5, SHA-256
			<< '"'

			<< ", nonce=\""
			<< nonce
			//<< "dcd98b7102dd2f0e8b11d0f600bfb0c093"
			<< '"'

			<< ", opaque=\""
			<< opaque
			//<< "5ccc069c403ebaf9f0171e9517f40e41"
			<< '"'

			;

		h["WWW-Authenticate"] = cyy::factory(ss.str());

	}

	void keep_alive(cyy::param_map_t& h)
	{
		h["Connection"] = cyy::string_factory("keep-alive");
	}

	void allow(cyy::param_map_t& h)
	{
		h["Allow"] = cyy::string_factory("OPTIONS, GET, HEAD, POST");
	}

	/**
	* Set allowed methodsas specified
	*/
	void allow(cyy::param_map_t& h, std::initializer_list<methods::method> l)
	{
		std::string str;
		bool init = false;
		for (auto m : l)
		{
			if (!init)
			{
				init = true;
				str += ", ";
			}
			str += methods::name(m);
		}
		h["Allow"] = cyy::factory(str);
	}

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::response>(cyx::response& rl, cyx::response& rr)
    {
        std::swap(rl.version_, rr.version_);
		std::swap(rl.status_, rr.status_);
		std::swap(rl.params_, rr.params_);
		std::swap(rl.body_, rr.body_);
	}

}
