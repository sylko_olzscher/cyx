/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/path.h>
#include <algorithm>
#include <boost/algorithm/string/predicate.hpp>

namespace cyx
{

    //	transformation
    boost::filesystem::path to_path(path_t const& v)
    {
        boost::filesystem::path	p("/");	//	initialize with a portable path
        for(auto const& segment : v)
        {
            //	append
            p /= segment;
        }
        return p;
    }

    path_t to_path(boost::filesystem::path const& p)
    {
        path_t r;
        std::for_each(p.begin(), p.end(), [&r](boost::filesystem::path const& segment){
			//if (!segment.empty() && segment.string().at(0) != boost::filesystem::path::preferred_separator)
			if (!segment.empty() && segment.string() != "/")
			{
				r.push_back(segment.string());
			}
        });
        return r;
    }

	path_t to_path(std::initializer_list<std::string> list)
	{
		path_t r;
		r.reserve(list.size());
		std::for_each(list.begin(), list.end(), [&r](std::string const& p) {

			r.push_back(p);

		});
		return r;
	}

    std::string to_str(path_t const& v)
    {
        std::string	r("/");	//	initialize with a portable path
        for(auto const& segment : v)
        {
            //	append
            r += segment;
			if (segment.find('.') == std::string::npos)
			{
				r += '/';
			}
        }

        return r;
    }

	bool is_favicon(path_t const& p)
	{
		return ((p.size() == 1) && (p.at(0).compare("favicon.ico") == 0));
	}

	bool is_directory(path_t const& p)
	{
		return (p.empty())
			? true
			: (p.back().find('.') == std::string::npos)
			;
	}

	bool starts_with(path_t const& inp, path_t const& test)
	{
		return boost::algorithm::starts_with(inp, test);
	}

	bool starts_with(path_t const& inp, std::initializer_list<std::string> test)
	{
		if (inp.size() >= test.size())
		{
			return std::equal(test.begin(), test.end(), inp.begin());
		}
		return false;

	}

	path_t join(std::initializer_list<path_t> list)
	{
		path_t r;
		std::for_each(list.begin(), list.end(), [&r](path_t const& p) {

			r.insert(r.end(), p.begin(), p.end());

		});
		return r;
	}

	bool is_equal(path_t const& lhs, std::initializer_list<std::string> rhs)
	{
		if (lhs.size() == rhs.size())
		{
			return std::equal(lhs.begin(), lhs.end(), rhs.begin());
		}
		return false;
	}

}

