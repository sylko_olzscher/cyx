/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/request.h>
#include <cyy/intrinsics/algorithm.h>
#include <boost/algorithm/string/predicate.hpp>

namespace cyx
{


    //	--- HTTP request

    request::request()
        : method_(methods::Unknown)
        , version_(1, 1)
	{}


    void clear(request& req)
    {
        request r;
        std::swap(req, r);
    }

	bool is_websocket(cyy::param_map_t const& attr)
	{
		//
		//	Some browsers combine the Upgrade directive with a Keep-Alive.
		//	
		cyy::vector_t ds;
		ds = cyy::find_value(attr, "Connection", ds);
		if (!ds.empty())
		{
			//	find() is case sensitive
			const auto obj = cyy::find(ds, "Upgrade");
			return obj && boost::algorithm::iequals(cyy::find_value(attr, "Upgrade", std::string()), "websocket");
		}
		return false;
	}

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::request>(cyx::request& rl, cyx::request& rr)
    {
        std::swap(rl.method_, rr.method_);
        std::swap(rl.version_, rr.version_);
        std::swap(rl.uri_, rr.uri_);
		std::swap(rl.params_, rr.params_);
		std::swap(rl.form_, rr.form_);
	}

}
