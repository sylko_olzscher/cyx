/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/factory/method_factory.h>
#include <cyx/http/intrinsics/type_traits.hpp>
#include <cyy/intrinsics/factory/null_factory.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/detail/make_value.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace cyy
{
	
	object factory(cyx::methods::method v)
	{
		return cyy::object(core::make_value(v));
	}

	object factory(std::initializer_list<cyx::methods::method> l)
	{
		cyy::vector_t vec;
		for (auto m : l)
		{
			vec.push_back(factory(m));
		}
		return factory(vec);
	}

	object  method_factory(std::string const& v)
	{
		if (boost::algorithm::iequals(v, "HEAD"))	return factory(cyx::methods::Head);
		else if (boost::algorithm::iequals(v, "GET"))	return factory(cyx::methods::Get);
		else if (boost::algorithm::iequals(v, "POST"))	return factory(cyx::methods::Post);
		else if (boost::algorithm::iequals(v, "PUT"))	return factory(cyx::methods::Put);
		else if (boost::algorithm::iequals(v, "DELETE"))	return factory(cyx::methods::Delete);
		else if (boost::algorithm::iequals(v, "TRACE"))	return factory(cyx::methods::Trace);
		else if (boost::algorithm::iequals(v, "OPTIONS"))	return factory(cyx::methods::Options);
		else if (boost::algorithm::iequals(v, "CONNECT"))	return factory(cyx::methods::Connect);
		else if (boost::algorithm::iequals(v, "COPY"))	return factory(cyx::methods::Copy);
		else if (boost::algorithm::iequals(v, "MOVE"))	return factory(cyx::methods::Move);
		else if (boost::algorithm::iequals(v, "LOCK"))	return factory(cyx::methods::Lock);
		else if (boost::algorithm::iequals(v, "UNLOCK"))	return factory(cyx::methods::Unlock);
		else if (boost::algorithm::iequals(v, "MKCOL"))	return factory(cyx::methods::MkCol);
		else if (boost::algorithm::iequals(v, "PROPFIND"))	return factory(cyx::methods::Propfind);
		else if (boost::algorithm::iequals(v, "PROPPATCH"))	return factory(cyx::methods::Proppatch);
		return cyy::factory();
	}

	object method_factory(std::uint32_t v)
	{
		return factory(static_cast<cyx::methods::method>(v));
	}

}