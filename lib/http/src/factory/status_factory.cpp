/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/factory/status_factory.h>
#include <cyx/http/intrinsics/type_traits.hpp>
#include <cyy/intrinsics/factory/null_factory.h>
#include <cyy/detail/make_value.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace cyy
{
	
	object factory(cyx::codes::status v)
	{
		return cyy::object(core::make_value(v));
	}

	object  status_factory(std::string const& v)
	{
		if (boost::algorithm::iequals(v, "Continue"))	return factory(cyx::codes::Continue);
		else if (boost::algorithm::iequals(v, "OK"))	return factory(cyx::codes::OK);
		//	ToDo: add missing status codes
		return cyy::factory();
	}

	object  status_factory(std::uint32_t v)
	{
		return factory(static_cast<cyx::codes::status>(v));
	}

}