/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/factory/query_factory.h>
#include <cyx/http/intrinsics/type_traits.hpp>
#include <cyy/intrinsics/factory/null_factory.h>
#include <cyy/detail/make_value.hpp>

namespace cyy
{
	
	object factory(cyx::query_t const& v)
	{
		return cyy::object(core::make_value(v));
	}

	object query_factory()
	{
		const cyx::query_t q;
		return factory(q);
	}

}