/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/factory/mime_factory.h>
#include <cyx/http/intrinsics/type_traits.hpp>
#include <cyy/intrinsics/factory/null_factory.h>
#include <cyy/detail/make_value.hpp>

namespace cyy
{
	
	object factory(cyx::mime_content_type const& v)
	{
		return cyy::object(core::make_value(v));
	}

	object factory(std::string const& type
		, std::string const& subtype
		, cyx::param_container_t const& phrases)
	{
		return cyy::object(core::make_value(cyx::mime_content_type(type, subtype, phrases)));
	}

	object mime_factory()
	{
		return factory("all", "types", cyx::param_container_t());
	}

}