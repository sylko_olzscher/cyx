/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/db/db.h>
#include <cyy/store/store.h>
#include <cyy/xml/xml_manip.h>
#include <pugixml.hpp>


namespace cyx
{
	void init(cyy::store::store& db)
	{
		//	TMime table
		//
		//	(1) ext - pk (file extension with dot)
		//	+-----------------
		//	(2) type [std::string] - type name / application
		//	(3) name [std::string] - subtype name
		//	(4) description [std::string]
		cyy::store::meta_table mime("TMime", 1, 3, { "ext", "type", "name", "descr" });
		db.create_table(mime);

	}

	void set_mime_default(cyy::store::store& db)
	{
		//
		//	text
		//
		db.insert("TMime"
			, cyy::store::key_generator(".html")
			, cyy::store::data_generator("text", "html", ""));

		db.insert("TMime"
			, cyy::store::key_generator(".txt")
			, cyy::store::data_generator("text", "plain", ""));

		db.insert("TMime"
			, cyy::store::key_generator(".log")
			, cyy::store::data_generator("text", "plain", ""));

		db.insert("TMime"
			, cyy::store::key_generator(".css")
			, cyy::store::data_generator("text", "css", ""));

		db.insert("TMime"
			, cyy::store::key_generator(".xml")
			, cyy::store::data_generator("text", "xml", ""));

		//	alternative: application/javascript 
		db.insert("TMime"
			, cyy::store::key_generator(".js")
			, cyy::store::data_generator("text", "json", "JavaScript"));

		//	"text/tab-separated-values"
		db.insert("TMime"
			, cyy::store::key_generator(".tsv")
			, cyy::store::data_generator("text", "tab-separated-values", "tab separated values"));

		db.insert("TMime"
			, cyy::store::key_generator(".csv")
			, cyy::store::data_generator("text", "csv", "RFC 7111"));

		//
		//	images
		//
		db.insert("TMime"
			, cyy::store::key_generator(".gif")
			, cyy::store::data_generator("image", "gif", "Graphics Interchange Format"));

		db.insert("TMime"
			, cyy::store::key_generator(".png")
			, cyy::store::data_generator("image", "png", "Portable Network Graphics"));

		db.insert("TMime"
			, cyy::store::key_generator(".jpg")
			, cyy::store::data_generator("image", "jpeg", "Joint Photographic Experts Group"));

		db.insert("TMime"
			, cyy::store::key_generator(".tiff")
			, cyy::store::data_generator("image", "tiff", "Tagged Image File Format"));

		db.insert("TMime"
			, cyy::store::key_generator(".tif")
			, cyy::store::data_generator("image", "tiff", "Tagged Image File Format"));

		db.insert("TMime"
			, cyy::store::key_generator(".svg")
			, cyy::store::data_generator("image", "svg+xml", "Scalable Vector Graphics (SVG)"));

		db.insert("TMime"
			, cyy::store::key_generator(".ico")
			, cyy::store::data_generator("image", "x-icon", "Icons"));

		db.insert("TMime"
			, cyy::store::key_generator(".bmp")
			, cyy::store::data_generator("image", "bmp", "BMP File Format"));

		//
		//	application
		//

		db.insert("TMime"
			, cyy::store::key_generator(".json")
			, cyy::store::data_generator("application", "json", "JavaScript Object Notation (JSON)"));

		db.insert("TMime"
			, cyy::store::key_generator(".tex")
			, cyy::store::data_generator("application", "x-tex", "TeX"));

		db.insert("TMime"
			, cyy::store::key_generator(".pdf")
			, cyy::store::data_generator("application", "pdf", "PDF"));

		db.insert("TMime"
			, cyy::store::key_generator(".zip")
			, cyy::store::data_generator("application", "zip", "ZIP"));


		db.insert("TMime"
			, cyy::store::key_generator(".xlsx")
			, cyy::store::data_generator("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet", "OOXML - Spreadsheet"));

		db.insert("TMime"
			, cyy::store::key_generator(".docx")
			, cyy::store::data_generator("application", "vnd.openxmlformats-officedocument.wordprocessingml.document", "OOXML - Word  Document"));

		//
		//	audio
		//

		db.insert("TMime"
			, cyy::store::key_generator(".midi")
			, cyy::store::data_generator("audio", "midi", "Musical Instrument Digital Interface"));

		db.insert("TMime"
			, cyy::store::key_generator(".oga")
			, cyy::store::data_generator("audio", "ogg", "[RFC5334][RFC7845]"));

		db.insert("TMime"
			, cyy::store::key_generator(".mp3")
			, cyy::store::data_generator("audio", "mpeg", ""));

		db.insert("TMime"
			, cyy::store::key_generator(".mp4a")
			, cyy::store::data_generator("audio", "mp4", ""));

		//
		//	video
		//

		db.insert("TMime"
			, cyy::store::key_generator(".mpeg")
			, cyy::store::data_generator("video", "mpeg", "Moving Picture Experts Group"));

		db.insert("TMime"
			, cyy::store::key_generator(".ogv")
			, cyy::store::data_generator("video", "ogg", "Ogg Video"));
	}

	bool load_mime_from_xml(cyy::store::store& db, boost::filesystem::path const& p)
	{
		//	create empty XML document
		pugi::xml_document doc;

		//	load file into memory
		//std::cout
		//	<< "load MIME configuration file ["
		//	<< p
		//	<< "]"
		//	<< std::endl
		//	;

		const pugi::xml_parse_result result = doc.load_file(p.c_str());

		if (!result)
		{
			std::cerr
				<< "***Warning: "
				<< result.description()
				<< ", character pos= "
				<< result.offset
				<< std::endl
				;

			return false;
		}

		pugi::xpath_node_set data = doc.select_nodes("media/group");
		for (pugi::xpath_node_set::const_iterator it = data.begin(); it != data.end(); ++it)
		{
			pugi::xpath_node node = *it;

			//
			//	get application type
			//
			const std::string app = node.node().attribute("type").value();

			//
			//	get key (file extension)
			//
			for (pugi::xml_node child = node.node().child("subtype"); child; child = child.next_sibling("subtype"))
			{
				const std::string type = child.attribute("name").as_string();
				const std::string ext = child.child_value();

				db.insert("TMime"
					, cyy::store::key_generator(ext)
					, cyy::store::data_generator(app, type, ""));

			}

		}
		return true;
	}

	//
	//	only local defined 
	//
	void store_mime_to_xml(pugi::xml_node& root, cyy::store::table const& tbl, std::string const& type)
	{
		auto group = root.append_child("group");
		group.append_attribute("type") = type.c_str();

		tbl.loop([&group, &type](cyy::store::record const& rec) {

			if (type == rec.get_value<std::string>("type"))
			{
				//	write to XML doc
				auto st = group.append_child("subtype");
				st.append_attribute("name") = rec.get_value<std::string>("name").c_str();
				st.append_attribute("descr") = rec.get_value<std::string>("descr").c_str();
				st.append_child(pugi::node_pcdata).set_value(rec.get_value<std::string>("ext").c_str());
			}
		});

	}

	bool store_mime_to_xml(cyy::store::store& db, boost::filesystem::path const& out)
	{
		//	create empty XML document
		pugi::xml_document doc;

		// Generate XML declaration
		auto declarationNode = doc.append_child(pugi::node_declaration);
		declarationNode.append_attribute("version") = "1.0";
		declarationNode.append_attribute("encoding") = "UTF-8";
		declarationNode.append_attribute("standalone") = "yes";

		auto root = doc.append_child("media");

		//
		//	search matching target names
		//
        db.access<void, cyy::store::table_write>(cyy::store::table_write("TMime"), [&root](cyy::store::table& tbl)  {


			store_mime_to_xml(root, tbl, "application");
			store_mime_to_xml(root, tbl, "audio");
			//store_mime_to_xml(root, tbl, "example");
			store_mime_to_xml(root, tbl, "image");
			store_mime_to_xml(root, tbl, "message");
			store_mime_to_xml(root, tbl, "model");
			store_mime_to_xml(root, tbl, "multipart");
			store_mime_to_xml(root, tbl, "text");
			store_mime_to_xml(root, tbl, "video");

		});

// 		db.entry<void>([&root](std::tuple<cyy::store::table const*> tpl) {
// 
// 			BOOST_ASSERT_MSG(std::get<0>(tpl) != nullptr, "no table");
// 
// 			//	extract TMime table
// 			cyy::store::table const& tbl = *std::get<0>(tpl);
// 
// 			store_mime_to_xml(root, tbl, "application");
// 			store_mime_to_xml(root, tbl, "audio");
// 			//store_mime_to_xml(root, tbl, "example");
// 			store_mime_to_xml(root, tbl, "image");
// 			store_mime_to_xml(root, tbl, "message");
// 			store_mime_to_xml(root, tbl, "model");
// 			store_mime_to_xml(root, tbl, "multipart");
// 			store_mime_to_xml(root, tbl, "text");
// 			store_mime_to_xml(root, tbl, "video");
// 
// 		}, cyy::store::table_read("TMime"));

        return doc.save_file(out.c_str(), PUGIXML_TEXT("  "));
	}


	std::string query_mime(cyy::store::store& db, std::string const& ext)
	{
		const auto r = db.get_values<std::string, std::string>("TMime", cyy::store::key_generator(ext), {"type", "name"});
		return (r.second)
			? (std::get<0>(r.first) + "/" + std::get<1>(r.first))
			: "text/html";

	}

}	//	cyx
