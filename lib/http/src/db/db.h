/*
 * Copyright Sylko Olzscher 2015
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef NODDY_MASTER_DB_H
#define NODDY_MASTER_DB_H

namespace cyy 
{
	namespace store 
	{
		class store;
	}
}

#include <boost/asio.hpp>
#include <boost/uuid/uuid.hpp>
#include <cyy/intrinsics/sets.hpp>

namespace noddy
{
	namespace store 
	{
		/**
		 * Create all required tables. Could be called only
		 * once.
		 */
		void init(cyy::store::store&, boost::uuids::uuid tag);
		
		/**
		 * Insert a record
		 */
		boost::system::error_code insert(cyy::store::store& db
		, cyy::vector_t const& tpl);

		/**
		 * de-register all targets of a specified client.
		 * @return count of deregisterd targets.
		 */
		std::size_t deregister_targets_of_client(cyy::store::store& db
			, boost::uuids::uuid client);

		/**
		 * de-register all targets of a specified peer
		 * @return count of deregisterd targets.
		 */
		std::size_t deregister_targets_of_peer(cyy::store::store& db
			, boost::uuids::uuid peer);

		/**
		 * @brief close all push lines (channels) of the specified source
		 * @return count of closed channels
		 */
		std::size_t close_push_lines_of_source(cyy::store::store& db
			, std::uint32_t source);

		/**
		* @brief close all push lines (channels) of the specified channels
		* @return count of closed channels
		*/
		std::size_t close_push_lines_of_channel(cyy::store::store& db
			, std::uint32_t channel);


		/**
		* Remove all session with the specified peer
		* @return count of removed sessios
		*/
		std::size_t remove_sessions_of_peer(cyy::store::store& db
			, boost::uuids::uuid peer);

	}	//	store
}	//	noddy

#endif	//	NODDY_MASTER_DB_H

