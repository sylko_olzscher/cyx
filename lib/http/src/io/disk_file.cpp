/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/io/disk_file.h>
#include <cyy/io/parser/bom_parser.h>
#include <fstream>

namespace cyx
{
	namespace io
	{
		std::pair<cyy::io::bom::code, std::uint64_t> read_file(std::string& buffer, boost::filesystem::path const& p)
		{
			std::ifstream infile;
			infile.open(p.string(), std::ios::in | std::ios::binary);
			if (infile.is_open())
			{
				//	initialize buffer
				cyy::bytes fs = size(p);
				buffer.resize(*fs);

				//	read file into buffer
				//	since C++17 std::string::data() returns a non-const pointer
				infile.read(const_cast<char*>(buffer.data()), buffer.size());

				cyy::io::bom_parser bom;
				return std::make_pair(bom.parse(buffer.begin(), buffer.end()), *fs);
			}
			return std::make_pair(cyy::io::bom::OTHER, 0UL);
		}

		cyy::bytes size(boost::filesystem::path const& p)
		{
			return cyy::bytes(boost::filesystem::file_size(p));
		}

		std::chrono::system_clock::time_point modification_time(boost::filesystem::path const& p)
		{
			return std::chrono::system_clock::from_time_t(boost::filesystem::last_write_time(p));
		}


	}
}

