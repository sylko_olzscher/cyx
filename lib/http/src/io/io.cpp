/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/intrinsics/io/io.h>
#include <cyx/http/intrinsics/type_traits.hpp>
#include <cyx/http/uri.hpp>
#include <cyx/http/content_type.hpp>
#include <cyy/value_cast.hpp>

namespace cyx
{
	namespace io
	{
		bool custom(std::ostream& os, cyy::object const& obj)
		{
			switch (cyy::get_code(obj))
			{
			case cyx::types::URL_PATH:
				os << to_str(cyy::value_cast(obj, path_t()));
				break;
			case cyx::types::QUERY_PATH:
				os << to_str(cyy::value_cast(obj, query_t()));
				break;
			case cyx::types::HTTP_METHOD:
				os << cyx::methods::name(cyy::value_cast(obj, cyx::methods::Unknown));
				break;
			case cyx::types::HTTP_STATUS:
				os << cyx::codes::name(cyy::value_cast(obj, cyx::codes::Unused));
				break;
			//case cyx::types::COOKIE:
			case cyx::types::MIME_TYPE:
				os << to_str(cyy::value_cast<mime_content_type>(obj));
				break;

			default:
				return false;
			}
			return true;
		}
	}

}

