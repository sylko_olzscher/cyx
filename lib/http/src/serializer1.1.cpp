/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/http/serializer1.1.h>
#include <cyx/http/intrinsics/io/io.h>
#include <cyx/http/http.h>
#include <cyx/http/intrinsics/io/disk_file.h>
#include <cyx/http/websocket.h>
#include <cyx/db/db.h>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/vm/code_generator.h>
#include <cyy/io/io_set.h>
#include <cyy/io.h>
#include <cyy/io/format/version_format.h>
#include <cyy/io/parser/bom_parser.h>
#include <cyy/store/store.h>
#include <cyy/json/json_io.h>
#include <cyy/io/hex_dump.hpp>
#include <cyy/util/read_sets.h>
#include "../../shared/resource/favicon.hpp"
#include <boost/endian/buffers.hpp>

namespace cyx
{
	serializer::serializer(cyy::vm_dispatcher& vm, cyy::store::store& db)
		: db_(db)
		, buffer_()
		, ostream_(&buffer_)
	{
		//
		//	define serializer methods
		//

		//
		//	Send a file (uncached)
		//

 		vm.async_run(cyy::register_function("io.send.file", 1, [this](cyy::context& ctx){
 			
 			const cyy::vector_t frame = ctx.stack_frame(false);

			//std::cerr << "send.file: ";
			//cyy::io::to_literal(std::cerr, frame, &io::custom);
			//std::cerr << std::endl;

			std::string buffer;
			const auto p = cyy::value_cast(frame.at(0), boost::filesystem::temp_directory_path());
			const std::pair<cyy::io::bom::code, std::uint64_t> res = io::read_file(buffer, p);
			if (res.second != 0)
			{
				response res(codes::OK, std::move(buffer));
				powered_by(res.params_);
				last_modified(res.params_, io::modification_time(p));

				//std::cout
				//	<< query_mime(db_, boost::filesystem::extension(p))
				//	<< std::endl;
				content_type(res.params_, query_mime(db_, boost::filesystem::extension(p)));
				write(res);
			}
			else
			{
				stock_404();
			}
 			
 		}));

		//
		//	Send a stock response.
		//
		vm.async_run(cyy::register_function("io.stock.response", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);

			//std::cerr << "send.stock.response: ";
			//cyy::io::to_literal(std::cerr, frame, &io::custom);
			//std::cerr << std::endl;

			const auto code = cyy::value_cast(frame.at(0), codes::NotFound);
			switch (code)
			{
				//	100s: Informational
				//case codes::Continue:	
				//case SwitchingProtocols:	return "101 Switching Protocols";
				//case Processing: return "102 Processing";

			case codes::OK:
			//	case codes::Created:
				//case Accepted:	return "202 Accepted";
				//case NonAuthoritativeInformation:	return "203 Non-Authoritative Information";
				//case NoContent:	return "204 No Content";
				//case ResetContent:	return "205 Reset Content";
				//case PartialContent:	return "206 Partial Content";
				//case MultiStatus:	return "207 Multi-Status ";
				//case AlreadyReported:	return "208 Already Reported";
				//case IMUsed:	return "226 IM Used";

				//	//	300s: Redirection requires "Location:" header
				//case MultipleChoices:	return "300 Multiple Choices";
				//case MovedPermanently:	return "301 Moved Permanently";
				//case MovedTemporarily:	return "302 Found";
				//case SeeOther:	return "303 See Other";
				//case NotModified:	return "304 Not Modified";
				//case UseProxy:	return "305 Use Proxy";
				//case Unused:	return "306 Switch Proxy";
				//case TemporaryRedirect:	return "307 Temporary Redirect";

					//	400s: Client Errors
			case codes::BadRequest:
			case codes::Unauthorized:	
			case codes::PaymentRequired:
			case codes::Forbidden:
			case codes::MethodNotAllowed:
			case codes::NotAcceptable:
			case codes::ProxyAuthRequired:
			case codes::RequestTimeout:
			case codes::Conflict:
			case codes::Gone:
			case codes::LengthRequired:
			case codes::PreconditionFailed:
			case codes::RequestEntityTooLarge:
			case codes::RequestUriTooLong:
			case codes::UnsupportedMediaType:
			case codes::RequestedRangeNotSatisfiable:
			case codes::ExpectationFailed:

				//	500s: Server Errors
			case codes::InternalServerError:
			case codes::NotImplemented:
			case codes::BadGateway:
			case codes::ServiceUnavailable:
			case codes::GatewayTimeout:
			case codes::HttpVersionNotSupported:
				stock(code);
				break;
			case codes::NotFound:
			default:
				stock_404();
				break;
			}

		}));

		//
		//	Send a stock response 303: see other.
		//
		vm.async_run(cyy::register_function("io.stock.303", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);

			//std::cerr << "send.stock.response.303: ";
			//cyy::io::to_literal(std::cerr, frame, &io::custom);
			//std::cerr << std::endl;

			const std::string loc = cyy::value_cast(frame.at(0), std::string());

			response res(codes::SeeOther);
			powered_by(res.params_);
			location(res.params_, loc);
			write(res);

		}));

		vm.async_run(cyy::register_function("io.send.favicon", 0, [this](cyy::context& ctx) {

			response res(codes::OK);
			powered_by(res.params_);
			content_type(res.params_, "image/x-icon");
			content_length(res.params_, favicon_size);
			write(res);

			//
			//	write content direct into stream
			//
			ostream_.write((const char*)favicon, favicon_size);

		}));

		vm.async_run(cyy::register_function("io.upgrade.websocket", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);

			response res(codes::SwitchingProtocols);
			powered_by(res.params_);

			accept_websocket(res.params_
				, cyy::value_cast(frame.at(0), std::string())
				, cyy::value_cast(frame.at(1), std::string()));
			write(res);

		}));
		
		//
		//	send text data using an open websocket connection
		//
		vm.async_run(cyy::register_function("io.ws.text", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);

			//
			//	get and send text
			//
			this->ws_text(cyy::string_cast(frame.at(0), ""));

		}));

		//
		//	send binary data using an open websocket connection
		//
		vm.async_run(cyy::register_function("io.ws.binary", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);

			//
			//	get and send binary data
			//
			cyy::buffer_t data;
			this->ws_binary(cyy::value_cast(frame.at(0), data));

		}));

		//
		//	Send an object as JSON string.
		//	Note that the frame could contain more than one object.
		//
		vm.async_run(cyy::register_function("io.ws.json", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);

			std::stringstream ss;
			std::for_each(frame.begin(), frame.end(), [this, &ss](cyy::object const& obj) {
				if (cyy::serialize_json(ss, obj))
				{
					ws_text(ss.str());
					ss.str("");
				}
			});

		}));

		//
		//	close an open websocket connection
		//
		vm.async_run(cyy::register_function("io.ws.close", 0, [this](cyy::context& ctx) {

			//
			//	prepare websocket header
			//
			rfc6455_header_overlay	h;
			h.header_.opcode = ws::close;
			h.header_.fin = 1;
			h.header_.length = 0;
			h.header_.mask = 0;	//	unmasked
			h.header_.rsv = 0;	//	reserved bits have to be zero

			//
			//	write content direct into stream
			//
			ostream_.put(h.overlay_[0]);
			ostream_.put(h.overlay_[1]);

		}));

		//
		//	send a PING 
		//
		vm.async_run(cyy::register_function("io.ws.ping", 0, [this](cyy::context& ctx) {

			//
			//	prepare websocket header
			//
			rfc6455_header_overlay	h;
			h.header_.opcode = ws::ping;
			h.header_.fin = 1;
			h.header_.length = 0;
			h.header_.mask = 0;	//	unmasked
			h.header_.rsv = 0;	//	reserved bits have to be zero

			//
			//	write content direct into stream
			//
			ostream_.put(h.overlay_[0]);
			ostream_.put(h.overlay_[1]);

		}));

		//
		//	send a PONG
		//
		vm.async_run(cyy::register_function("io.ws.pong", 0, [this](cyy::context& ctx) {

			//
			//	prepare websocket header
			//
			rfc6455_header_overlay	h;
			h.header_.opcode = ws::pong;
			h.header_.fin = 1;
			h.header_.length = 0;
			h.header_.mask = 0;	//	unmasked
			h.header_.rsv = 0;	//	reserved bits have to be zero

			//
			//	write content direct into stream
			//
			ostream_.put(h.overlay_[0]);
			ostream_.put(h.overlay_[1]);

		}));

		//
		//	Send JSON object
		//
		vm.async_run(cyy::register_function("io.send.json", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);

			std::stringstream ss;
			if (cyy::serialize_json(ss, frame.at(0)))
			{
				response res(codes::OK, ss.str());
				powered_by(res.params_);
				last_modified(res.params_, std::chrono::system_clock::now());
				content_type(res.params_, make_mime_type("application", "json"));
				write(res);
			}
			else
			{
				//	serialization failes
				stock(codes::InternalServerError);
			}
		}));

		//
		//	Send basic authorization request
		//
		vm.async_run(cyy::register_function("io.auth.basic", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);

			cyy::vector_reader	reader(frame);
			const auto realm = reader.get_string(0, "CyX");
			//	the second parameter is pure optional
			const auto msg = reader.get_string(1, "not authorized");

			std::string body =
				"<!DOCTYPE html>\n"
				"<html lang=en>\n"
				"\t<head>\n"
				"\t\t<title>not authorized</title>\n"
				"\t</head>\n"
				"\t<body style=\"font-family:'Courier New', Arial, sans-serif\">\n"
				"\t\t<h1>&#x1F6AB; " + 	msg + "</h1>\n"
				"\t</body>\n"
				"</html>\n"
				;

			response res(codes::Unauthorized, std::move(body));
			content_type(res.params_, "text/html");
			powered_by(res.params_);
			basic_auth(res.params_, realm);
			keep_alive(res.params_);
			write(res);

		}));

		//
		//	Send digest authorization request
		//
		vm.async_run(cyy::register_function("io.auth.digest", 4, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);
			const auto realm = cyy::string_cast(frame.at(0));
			const auto algo = cyy::string_cast(frame.at(1));
			const auto nonce = cyy::string_cast(frame.at(2));
			const auto opaque = cyy::string_cast(frame.at(3));

			std::string body =
				"<!DOCTYPE html>\n"
				"<html lang=en>\n"
				"\t<head>\n"
				"\t\t<title>not authorized</title>\n"
				"\t</head>\n"
				"\t<body style=\"font-family:'Courier New', Arial, sans-serif\">\n"
				"\t\t<h1>&#x1F6AB; not authorized</h1>\n"
				"\t</body>\n"
				"</html>\n"
				;

			response res(codes::Unauthorized, std::move(body));
			powered_by(res.params_);
			digest_auth(res.params_, realm, algo, nonce, opaque);
			keep_alive(res.params_);
			write(res);

		}));

		vm.async_run(cyy::register_function("io.options", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);
			response res(codes::OK, "");
			powered_by(res.params_);
			allow(res.params_, { methods::Copy, methods::Delete, methods::Get, methods::Head, methods::Lock, methods::MkCol, methods::Move, methods::Options, methods::Post, methods::Propfind, methods::Proppatch, methods::Put, methods::Unlock });
			write(res);

		}));
	}

	serializer::~serializer()
	{

	}
	
	boost::system::error_code serializer::flush(boost::asio::ip::tcp::socket& s)
	{
#ifdef __DEBUG
			//	get content of buffer
			boost::asio::const_buffer cbuffer(*buffer_.data().begin());
			const char* p = boost::asio::buffer_cast<const char*>(cbuffer);
			const std::size_t size = boost::asio::buffer_size(cbuffer);

			cyy::io::hex_dump hd;
			std::cerr 
			<< "send "
			<< size
			<< " bytes:\n"
			;
			hd(std::cerr, p, p + size);
#endif
		
		boost::system::error_code ec;
		boost::asio::write(s, buffer_, ec);
		return ec;
	}

	boost::system::error_code serializer::flush(boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& s)
	{
		boost::system::error_code ec;
		boost::asio::write(s, buffer_, ec);
		return ec;
	}

	void serializer::stock(codes::status code)
	{
		std::string body = 
			"<!DOCTYPE html>\n"
			"<html lang=en>\n"
			"\t<head>\n"
			"\t\t<title>" + codes::name(code) + "</title>\n"
			"\t</head>\n"
			"\t<body style=\"font-family:'Courier New', Arial, sans-serif\">\n"
			"\t\t<h1>" + codes::name(code) + "</h1>\n"
			"\t</body>\n"
			"</html>\n"
			;

		response res(codes::NotFound, std::move(body));
		powered_by(res.params_);
		content_type(res.params_, "text/html");
		write(res);

	}

	void serializer::stock_404()
	{
		//	body message
		std::string body =
			"<!DOCTYPE html>\n"
			"<html lang=en>\n"
			"\t<head>\n"
			"\t\t<title>Error 404</title>\n"
			"\t</head>\n"
			"\t<body style=\"font-family:'Courier New', Arial, sans-serif\">\n"
//			"\t\t<h1>&#xf06a; 404 page not found (yet).</h1>\n"	//	doesn't work for chrome
			"\t\t<h1>&#x270B; 404 page not found (yet).</h1>\n"
			"\t</body>\n"
			"</html>\n"
			;

		response res(codes::NotFound, std::move(body));
		powered_by(res.params_);
		content_type(res.params_, "text/html");
//		content_length(res.params_, body.size());
		write(res);
	}

	void serializer::write(response const& res)
	{
		ostream_
			<< "HTTP/"
			<< res.version_
			<< ' '
			<< codes::name(res.status_)
			<< "\r\n"
			;

		//	serialize headers
		std::for_each(res.params_.begin()
			, res.params_.end()
			, [this](cyy::param_map_t::value_type const& v) {

			ostream_
				<< v.first
				<< ':'
				<< ' '
				<< cyy::to_string(v.second, &io::custom)
				<< "\r\n"
				;

		});

		ostream_
			<< "\r\n"
			<< res.body_
			;

	}

	void serializer::ws_text(std::string const& text)
	{
		//
		//	prepare websocket header
		//
		rfc6455_header_overlay	h;
		h.overlay_[0] = 0;
		h.overlay_[1] = 0;
		h.header_.opcode = ws::text;
		h.header_.fin = 1;	//	not fragmented (this is the last fragment)
		h.header_.rsv = 0;	//	reserved bits have to be zero
		h.header_.mask = 0;	//	unmasked

		//
		//	write first header byte
		//
		ostream_.put(h.overlay_[0]);

		if (text.size() < 126)
		{
			h.header_.length = text.size();

			//
			//	write second header byte
			//
			ostream_.put(h.overlay_[1]);
		}
		else if (text.size() < 0xffff)
		{
			h.overlay_[1] |= 126;
			const boost::endian::big_uint16_buf_t extl{ (std::uint16_t)text.size() };
			static_assert(sizeof(boost::endian::big_uint16_buf_t) == 2, "invalid size of boost::endian::big_uint16_buf_t");

			//
			//	write extended payload length
			//
			ostream_.put(h.overlay_[1]);
			ostream_.write((const char*)&extl, sizeof(extl));
		}
		else
		{
			h.overlay_[1] |= 127;
			const boost::endian::big_uint64_buf_t extl{ text.size() };
			static_assert(sizeof(boost::endian::big_uint64_buf_t) == 8, "invalid size of boost::endian::big_uint64_buf_t");

			//
			//	write extended payload length
			//
			ostream_.put(h.overlay_[1]);
			ostream_.write((const char*)&extl, sizeof(extl));
		}

		//
		//	write content direct into stream
		//
		ostream_.write(text.c_str(), text.size());
	}

	void serializer::ws_binary(cyy::buffer_t const& data)
	{
		//
		//	prepare websocket header
		//
		rfc6455_header_overlay	h;
		h.header_.opcode = ws::binary;
		h.header_.fin = 1;	//	not fragmented (this is the last fragment)
		h.header_.rsv = 0;	//	reserved bits have to be zero
		h.header_.mask = 0;	//	unmasked

		//
		//	write first header byte
		//
		ostream_.put(h.overlay_[0]);

		//
		//	write first header byte
		//
		ostream_.put(h.overlay_[0]);

		if (data.size() < 126)
		{
			h.header_.length = data.size();

			//
			//	write second header byte
			//
			ostream_.put(h.overlay_[1]);
		}
		else if (data.size() < 0xffff)
		{
			h.overlay_[1] |= 126;
			const boost::endian::big_uint16_buf_t extl{ (std::uint16_t)data.size() };
			static_assert(sizeof(boost::endian::big_uint16_buf_t) == 2, "invalid size of boost::endian::big_uint16_buf_t");

			//
			//	write extended payload length
			//
			ostream_.put(h.overlay_[1]);
			ostream_.write((const char*)&extl, sizeof(extl));
		}
		else
		{
			h.overlay_[1] |= 127;
			const boost::endian::big_uint64_buf_t extl{ data.size() };
			static_assert(sizeof(boost::endian::big_uint64_buf_t) == 8, "invalid size of boost::endian::big_uint64_buf_t");

			//
			//	write extended payload length
			//
			ostream_.put(h.overlay_[1]);
			ostream_.write((const char*)&extl, sizeof(extl));
		}

		//
		//	write content direct into stream
		//
		ostream_.write(data.data(), data.size());

	}

}
