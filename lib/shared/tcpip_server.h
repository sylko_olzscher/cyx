/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_TCPIP_SERVER_H
#define CYX_TCPIP_SERVER_H

#include <cyx/layer4/tcpip_socket.h>
#include <atomic>
#include <condition_variable>

namespace cyx
{
    /**
     * Base class for TCP/IP server without SSL/TLS encryption
     * support. Works for both TCP/IP v4 and v6.
     */
    class tcpip_srv
    {
//     public:
//         typedef boost::asio::ip::tcp::socket	socket_t;

    public:
        tcpip_srv() = delete;
        tcpip_srv(tcpip_srv const&) = delete;
        tcpip_srv(boost::asio::io_service& scheduler, socket_t&& s);
        tcpip_srv(boost::asio::io_service& scheduler);
        virtual ~tcpip_srv();

        /**
         * @param address listener address. May be a descriptive name or a numeric address string.
         * @param service the service - mostly a port number
         */
        bool run(std::string const& address, std::string const& service);

        /**
         * Stop listening.
         * This terminates any connections waiting to be accepted.
         *
         * @return true if no error occured
         */
        bool shutdown();

    protected:
        /**
        * Overwrite this method to get the startup event
        */
        virtual void on_startup(boost::asio::ip::tcp::endpoint const& ep);

        /**
         * To overwrite.
         *
         * Move socket into connection manager of application layer.
         * tcp_server does not care about open sockets when going down.
         * After moving the socket the moved socket has to be in the same
         * state as a new socket. This could be achieved by moving or
         * closing the socket.
         */
        virtual void on_connect(socket_t&& s) = 0;

        /**
         * Overwrite this method to log error messages.
         */
        virtual void on_error(boost::system::error_code const& ec);

    private:
        /**
         * Could throw!
         * query works only if network is running. Otherwise address resolution fails.
         *
         * @param address listener address. May be a descriptive name or a numeric address string.
         * @param service the service - mostly a port number
         */
        void resolve(std::string const& address, std::string const& service);

        /**
         *	Wait for incoming connections.
         */
        void listen();

    private:
        /**
         * The workhorse
         */
        boost::asio::io_service&	scheduler_;

        /**
         *	The next socket to be accepted.
         */
        socket_t socket_;

        /**
         *	manages async TCP connects
         */
        boost::asio::ip::tcp::acceptor		tcp_acceptor_;

        /**
         *	set to true when the server is listening for new connections
         */
        std::atomic<bool>	is_listening_;

        /**
         *	Condition variable for proper shutdown
         */
        std::condition_variable shutdown_complete_;
        std::mutex mutex_; //!< synchronise shutdown

    };
}

#endif	//	CYX_TCPIP_SERVER_H
