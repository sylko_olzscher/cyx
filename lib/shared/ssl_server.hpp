/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_SSL_SERVER_H
#define CYX_SSL_SERVER_H

#include <cyx/layer4/ssl_socket.h>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <boost/exception/diagnostic_information.hpp> //	requires boost >= 105400 

namespace cyx
{
    /**
     * Base class for TCP/IP server with SSL/TLS encryption
     * support. Works for both TCP/IP v4 and v6.
     * 
     * @tparam T session type (it's assumed that this is a shared type)
     */
    template < typename T >
    class ssl_srv
    {
    public:
        typedef std::function<T(boost::asio::ssl::context&)> cb_t;
        
    public:
        ssl_srv() = delete;
        ssl_srv(ssl_srv const&) = delete;
        
        ssl_srv(boost::asio::io_service& scheduler
                , cb_t cb
                , std::string const& ssl_pwd
                , std::string const& ssl_certificate
                , std::string const& ssl_private_key
                , std::string const& ssl_dh)
            : scheduler_(scheduler)
            , session_factory_(cb)
            , tcp_acceptor_(scheduler)
            , context_(boost::asio::ssl::context::sslv23)
            , is_listening_(false)
            , shutdown_complete_()
            , mutex_()
        {
            context_.set_options(
                boost::asio::ssl::context::default_workarounds
                | boost::asio::ssl::context::no_sslv2
                | boost::asio::ssl::context::single_dh_use);        
            
            context_.set_password_callback([ssl_pwd](std::size_t max_length, boost::asio::ssl::context::password_purpose purpose)->std::string {
                BOOST_ASSERT_MSG(ssl_pwd.size() < max_length, "SSL password to long");
                return ssl_pwd;
            });
            
			//	openssl req -x509 -newkey rsa:4096 -keyout key.pem -out server.pem -days 365
			context_.use_certificate_chain_file(ssl_certificate);
			context_.use_private_key_file(ssl_private_key, boost::asio::ssl::context::pem);
			context_.use_tmp_dh_file(ssl_dh);
            
        }
        virtual ~ssl_srv()
        {
            BOOST_ASSERT_MSG(!is_listening_, "still listening");
        }

        /**
         * @param address listener address. May be a descriptive name or a numeric address string.
         * @param service the service - mostly a port number
         */
        bool run(std::string const& address, std::string const& service)
        {
            //
            //	set listener flag immediately to prevent
            //	a concurrent call of this same
            //	method
            //
            if (!is_listening_.exchange(true))
            {
                try
                {
                    //	resolve address and start listening
                    resolve(address, service);

                    //	wait for incoming connects
                    listen();

                }
                catch (boost::system::system_error const& ex)
                {
                    //	reset listener flag
                    is_listening_.exchange(false);

                    std::cerr
                            << boost::diagnostic_information(ex, true)
                            << std::endl
                            ;
                }
            }

            return is_listening_;
        }


        /**
         * Stop listening.
         * This terminates any connections waiting to be accepted.
         *
         * @return true if no error occured
         */
        bool shutdown()
        {
            std::unique_lock<std::mutex> lock(mutex_);

            boost::system::error_code ec;
            tcp_acceptor_.close(ec);
            if (!ec)
            {
                //	wait for cancellation of accept operation
                shutdown_complete_.wait(lock, [this] {
                    return !is_listening_.load();
                });

                //	no error
                return true;
            }
            return false;
        }
        
    protected:
        /**
         * Overwrite this method to get the startup event
         */
        virtual void on_startup(boost::asio::ip::tcp::endpoint const& ep)
        {
#ifdef _DEBUG
            std::cout
                << "startup TCP/IP server on "
                << ep
                << std::endl
                ;
#endif
        }

        /**
         * To overwrite.
         *
         * Move socket into connection manager of application layer.
         * tcp_server does not care about open sockets when going down.
         * After moving the socket the moved socket has to be in the same
         * state as a new socket. This could be achieved by moving or
         * closing the socket.
         */
		virtual void on_connect(T s) = 0;

        /**
         * Overwrite this method to log error messages.
         */
        virtual void on_error(boost::system::error_code const& ec)
        {
 #ifdef _DEBUG
            std::cerr
                << "***Error: ssl_srv("
                << ec.message()
                << ")"
                << std::endl
                ;
#endif
        }        

    private:
        /**
         * Could throw!
         * query works only if network is running. Otherwise address resolution fails.
         *
         * @param address listener address. May be a descriptive name or a numeric address string.
         * @param service the service - mostly a port number
         */
        void resolve(std::string const& address, std::string const& service)
        {
            boost::asio::ip::tcp::resolver resolver(scheduler_);
            boost::asio::ip::tcp::resolver::query query(address, service);
            boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);

            tcp_acceptor_.open(endpoint.protocol());
            tcp_acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
            tcp_acceptor_.bind(endpoint);	//	could throw boost::system::system_error: permission denied

            //	startup event
            on_startup(endpoint);

            tcp_acceptor_.listen(boost::asio::socket_base::max_connections);
        }        

        /**
         *	Wait for incoming connections.
         */
        void listen()
        {
            BOOST_ASSERT_MSG(is_listening_, "no TCP/IP listener");

            //
            //  generate next session
            //
            auto s = session_factory_(context_);
            tcp_acceptor_.async_accept( s->get_socket(), [this, s](boost::system::error_code const& ec) {

                if (tcp_acceptor_.is_open() && !ec)
                {
                    //	Following the move, the moved-from object is in the same state
                    //	as if constructed using the basic_stream_socket(io_service&) constructor.
					on_connect(s);

                    //	continue
                    listen();
                }
                else
                {
                    on_error(ec);

                    //
                    //	remove listener flag
                    //
                    is_listening_.exchange(false);

                    //
                    //	notify
                    //
                    std::lock_guard<std::mutex> lk(mutex_);
                    shutdown_complete_.notify_all();
                }
            });
        }
        

    private:
        /**
         * The workhorse
         */
        boost::asio::io_service&	scheduler_;

        /**
         * Generates new sessions
         */
        cb_t    session_factory_;
        
        /**
         *	manages async TCP connects
         */
        boost::asio::ip::tcp::acceptor		tcp_acceptor_;
        
        /**
         * Provide an SSL context
         */
        boost::asio::ssl::context context_;

        /**
         *	set to true when the server is listening for new connections
         */
        std::atomic<bool>	is_listening_;

        /**
         *	Condition variable for proper shutdown
         */
        std::condition_variable shutdown_complete_;
        std::mutex mutex_; //!< synchronise shutdown

    };
}

#endif	//	CYX_SSL_SERVER_H
