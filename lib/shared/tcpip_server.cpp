/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "tcpip_server.h"
#include <iostream>
#include <boost/exception/diagnostic_information.hpp> //	requires boost >= 105400 

namespace cyx
{
tcpip_srv::tcpip_srv(boost::asio::io_service& scheduler, socket_t&& s)
    : scheduler_(scheduler)
    , socket_(std::forward<socket_t>(s))
    , tcp_acceptor_(scheduler)
    , is_listening_(false)
    , shutdown_complete_()
    , mutex_()
{}

tcpip_srv::tcpip_srv(boost::asio::io_service& scheduler)
    : scheduler_(scheduler)
    , socket_(scheduler)
    , tcp_acceptor_(scheduler)
    , is_listening_(false)
    , shutdown_complete_()
    , mutex_()
{}

tcpip_srv::~tcpip_srv()
{
    BOOST_ASSERT_MSG(!is_listening_, "still listening");
}

bool tcpip_srv::run(std::string const& address, std::string const& service)
{
    //
    //	set listener flag immediately to prevent
    //	a concurrent call of this same
    //	method
    //
    if (!is_listening_.exchange(true))
    {
        try
        {
            //	resolve address and start listening
            resolve(address, service);

            //	wait for incoming connects
            listen();

        }
        catch (boost::system::system_error const& ex)
        {
            //	reset listener flag
            is_listening_.exchange(false);

            std::cerr
                    << boost::diagnostic_information(ex, true)
                    << std::endl
                    ;
        }
    }

    return is_listening_;
}

void tcpip_srv::resolve(std::string const& address, std::string const& service)
{
    boost::asio::ip::tcp::resolver resolver(scheduler_);
    boost::asio::ip::tcp::resolver::query query(address, service);
    boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);

    tcp_acceptor_.open(endpoint.protocol());
    tcp_acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    tcp_acceptor_.bind(endpoint);	//	could throw boost::system::system_error: permission denied

    //	startup event
    on_startup(endpoint);

    tcp_acceptor_.listen(boost::asio::socket_base::max_connections);
}

void tcpip_srv::on_startup(boost::asio::ip::tcp::endpoint const& ep)
{
#ifdef __DEBUG
    std::cout
            << "startup TCP/IP server on "
            << ep
            << std::endl
            ;
#endif
}

void tcpip_srv::listen()
{
    BOOST_ASSERT_MSG(is_listening_, "no TCP/IP listener");

    tcp_acceptor_.async_accept( socket_, [=](boost::system::error_code const& ec) {

        if (tcp_acceptor_.is_open() && !ec)
        {
            //	Following the move, the moved-from object is in the same state
            //	as if constructed using the basic_stream_socket(io_service&) constructor.
            on_connect(std::move(socket_));

            //
            //	After on_connect() the socket has to be in an initial state.
            //
            BOOST_ASSERT_MSG(!socket_.is_open(), "socket is still open");

            //	continue
            listen();
        }
        else
        {
            on_error(ec);

            //
            //	remove listener flag
            //
            is_listening_.exchange(false);

            //
            //	notify
            //
            std::lock_guard<std::mutex> lk(mutex_);
            shutdown_complete_.notify_all();
        }
    });
}

bool tcpip_srv::shutdown()
{
    std::unique_lock<std::mutex> lock(mutex_);

    boost::system::error_code ec;
    tcp_acceptor_.close(ec);
    if (!ec)
    {
        //	wait for cancellation of accept operation
        shutdown_complete_.wait(lock, [this] {
            return !is_listening_.load();
        });

        //	no error
        return true;
    }
    return false;
}

void tcpip_srv::on_error(boost::system::error_code const& ec)
{
    std::cerr
            << "***Error: tcpip_srv("
            << ec.message()
            << ")"
            << std::endl
            ;
}
}	//	cyx
