/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <cyx/smtp/client.h>
#include <cyy/crypto/base64.hpp>

#include <iostream>
#include <thread>

#include <boost/algorithm/string/predicate.hpp>

namespace cyx	
{
	namespace smtp	
	{

		client::client(boost::asio::io_service& io_service
			, std::string const& host
			, ip_port_t port
			, std::string const& user
			, std::string const& pwd)
			: socket_(io_service)
			, resolver_(io_service)
			, request_()
			, local_host_(boost::asio::ip::host_name())
			, remote_host_(host)
			, remote_port_(port)
			, user_(user)
			, pwd_(pwd)
		{
			const std::string smtp_service = (remote_port_ == 0)
				? "SMTP"
				: std::to_string(remote_port_);

			// connecting to remote host if not already connected:
			boost::asio::ip::tcp::resolver::query query(remote_host_, smtp_service);
			boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver_.resolve(query);

			std::cout
				<< (*endpoint_iterator).endpoint().address().to_string()
				<< ":"
				<< (*endpoint_iterator).endpoint().port()
				<< std::endl
				;

			// Try each endpoint until we successfully establish a connection.
			boost::asio::connect(socket_, endpoint_iterator);

			std::cout
				<< "---=== TCP/IP CONNECT ===---"
				<< std::endl
				;

		}
			
		void client::send(std::string const& from
			, std::vector<std::string> const& to
			, std::string const& subject
			, std::string const& message)	
		{

			//	read response
			std::string line;
			read_line(line);	//	220 mailrelay.gwf.ch Microsoft ESMTP MAIL Service ready at Fri, 7 Nov 2014 11:02:29 +0100
			std::cout << "welcome: " << line << std::endl;

			// Form the request. We specify the "Connection: close" header so that the
			// server will close the socket after transmitting the response. This will
			// allow us to treat all data up until the EOF as the content.
			boost::asio::streambuf request;
			std::ostream request_stream(&request);

			write_line("EHLO " + remote_host_);

			//	read response
			read_line(line);
			std::cout << line << std::endl;
			//while (boost::algorithm::istarts_with(line, "250-"))	{

			//	std::cout
			//		<< "...try to read more..."
			//		<< std::endl
			//		;
			//	std::this_thread::sleep_for(std::chrono::seconds(1));
			//	read_line(line);
			//	std::cout << line << std::endl;
			//}
			//read_line(line);
			//std::cout << line << std::endl;
			//250-mailrelay.gwf.ch Hello [192.168.50.27]
			//250-SIZE 2147482624
			//250-PIPELINING
			//250-DSN
			//250-ENHANCEDSTATUSCODES
			//250-AUTH
			//250-8BITMIME
			//250-BINARYMIME
			//250 CHUNKING

			if (!user_.empty())	
			{
				write_line("AUTH LOGIN");
				read_line(line);
				std::cout << "AUTH LOGIN: " << line << std::endl;

				const std::string user = cyy::crypto::encode_base64(user_);
				write_line(user);
				read_line(line);
				std::cout
					<< "user ("
					<< user
					<< "): "
					<< line
					<< std::endl;

				const std::string pwd = cyy::crypto::encode_base64(pwd_);
				write_line(pwd);
				read_line(line);
				std::cout
					<< "pwd ("
					<< pwd
					<< "): "
					<< line
					<< std::endl;
			}

			write_line("MAIL FROM:<" + from + ">");
			//	read 250 OK
			for (auto const& address : to)	
			{
				write_line("RCPT TO:<" + address + ">");
				//	read: 250 OK
				//	read: 550 No such user here
			}
			write_line("DATA");
			//	read 354 Start mail input; end with <CRLF>.<CRLF>
			write_line("SUBJECT:" + subject);
			write_line("From:" + from);
			for (auto const& address : to)	{
				write_line("To:<" + address + ">");
			}
			write_line("");
			write_line(message);
			write_line("\r\n.\r\n");

			read_line(line);
			std::cout << "send: " << line << std::endl;
			if (line == "250")	{
				//mHasError = false;
			}
			write_line("QUIT");
			//	read 221 USC-ISIE.ARPA Service closing transmission channel

		}

		//void smtp_client::set_sender_mail(std::string const& s)	{
		//	mail_from_ = s;
		//}

		void client::write_line(std::string const& line)	
		{

			std::cout
				<< "EMIT: "
				<< line
				<< std::endl
				;
			std::ostream req_strm(&request_);
			req_strm << line << "\r\n";
			boost::asio::write(socket_, request_);
			req_strm.clear();
		}

		void client::read_line(std::string& line)	
		{

			boost::asio::streambuf response;
			const std::string delim("\r\n");
			const std::size_t bytes = boost::asio::read_until(socket_, response, delim);

			std::cout
				<< bytes
				<< " bytes received"
				<< std::endl
				;
			//std::istream response_stream(&response);
			std::istream is(&response);
			//is.unsetf(std::ios::skipws);
			std::getline(is, line);

			//	consider this:
			//std::string gulp(std::istream &in)
			//{
			//	std::string ret;
			//	char buffer[4096];
			//	while (in.read(buffer, sizeof(buffer)))
			//		ret.append(buffer, sizeof(buffer));
			//	ret.append(buffer, in.gcount());
			//	return ret;
			//}

		}

	}	//	smtp
}	//	m2m


