# top level files
set (smtp_lib)

set (smtp_lib_cpp

	lib/smtp/src/client.cpp
)

set (smtp_lib_h

	src/main/include/cyx/cyx.h	
	src/main/include/cyx/smtp/smtp.h	
	src/main/include/cyx/smtp/client.h
)




# define the master program
set (smtp_lib
  ${smtp_lib_cpp}
  ${smtp_lib_h}
)
