/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/docscript/generator.h>
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/factory/chrono_factory.h>
#include <cyy/intrinsics/factory/op_factory.h>
#include <cyy/intrinsics/factory/string_factory.h>
#include <cyy/intrinsics/factory/generic_factory.hpp>

#include <cyy/io/io_set.h>
#include <cyy/json/json_io.h>
#include <cyy/crypto/base64.hpp>

#include <iostream>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace cyx	
{
	namespace docscript
	{

		generator::generator(std::vector< boost::filesystem::path > const& incs, int verbosity, bool body_only)
			: includes_(incs)
			, verbosity_(verbosity)
			, body_only_(body_only)
			, uuid_gen_()
			, name_gen_(uuid_gen_())
			//, name_gen_({ 0x34, 0x49, 0x58, 0x5e, 0x83, 0x7d, 0x4b, 0x9a, 0x81, 0x6e, 0x90, 0x4f, 0x94, 0x70, 0x9b, 0x02 })
			, scheduler_()
			, vm_(scheduler_.get_io_service())
			, meta_()
			, numeration_()
			, structure_()
		{
			register_this();
		}

		generator::~generator()
		{
			scheduler_.stop();
		}

		void generator::register_this()
		{
			vm_.async_run(cyy::register_function("now", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
#ifdef _DEBUG
				//std::cout
				//	<< "\n***info: now("
				//	<< cyy::io::to_literal(frame)
				//	<< ")"
				//	<< std::endl;
#endif
				ctx.set_return_value(cyy::time_point_factory(), 0);
			}));

			vm_.async_run(cyy::register_function("meta", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//	[1idx,true,%(("author":"Sylko Olzscher"),("released":"2017.10.22 12:57:35.78909010"ts))]
				std::cout
					<< "\n***info: meta("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				cyy::param_map_t data;
 				data = reader.get(2, data);
 				BOOST_ASSERT_MSG(size == 1, "internal error (meta)");
 				this->update_meta(data);

				//
				// function meta has no return values
				//
			}));

			vm_.async_run(cyy::register_function("debug", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//std::cout
				//	<< "\n***info: debug("
				//	<< cyy::io::to_literal(frame)
				//	<< ")"
				//	<< std::endl;

#endif
				const std::string v = cyy::string_cast(frame.at(2));
				const cyy::codes::op_code op = boost::algorithm::iequals(v, "on")
					? cyy::codes::DBG_ON
					: cyy::codes::DBG_OFF
					;

				if (verbosity_ > 1)
				{
					std::cout
						<< "***info: debug("
						<< std::boolalpha
						<< boost::algorithm::iequals(v, "on")
						<< ")"
						<< std::endl;
				}


				ctx.squeeze(cyy::vector_t{ cyy::factory(op) });

				//
				// function meta has no return values
				//

			}));

			vm_.async_run(cyy::register_function("title", 1, [this](cyy::context& ctx) {

				//	[1idx,true,"Introduction into docScript"]
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
				//	[3idx,true,"docScript","into","Introduction"]
				std::cout
					<< "\n***info: title("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (title)");

				std::string title;
				for (std::size_t idx = size + 1; idx > 1; idx--)
				{
					title += reader.get_string(idx);
					if (idx > 2)
					{
						title += " ";
					}
				}

				if (verbosity_ > 1)
				{
					std::cout
						<< "***info: title("
						<< title
						<< ")"
						<< std::endl;
				}

				meta_.emplace("title", cyy::factory(title));

				//
				// function meta has no return values
				//
			}));

			vm_.async_run(cyy::register_function("contents", 2, [this](cyy::context& ctx) {

				//	[1idx,true,%(("depth":"4"))]
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: contents("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
			}));

			vm_.async_run(cyy::register_function("header", 3, [this](cyy::context& ctx) {

				//	[1idx,true,%(("level":"1"),("tag":"79bf3ba0-2362-4ea5-bcb5-ed93844ac59a"),("title":"Basics"))]
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: header("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				const std::string txt = reader[2].get_string("title", "NO TITLE");
				const std::size_t level = reader[2].get_index("level", 0);
				const std::string stag = reader[2].get_string("tag", boost::uuids::to_string(uuid_gen_()));
				const boost::uuids::uuid tag = name_gen_(stag);

				const std::string node = generate_header(level, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);
			}));

			vm_.async_run(cyy::register_function("header.1", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: header.1("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
				{
					BOOST_ASSERT(size == 1);
					const std::string txt = reader[2].get_string("title", "NO TITLE");
					const std::string stag = reader[2].get_string("tag", boost::uuids::to_string(uuid_gen_()));
					const boost::uuids::uuid tag = name_gen_(stag);

					const std::string node = generate_header(1, txt, tag);
					ctx.set_return_value(cyy::factory(node), 0);
				}
				else
				{
					const std::string txt = accumulate(reader, size + 1, 1);
					const std::string node = generate_header(1, txt, uuid_gen_());
					ctx.set_return_value(cyy::factory(node), 0);
				}
			}));

			vm_.async_run(cyy::register_function("header.2", 3, [this](cyy::context& ctx) {

				//	[1idx,true,"Examples"]
				//	[3idx, true, "DocScript", "of", "Examples"]
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: header.2("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
				{
					BOOST_ASSERT(size == 1);
					const std::string txt = reader[2].get_string("title", "NO TITLE");
					const std::string stag = reader[2].get_string("tag", boost::uuids::to_string(uuid_gen_()));
					const boost::uuids::uuid tag = name_gen_(stag);

					const std::string node = generate_header(2, txt, tag);
					ctx.set_return_value(cyy::factory(node), 0);
				}
				else
				{
					const std::string txt = accumulate(reader, size + 1, 1);
					const std::string node = generate_header(2, txt, uuid_gen_());
					ctx.set_return_value(cyy::factory(node), 0);
				}
			}));

			vm_.async_run(cyy::register_function("header.3", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: header.3("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
				{
					BOOST_ASSERT(size == 1);
					const std::string txt = reader[2].get_string("title", "NO TITLE");
					const std::string stag = reader[2].get_string("tag", boost::uuids::to_string(uuid_gen_()));
					const boost::uuids::uuid tag = name_gen_(stag);

					const std::string node = generate_header(3, txt, tag);
					ctx.set_return_value(cyy::factory(node), 0);
				}
				else
				{
					const std::string txt = accumulate(reader, size + 1, 1);
					const std::string node = generate_header(3, txt, uuid_gen_());
					ctx.set_return_value(cyy::factory(node), 0);
				}
			}));

			vm_.async_run(cyy::register_function("header.4", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				std::cout
					<< "\n***info: header.4("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
				{
					BOOST_ASSERT(size == 1);
					const std::string txt = reader[2].get_string("title", "NO TITLE");
					const std::string stag = reader[2].get_string("tag", boost::uuids::to_string(uuid_gen_()));
					const boost::uuids::uuid tag = name_gen_(stag);

					const std::string node = generate_header(4, txt, tag);
					ctx.set_return_value(cyy::factory(node), 0);
				}
				else
				{
					const std::string txt = accumulate(reader, size + 1, 1);
					const std::string node = generate_header(4, txt, uuid_gen_());
					ctx.set_return_value(cyy::factory(node), 0);
				}
			}));

			vm_.async_run(cyy::register_function("paragraph", 1, [this](cyy::context& ctx) {

				//	[131idx,".","power","more","you", ..."]
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
				std::cout
					<< "\n***info: paragraph("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;
#endif
				const cyy::vector_reader reader(frame);

				const std::size_t size = reader.get_index(0);
				BOOST_ASSERT_MSG(size < frame.size(), "internal error (paragraph)");

				const std::string node = "\n<p>" + accumulate(reader, size, 0) + "</p>";
				ctx.set_return_value(cyy::factory(node), 0);

			}));

			vm_.async_run(cyy::register_function("bold", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//	[3idx,false,"here","bold","everything"]
				std::cout
					<< "\n***info: bold("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				//BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (bold)");

				const std::string node = "<b>"
					+ accumulate(reader, size + 1, 1)
					+ "</b>"
					;
				//if (verbosity_ > 2)
				//{
				//	std::cout
				//		<< "***info: bold("
				//		<< node
				//		<< ")"
				//		<< std::endl;
				//}

				ctx.set_return_value(cyy::factory(node), 0);

			}));

			vm_.async_run(cyy::register_function("emphasise", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//	[1idx,"program"]
				//std::cout
				//	<< "\n***info: emphasise("
				//	<< cyy::io::to_literal(frame)
				//	<< ")"
				//	<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				//BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (italic)");

				const std::string node = "<em>"
					+ accumulate(reader, size + 1, 1)
					+ "</em>"
					;
				//if (verbosity_ > 2)
				//{
				//	std::cout
				//		<< "***info: italic("
				//		<< node
				//		<< ")"
				//		<< std::endl;
				//}

				ctx.set_return_value(cyy::factory(node), 0);

			}));

			vm_.async_run(cyy::register_function("color", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//	[1idx,true,%(("red":"spiced up"))]
				//std::cout
				//	<< "\n***info: color("
				//	<< cyy::io::to_literal(frame)
				//	<< ")"
				//	<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				const auto map = reader.get(2, cyy::param_map_t());
				//BOOST_ASSERT_MSG(map.size() == 1, "internal error (color)");

				std::stringstream ss;
				ss
					<< "<span style=\"color:"
					<< map.begin()->first
					<< "\">"
					<< cyy::to_string(map.begin()->second)
					<< "</span>"
					;
				const std::string node = ss.str();
				if (verbosity_ > 2)
				{
					std::cout
						<< "***info: color("
						<< node
						<< ")"
						<< std::endl;
				}

				//ctx.set_return_value_invoke(cyy::factory(node), 0);
				ctx.set_return_value(cyy::factory(node), 0);

			}));

			vm_.async_run(cyy::register_function("link", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

				//	[1idx,false,%(("text":"LaTeX"),("url":"https://www.latex-project.org/"))]
				std::cout
					<< "\n***info: link("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				//const auto map = reader.get(2, cyy::param_map_t());
				BOOST_ASSERT_MSG(size == 1, "internal error (link)");

				const std::string url = reader[2].get_string("url");
				const std::string node = "<a href=\""
					+ url
					+ "\" title=\""
					+ reader[2].get_string("title", url)
					+ "\">"
					+ reader[2].get_string("text")
					+ "</a>"
					;

				ctx.set_return_value(cyy::factory(node), 0);
				//ctx.set_return_value_invoke(cyy::factory(node), 0);

			}));

			//<figure>
			//  <img src="img_pulpit.jpg" alt="The Pulpit Rock" width="304" height="228">
			//  <figcaption>Fig1. - A view of the pulpit rock in Norway.</figcaption>
			//</figure>
			vm_.async_run(cyy::register_function("figure", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
				//	[1idx,false,%(("caption":"figure with caption"),("source":"LogoSmall.jpg"))]
				std::cout
					<< "\n***info: figure("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;
#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);
				//BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (quote)");

				const std::string source = reader[2].get_string("source");
				const boost::filesystem::path p = resolve_path(source);
				std::ifstream file(p.string(), std::ios::binary | std::ios::ate);
				if (!file.is_open())
				{
					std::cerr
						<< "***error cannot open figure file "
						<< source
						<< std::endl;
				}
				else
				{
					//
					//	do not skip 
					//
					file.unsetf(std::ios::skipws);

					//
					//	get file size
					//
					std::streamsize size = file.tellg();
					file.seekg(0, std::ios::beg);

					//
					//	read into buffer
					//
					cyy::buffer_t buffer(size);
					file.read(buffer.data(), size);

					//
					//	encode image as base 64
					//
					const std::string alt = reader[2].get_string("alt");
					const std::string cap = reader[2].get_string("caption");

					std::stringstream ss;
					ss
						<< std::endl
						<< "<figure>"
						<< "<img alt=\""
						<< alt
						<< "\" src=\"data:image/"
						<< get_extension(p)
						<< ";base64,"
						<< cyy::crypto::base64::encode<>(buffer.begin(), buffer.end())
						<< "\" />\n<figcaption>"
						<< cap
						<< "</figcaption>\n"
						<< "</figure>"
						;

					const std::string node = ss.str();
					if (verbosity_ > 1)
					{
						std::cout
							<< "***info: figure("
							<< cap
							<< " - "
							<< size
							<< " bytes)"
							<< std::endl;
					}
					ctx.set_return_value(cyy::factory(node), 0);
				}
			}));


			vm_.async_run(cyy::register_function("quote", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
				//	[1idx,true,%(("source":"Earl Wilson"),("url":"https://www.brainyquote.com/quotes/quotes/e/earlwilson385998.html"))]
				std::cout
					<< "\n***info: quote("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;
#endif
				const cyy::vector_reader reader(frame);
				const std::size_t size = reader.get_index(0);

				const std::string source = reader[size + 1].get_string("source", "source");
				const std::string url = reader[size + 1].get_string("url", "");

				std::stringstream ss;
				ss
					<< std::endl
					<< "<blockquote cite=\""
					<< url
					<< "\">"
					<< accumulate(reader, size, 1)
					<< std::endl
					<< "<footer>- <cite>"
					<< source
					<< "</cite></footer>"
					<< std::endl
					<< "</blockquote>"
					;

				const std::string node = ss.str();
				ctx.set_return_value(cyy::factory(node), 0);

			}));

			vm_.async_run(cyy::register_function("generate", 1, [this](cyy::context& ctx) {
				cyy::vector_t frame = ctx.stack_frame(true);
#ifdef _DEBUG

				//	["C:/projects/cyx/Debug/out.html"path,null,null]
				std::cout
					<< "\n***info: generate("
					<< cyy::io::to_literal(frame)
					<< ")"
					<< std::endl;

#endif
				if (!cyy::primary_type_test<boost::filesystem::path>(frame.at(0)))
				{
					std::cerr
						<< "***error: input parameter for function generate() is not of type filesystem::path "
						<< std::endl;

				}
				const boost::filesystem::path p = cyy::value_cast(frame.at(0), boost::filesystem::path());

				if (verbosity_ > 1)
				{
					std::cout
						<< "***info: serialize "
						<< frame.size()
						<< " objects into file "
						<< p
						<< std::endl;
				}

				std::ofstream ofs(p.string(), std::ios::out | std::ios::trunc);
				if (!ofs.is_open())
				{
					std::cerr
						<< "***error cannot open output file "
						<< p
						<< std::endl;
				}
				else
				{
					this->generate(ofs, ++frame.begin(), frame.end());
					//serialize(file, ++frame.begin(), frame.end());
				}

			}));


		}

		void generator::update_meta(cyy::param_map_t const& data)
		{
			//
			//	update meta data 
			//
			for (auto e : data)
			{
				meta_.insert(e);
			}
		}

		void generator::generate(std::ostream& os, cyy::vector_t::const_iterator begin, cyy::vector_t::const_iterator end)
		{
			if (!body_only_)
			{
				//
				//	generate complete HTML file
				//	
				os 
					<< "<!DOCTYPE html>"
					<< std::endl
					<< "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
					<< std::endl
					<< "<head>"
					<< std::endl
					<< "\t<meta charset=\"utf-8\" />"
					<< std::endl
					;

				for (auto e : meta_)
				{
					if (boost::algorithm::equals(e.first, "title"))
					{
						os
							<< "\t<title>"
							<< cyy::to_string(e.second)
							<< "</title>"
							<< std::endl
							;

					}
					else
					{
						os
							<< "\t<meta name=\""
							<< e.first
							<< "\" content=\""
							<< cyy::to_string(e.second)
							<< "\" />"
							<< std::endl
							;
					}
				}

				os
					<< "</head>"
					<< std::endl
					;

			}

			//
			//	generate body
			//
			os
				<< "<body>"
				<< std::endl
				;
			std::for_each(begin, end, [this, &os](cyy::object const& obj) {
				os << cyy::to_string(obj);
				//os << this->backpatch(cyy::to_string(obj));
			});
			os
				<< "</body>"
				<< std::endl
				;

			if (!body_only_)
			{
				//
				//	generate complete HTML file
				//	
				os
					<< "</html>"
					<< std::endl
					;
			}

		}

		boost::filesystem::path generator::resolve_path(std::string const& s) const
		{
			boost::filesystem::path p(s);
			for (auto dir : includes_)
			{
				if (boost::filesystem::exists(dir / p))
				{
					return boost::filesystem::path(dir / p);
				}
			}
			return boost::filesystem::path();
		}

		boost::system::error_code generator::run(cyy::vector_t const& prg)
		{
			if (verbosity_ > 2)
			{
				std::cout
					<< "***info: start generator with "
					<< prg.size()
					<< " op codes"
					<< std::endl;
			}
			return vm_.run(cyy::vector_t(prg));
		}

		std::size_t generator::meta(boost::filesystem::path const& out) const
		{
			if (verbosity_ > 1)
			{
				std::cout
					<< "***info: generate meta file "
					<< out
					<< " with "
					<< meta_.size()
					<< " entries"
					<< std::endl;
			}

			std::ofstream file(out.string(), std::ios::out | std::ios::trunc);
			if (!file.is_open())
			{
				std::cerr
					<< "***error cannot open file "
					<< out
					<< std::endl;
			}
			else
			{
				if (verbosity_ > 3)
				{
					cyy::serialize_json_pretty(std::cout, cyy::factory(meta_), cyy::io::custom_callback());
				}
				cyy::serialize_json(file, meta_, cyy::io::custom_callback());
			}
			return meta_.size();
		}

		std::string generator::generate_header(std::size_t level, std::string const& txt, boost::uuids::uuid tag)
		{
			if (level == 0)
			{
				std::cerr
					<< "***error: header indentation level is zero ("
					<< txt
					<< ")"
					<< std::endl;
				return "error";
			}

			//
			//	update numeration
			//
			std::ptrdiff_t diff = level - numeration_.size();
			if (diff == 1)
			{
				//
				//	increase indention
				//
				numeration_.push_back(1);
			}
			else if (diff == 0)
			{
				//
				//	indention unchanged
				//
				numeration_.at(level - 1)++;
			}
			else if (diff == -1)
			{
				//
				//	decrease indention
				//
				numeration_.at(level - 1)++;
				numeration_.pop_back();
			}
			else
			{
				std::cerr
					<< "***error: header with wrong indentation level: "
					<< diff
					<< " - "
					<< txt
					<< std::endl;
			}


			//
			//	update structure
			//
			auto pos = structure_.emplace(std::piecewise_construct,
				std::forward_as_tuple(tag),
				std::forward_as_tuple(element::HEADER, txt, numeration_));

			if (pos.second)
			{
				std::stringstream ss;
				ss
					<< std::endl
					<< "<h"
					<< level
					<< " class=\"header."
					<< level
					<< "\" id=\""
					<< tag
					<< "\">"
					<< pos.first->second.to_str()
					<< "</h"
					<< level
					<< ">"
					;
				const std::string node = ss.str();
				if (verbosity_ > 1)
				{
					std::cout
						<< "***info: header("
						<< pos.first->second.to_str()
						//<< node
						<< ")"
						<< std::endl;
				}
				return node;

			}

			std::cerr
				<< "***error: cannot insert new header "
				<< txt
				<< std::endl;
			return "error";


		}

		//
		//	free functions
		//
		std::string accumulate(cyy::vector_reader const& reader, std::size_t start, std::size_t end)
		{
			//BOOST_ASSERT_MSG(start < reader.size(), "out of range");
			std::string str;
			for (std::size_t idx = start; idx > end; idx--)
			{
				const auto s = reader.get_string(idx);
				if ((idx > 1) && (idx != start) && !(s == "." || s == "," || s == ":"))
				{
					str += " ";
				}
				str += s;
			}
			return str;
		}

		std::string get_extension(boost::filesystem::path const& p)
		{
			if (p.empty())
			{
				return "";
			}
			std::string s = p.extension().string();
			return s.substr(1, s.size() - 1);
		}

		//
		//	element
		//
		element::element(type t, std::string const& s, std::vector<std::size_t> const& chapter)
			: type_(t)
			, text_(s)
			, chapter_(chapter)
		{}

		std::string element::level() const
		{
			std::stringstream ss;

			//
			// handle first element
			//
			auto pos = chapter_.begin();
			if (pos != chapter_.end())
			{
				ss << *pos;
				++pos;
			}

			//
			// join all other elements
			//
			std::for_each(pos, chapter_.end(), [&ss](std::size_t n) {
				ss << '.' << n;
			});

			return ss.str();
		}

		std::string element::to_str() const
		{
			return level() + "&nbsp;" + text_;
		}

		std::size_t element::depth() const
		{
			return chapter_.size();
		}

		//element& element::operator=(element const& other)
		//{
		//	if (this != &other)
		//	{
		//		//const type type_;
		//		//const std::string text_;
		//		//const std::vector<std::size_t>	chapter_;

		//	}
		//	return *this;
		//}

		//	comparison
		bool operator==(element const& lhs, element const& rhs)
		{
			return (lhs.type_ == rhs.type_)
				? (lhs.chapter_ == rhs.chapter_)
				: false
				;
		}
		bool operator<(element const& lhs, element const& rhs)
		{
			return (lhs.type_ == rhs.type_)
				? (lhs.chapter_ < rhs.chapter_)
				: (lhs.type_ < rhs.type_)
				;
		}
		bool operator!=(element const& lhs, element const& rhs)
		{
			return !(lhs == rhs);
		}
		bool operator>(element const& lhs, element const& rhs)
		{
			//	note the reversed notation
			return rhs < lhs;
		}
		bool operator<=(element const& lhs, element const& rhs)
		{
			return !(lhs > rhs);
		}
		bool operator>=(element const& lhs, element const& rhs)
		{
			return !(lhs < rhs);
		}


		//	comparison
		bool operator==(element_t const& lhs, element_t const& rhs)
		{
			return lhs.second == rhs.second;
		}
		bool operator<(element_t const& lhs, element_t const& rhs)
		{
			return lhs.second < rhs.second;
		}
		bool operator!=(element_t const& lhs, element_t const& rhs)
		{
			return !(lhs == rhs);
		}
		bool operator>(element_t const& lhs, element_t const& rhs)
		{
			//	note the reversed notation
			return rhs < lhs;
		}
		bool operator<=(element_t const& lhs, element_t const& rhs)
		{
			return !(lhs > rhs);
		}
		bool operator>=(element_t const& lhs, element_t const& rhs)
		{
			return !(lhs < rhs);
		}

	}	//	docscript
}	//	cyx


