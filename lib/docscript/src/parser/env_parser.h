/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_ENV_PARSER_H
#define CYX_DOCSCRIPT_ENV_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/docscript/parser.h>
#include <map>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx	
{
	namespace docscript
	{
		/**
		 *	environment parser.
		 */
		template <typename Iterator, typename Skipper = boost::spirit::qi::standard_wide::space_type>
		struct environment_parser : boost::spirit::qi::grammar<Iterator, std::map<u32_string, u32_string>(), Skipper>
		{
			typedef std::map<u32_string, u32_string>	map_type;

			environment_parser();
			boost::spirit::qi::rule<Iterator, map_type(), Skipper> r_start, r_list;
			pair_parser<Iterator> r_pair;
		};

		/**
		 *	call environment parser.
		 */
		bool parse_environment(std::map<u32_string, u32_string>&, std::string const&);

		template <typename Iterator, typename Skipper = boost::spirit::qi::standard_wide::space_type>
		struct boundary_parser
			: boost::spirit::qi::grammar<Iterator, u32_string(), Skipper>
		{
			boundary_parser();
			boost::spirit::qi::rule<Iterator, u32_string(), Skipper>		r_start, r_boundary;
			quote_parser<Iterator>  r_quote;
			static const u32_string epsilon_;
		};

		/**
		 *	call boundary parser.
		 */
		bool parse_boundary(u32_string&, std::string const&);
	}
}	//	cyx

#endif	//	CYX_DOCSCRIPT_ENV_PARSER_H


