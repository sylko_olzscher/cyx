/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "env_parser.hpp"

namespace cyx
{
	namespace docscript
	{
		template < typename T, typename S >
		const u32_string boundary_parser<T, S>::epsilon_ = parse_utf8("EPSILON");

		bool parse_environment(std::map<u32_string, u32_string>& result, std::string const& input)
		{
			//	prepare input
			auto first(begin(input)), last(end(input));

			typedef boost::u8_to_u32_iterator<decltype(first)> Conv2Utf32;
			Conv2Utf32 f(first), saved = f, l(last);

			//
			//	grammar
			//
			static const environment_parser<Conv2Utf32, boost::spirit::qi::standard_wide::space_type> p;

			//
			//  call parser
			//
			if (!boost::spirit::qi::phrase_parse(f, l, p, boost::spirit::qi::standard_wide::space, result))
			{
				std::cerr << "whoops at position #" << std::distance(saved, f) << "\n";
				return false;
			}
			return true;
		}

		bool parse_boundary(u32_string& result, std::string const& input)
		{
			//	prepare input
			auto first(begin(input)), last(end(input));

			typedef boost::u8_to_u32_iterator<decltype(first)> Conv2Utf32;
			Conv2Utf32 f(first), saved = f, l(last);

			//
			//	grammar
			//
			static const boundary_parser<Conv2Utf32, boost::spirit::qi::standard_wide::space_type> p;

			//
			//  call parser
			//
			if (!boost::spirit::qi::phrase_parse(f, l, p, boost::spirit::qi::standard_wide::space, result))
			{
				std::cerr << "whoops at position #" << std::distance(saved, f) << "\n";
				return false;
			}
			return true;
		}
	}
}	//	cyx
