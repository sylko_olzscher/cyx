/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_ENV_PARSER_HPP
#define CYX_DOCSCRIPT_ENV_PARSER_HPP


#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "env_parser.h"
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/std_pair.hpp>

namespace cyx
{
	namespace docscript
	{
		template <typename Iterator, typename Skipper>
		environment_parser< Iterator, Skipper > ::environment_parser()
			: environment_parser::base_type(r_start)
		{
			r_start
				= (L'(' >> r_list >> L')')[boost::spirit::_val = boost::spirit::_1]
				;

			r_list
				%= r_pair % L','
				;
		};


		template <typename Iterator, typename Skipper>
		boundary_parser < Iterator, Skipper > ::boundary_parser()
			: boundary_parser::base_type(r_start)
		{
			r_start
				= (L'-' >> r_boundary)[boost::spirit::_val = boost::spirit::_1]
				;

			r_boundary
				= (L'(' >> r_quote >> L')')[boost::spirit::_val = boost::spirit::_1]
				| boost::spirit::eps[boost::spirit::_val = boost::phoenix::construct<u32_string>(epsilon_)]
				;

		}

	}
}	//	cyx

#endif	//	CYX_DOCSCRIPT_ENV_PARSER_HPP
