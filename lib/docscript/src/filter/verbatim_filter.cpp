/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/docscript/filter/verbatim_filter.h>
//#include <cyx/docscript/compiler.h>
//#include <cyy/vm/code_generator.h>
//#include <cyy/intrinsics/factory/string_factory.h>
//#include <cyy/intrinsics/factory/numeric_factory.h>
//#include <cyy/intrinsics/factory/boost_factory.h>
//#include <cyy/intrinsics/factory/set_factory.h>
//#include <cyy/intrinsics/factory/op_factory.h>
//#include <cyy/intrinsics/factory/std_factory.h>
//#include <cyy/intrinsics/factory/boolean_factory.h>
//#include <iostream>
//#include <string>
//#include <iomanip>
//#include <boost/assert.hpp>
//#include <boost/regex/pending/unicode_iterator.hpp>
//#include <boost/algorithm/string.hpp>
#include <boost/regex/pending/unicode_iterator.hpp>

namespace cyx	
{
	namespace filter
	{
		verbatim::verbatim()
			: result_()
		{}

		std::string verbatim::get_result()
		{
			return std::string(boost::u32_to_u8_iterator<u32_string::const_iterator>(result_.begin()), boost::u32_to_u8_iterator<u32_string::const_iterator>(result_.end()));
		}

		void verbatim::put(std::uint32_t c)
		{
			switch (c)
			{
			case '<':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('0');
				result_.push_back(';');
				break;
			case '>':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('2');
				result_.push_back(';');
				break;
			case '&':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('3');
				result_.push_back('8');
				result_.push_back(';');
				break;
			case '"':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('3');
				result_.push_back('4');
				result_.push_back(';');
				break;
			default:
				result_.push_back(c);
				break;
			}
		}
	}
}	//	cyx


