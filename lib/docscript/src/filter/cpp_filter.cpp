/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyx/docscript/filter/cpp_filter.h>
#include <cyx/docscript/parser.h>
#include <boost/regex/pending/unicode_iterator.hpp>
#include <cctype>
#include <iomanip>

namespace cyx	
{
	namespace filter
	{
		cpp::cpp(int verbose, bool linenumbers)
			: verbose_(verbose)
			, result_()
			, linenumbers_(linenumbers)
			, state_(INITIAL_)
			, parenthesis_(0)
			, braces_(0)
			, pos_(0)
			, keywords_()
			, line_(1)
		{
			//
			//	initialize keyword list
			//
			keywords_.emplace(docscript::parse_utf8("auto"));
			keywords_.emplace(docscript::parse_utf8("bool"));
			keywords_.emplace(docscript::parse_utf8("break"));
			keywords_.emplace(docscript::parse_utf8("case"));
			keywords_.emplace(docscript::parse_utf8("catch"));
			keywords_.emplace(docscript::parse_utf8("char"));
			keywords_.emplace(docscript::parse_utf8("class"));
			keywords_.emplace(docscript::parse_utf8("const"));
			keywords_.emplace(docscript::parse_utf8("const_cast"));
			keywords_.emplace(docscript::parse_utf8("continue"));
			keywords_.emplace(docscript::parse_utf8("decltype"));
			keywords_.emplace(docscript::parse_utf8("default"));
			keywords_.emplace(docscript::parse_utf8("delete"));
			keywords_.emplace(docscript::parse_utf8("do"));
			keywords_.emplace(docscript::parse_utf8("double"));
			keywords_.emplace(docscript::parse_utf8("dynamic_cast"));
			keywords_.emplace(docscript::parse_utf8("else"));
			keywords_.emplace(docscript::parse_utf8("enum"));
			keywords_.emplace(docscript::parse_utf8("explicit"));
			keywords_.emplace(docscript::parse_utf8("extern"));
			keywords_.emplace(docscript::parse_utf8("false"));
			keywords_.emplace(docscript::parse_utf8("float"));
			keywords_.emplace(docscript::parse_utf8("for"));
			keywords_.emplace(docscript::parse_utf8("friend"));
			keywords_.emplace(docscript::parse_utf8("goto"));
			keywords_.emplace(docscript::parse_utf8("if"));
			keywords_.emplace(docscript::parse_utf8("inline"));
			keywords_.emplace(docscript::parse_utf8("int"));
			keywords_.emplace(docscript::parse_utf8("long"));
			keywords_.emplace(docscript::parse_utf8("mutable"));
			keywords_.emplace(docscript::parse_utf8("namespace"));
			keywords_.emplace(docscript::parse_utf8("new"));
			keywords_.emplace(docscript::parse_utf8("nullptr"));
			keywords_.emplace(docscript::parse_utf8("operator"));
			keywords_.emplace(docscript::parse_utf8("private"));
			keywords_.emplace(docscript::parse_utf8("protected"));
			keywords_.emplace(docscript::parse_utf8("public"));
			keywords_.emplace(docscript::parse_utf8("register"));
			keywords_.emplace(docscript::parse_utf8("reinterpret_cast"));
			keywords_.emplace(docscript::parse_utf8("return"));
			keywords_.emplace(docscript::parse_utf8("short"));
			keywords_.emplace(docscript::parse_utf8("signed"));
			keywords_.emplace(docscript::parse_utf8("sizeof"));
			keywords_.emplace(docscript::parse_utf8("static"));
			keywords_.emplace(docscript::parse_utf8("static_assert"));
			keywords_.emplace(docscript::parse_utf8("switch"));
			keywords_.emplace(docscript::parse_utf8("template"));
			keywords_.emplace(docscript::parse_utf8("this"));
			keywords_.emplace(docscript::parse_utf8("throw"));
			keywords_.emplace(docscript::parse_utf8("true"));
			keywords_.emplace(docscript::parse_utf8("try"));
			keywords_.emplace(docscript::parse_utf8("typedef"));
			keywords_.emplace(docscript::parse_utf8("typeid"));
			keywords_.emplace(docscript::parse_utf8("typename"));
			keywords_.emplace(docscript::parse_utf8("union"));
			keywords_.emplace(docscript::parse_utf8("unsigned"));
			keywords_.emplace(docscript::parse_utf8("using"));
			keywords_.emplace(docscript::parse_utf8("virtual"));
			keywords_.emplace(docscript::parse_utf8("void"));
			keywords_.emplace(docscript::parse_utf8("volatile"));
			keywords_.emplace(docscript::parse_utf8("while"));

			if (verbose_ > 3)
			{
				std::cout
					<< "***info: C++ keyword list initialized with "
					<< keywords_.size()
					<< " entries"
					<< std::endl;
			}

		}

		void cpp::put(std::uint32_t c)
		{
			if ((line_ == 1) && (c != '\n'))
			{
				nl();
			}
			switch (state_)
			{
			case QUOTE_:
				state_ = quote_state(c);
				break;
			case CHAR_:
				state_ = char_state(c);
				break;
			case SLASH_:
				state_ = slash_state(c);
				break;
			case COMMENT_:
				state_ = comment_state(c);
				break;
			case LITERAL_:
				state_ = literal_state(c);
				break;
			case NUMBER_:
				state_ = number_state(c);
				break;
			case PRE_:
				state_ = pre_state(c);
				break;
			case INCLUDE_:
				state_ = include_state(c);
				break;
			case INITIAL_:
			default:
				state_ = initial_state(c);
				break;
			}
		}

		std::string cpp::get_result()
		{
			if (verbose_ > 0)
			{
				if (parenthesis_ != 0)
				{
					std::cerr
						<< "***warning: C++ parentheses count not balanced "
						<< parenthesis_
						<< std::endl;
				}
				if (braces_ != 0)
				{
					std::cerr
						<< "***warning: C++ curly brackets count not balanced "
						<< braces_
						<< std::endl;
				}
			}

			return std::string(boost::u32_to_u8_iterator<u32_string::const_iterator>(result_.begin()), boost::u32_to_u8_iterator<u32_string::const_iterator>(result_.end()));
		}

		cpp::state cpp::initial_state(std::uint32_t c)
		{
			switch (c)
			{
			case '{':
				//result_.push_back('\n');
				nl();
				repeat(' ', braces_ * 2);
				braces_++;
				result_.push_back('{');
				break;
			case '}':
				//result_.push_back('\n');
				nl();
				braces_--;
				repeat(' ', braces_ * 2);
				result_.push_back('}');
				break;
			case '(':
				parenthesis_++;
				//result_.push_back(' ');
				result_.push_back('(');
				break;
			case ')':
				parenthesis_--;
				result_.push_back(')');
				break;
			case '<':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('0');
				result_.push_back(';');
				//result_.push_back(' ');
				break;
			case '>':
				//result_.push_back(' ');
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('2');
				result_.push_back(';');
				break;
			case '&':
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('3');
				result_.push_back('8');
				result_.push_back(';');
				break;
			case '"':
				//result_.push_back(c);
				result_.push_back('&');
				result_.push_back('q');
				result_.push_back('u');
				result_.push_back('o');
				result_.push_back('t');
				result_.push_back(';');
				result_.insert(result_.end(), color_green_.begin(), color_green_.end());
				return QUOTE_;
			case '\'':
				result_.push_back(c);
				result_.insert(result_.end(), color_brown_.begin(), color_brown_.end());
				return CHAR_;
			case '/':
				return SLASH_;

			case '\t':
				repeat(' ', 2);
				break;

			case '#':
				result_.push_back(c);
				pos_ = result_.size();
				return PRE_;

			case '\n':
				nl();
				break;

			default:
				if (std::isalpha(c))
				{
					pos_ = result_.size();
					result_.push_back(c);
					return LITERAL_;
				}
				else if (std::isdigit(c))
				{
					pos_ = result_.size();
					result_.push_back(c);
					return NUMBER_;
				}
				else
				{
					result_.push_back(c);
				}
				break;
			}
			return INITIAL_;
		}

		cpp::state cpp::literal_state(std::uint32_t c)
		{
			switch (c)
			{
			case ')':
				parenthesis_--;
				stop_keyword();
				result_.push_back(c);
				return INITIAL_;
			case '(':
				parenthesis_++;
				stop_keyword();
				result_.push_back(c);
				return INITIAL_;
			case '{':
				braces_++;
				stop_keyword();
				result_.push_back(c);
				return INITIAL_;
			case '}':
				braces_--;
				stop_keyword();
				result_.push_back(c);
				return INITIAL_;
			case ' ':
			case ':':
			case ';':
			case '&':
			case '+':
			case '-':
			case '*':
			case '/':
				stop_keyword();
				result_.push_back(c);
				return INITIAL_;

			case '\n':
				stop_keyword();
				nl();
				return INITIAL_;

			case '<':
				if (is_keyword())
				{
					result_.insert(pos_, color_blue_);
					result_.insert(result_.end(), end_.begin(), end_.end());
				}
				result_.push_back(c);
				result_.push_back(' ');
				return INITIAL_;
			case '>':
				if (is_keyword())
				{
					result_.insert(pos_, color_blue_);
					result_.insert(result_.end(), end_.begin(), end_.end());
				}
				result_.push_back(' ');
				result_.push_back(c);
				return INITIAL_;
			case '\t':
				repeat(' ', 2);
				return INITIAL_;
			default:
				result_.push_back(c);
				break;
			}
			return LITERAL_;
		}

		bool cpp::stop_keyword()
		{
			if (is_keyword())
			{
				result_.insert(pos_, color_blue_);
				result_.insert(result_.end(), end_.begin(), end_.end());
				return true;
			}
			return false;
		}

		cpp::state cpp::number_state(std::uint32_t c)
		{
			if (std::isdigit(c) || (c == '.'))
			{
				result_.push_back(c);
				return NUMBER_;
			}

			switch (c)
			{
			case '{':
				braces_++;
				break;
			case '}':
				braces_--;
				break;
			case '(':
				parenthesis_++;
				break;
			case ')':
				parenthesis_--;
				break;
			}

			//
			//	brown numbers
			//
			result_.insert(pos_, color_brown_);
			result_.insert(result_.end(), end_.begin(), end_.end());

			result_.push_back(c);
			return INITIAL_;
		}

		cpp::state cpp::quote_state(std::uint32_t c)
		{
			switch (c)
			{
			case '"':
				result_.insert(result_.end(), end_.begin(), end_.end());
				result_.push_back('&');
				result_.push_back('q');
				result_.push_back('u');
				result_.push_back('o');
				result_.push_back('t');
				result_.push_back(';');
				return INITIAL_;

			default:
				result_.push_back(c);
				break;
			}
			return QUOTE_;
		}

		cpp::state cpp::char_state(std::uint32_t c)
		{
			switch (c)
			{
			case '\'':
				result_.insert(result_.end(), end_.begin(), end_.end());
				result_.push_back(c);
				return INITIAL_;

			default:
				result_.push_back(c);
				break;
			}
			return CHAR_;
		}

		cpp::state cpp::slash_state(std::uint32_t c)
		{
			if (c != '/')
			{
				result_.push_back('/');
				result_.push_back(c);
				return INITIAL_;
			}
			pos_ = result_.size();
			result_.push_back(c);
			result_.push_back(c);
			return COMMENT_;
		}

		cpp::state cpp::comment_state(std::uint32_t c)
		{
			if (c == '\n')
			{
				result_.insert(pos_, color_grey_);
				result_.insert(result_.end(), end_.begin(), end_.end());
				//result_.push_back(c);
				nl();
				return INITIAL_;
			}
			result_.push_back(c);
			return COMMENT_;
		}

		cpp::state cpp::pre_state(std::uint32_t c)
		{
			if (std::isalpha(c))
			{
				result_.push_back(c);
				return PRE_;
			}
			else if (c == ' ' || c == '\t')
			{
				//	skip spaces
				return PRE_;
			}
			else if (c == '<')
			{
				result_.insert(pos_, color_red_);
				result_.insert(result_.end(), end_.begin(), end_.end());

				result_.push_back(' ');
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('0');
				result_.push_back(';');
				pos_ = result_.size();
				return INCLUDE_;
			}
			else if (c == '"')
			{
				result_.insert(pos_, color_red_);
				result_.insert(result_.end(), end_.begin(), end_.end());

				result_.push_back(' ');
				result_.push_back(c);
				pos_ = result_.size();
				return INCLUDE_;
			}

			//
			//	other
			//
			result_.insert(pos_, color_red_);
			result_.insert(result_.end(), end_.begin(), end_.end());
			result_.push_back(' ');
			result_.push_back(c);
			return INITIAL_;
		}

		cpp::state cpp::include_state(std::uint32_t c)
		{
			if (c == '>')
			{
				result_.insert(pos_, color_blue_);
				result_.insert(result_.end(), end_.begin(), end_.end());
				result_.push_back('&');
				result_.push_back('#');
				result_.push_back('6');
				result_.push_back('2');
				result_.push_back(';');
				return INITIAL_;
			}
			else if (c == '"')
			{
				result_.insert(pos_, color_blue_);
				result_.insert(result_.end(), end_.begin(), end_.end());
				result_.push_back(c);
				return INITIAL_;
			}
			else if (c == '\n')
			{
				nl();
				return INITIAL_;
			}
			result_.push_back(c);
			return INCLUDE_;
		}

		void cpp::repeat(std::uint32_t c, std::size_t count)
		{
			while (count-- != 0)
			{
				result_.push_back(c);
			}
		}

		bool cpp::is_keyword() const
		{
			if (pos_ < result_.size())
			{
				if (verbose_ > 4)
				{
					std::cout
						<< "***info: C++ lookup "
						<< std::string(&result_[pos_], &result_[result_.size()])
						<< std::endl;
				}

				return keywords_.find(result_.substr(pos_)) != keywords_.end();
			}
			return false;
		}

		void cpp::nl()
		{
			result_.push_back('\n');

			//
			//	write line number
			//
			if (linenumbers_)
			{
				result_.insert(result_.end(), color_cyan_.begin(), color_cyan_.end());

				std::stringstream ss;
				ss
					<< std::setw(4)
					<< std::setfill('0')
					<< line_
					;
				auto nr = docscript::parse_utf8(ss.str());
				result_.insert(result_.end(), nr.begin(), nr.end());
				result_.insert(result_.end(), end_.begin(), end_.end());
				result_.push_back(' ');
			}

			line_++;
		}

		//
		//	constants
		//
		const u32_string cpp::color_green_ = docscript::parse_utf8("<code style=\"color: green;\">");
		const u32_string cpp::color_blue_ = docscript::parse_utf8("<code style=\"color: blue;\">");
		const u32_string cpp::color_grey_ = docscript::parse_utf8("<code style=\"color: grey;\">");
		const u32_string cpp::color_red_ = docscript::parse_utf8("<code style=\"color: red;\">");
		const u32_string cpp::color_cyan_ = docscript::parse_utf8("<code style=\"color: DarkCyan; font-size: smaller;\">");
		const u32_string cpp::color_brown_ = docscript::parse_utf8("<code style=\"color: brown;\">");
		const u32_string cpp::end_ = docscript::parse_utf8("</code>");
	}
}	//	cyx


