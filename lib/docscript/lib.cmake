# top level files
set (docscript_lib)

set (docscript_lib_cpp

	lib/docscript/src/token.cpp
	lib/docscript/src/tokenizer.cpp
	lib/docscript/src/symbol.cpp
	lib/docscript/src/lexer.cpp
	lib/docscript/src/compiler.cpp
	lib/docscript/src/statistics.cpp
	lib/docscript/src/library.cpp
	lib/docscript/src/generator.cpp
)

set (docscript_lib_h

	src/main/include/cyx/docscript/docscript.h	
	src/main/include/cyx/docscript/token.h
	src/main/include/cyx/docscript/tokenizer.h
	src/main/include/cyx/docscript/symbol.h
	src/main/include/cyx/docscript/lexer.h
	src/main/include/cyx/docscript/compiler.h
	src/main/include/cyx/docscript/library.h
	src/main/include/cyx/docscript/generator.h
	src/main/include/cyx/docscript/statistics.h
)

set (docscript_lib_parser
#
	lib/docscript/src/parser.hpp
	lib/docscript/src/parser.cpp
	src/main/include/cyx/docscript/parser.h
#	lib/docscript/src/parser/env_parser.h
#	lib/docscript/src/parser/env_parser.hpp
#	lib/docscript/src/parser/env_parser.cpp
)

set (docscript_lib_doc

	lib/docscript/src/doc/intro.docscript
	lib/docscript/src/doc/snippet.docscript
	lib/docscript/src/doc/expression-templates.docscript
)

set (docscript_lib_filter

	lib/docscript/src/filter/cpp_filter.cpp
	lib/docscript/src/filter/verbatim_filter.cpp
	src/main/include/cyx/docscript/filter/cpp_filter.h
	src/main/include/cyx/docscript/filter/verbatim_filter.h
)

source_group(parser FILES ${docscript_lib_parser})
source_group(doc FILES ${docscript_lib_doc})
source_group(filter FILES ${docscript_lib_filter})


# define the master program
set (docscript_lib
  ${docscript_lib_cpp}
  ${docscript_lib_h}
  ${docscript_lib_parser}
  ${docscript_lib_filter}
  ${docscript_lib_doc}
)
