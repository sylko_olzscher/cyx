/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_GENERATOR_H
#define CYX_DOCSCRIPT_GENERATOR_H

#include <cyy/vm/vm_dispatcher.h>
#include <cyy/scheduler/thread/scheduler.h>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/name_generator.hpp>

namespace cyy
{
	class vector_reader;
}

namespace cyx
{
	/**
	 * helper to store structure information
	 */
	struct element
	{
		enum type {
			HEADER,
			FIGURE,
			TABLE,
			LISTING,
		} ;
		element(type, std::string const&, std::vector<std::size_t> const& chapter);
		std::string level() const;
		std::string to_str() const;
		std::size_t depth() const;

		//	assignment
		//element& operator=(element const&);

		const type type_;
		const std::string text_;
		const std::vector<std::size_t>	chapter_;
	};

	//	comparison
	/*
	 * Structure elements are first sorted by type and than lexicographically 
	 * by the chapter vector
	 */
	bool operator==(element const&, element const&);
	bool operator<(element const&, element const&);
	bool operator!=(element const&, element const&);
	bool operator>(element const&, element const&);
	bool operator<=(element const&, element const&);
	bool operator>=(element const&, element const&);

	using structure_t = std::map<boost::uuids::uuid, element>;
	using element_t = typename structure_t::value_type;

	/**
	 * generates the creation program
	 */
	class generator
	{

	public:
		generator(std::vector< std::string > const& inc, int verbose);
		virtual ~generator();

		/**
		 * produce the document
		 */
		boost::system::error_code run(cyy::vector_t const&);

		/**
		 * generate an index file (JSON)
		 */
		std::size_t index(boost::filesystem::path const& out) const;

		/**
		* generate a meta file (JSON)
		*/
		std::size_t meta(boost::filesystem::path const& out) const;

	private:
		void register_this();
		std::string accumulate(std::size_t, cyy::vector_reader const&) const;
		std::string generate_header(std::size_t level, std::string const& , boost::uuids::uuid tag);
		std::string generate_comment(std::string const&);
		std::string backpatch(std::string&&);
		void serialize(std::ofstream&, cyy::vector_t::const_iterator, cyy::vector_t::const_iterator);
		boost::filesystem::path resolve_path(std::string const& p) const;

		static std::string to_string(element_t const&);
		static std::string get_extension(boost::filesystem::path const&);

	private:
		const int verbose_;
		const std::vector< std::string >& includes_;
		cyy::thread::scheduler	scheduler_;
		cyy::vm_dispatcher vm_;
		boost::uuids::random_generator	uuid_gen_;	//	basic_random_generator<mt19937>
		boost::uuids::name_generator name_gen_;
		std::string header_, body_;
		std::size_t content_depth_;
		structure_t	structure_;
		std::set<boost::uuids::uuid>	backpatches_;
		std::vector<std::size_t> numeration_;
		cyy::param_map_t meta_;
	};

	//	comparison
	/*
	* Structure elements are first sorted by type and than lexicographically
	* by the chapter vector
	*/
	//using structure_element = std::pair<boost::uuids::uuid, element>;
	bool operator==(element_t const&, element_t const&);
	bool operator<(element_t const&, element_t const&);
	bool operator!=(element_t const&, element_t const&);
	bool operator>(element_t const&, element_t const&);
	bool operator<=(element_t const&, element_t const&);
	bool operator>=(element_t const&, element_t const&);


}

#endif	//	CYX_DOCSCRIPT_GENERATOR_H
