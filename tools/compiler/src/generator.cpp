/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#include "generator.h"
#include <cyy/io/io_set.h>
//#include <cyy/io/native_io.h>	//!
#include <cyy/intrinsics/factory/chrono_factory.h>
#include <cyy/intrinsics/factory/op_factory.h>
#include <cyy/intrinsics/factory/string_factory.h>
#include <cyy/intrinsics/factory/boost_factory.h>
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <cyy/vm/code_generator.h>
#include <cyy/util/read_sets.h>
#include <cyy/crypto/base64.hpp>
#include <cyy/json/json_io.h>
#include <iostream>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace cyx
{
	generator::generator(std::vector< std::string > const& incs, int verbose)
	: verbose_(verbose)
		, includes_(incs)
		, scheduler_()
		, vm_(scheduler_.get_io_service())
		, uuid_gen_()
		, name_gen_({ 0x34, 0x49, 0x58, 0x5e, 0x83, 0x7d, 0x4b, 0x9a, 0x81, 0x6e, 0x90, 0x4f, 0x94, 0x70, 0x9b, 0x02 })
		, header_()
		, body_()
		, content_depth_(3)
		, structure_()
		, backpatches_()
		, numeration_()
		, meta_()
	{
		register_this();
	}
	
	generator::~generator()
	{
		scheduler_.stop();
	}

	void generator::register_this()
	{
		vm_.async_run(cyy::register_function("now", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(true);
#ifdef _DEBUG
			//std::cout
			//	<< "\n***info: now("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;
#endif
			ctx.set_return_value(cyy::time_point_factory(), 0);
		}));

		vm_.async_run(cyy::register_function("meta", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[3idx,"global",%(("author":"Sylko Olzscher"),("released":"2017.06.18 12:25:47.60177980"ts),("title":"Introduction into docScript"))]
			//	[3idx,"global",%(("file-size":2968u64),("last-write-time":"2017.06.15 08:20:37.00000000"ts))]
			std::cout
				<< "\n***info: meta("
				<< cyy::io::to_literal(frame)
				<< ")"
				<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			cyy::param_map_t meta;
            //  problems with compilers before gcc 7
            // 			meta = reader.get(2, meta);
// 			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (meta)");

			//
			//	update meta data 
			//
// #if (__cplusplus > 201402L)
// 			meta_.merge(meta); //	C++17 required
// #else
// 			for (auto e : meta)
// 			{
// 				meta_.insert(e);
// 			}
// #endif

			//
			// function meta has no return values
			//
		}));

		vm_.async_run(cyy::register_function("paragraph", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//std::cout
			//	<< "\n***info: paragraph("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size < frame.size(), "internal error (paragraph)");

			std::stringstream ss;
			ss 
				<< std::endl
				<< "<p>"
				;

			std::size_t column{ 0 };
			std::for_each(frame.rbegin(), --frame.rend(), [&ss, &column](cyy::object const& obj) {

				const std::string s = cyy::to_string(obj);

				if (column != 0 && !(s == "," || s == ":"))
				{
					ss << ' ';
				}
				ss << s;
				
				column += s.size();
				if (column > 72)
				{
					ss << std::endl;
					column = 0;
				}

			});

			ss << "</p>";

			const std::string node = ss.str();
			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("title", 1, [this](cyy::context& ctx) {

			//	[1idx,true,"Introduction into docScript"]
			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//std::cout
			//	<< "\n***info: title("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (title)");

			std::string title;
			for (std::size_t idx = size + 1; idx > 1; idx--)
			{
				title += reader.get_string(idx);
				if (idx > 2)
				{
					title += " ";
				}
			}

			if (verbose_ > 1)
			{
				std::cout
					<< "***info: title("
					<< title
					<< ")"
					<< std::endl;
			}

			meta_.emplace("title", cyy::factory(title));

			//
			// function meta has no return values
			//
		}));

		vm_.async_run(cyy::register_function("debug", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//std::cout
			//	<< "\n***info: debug("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const std::string v = cyy::string_cast(frame.at(2));
			const cyy::codes::op_code op = boost::algorithm::iequals(v, "on")
				? cyy::codes::DBG_ON
				: cyy::codes::DBG_OFF
				;
			if (verbose_ > 1)
			{
				std::cout
					<< "***info: debug("
					<< std::boolalpha
					<< boost::algorithm::iequals(v, "on")
					<< ")"
					<< std::endl;
			}


			ctx.squeeze(cyy::vector_t{cyy::factory(op)});
			//
			// function meta has no return values
			//
			//ctx.set_return_value(cyy::factory(generate_comment("DEBUG")), 0);

		}));

		vm_.async_run(cyy::register_function("contents", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[1idx,true,("depth":3idx)]
			//std::cout
			//	<< "\n***info: contents("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			content_depth_ = reader[2].get_index("depth", content_depth_);

			if (verbose_ > 1)
			{
				std::cout
					<< "***info: content.depth = "
					<< content_depth_
					<< std::endl;
			}

			//
			// function meta has no return values
			//
			//ctx.set_return_value(cyy::factory(generate_comment("CONTENTS")), 0);
			
		}));

		vm_.async_run(cyy::register_function("header", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[3idx,true,%(("level":1idx),("title":"Basics"),("uuid":"79bf3ba0-2362-4ea5-bcb5-ed93844ac59a"))]
			//std::cout
			//	<< "\n***info: header("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::string txt = reader[2].get_string("title");
			const std::size_t level = reader[2].get_index("level", 0);
			const boost::uuids::uuid tag = name_gen_(reader[2].get_string("tag"));

			const std::string node = generate_header(level, txt, tag);
			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("header.01", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,true,"Considerations","Final"]
			//std::cout
			//	<< "\n***info: header.01("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
			{
				const std::string txt = reader[2].get_string("title");
				const boost::uuids::uuid tag = name_gen_(reader[2].get_string("tag"));

				const std::string node = generate_header(1, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);

			}
			else
			{

				BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (header.01)");
				const boost::uuids::uuid tag = uuid_gen_();
				const std::string txt = accumulate(size + 1, reader);

				const std::string node = generate_header(1, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);
			}

		}));

		vm_.async_run(cyy::register_function("header.02", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,true,"Considerations","Final"]
			//	[2idx,true,%(("tag":"b3fc0bdc-5d95-45e9-b66c-f0c541f1592c"),("title":"Basic Rules"))]
			//std::cout
			//	<< "\n***info: header.02("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
			{
				const std::string txt = reader[2].get_string("title");
				const boost::uuids::uuid tag = name_gen_(reader[2].get_string("tag"));

				const std::string node = generate_header(2, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);

			}
			else
			{
				BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (header.02)");
				const boost::uuids::uuid tag = uuid_gen_();
				const std::string txt = accumulate(size + 1, reader);

				const std::string node = generate_header(2, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);

			}

		}));

		vm_.async_run(cyy::register_function("header.03", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,true,"Considerations","Final"]
			//std::cout
			//	<< "\n***info: header.03("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
			{
				const std::string txt = reader[2].get_string("title");
				const boost::uuids::uuid tag = name_gen_(reader[2].get_string("tag"));

				const std::string node = generate_header(3, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);

			}
			else
			{

				BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (header.03)");
				const boost::uuids::uuid tag = uuid_gen_();
				const std::string txt = accumulate(size + 1, reader);

				const std::string node = generate_header(3, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);
			}

		}));

		vm_.async_run(cyy::register_function("header.04", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,true,"Considerations","Final"]
			//std::cout
			//	<< "\n***info: header.04("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(2)))
			{
				const std::string txt = reader[2].get_string("title");
				const boost::uuids::uuid tag = name_gen_(reader[2].get_string("tag"));

				const std::string node = generate_header(4, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);

			}
			else
			{

				BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (header.04)");
				const std::string txt = accumulate(size + 1, reader);
				const boost::uuids::uuid tag = uuid_gen_();

				const std::string node = generate_header(4, txt, tag);
				ctx.set_return_value(cyy::factory(node), 0);
			}

		}));


		vm_.async_run(cyy::register_function("link", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,false,%(("text":"troff/nroff"),("url":"https://en.wikipedia.org/wiki/Troff"))]
			//std::cout
			//	<< "\n***info: link("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;
#endif
			const cyy::vector_reader reader(frame);

			const std::string url = reader[2].get_string("url");
			const std::string node = "<a href=\""
				+ url
				+ "\" title=\""
				+ reader[2].get_string("title", url)
				+ "\">"
				+ reader[2].get_string("text")
				+ "</a>"
				;
			if (verbose_ > 1)
			{
				std::cout
					<< "***info: link("
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("bold", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[1idx,"program"]
			//std::cout
			//	<< "\n***info: bold("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (bold)");

			const std::string node = "<b>"
				+ accumulate(size + 1, reader)
				+ "</b>"
				;
			if (verbose_ > 2)
			{
				std::cout
					<< "***info: bold("
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("emphasise", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[1idx,"program"]
			//std::cout
			//	<< "\n***info: emphasise("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (italic)");

			const std::string node = "<em>"
				+ accumulate(size + 1, reader)
				+ "</em>"
				;
			if (verbose_ > 2)
			{
				std::cout
					<< "***info: italic("
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));


		vm_.async_run(cyy::register_function("color", 2, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[1idx,true,%(("red":"spiced up"))]
			//std::cout
			//	<< "\n***info: color("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			const auto map = reader.get(2, cyy::param_map_t());
			BOOST_ASSERT_MSG(map.size() == 1, "internal error (color)");

			std::stringstream ss;
			ss
				<< "<span style=\"color:"
				<< map.begin()->first
				<< "\">"
				<< cyy::to_string(map.begin()->second)
				<< "</span>"
				;
			const std::string node = ss.str();
			if (verbose_ > 2)
			{
				std::cout
					<< "***info: color("
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("insert.entity", 1, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	["5/7"]
			//std::cout
			//	<< "***info: insert.entity("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
			const std::string entity = reader.get_string(0);
			BOOST_ASSERT_MSG(entity.size() < 4, "entity to long");

			std::string node;
			if (boost::algorithm::equals(entity, "->"))
			{
				//	&rightarrow;
				node = "&rightarrow;";
			}
			else if (boost::algorithm::equals(entity, "<-"))
			{
				//	&leftarrow;
				node = "&leftarrow;";
			}
			else if (boost::algorithm::equals(entity, "=>"))
			{
				//	&DoubleRightArrow;
				node = "&DoubleRightArrow;";
			}
			else if (boost::algorithm::equals(entity, "<="))
			{
				//	&DoubleLeftArrow;
				node = "&DoubleLeftArrow;";
			}
			else if (boost::algorithm::equals(entity, "<->"))
			{
				//	&leftrightarrow; 
				node = "&leftrightarrow;";
			}
			else if (boost::algorithm::equals(entity, "<=>"))
			{
				//	&DoubleLeftRightArrow
				node = "&DoubleLeftRightArrow;";
			}
			else if (boost::algorithm::equals(entity, "!="))
			{
				//	&NotEqual;
				node = "&NotEqual;";
			}
			else if (boost::algorithm::equals(entity, "=="))
			{
				//	&equiv;
				node = "&equiv;";
			}
			else if (boost::algorithm::equals(entity, "1/2"))
			{
				//	&frac12;
				node = "&frac12;";
			}
			else if (boost::algorithm::equals(entity, "1/3"))
			{
				//	&frac13;
				node = "&frac13;";
			}
			else if (boost::algorithm::equals(entity, "1/4"))
			{
				//	&frac14;
				node = "&frac14;";
			}
			else if (boost::algorithm::equals(entity, "1/5"))
			{
				//	&frac15;
				node = "&frac15;";
			}
			else if (boost::algorithm::equals(entity, "1/6"))
			{
				//	&frac16;
				node = "&frac16;";
			}
			else if (boost::algorithm::equals(entity, "1/8"))
			{
				//	&frac18;
				node = "&frac18;";
			}
			else if (boost::algorithm::equals(entity, "2/3"))
			{
				//	&frac23;
				node = "&frac23;";
			}
			else if (boost::algorithm::equals(entity, "2/5"))
			{
				//	&frac25;
				node = "&frac25;";
			}
			else if (boost::algorithm::equals(entity, "3/4"))
			{
				//	&frac34;
				node = "&frac34;";
			}
			else if (boost::algorithm::equals(entity, "3/5"))
			{
				//	&frac35;
				node = "&frac35;";
			}
			else if (boost::algorithm::equals(entity, "3/8"))
			{
				//	&frac38;
				node = "&frac38;";
			}
			else if (boost::algorithm::equals(entity, "4/5"))
			{
				//	&frac45;
				node = "&frac45;";
			}
			else if (boost::algorithm::equals(entity, "5/8"))
			{
				//	&frac58;
				node = "&frac58;";
			}
			else if (boost::algorithm::equals(entity, "5/6"))
			{
				//	&frac56;
				node = "&frac56;";
			}
			else if (boost::algorithm::equals(entity, "7/8"))
			{
				//	&frac78;
				node = "&frac78;";
			}
			else if (boost::algorithm::equals(entity, ":-)"))
			{
				//	&#x1f642;
				node = "&#x1f642;";
			}
			else if (boost::algorithm::equals(entity, ":-("))
			{
				//	&#x1f641;
				node = "&#x1f641;";
			}
			else if (boost::algorithm::equals(entity, ":-D"))
			{
				//	&#x1f600;
				node = "&#x1f600;";
			}
			else if (boost::algorithm::equals(entity, ":-3"))
			{
				//	&#x1f61a;
				node = "&#x1f61a;";
			}
			else if (boost::algorithm::equals(entity, ";-)"))
			{
				//	&#x1f609;
				node = "&#x1f609;";
			}
			else
			{
				std::cout
					<< "***error: undefined entity: "
					<< entity
					<< std::endl;

			}
			if (verbose_ > 2)
			{
				std::cout
					<< "***info: convert("
					<< entity
					<< " => "
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("reference", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[1idx,false,"79bf3ba0-2362-4ea5-bcb5-ed93844ac59a"]
			//std::cout
			//	<< "\n***info: reference("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;
#endif
			const cyy::vector_reader reader(frame);
			const boost::uuids::uuid tag = name_gen_(reader.get_string(2));

			auto pos = structure_.find(tag);
			if (pos != structure_.end())
			{
				//	(<a href="#799ad680-e833-4258-8dbf-00ee48fd8d82">see chapter N</a>)
				const std::string node = to_string(*pos);
				if (verbose_ > 1)
				{
					std::cout
						<< "***info: reference("
						<< node
						<< ")"
						<< std::endl;
				}
				ctx.set_return_value(cyy::factory(node), 0);
			}
			else
			{
#ifdef _DEBUG
				//std::cerr
				//	<< "\n***warning: reference "
				//	<< reader.get_string(2)
				//	<< "/"
				//	<< tag
				//	<< " not found - try backpatching"
				//	<< std::endl;
#endif
				if (verbose_ > 0)
				{
					std::cout
						<< "***info: set backpatching tag "
						<< reader.get_string(2)
						<< std::endl;
				}

				std::stringstream ss;
				ss
					<< '{'
					<< tag
					<< '}'
					;
				const std::string node = ss.str();
				ctx.set_return_value(cyy::factory(node), 0);

				//
				//	update backpatch list
				//
				auto res = backpatches_.insert(tag);
				if (!res.second && verbose_ > 2)
				{
					std::cout
						<< "***info: "
						<< node
						<< " is referenced more then once"
						<< std::endl;

				}
			}
		}));

		vm_.async_run(cyy::register_function("begin.env", 3, [this](cyy::context& ctx) {

			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[3idx,"global",%(("file-name":"C:/Users/Pyrx/AppData/Local/Temp/docscript-bc1eb21f-2b50-4949-b022-ca1219b4a3e8.bin"path),("filter":"JavaScript"),("process":"none"))]
			//	[3idx,true,%(("file-name":"C:/Users/Pyrx/AppData/Local/Temp/docscript-fd3d1fb4-ec0a-5a6a-907b-c39257b09690.bin"path),("filter":"C++"),("process":"K&R"))]
			//std::cout
			//	<< "\n***info: begin.env("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			const cyy::vector_reader reader(frame);
		}));

		vm_.async_run(cyy::register_function("list", 2, [this](cyy::context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
			//	[3idx,"env.close","<li>world!</li>","<li>hello, </li>",%(("type":"unordered"))]
			//	[5idx, "env.close", "\n<li>Some ...</li>", "\n<li>See ...</li>", "\n<li>Change ...</li>", %(("style":"upper-roman"),("type":"ordered"))]
			//std::cout
			//	<< "\n***info: list("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;
#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (list)");

			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(size + 1)))
			{ 
				const std::string type = reader[size + 1].get_string("type", "ordered");
				std::stringstream ss;
				ss << std::endl;

				if (boost::algorithm::equals(type, "ordered"))
				{
					const std::string style = reader[size + 1].get_string("style", "decimal-leading-zero");
					ss
						<< "<ol style=\"list-style-type: "
						<< style
						<< "\">"
						<< accumulate(size + 1, reader)
						<< std::endl
						<< "</ol>"
						;
				}
				else
				{
					const std::string style = reader[size + 1].get_string("style", "square");
					ss
						<< "<ul style=\"list-style-type: "
						<< style
						<< "\">"
						<< accumulate(size + 1, reader)
						<< std::endl
						<< "</ul>"
						;

				}
				const std::string node = ss.str();
				ctx.set_return_value(cyy::factory(node), 0);
			}
			else 
			{ 
				const std::string node = "\n<ul><li>internal error: parameter map expected (list)</li></ul>";
				ctx.set_return_value(cyy::factory(node), 0);
			}
		}));

		vm_.async_run(cyy::register_function("entry", 2, [this](cyy::context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
			//	[1idx,"env.run","hello, "]
			std::cout
				<< "\n***info: entry("
				<< cyy::io::to_literal(frame)
				<< ")"
				<< std::endl;
#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (entry)");

			const std::string node = "\n<li>"
				+ accumulate(size + 1, reader)
				+ "</li>"
				;
			if (verbose_ > 2)
			{
				std::cout
					<< "***info: entry("
					<< node
					<< ")"
					<< std::endl;
			}

			ctx.set_return_value(cyy::factory(node), 0);

		}));

		vm_.async_run(cyy::register_function("quote", 1, [this](cyy::context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG
			//	quote([1idx,"env.close","break.","coffee","the","than","system","communication","office","better","a","with","up","come","never","may","Science",%(("source":"Earl Wilson"),("url":"https://www.brainyquote.com/quotes/quotes/e/earlwilson385998.html"))])
			//std::cout
			//	<< "\n***info: quote("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;
#endif
			const cyy::vector_reader reader(frame);
			const std::size_t size = reader.get_index(0);
			BOOST_ASSERT_MSG(size + 2 == frame.size(), "internal error (quote)");

			if (cyy::primary_type_test<cyy::param_map_t>(reader.get_object(size + 1)))
			{
				const std::string source = reader[size + 1].get_string("source", "source");
				const std::string url = reader[size + 1].get_string("url", "");

				std::stringstream ss;
				ss 
					<< std::endl
					<< "<blockquote cite=\""
					<< url
					<< "\">"
					<< accumulate(size, reader)
					<< std::endl
					<< "<footer>- <cite>"
					<< source
					<< "</cite></footer>"
					<< std::endl
					<< "</blockquote>"
					;

				const std::string node = ss.str();
				ctx.set_return_value(cyy::factory(node), 0);
			}
			else
			{
				const std::string node = "\n<blockquote>internal error: parameter map expected<footer>- <cite>quote</cite></footer></blockquote>";
				ctx.set_return_value(cyy::factory(node), 0);
			}

		}));

		vm_.async_run(cyy::register_function("figure", 3, [this](cyy::context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(false);
#ifdef _DEBUG

			//	[2idx,false,%(("caption":"figure with caption"),("source":"LogoSmall.jpg"))]
			//std::cout
			//	<< "\n***info: figure("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif

			//<figure>
			//  <img src="img_pulpit.jpg" alt="The Pulpit Rock" width="304" height="228">
			//  <figcaption>Fig1. - A view of the pulpit rock in Norway.</figcaption>
			//</figure>

			const cyy::vector_reader reader(frame);
			const std::string source = reader[2].get_string("source");
			const boost::filesystem::path p = resolve_path(source);
			std::ifstream file(p.string(), std::ios::binary | std::ios::ate);
			if (!file.is_open())
			{
				std::cerr
					<< "***error cannot open figure file "
					<< source
					<< std::endl;
			}
			else
			{
				//
				//	do not skip 
				//
				file.unsetf(std::ios::skipws);

				//
				//	get file size
				//
				std::streamsize size = file.tellg();
				file.seekg(0, std::ios::beg);

				//
				//	read into buffer
				//
				cyy::buffer_t buffer(size);
				file.read(buffer.data(), size);

				//
				//	encode image as base 64
				//
				const std::string alt = reader[2].get_string("alt");
				const std::string cap = reader[2].get_string("caption");

				std::stringstream ss;
				ss
					<< std::endl
					<< "<figure>"
					<< "<img alt=\""
					<< alt
					<< "\" src=\"data:image/"
					<< get_extension(p)
					<< ";base64,"
					<< cyy::crypto::base64::encode<>(buffer.begin(), buffer.end())
					<< "\" />\n<figcaption>"
					<< cap
					<< "</figcaption>\n"
					<< "</figure>"
					;

				const std::string node = ss.str();
				if (verbose_ > 1)
				{
					std::cout
						<< "***info: figure("
						<< cap
						<< " - "
						<< size
						<< " bytes)"
						<< std::endl;
				}
				ctx.set_return_value(cyy::factory(node), 0);
			}


		}));
		

		vm_.async_run(cyy::register_function("generate", 1, [this](cyy::context& ctx) {
			cyy::vector_t frame = ctx.stack_frame(true);
#ifdef _DEBUG

			//	
			//std::cout
			//	<< "\n***info: generate("
			//	<< cyy::io::to_literal(frame)
			//	<< ")"
			//	<< std::endl;

#endif
			if (!cyy::primary_type_test<boost::filesystem::path>(frame.at(0)))
			{
				std::cerr
					<< "***error: input parameter for function generate() is not of type filesystem::path "
					<< std::endl;

			}
			const boost::filesystem::path p = cyy::value_cast(frame.at(0), boost::filesystem::path());

			if (verbose_ > 1)
			{
				std::cout
					<< "***info: serialize "
					<< frame.size()
					<< " objects into file "
					<< p
					<< std::endl;
			}

			std::ofstream file(p.string(), std::ios::out | std::ios::trunc);
			if (!file.is_open())
			{
				std::cerr
					<< "***error cannot open output file "
					<< p
					<< std::endl;
			}
			else
			{
				serialize(file, ++frame.begin(), frame.end());
			}

		}));


	}

	std::string generator::accumulate(std::size_t start, cyy::vector_reader const& reader) const
	{
		std::string str;
		for (std::size_t idx = start; idx > 1; idx--)
		{
			const auto s = reader.get_string(idx);
			if ((idx > 1) && (idx != start) && !(s == "," || s == ":"))
			{
				str += " ";
			}
			str += s;
		}
		return str;
	}

	std::string generator::generate_header(std::size_t level, std::string const& txt, boost::uuids::uuid tag)
	{
		if (level == 0)
		{
			std::cerr
				<< "***error: header indentation level is zero ("
				<< txt
				<< ")"
				<< std::endl;
			return "error";
		}

		//
		//	update numeration
		//
		std::ptrdiff_t diff = level - numeration_.size();
		if (diff == 1)
		{
			//
			//	increase indention
			//
			numeration_.push_back(1);
		}
		else if (diff == 0)
		{
			//
			//	indention unchanged
			//
			numeration_.at(level - 1)++;
		}
		else if (diff == -1)
		{
			//
			//	decrease indention
			//
			numeration_.at(level - 1)++;
			numeration_.pop_back();
		}
		else
		{
			std::cerr
				<< "***error: header with wrong indentation level: "
				<< diff
				<< " - "
				<< txt
				<< std::endl;
		}


		//
		//	update structure
		//
		auto pos = structure_.emplace(std::piecewise_construct,
			std::forward_as_tuple(tag),
			std::forward_as_tuple(element::HEADER, txt, numeration_));

		if (pos.second)
		{
			std::stringstream ss;
			ss
				<< std::endl
				<< "<h"
				<< level
				<< " class=\"header."
				<< level
				<< "\" id=\""
				<< tag
				<< "\">"
				<< pos.first->second.to_str()
				<< "</h"
				<< level
				<< ">"
				;
			const std::string node = ss.str();
			if (verbose_ > 1)
			{
				std::cout
					<< "***info: header("
					<< pos.first->second.to_str()
					//<< node
					<< ")"
					<< std::endl;
			}
			return node;

		}

		std::cerr
			<< "***error: cannot insert new header "
			<< txt
			<< std::endl;
		return "error";


	}

	std::string generator::generate_comment(std::string const& txt)
	{
		std::stringstream ss;
		ss
			<< "<!-- "
			<< txt
			<< " -->"
			;
		return ss.str();

	}


	std::string generator::backpatch(std::string&& str)
	{
		std::string data = std::move(str);

		if (data.size() > 36)
		{
			if (verbose_ > 3)
			{
				std::cout
					<< "***info: scan "
					<< data.size()
					<< " bytes for "
					<< backpatches_.size()
					<< " patch(es)"
					<< std::endl;
			}

			std::for_each(backpatches_.begin(), backpatches_.end(), [this, &data](boost::uuids::uuid tag) {
				auto pos = structure_.find(tag);
				if (pos != structure_.end())
				{
					const std::string s = "{" + boost::uuids::to_string(tag) + "}";
					const std::string r = to_string(*pos);

					boost::algorithm::replace_first(data, s, r);
				}
			});
		}

		return data;

	}

	void generator::serialize(std::ofstream& os, cyy::vector_t::const_iterator begin, cyy::vector_t::const_iterator end)
	{
		std::for_each(begin, end, [this, &os](cyy::object const& obj) {
			os << this->backpatch(cyy::to_string(obj));
		});
	}

	std::string generator::to_string(element_t const& e)
	{
		std::stringstream ss;
		ss
			<< "<a href=\"#"
			<< e.first
			<< "\">"
			<< e.second.to_str()
			<< "</a>"
			;
		return ss.str();

	}

	boost::filesystem::path generator::resolve_path(std::string const& s) const
	{
		boost::filesystem::path p(s);
		for (auto dir : includes_)
		{
			if (boost::filesystem::exists(dir / p))
			{
				return boost::filesystem::path(dir / p);
			}
		}
		return boost::filesystem::path();
	}

	std::string generator::get_extension(boost::filesystem::path const& p)
	{
		if (p.empty())
		{
			return "";
		}
		std::string s = p.extension().string();
		return s.substr(1, s.size() - 1);
	}

	//std::string generator::sanify(std::string const& inp)
	//{
	//	return inp;
	//}

	boost::system::error_code generator::run(cyy::vector_t const& prg)
	{
		if (verbose_ > 2)
		{
			std::cout
				<< "***info: start generator with "
				<< prg.size()
				<< " op codes"
				<< std::endl;
		}
		return vm_.run(cyy::vector_t(prg));
	}

	std::size_t generator::index(boost::filesystem::path const& out) const
	{
		if (verbose_ > 1)
		{
			std::cout
				<< "***info: generate index file "
				<< out
				<< " with "
				<< structure_.size()
				<< " entries"
				<< std::endl;
		}

		//
		//	sorting structure by inserting into a set
		//
		std::set<element_t> ordered(structure_.begin(), structure_.end());

		std::stack<cyy::vector_t>	tree;
		for (auto e : ordered)
		{
#ifdef _DEBUG
			//std::cout
			//	<< e.first
			//	<< " - "
			//	<< e.second.to_str()
			//	<< std::endl;
#endif
			std::ptrdiff_t diff = e.second.depth() - tree.size();

			if (diff == 0)
			{
				//
				//	indention unchanged
				//
				auto tpl = cyy::tuple_factory(cyy::make_parameter("tag", e.first)
					, cyy::make_parameter("level", e.second.level())
					, cyy::make_parameter("title", e.second.text_));

				tree.top().push_back(tpl);
				
			}
			else if (diff == 1)
			{
				//
				//	increase indention
				//

				//
				//	add a new vector
				//
				cyy::vector_t idx;
				auto tpl = cyy::tuple_factory(cyy::make_parameter("tag", e.first)
					, cyy::make_parameter("level", e.second.level())
					, cyy::make_parameter("title", e.second.text_));
				idx.push_back(tpl);

				tree.push(idx);
			}
			else if (diff == -1)
			{
				//
				//	decrease indention
				//
				auto idx = tree.top();
				tree.pop();
				tree.top().push_back(cyy::factory(idx));

				//
				//	bext entry like diff == 0
				//
				auto tpl = cyy::tuple_factory(cyy::make_parameter("tag", e.first)
					, cyy::make_parameter("level", e.second.level())
					, cyy::make_parameter("title", e.second.text_));

				tree.top().push_back(tpl);
			}
			else
			{
				std::cout
					<< e.second.depth()
					<< std::endl;
			}
		}

		while (tree.size() > 1)
		{
			//
			//	decrease indention
			//
			auto idx = tree.top();
			tree.pop();
			tree.top().push_back(cyy::factory(idx));
		}

		if (!tree.empty())
		{
			if (verbose_ > 3)
			{
				cyy::serialize_json_pretty(std::cout, cyy::factory(tree.top()), cyy::io::custom_callback());
			}

			std::ofstream file(out.string(), std::ios::out | std::ios::trunc);
			if (!file.is_open())
			{
				std::cerr
					<< "***error cannot open file "
					<< out
					<< std::endl;
			}
			else
			{
				cyy::serialize_json(file, cyy::factory(tree.top()), cyy::io::custom_callback());
			}
		}
		else
		{
			std::cerr
				<< "***warning: structur tree is empty "
				<< std::endl;

		}

		return 0;
	}

	std::size_t generator::meta(boost::filesystem::path const& out) const
	{
		if (verbose_ > 1)
		{
			std::cout
				<< "***info: generate meta file "
				<< out
				<< " with "
				<< meta_.size()
				<< " entries"
				<< std::endl;
		}

		std::ofstream file(out.string(), std::ios::out | std::ios::trunc);
		if (!file.is_open())
		{
			std::cerr
				<< "***error cannot open file "
				<< out
				<< std::endl;
		}
		else
		{
			if (verbose_ > 3)
			{
				cyy::serialize_json_pretty(std::cout, cyy::factory(meta_), cyy::io::custom_callback());
			}
			cyy::serialize_json(file, meta_, cyy::io::custom_callback());
		}
		return meta_.size();
	}

	element::element(type t, std::string const& s, std::vector<std::size_t> const& chapter)
		: type_(t)
		, text_(s)
		, chapter_(chapter)
	{}

	std::string element::level() const
	{
		std::stringstream ss;

		//
		// handle first element
		//
		auto pos = chapter_.begin();
		if (pos != chapter_.end())
		{
			ss << *pos;
			++pos;
		}

		//
		// join all other elements
		//
		std::for_each(pos, chapter_.end(), [&ss](std::size_t n) {
			ss << '.' << n;
		});

		return ss.str();
	}

	std::string element::to_str() const
	{
		return level() + "&nbsp;" + text_;
	}

	std::size_t element::depth() const
	{
		return chapter_.size();
	}

	//element& element::operator=(element const& other)
	//{
	//	if (this != &other)
	//	{
	//		//const type type_;
	//		//const std::string text_;
	//		//const std::vector<std::size_t>	chapter_;

	//	}
	//	return *this;
	//}

	//	comparison
	bool operator==(element const& lhs, element const& rhs)
	{
		return (lhs.type_ == rhs.type_)
			? (lhs.chapter_ == rhs.chapter_)
			: false
			;
	}
	bool operator<(element const& lhs, element const& rhs)
	{
		return (lhs.type_ == rhs.type_)
			? (lhs.chapter_ < rhs.chapter_)
			: (lhs.type_ < rhs.type_)
			;
	}
	bool operator!=(element const& lhs, element const& rhs)
	{
		return !(lhs == rhs);
	}
	bool operator>(element const& lhs, element const& rhs)
	{
		//	note the reversed notation
		return rhs < lhs;
	}
	bool operator<=(element const& lhs, element const& rhs)
	{
		return !(lhs > rhs);
	}
	bool operator>=(element const& lhs, element const& rhs)
	{
		return !(lhs < rhs);
	}


	//	comparison
	bool operator==(element_t const& lhs, element_t const& rhs)
	{
		return lhs.second == rhs.second;
	}
	bool operator<(element_t const& lhs, element_t const& rhs)
	{
		return lhs.second < rhs.second;
	}
	bool operator!=(element_t const& lhs, element_t const& rhs)
	{
		return !(lhs == rhs);
	}
	bool operator>(element_t const& lhs, element_t const& rhs)
	{
		//	note the reversed notation
		return rhs < lhs;
	}
	bool operator<=(element_t const& lhs, element_t const& rhs)
	{
		return !(lhs > rhs);
	}
	bool operator>=(element_t const& lhs, element_t const& rhs)
	{
		return !(lhs < rhs);
	}

}
