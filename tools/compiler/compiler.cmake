# top level files
set (compiler)

set (compiler_cpp

	tools/compiler/src/main.cpp
	tools/compiler/src/driver.cpp
	tools/compiler/src/reader.cpp
	
)

set (compiler_h

	tools/compiler/src/driver.h
	tools/compiler/src/reader.h
	
)

# define the compiler program
set (compiler
  ${compiler_cpp}
  ${compiler_h}
)
