# top level files
set (test)

set (test_cpp

	tools/test/src/main.cpp
	tools/test/src/driver.cpp
	tools/test/src/reader.cpp
	
)

set (test_h

	tools/test/src/driver.h
	tools/test/src/reader.h

)

set (test_lib

	tools/test/src/lib/token.h
	tools/test/src/lib/token.cpp
	tools/test/src/lib/tokenizer.h
	tools/test/src/lib/tokenizer.cpp
	tools/test/src/lib/symbol.h
	tools/test/src/lib/symbol.cpp
	tools/test/src/lib/lexer.h
	tools/test/src/lib/lexer.cpp
	tools/test/src/lib/compiler.h
	tools/test/src/lib/compiler.cpp
	tools/test/src/lib/generator.h
	tools/test/src/lib/generator.cpp
	tools/test/src/lib/library.h
	tools/test/src/lib/library.cpp

)

set (test_doc

	tools/test/src/doc/tokenizer-test.docscript	
)

source_group("resources" FILES ${test_doc})
source_group("lib" FILES ${test_lib})

# define the test program
set (test
  ${test_cpp}
  ${test_h}
  ${test_lib} 
  ${test_doc} 
)
