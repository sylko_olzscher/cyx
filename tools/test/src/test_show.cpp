﻿/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#if defined(_WIN32)
#include <windows.h>
#endif
#include "test_show.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ostream>
#include <iterator>

namespace cyx
{

	std::vector<std::wstring> locals;
#if defined(_WIN32)
	BOOL CALLBACK MyFuncLocaleEx(LPWSTR pStr, DWORD dwFlags, LPARAM lparam)
	{
		locals.push_back(pStr);
		return TRUE;
	}
#endif

	bool test_show_locals()
	{
#if defined(_WIN32)
		::EnumSystemLocalesEx(MyFuncLocaleEx, LOCALE_ALL, NULL, NULL);

		for (std::vector<std::wstring>::const_iterator str = locals.begin(); str != locals.end(); ++str)
		{ 
			std::wcout 
				<< *str 
				<< std::endl
				;
		}
		std::wcout 
			<< "Total " 
			<< locals.size() 
			<< " locals found." 
			<< std::endl
			;
#endif
		return !locals.empty();
	}

}	//	cyx
