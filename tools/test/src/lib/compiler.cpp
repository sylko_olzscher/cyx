/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "compiler.h"
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/factory.h>
#include <cyy/io/format/bytes_format.h>

namespace cyx	
{
	namespace docscript
	{

		compiler::compiler(std::list<symbol> const& sl, int verbose)
			: producer_(sl)
			, look_ahead_(&producer_.get())
			, verbose_(verbose)
			, library_()
			, prg_()
		{
			init_library();
		}

		void compiler::run(std::chrono::system_clock::time_point const& last_write_time
			, uintmax_t file_size
			, boost::filesystem::path const& out)
		{
			//
			//	call initial functions and open the "generate"
			//	call frame
			//
			prg_
				//	meta data
				<< cyy::codes::ESBA
				<< cyy::codes::param(cyy::param_factory("last-write-time", last_write_time))
				<< cyy::numeric_factory<std::uint64_t>(file_size)
				<< cyy::string_factory("file-size")
				<< cyy::codes::PARAM
				<< cyy::index(2)	//	2 parameters
				<< cyy::codes::PARAM_MAP
				<< cyy::true_factory()	//	simulate call of a global function
				<< cyy::index(1)		//	parameter count
				<< cyy::codes::invoke("meta")
				<< cyy::codes::REBA

				//	build a call frame generate function
				<< cyy::codes::ESBA
				<< out
				;

			bool loop = true;
			while (loop)
			{
				switch (look_ahead_->type_)
				{
				case SYM_EOF:
					if (verbose_ > 1)
					{
						std::cout
							<< "***info: EOF"
							<< std::endl
							;
					}
					loop = false;
					break;

				case SYM_FUN_NL:		
					//	functions at beginning of line are global
					fun_nl(look_ahead_->value_);
					break;
				case SYM_FUN_WS:
					// all other functions are local	
					fun_ws(look_ahead_->value_);
					break;
				case SYM_FUN_PAR:
					//	new paragraph
					fun_par(set_preamble(look_ahead_->value_));
					break;
				default:
					std::cerr
						<< "***warning: unknown symbol "
						<< (*look_ahead_)
						<< std::endl
						;
					match(look_ahead_->type_);
					break;
				}
			}

			prg_
				//	generate output file
				<< cyy::codes::invoke("generate")
				<< cyy::codes::REBA
				;

			if (verbose_ > 1)
			{
				std::cout
					<< "***info: EOF"
					<< std::endl
					;
			}

		}

		bool compiler::match(symbol_type st)
		{
			if (look_ahead_->type_ != st)
			{
				std::cerr
					<< "***error (wrong lookahead) expected: ["
					<< name(st)
					<< "] but get "
					<< *look_ahead_
					<< std::endl
					;

				//
				//	skip unmatching symbols
				//
				while (look_ahead_->type_ != st && look_ahead_->type_ != SYM_EOF)
				{
					look_ahead_ = &producer_.get();
				}
				return false;
			}

			BOOST_ASSERT_MSG(look_ahead_->type_ == st, "wrong symbol");
			look_ahead_ = &producer_.get();

			if (verbose_ > 4)
			{
				std::cout
					<< "***info: look ahead "
					<< *look_ahead_
					<< std::endl
					;
			}

			return true;
		}

		void compiler::fun_nl(std::string name)
		{
			match(SYM_FUN_NL);
			switch (look_ahead_->type_)
			{
			case SYM_KEY:
				key(name, true, look_ahead_->value_);
				break;
			case SYM_ARG:
				//	single argument
				arg(name, true, look_ahead_->value_);
				break;
			case SYM_FUN_WS:
				name = set_preamble(name);
				fun_ws(look_ahead_->value_);
				match(SYM_FUN_CLOSE);

				prg_
					//	we asume that the function produced 1 return value
					<< cyy::index_factory(1)
					<< cyy::codes::invoke(name)
					<< cyy::codes::REBA
					;
				break;
			case SYM_FUN_CLOSE:
				//	no arguments
			{
				std::cout
					<< "***info: compile function "
					<< name
					<< "() - without arguments"
					<< std::endl
					;
			}
				name = set_preamble(name);
				prg_
						<< cyy::true_factory()		//	global function
						<< cyy::index_factory(0)	//	no parameters
						<< cyy::codes::invoke(name)
						<< cyy::codes::REBA
						;
				match(SYM_FUN_CLOSE);
				break;
			default:
				std::cerr
					<< "***warning: unknown symbol (fun-nl)"
					<< (*look_ahead_)
					<< std::endl
					;
				break;
			}
		}

		void compiler::fun_ws(std::string name)
		{
			match(SYM_FUN_WS);
			switch (look_ahead_->type_)
			{
			case SYM_KEY:
				key(name, false, look_ahead_->value_);
				break;
			case SYM_ARG:
				//	single argument
				arg(name, false, look_ahead_->value_);
				break;
			case SYM_FUN_WS:
				name = set_preamble(name);
				fun_ws(look_ahead_->value_);

				//
				//	It's possible to get one more function call (SYM_FUN_WS) that produces the argument(s)
				//	for the previous function call. To handle this properly the order of function calls
				//	must be changed. To make the right decision, we have to know if there are missing arguments
				//	open.
				//
				//if (look_ahead_ == SYM_FUN_WS)	...
				match(SYM_FUN_CLOSE);
				
				prg_
					<< cyy::false_factory()		//	local function
					<< cyy::index_factory(lookup(name)->rvs_)
					<< cyy::codes::invoke(name)
					<< cyy::codes::REBA
					;
				break;
			case SYM_FUN_CLOSE:
				//	no arguments
				if (verbose_ > 3)
				{
					std::cout
						<< "***info: compile function "
						<< name
						<< "() - without arguments"
						<< std::endl
						;
				}
				name = set_preamble(name);
				prg_
					<< cyy::index_factory(0)
					<< cyy::codes::invoke(name)
					<< cyy::codes::REBA
					;
				match(SYM_FUN_CLOSE);
				break;
			default:
				std::cerr
					<< "***warning: unknown symbol (fun-nl)"
					<< (*look_ahead_)
					<< std::endl
					;
				break;
			}
		}

		void compiler::fun_par(std::string name)
		{
			match(SYM_FUN_PAR);
			std::size_t counter{ 0 };

			while (look_ahead_->type_ != SYM_FUN_PAR && look_ahead_->type_ != SYM_FUN_NL && look_ahead_->type_ != SYM_EOF)
			{
				switch (look_ahead_->type_)
				{
				case SYM_WORD:
				case SYM_CHAR:
					prg_ << cyy::factory(look_ahead_->value_);
					match(look_ahead_->type_);
					counter++;
					break;
				case SYM_FUN_WS:
					//	number of return values
					counter += lookup(look_ahead_->value_)->rvs_;
					fun_ws(look_ahead_->value_);
					break;
				default:
					match(look_ahead_->type_);
					counter++;
					break;
				}					
			}
			if (verbose_ > 3)
			{
				std::cout
					<< "***info: compile paragraph with "
					<< counter
					<< " elements"
					<< std::endl
					;
			}
			prg_
				<< cyy::index_factory(counter)
				<< cyy::codes::invoke(name)
				<< cyy::codes::REBA
				;
		}

		void compiler::key(std::string name, bool nl, std::string key)
		{
			name = set_preamble(name);

			//
			//	iterate until SYM_FUN_CLOSE
			//
			std::size_t counter{ 0 };
			while (look_ahead_->type_ != SYM_FUN_CLOSE && look_ahead_->type_ != SYM_EOF)
			{
				match(SYM_KEY);

				switch (look_ahead_->type_)
				{
				case SYM_VALUE:
					if (verbose_ > 3)
					{
						std::cout
							<< "***info: compile function "
							<< name
							<< " with "
							<< key
							<< " = "
							<< look_ahead_->value_
							<< std::endl
							;
					}
					prg_
						<< cyy::factory(look_ahead_->value_)	//	value
						<< cyy::factory(key)	//	key
						<< cyy::codes::PARAM
						;
					match(SYM_VALUE);
					break;
				case SYM_NUMBER:
					if (verbose_ > 3)
					{
						std::cout
							<< "***info: compile function "
							<< name
							<< " with "
							<< key
							<< " = "
							<< look_ahead_->value_
							<< std::endl
							;
					}
					prg_
						<< cyy::index_from_str_factory(look_ahead_->value_)	//	value
						<< cyy::factory(key)	//	key
						<< cyy::codes::PARAM
						;
					match(SYM_NUMBER);
					break;
				case SYM_FUN_WS:
					if (verbose_ > 3)
					{
						std::cout
							<< "***info: compile function "
							<< name
							<< " with "
							<< key
							<< " = "
							<< look_ahead_->value_
							<< "(...)"
							<< std::endl
							;
					}
					fun_ws(look_ahead_->value_);
					prg_
						<< cyy::factory(key)	//	key
						<< cyy::codes::PARAM
						;
					break;
				default:
					//	error
					break;
				}

				//
				//	next key
				//
				key = look_ahead_->value_;
				counter++;
			}
			match(SYM_FUN_CLOSE);

			if (look_ahead_->type_ == SYM_ARG)
			{
				if (verbose_ > 3)
				{
					std::cout
						<< "***info: environment "
						<< name
						<< " has an argument of "
						<< cyy::bytes_format(look_ahead_->value_.size())
						<< std::endl
						;
				}

				counter++;
				prg_
					<< cyy::factory(look_ahead_->value_)	//	value
					<< cyy::string_factory("$")	//	key
					<< cyy::codes::PARAM
					;

				//	
				//	This is the content of an environment
				//
				match(SYM_ARG);
			}

			prg_
				<< cyy::index(counter)	//	parameters
				<< cyy::codes::PARAM_MAP

				//
				//	close call frame and call function
				//
				<< cyy::factory(nl)
				<< cyy::index_factory(1)	//	one parameter map
				<< cyy::codes::invoke(name)
				<< cyy::codes::REBA
				;

		}

		void compiler::arg(std::string name, bool nl, std::string value)
		{
			name = set_preamble(name);
			
			//
			//	iterate until SYM_FUN_CLOSE
			//
			std::size_t counter{ 0 };
			while (look_ahead_->type_ != SYM_FUN_CLOSE && look_ahead_->type_ != SYM_EOF)
			{
				//
				// process argument (value)
				//
				if (verbose_ > 3)
				{
					std::cout
						<< "***info: compile function "
						<< name
						<< " argument #"
						<< counter
						<< ": "
						<< value
						<< std::endl
						;
				}

				prg_
					<< cyy::factory(value);
					;

				match(SYM_ARG);
				value = look_ahead_->value_;
				counter++;
			}

			match(SYM_FUN_CLOSE);

			//
			//	close call frame and call function
			//
			prg_
				<< cyy::factory(nl)
				<< cyy::index_factory(counter)
				<< cyy::codes::invoke(name)
				<< cyy::codes::REBA
				;

		}

		void compiler::init_library()
		{

			insert(library_, std::make_shared<function>("bold", 1, WS_), {"b"});
			insert(library_, std::make_shared<function>("emphasise", 1, WS_), { "i", "italic", "em" });
			insert(library_, std::make_shared<function>("color", 1, WS_), { "col" });
			insert(library_, std::make_shared<function>("reference", 1, WS_), { "ref" });
			insert(library_, std::make_shared<function>("figure", 1, WS_), { "fig" });

			insert(library_, std::make_shared<function>("meta", 0, NL_), {});
			insert(library_, std::make_shared<function>("title", 0, NL_), {});
			insert(library_, std::make_shared<function>("debug", 0, NL_), {});
			insert(library_, std::make_shared<function>("contents", 0, NL_), {});

			insert(library_, std::make_shared<function>("paragraph", 1, NL_), { u8"�" });
			insert(library_, std::make_shared<function>("header", 1, NL_), { "h" });
			insert(library_, std::make_shared<function>("header.1", 1, NL_), { "h1" });
			insert(library_, std::make_shared<function>("header.2", 1, NL_), { "h2" });
			insert(library_, std::make_shared<function>("header.3", 1, NL_), { "h3" });
			insert(library_, std::make_shared<function>("header.4", 1, NL_), { "h4" });

			insert(library_, std::make_shared<function>("quote", 1, ENV_), { "q" });
			insert(library_, std::make_shared<function>("env.open", 1, ENV_), { "+" });
			insert(library_, std::make_shared<function>("env.close", 1, ENV_), { "-" });
		}

		std::shared_ptr<function const> compiler::lookup(std::string const& name) const
		{
			return cyx::docscript::lookup(library_, name);
		}

		std::string compiler::set_preamble(std::string const& name)
		{
			auto fp = lookup(name);
			BOOST_ASSERT(!!fp);	//	it's guaranteed to get a valid pointer
			for (auto idx = 0; idx < fp->rvs_; ++idx)
			{
				prg_ << cyy::codes::ASP;	//	return value
			}
			prg_ << cyy::codes::ESBA;
			return fp->name_;
		}

		cyy::vector_t move_program(compiler& c)
		{
			return std::move(c.prg_);
		}

	}	//	docscript
}	//	cyx


