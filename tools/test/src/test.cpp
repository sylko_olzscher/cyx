/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "test.h"
#include "test_parse.h"
#include "test_show.h"
#include <cyx/version_info.h>
#include <cyx/constants.h>
#include <cyy/parser/token_parser.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/intrinsics/factory/boost_factory.h>
#include <cyy/json/json_io.h>
#include <cyy/io.h>
#include <cyy/util/verify_file_extension.h>
#include <cyy/util/read_sets.h>
#include <cyx/http/response.h>


#include <boost/algorithm/string/predicate.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/config.hpp>

namespace cyx
{
	launch::launch(std::string const& json_path)
	: console(std::cout, std::cin)
	{
		//	read launch file
		load_config(json_path);

// 		const std::string r1 = calculate_websocket_hash("dGhlIHNhbXBsZSBub25jZQ==");	//	s3pPLMBiTxaQ9kYGzzhZRbK+xOo=
// 		const std::string r2 = calculate_websocket_hash("x3JJHMbDL1EzLkh9GBhXDw==");	//	HSmrc0sMlYUkAGmm5OPpG2HaGWk=
// 		const std::string r3 = calculate_websocket_hash("c1MmSCgetSGI7HVDBgiqjA==");	//	ecjKI6pKIYHN2JQcj7wLAxp9Hn8=

	}

	launch::~launch()
	{}
	
	int launch::run()
	{
		//
		//	Welcome message
		//
		out_
			<< "Welcome to cyx test tool v"
			<< CYX_VERSION
			<< std::endl
			<< "Enter \"q\" to leave or \"help\" for instructions"
			<< std::endl
			;
			
		return loop();
	}

	int launch::run(std::string const& file_name)
	{
		return (cmd_run_script(file_name))
		? EXIT_SUCCESS
		: EXIT_FAILURE
		;
	}
	
	void launch::shutdown()
	{
		out_ << "Stop scheduler" << std::endl;
// 		scheduler_.stop();
	}

	bool launch::parse(std::string const& line)
	{
		std::pair<cyy::tuple_t, bool > r = cyy::io::parse_token(line);
		if (r.second)
		{
			if (!r.first.empty())	
			{
				cyy::tuple_t::const_iterator pos = r.first.begin();
				BOOST_ASSERT(pos != r.first.end());
				
				//	get the first command
				const std::string cmd = cyy::to_string(*pos);
				
				if (boost::algorithm::iequals(cmd, "run") || boost::algorithm::iequals(cmd, "r"))
				{
					return cmd_run(++pos, r.first.end());
				}
				else if (boost::algorithm::iequals(cmd, "echo"))
				{
					return cmd_echo(++pos, r.first.end());
				}
				else if (boost::algorithm::iequals(cmd, "ls") || boost::algorithm::iequals(cmd, "dir"))
				{
					return cmd_list(++pos, r.first.end());
				}
 				else if (boost::algorithm::iequals(cmd, "parse"))
 				{
 					return cmd_parse(++pos, r.first.end());
 				}
 				else if (boost::algorithm::iequals(cmd, "show") || boost::algorithm::iequals(cmd, "sh"))
 				{
 					return cmd_show(++pos, r.first.end());
 				}
 				else
 				{
 					return cmd_help(++pos, r.first.end());
 				}
			}
			
			//	no usefull input
			return false;
		}
		else 
		{
			//	syntax error
			std::cerr
				<< "***Error (syntax): "
				<< line
				<< std::endl
				;	
		}
		return r.second;
	}

	int launch::create_config(std::string const& json_path)
	{
		const boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
		
		std::fstream fout(json_path, std::ios::trunc | std::ios::out);
		if (fout.is_open())
		{
			cyy::uuid_random_factory uuid_gen;
			cyy::tuple_t json_data;
			json_data.push_back(cyy::set_factory( "log-dir", tmp.string()));
			json_data.push_back(cyy::factory("tag", uuid_gen()));
// 			json_data.push_back(cyy::set_factory("timeout", std::chrono::seconds(12)));	//	gatekeeper
// 			json_data.push_back(cyy::set_factory("monitor", std::chrono::seconds(4)));	//	monitor

// 			cyy::tuple_t target_data;
// 			target_data.push_back(cyy::set_factory( "host", "127.0.0.1"));
// 			target_data.push_back(cyy::set_factory( "service", "7701"));
// 			target_data.push_back(cyy::set_factory( "account", "root"));

			//	generate a pseudo random password
// 			const auto hk = cyy::factory_md5("to-specify");
// 			const std::string hstr = cyy::to_string(hk);
// 			target_data.push_back(cyy::set_factory("pwd", hstr));
// 			
// 			json_data.push_back(cyy::set_factory( "target", target_data));
			
			cyy::serialize_json(fout, cyy::factory(json_data), cyy::io::custom_callback());
			cyy::serialize_json(std::cout, cyy::factory(json_data), cyy::io::custom_callback());

			std::cerr 
				<< std::endl
				<< "***info: config file ["
				<< json_path
				<< "] ready"
				<< std::endl;
			
			return EXIT_SUCCESS;
		}
		
		std::cerr 
			<< "***error: cannot open file ["
			<< json_path
			<< "]"
			<< std::endl;
		return EXIT_FAILURE;
		
	}

	void launch::load_config(std::string const& json_path)
	{
		const auto cfg = cyy::read_config(json_path);
		if (cfg.second)
		{
			cyy::object_reader reader(cfg.first);
			std::cout 
			<< "load "
			<< cyy::to_literal(cfg.first)
			<< std::endl 
			;

		}
		else 
		{
			std::cerr
				<< "***error: configuration file ["
				<< json_path
				<< "] not found"
				<< std::endl;
		}
	}
	
	bool launch::cmd_parse(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end)
	{
		if (pos != end)	
		{
			const std::string file_name = cyy::to_string(*pos);
			const boost::filesystem::path p = cyy::verify_extension(file_name, ".docscript");
			if (boost::filesystem::exists(p))
			{
				return test_parse(p, ++pos, end);
			}
			else
			{
				return test_parse(p, ++pos, end);
				std::cerr
					<< "***Warning: File ["
					<< p
					<< "] not found"
					<< std::endl
					;
			}
		}
		else
		{
			std::cerr
				<< "***Info: "
				<< "use parse [config]"
				<< std::endl;
		}
		return false;
	}
	
	bool launch::cmd_show(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end)
	{
		if (pos != end)
		{
			const std::string cmd = cyy::to_string(*pos);
			if (boost::algorithm::iequals(cmd, "locals") || boost::algorithm::iequals(cmd, "loc"))
			{
				return test_show_locals();
			}
		}
		return false;
	}

	bool launch::cmd_help(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end)
	{
		if (pos != end)
		{
			//	get help topic
			const std::string topic = cyy::to_string(*pos);
			if (boost::algorithm::iequals(topic, "run") || boost::algorithm::iequals(topic, "r"))
			{
				std::cout
				<< "run a script"
				<< std::endl
				<< "example:"
				<< std::endl
				<< "run \"startup\""
				<< std::endl;
			}
			else if (boost::algorithm::iequals(topic, "echo"))
			{
			}
			else if (boost::algorithm::iequals(topic, "ls") || boost::algorithm::iequals(topic, "dir"))
			{
			}
			else if (boost::algorithm::iequals(topic, "show") || boost::algorithm::iequals(topic, "s"))
			{
				std::cout
					<< "Available objects are: "
					<< std::endl
					<< "loc[als]"
					<< std::endl;
			}
			else if (boost::algorithm::iequals(topic, "help"))
			{
				std::cout
				<< "Display this help menu"
				<< std::endl;
			}
			else
			{
				std::cerr
					<< "***Warning (unknown command): "
					<< topic
					<< std::endl
					<< "use [q | run | echo | ls |  ip | history | show]"
					<< std::endl
					;	
			}
		}
		else 
		{
		
			std::cout
			<< "The following commands are available: "
			<< std::endl
			<< "q\t- quit application"
			<< std::endl
			<< "run\t- run a script"
			<< std::endl
			<< "echo\t- echo input line"
			<< std::endl
			<< "ls\t- list directory content" 
			<< std::endl
			<< "ip\t- list netword adapters" 
			<< std::endl
			<< "history\t- print last commands" 
			<< std::endl
			<< "show\t- show system values" 
			<< std::endl
			<< "login\t- start online session" 
			<< std::endl
			;	
		}
		
		return true;
	}
	
}	//	cyx
