/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_TOOLS_TEST_H
#define CYX_TOOLS_TEST_H

#include <cyy/util/shared/console.h>
//#include <cyy/util/read_sets.h>

namespace cyx
{
	class launch
	: public cyy::console
	{
	public:
		launch(std::string const&);
		virtual ~launch();
		
		int run();
		int run(std::string const&);

		static int create_config(std::string const& json_path);
		
	protected:		
		virtual bool parse(std::string const&);
		virtual void shutdown() override;

	private:
		void load_config(std::string const& json_path);
 		bool cmd_help(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end);
 		bool cmd_parse(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end);
		bool cmd_show(cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end);

	private:
	};
	
}	//	cyx

#endif	//	CYX_TOOLS_TEST_H
