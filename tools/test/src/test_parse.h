/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_TOOLS_TEST_PARSE_H
#define CYX_TOOLS_TEST_PARSE_H

#include <string>
#include <boost/filesystem.hpp>
#include <cyy/intrinsics/sets.hpp>

namespace cyx
{
	bool test_parse(boost::filesystem::path const& pin, cyy::tuple_t::const_iterator pos, cyy::tuple_t::const_iterator end);

}	//	cyx

#endif	//	CYX_TOOLS_TEST_PARSE_H
