/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_READER_H
#define CYX_DOCSCRIPT_READER_H

#include <cstdint>
#include <boost/filesystem.hpp>

namespace cyx
{
	class driver;
	class reader 
	{
	public:
		reader(driver&, boost::filesystem::path const&);
		bool run(std::size_t depth);

	private:
		boost::filesystem::path parse_include(std::string const& line);
		void tokenize(std::string const& );

	private:
		driver& driver_;
		boost::filesystem::path const& source_;
		std::size_t	line_;
	};
}

#endif	//	CYX_DOCSCRIPT_READER_H