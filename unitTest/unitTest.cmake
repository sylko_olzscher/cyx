# top level files
set (unit_test)

set (test_cpp

	unitTest/src/main.cpp
	unitTest/src/test_parser.cpp
	unitTest/src/test_serializer.cpp
)

set (test_h

	unitTest/src/test_parser.h
	unitTest/src/test_serializer.h
)


# define the unit test executable
set (unit_test
  ${test_cpp}
  ${test_h}
)
