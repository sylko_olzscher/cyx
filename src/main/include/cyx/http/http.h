/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_H
#define CYX_HTTP_H

#include <cstdint>
#include <string>

namespace cyx
{
	namespace methods 
	{

		/**
		 *	All supported HTTP methods
		 */
		enum method : std::uint32_t 
		{
			Unknown,
			Head,		//	See 'Get' but without the response body
			Get,		//	Requests a representation of the specified resource
			Post,		//	Submits data to be processed (e.g., from an HTML form)
			Put,		//	Uploads a representation of the specified resource - Update
			Delete,		//	Deletes the specified resource
			Trace,		//	Echoes back the received request
			Options,	//	Returns the HTTP methods that the server supports for specified URL
			Connect,	//	Converts the request connection to a transparent TCP/IP tunnel

			//	WebDAV
			Copy,		//	copy a resource from one URI to another
			Move,		//	move a resource from one URI to another
			Lock,		//	put a lock on a resource. WebDAV supports both shared and exclusive locks.
			Unlock,		//	remove a lock from a resource
			MkCol,		//	create collections (a.k.a. a directory)
			Propfind,	//	retrieve properties, stored as XML, from a web resource.
			Proppatch,	//	change and delete multiple properties on a resource in a single atomic act
		};

		/*
		 *	@return HTTP method name
		 */
		inline std::string name(method m)
		{
			switch (m)
			{
			case Unknown:	return "UNKNOWN";
			case Head:		return "HEAD";
			case Get:		return "GET";
			case Post:		return "POST";
			case Put:		return "PUT";
			case Delete:	return "DELETE";
			case Trace:		return "TRACE";
			case Options:	return "OPTIONS";
			case Connect:	return "CONNECT";
			case Copy:		return "COPY";
			case Move:		return "MOVE";
			case Lock:		return "LOCK";
			case Unlock:	return "UNLOCK";
			case MkCol:		return "MKCOL";
			case Propfind:	return "PROPFIND";
			case Proppatch:	return "PROPPATCH";

			default:
				break;
			}
			return "";
		}

	}	//	methods

    namespace codes
    {

		/**
         *	common HTTP response codes
         */
        enum status : std::uint32_t
        {
			//	100s: Informational
			Continue = 100,
			SwitchingProtocols = 101,
			Processing = 102,

			//	200s: Success
			OK = 200,
			Created = 201,
			Accepted = 202,
			NonAuthoritativeInformation = 203,
			NoContent = 204,
			ResetContent = 205,
			PartialContent = 206,
			MultiStatus = 207,
			AlreadyReported = 208,
			IMUsed = 226,

			//	300s: Redirection
			MultipleChoices = 300,
			MovedPermanently = 301,
			MovedTemporarily = 302,	//!<	originally temporary redirect, but now commonly used to specify redirection for unspecified reason
			SeeOther = 303,	//!<	e.g. for results of cgi-scripts
			NotModified = 304,
			UseProxy = 305,
			Unused = 306,
			TemporaryRedirect = 307,

			//	400s: Client Errors
			BadRequest = 400,
			Unauthorized = 401,
			PaymentRequired = 402,
			Forbidden = 403,
			NotFound = 404,
			MethodNotAllowed = 405,
			NotAcceptable = 406,
			ProxyAuthRequired = 407,
			RequestTimeout = 408,
			Conflict = 409,
			Gone = 410,
			LengthRequired = 411,
			PreconditionFailed = 412,
			RequestEntityTooLarge = 413,
			RequestUriTooLong = 414,
			UnsupportedMediaType = 415,
			RequestedRangeNotSatisfiable = 416,
			ExpectationFailed = 417,

			//	500s: Server Errors
			InternalServerError = 500,
			NotImplemented = 501,
			BadGateway = 502,
			ServiceUnavailable = 503,
			GatewayTimeout = 504,
			HttpVersionNotSupported = 505
		};

		/*
         *	@return HTTP method name
         */
        inline std::string name(status s)
        {
			switch (s)			{
				//	100s: Informational
			case Continue:	return "100 Continue";
			case SwitchingProtocols:	return "101 Switching Protocols";
			case Processing: return "102 Processing";

			case OK:	return "200 OK";
			case Created:	return "201 Created";
			case Accepted:	return "202 Accepted";
			case NonAuthoritativeInformation:	return "203 Non-Authoritative Information";
			case NoContent:	return "204 No Content";
			case ResetContent:	return "205 Reset Content";
			case PartialContent:	return "206 Partial Content";
			case MultiStatus:	return "207 Multi-Status ";
			case AlreadyReported:	return "208 Already Reported";
			case IMUsed:	return "226 IM Used";

					//	300s: Redirection
			case MultipleChoices:	return "300 Multiple Choices";
			case MovedPermanently:	return "301 Moved Permanently";
			case MovedTemporarily:	return "302 Found";
			case SeeOther:	return "303 See Other";
			case NotModified:	return "304 Not Modified";
			case UseProxy:	return "305 Use Proxy";
			case Unused:	return "306 Switch Proxy";
			case TemporaryRedirect:	return "307 Temporary Redirect";

					//	400s: Client Errors
			case BadRequest:	return "400 Bad Request";
			case Unauthorized:	return "401 Unauthorized";
			case PaymentRequired:	return "402 Payment Required";
			case Forbidden:	return "403 Forbidden";
			case NotFound:	return "404 Not Found";
			case MethodNotAllowed:	return "405 Method Not Allowed";
			case NotAcceptable:	return "406 Not Acceptable";
			case ProxyAuthRequired:	return "407 Proxy Authentication Required";
			case RequestTimeout:	return "408 Request Timeout";
			case Conflict:	return "409 Conflict";
			case Gone:	return "410 Gone";
			case LengthRequired:	return "411 Length Required";
			case PreconditionFailed:	return "412 Precondition Failed";
			case RequestEntityTooLarge:	return "413 Request Entity Too Large";
			case RequestUriTooLong:	return "414 Request-URI Too Long";
			case UnsupportedMediaType:	return "415 Unsupported Media Type";
			case RequestedRangeNotSatisfiable:	return "416 Requested Range Not Satisfiable";
			case ExpectationFailed:	return "417 Expectation Failed";

			//	500s: Server Errors
			case InternalServerError:	return "500 Internal Server Error";
			case NotImplemented:	return "501 Not Implemented";
			case BadGateway:	return "502 Bad Gateway";
			case ServiceUnavailable:	return "503 Service Unavailable";
			case GatewayTimeout:	return "504 Gateway Timeout";
			case HttpVersionNotSupported:	return "505 HTTP Version Not Supported";

			default:
				break;
			}
			return "";
		}


	}	//	end namespace codes

	/**
     *	All supported HTTP header types
     */
    namespace keys
    {
        enum key
        {
			Unknown,
			Accept,				//	"Accept"
			AcceptCharset,		//	"Accept-Charset"
			AcceptEncoding,		//	"Accept-Encoding"
			AcceptLanguage,		//	"Accept-Language"
			AcceptRanges,		//	"Accept-Ranges"
			Age,				//	"Age"
			Allow,				//	"Allow"
			Authorization,		//	"Authorization"
			CacheControl,		//	"Cache-Control"
			Connection,			//	"Connection"
			ContentEncoding,	//	"Content-Encoding"
			ContentDisposition, //	"Content-Disposition"
			ContentLanguage,	//	"Content-Language"
			ContentLength,		//	"Content-Length"
			ContentLocation,	//	"Content-Location"
			ContentMD5,			//	"Content-MD5"
			ContentRange,		//	"Content-Range"
			ContentType,		//	"Content-Type"
			Cookie,				//	"Cookie"
			Date,				//	"Date"
			ETag,				//	"ETag"
			Expect,				//	"Expect"
			Expires,			//	"Expires"
			From,				//	"From"
			Host,				//	"Host"
			IfMatch,			//	"If-Match"
			IfModifiedSince,	//	"If-Modified-Since"
			IfNoneMatch,		//	"If-None-Match"
			IfRange,			//	"If-Range"
			IfUnmodifiedSince,	//	"If-Unmodified-Since"
			KeepAlive,			//	"Keep-Alive"
			LastModified,		//	"Last-Modified"
			Location,			//	"Location"
			MaxForwards,		//	"Max-Forwards"
			Pragma,				//	"Pragma"
			ProxyAuthenticate,	//	"Proxy-Authenticate"
			ProxyAuthorization, //	"Proxy-Authorization"
			ProxyConnection,	//	"Proxy-Connection"
			Range,				//	"Range"
			Referer,			//	"Referer"
			RetryAfter,			//	"Retry-After"
			Server,				//	"Server"
			SetCookie,			//	"Set-Cookie"
			TE,					//	"TE"
			Trailer,			//	"Trailer"
			TransferEncoding,	//	"Transfer-Encoding"
			Upgrade,			//	"Upgrade"
			UserAgent,			//	"User-Agent"
			Vary,				//	"Vary"
			Via,				//	"Via"
			Warning,			//	"Warning"
			WWWAuthenticate,	//	"WWW-Authenticate"
			XPoweredBy,			//	"X-Powered-By"
		};
	}	//	end namespace keys

	/**
     *	auth-scheme
     */
    namespace auth
    {
        enum scheme
        {
			BASIC,		//!<	 RFC 2617
			DIGEST,		//!<	 RFC 2617
			COOKIE,
			NONE,
		};
	}

}

#endif	//	CYX_HTTP_H
