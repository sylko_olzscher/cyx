/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_AUTH_TYPE_H
#define CYX_HTTP_AUTH_TYPE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200) 

#include <cyx/http/http.h>
#include <cyx/http/uri.hpp>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include <boost/algorithm/string/predicate.hpp>

namespace cyx	
{
	/**
	 *	From client.
	 *	@see http://tools.ietf.org/html/rfc2617#section-3.5
	 */
	struct authentification
	{
		enum qop_enum 
		{
			AUTH,		//!<	"auth"
			AUTH_INT,	//!<	"auth-int"
			TOKEN,		//!<	"token"
			AUTH_CONF,	//!<	"auth-conf"
			OTHER,
		};

		/**
		*	Default constructor
		*/
		authentification()
			: scheme_(auth::BASIC)
			, credentials_()
			, user_()
			, realm_()
			, nonce_()
			, uri_()
			, algorithm_("MD5")
			, response_()
			, opaque_()
			, qop_(authentification::OTHER)
			, nc_(0)
			, cnonce_()
		{}

		/**
		 *	Copy constructor
		 */
		authentification(authentification const& a)
			: scheme_(a.scheme_)
			, credentials_(a.credentials_)
			, user_(a.user_)
			, realm_(a.realm_)
			, nonce_(a.nonce_)
			, uri_(a.uri_)
			, algorithm_(a.algorithm_)
			, response_(a.response_)
			, opaque_(a.opaque_)
			, qop_(a.qop_)
			, nc_(a.nc_)
			, cnonce_(a.cnonce_)
		{}


		/**
		 *	assignment
		 */
		authentification& operator=(authentification const& a) 
		{
			if (this != &a) 
			{
				scheme_ = a.scheme_;
				credentials_ = a.credentials_;
				user_ = a.user_;
				realm_ = a.realm_;
				nonce_ = a.nonce_;
				uri_ = a.uri_;
				algorithm_ = a.algorithm_;
				response_ = a.response_;
				opaque_ = a.opaque_;
				qop_ = a.qop_;
				nc_ = a.nc_;
				cnonce_ = a.cnonce_;
			}
			return *this;
		}

		auth::scheme		scheme_;
		std::string			credentials_;	//!<	base64 encoded
		std::string 		user_;
		std::string 		realm_;
		std::string 		nonce_;			//!<	n-umber once
		uri					uri_;
		std::string			algorithm_;		//!<	MD5 always
		std::string 		response_;
		std::string 		opaque_;
		qop_enum 			qop_;
		std::uint32_t		nc_;
		std::string 		cnonce_;
	};

	inline void clear(authentification& a) 
	{

		a.scheme_ = auth::BASIC;
		a.credentials_.clear();
		a.user_.clear();
		a.realm_.clear();
		a.nonce_.clear();
		a.uri_.path_.clear();
		a.uri_.query_.clear();
		a.uri_.fragment_.clear();
		a.response_.clear();
		a.opaque_.clear();
		a.qop_ = authentification::OTHER;
		a.nc_ = 0;
		a.cnonce_.clear();
	}

}	//	cyx

#endif	//	CYX_HTTP_AUTH_TYPE_H
