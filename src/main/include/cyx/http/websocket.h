/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_WEBSOCKET_H
#define CYX_HTTP_WEBSOCKET_H

/** @file websocket.h
 * 
 * @code
  0                               1                               2
  0 1 2 3 4 5 6 7 8 9 A B C D E F 0 1 2 3 4 5 6 7 8 9 A B C D E F
 +-+-+-+-+-------+-+-------------+-------------------------------+
 |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
 |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
 |N|V|V|V|       |S|             |   (if payload len==126/127)   |
 | |1|2|3|       |K|             |                               |
 +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
 |     Extended payload length continued, if payload len == 127  |
 + - - - - - - - - - - - - - - - +-------------------------------+
 |                               |Masking-key, if MASK set to 1  |
 +-------------------------------+-------------------------------+
 | Masking-key (continued)       |          Payload Data         |
 +-------------------------------- - - - - - - - - - - - - - - - +
 :                     Payload Data continued ...                :
 + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
 |                     Payload Data continued ...                |
 +---------------------------------------------------------------+
 * @endcode
 *
 * The most significant bit is the leftmost in the ABNF
 */

#if defined(_MSC_VER)
#pragma once
#pragma pack(push,1)
#endif 

#include <cstdint>
//#include <boost/endian/buffers.hpp>	//	! check this

namespace cyx
{
	/**
	 *	Define the header according to RFC 6455
	 */
	struct 
#if !defined(_MSC_VER)
		__attribute__((__packed__))
#endif
	rfc6455_header 
	{
		//	see https://github.com/vinniefalco/Beast/blob/master/include/beast/websocket/detail/frame.hpp

		//
		//	first byte
		//	Note the reversed order of the byte layout (network byte order).The first bit in memory (bit 0)
		//	is the last on specified in this bitfield.
		//
		std::uint8_t opcode : 4;	//	see enum opcode
		std::uint8_t rsv : 3;	//	reserved
		std::uint8_t fin : 1;

		//
		//	second byte
		//	Note the reversed order of the byte layout
		//
		std::uint8_t length : 7;
		std::uint8_t mask : 1;		//	has to be one
	};

	union rfc6455_header_overlay
	{
		rfc6455_header	header_;
		char overlay_[2];
		static_assert(sizeof(rfc6455_header) == 2, "wrong header size");
	};


	namespace ws
	{
		enum /*class*/ opcode : std::uint8_t
		{
			cont	= 0x0,		//	denotes a continuation frame
			text	= 0x1,		//	denotes a text frame
			binary	= 0x2,		//	denotes a binary frame
			rsv1	= 0x3,		//	3- 7 are reserved for further non - control frames
			rsv2	= 0x4,		//	3- 7 are reserved for further non - control frames
			rsv3	= 0x5,		//	3- 7 are reserved for further non - control frames
			rsv4	= 0x6,		//	3- 7 are reserved for further non - control frames
			rsv5	= 0x7,		//	3- 7 are reserved for further non - control frames
			close	= 0x8,		// denotes a connection close
			ping	= 0x9,		// denotes a ping
			pong	= 0xa,		// denotes a pong
			crsvb	= 0xb,		//	B - F are reserved for further control frames
			crsvc	= 0xc,
			crsvd	= 0xd,
			crsve	= 0xe,
			crsvf	= 0xf
		};

		/*
		*	@retur nwebsocket opcode name
		*/
		inline std::string name(std::uint8_t c)
		{
			switch (c)
			{
			case cont:		return "CONTINUATION";
			case text:		return "TEXT";
			case binary:	return "BINARY";
			case close:		return "CLOSE";
			case ping:		return "PING";
			case pong:		return "PONG";

			default:
				break;
			}
			return "RESERVED";
		}

	}

	namespace close_code
	{
		enum close_code
		{
			/// used internally to mean "no error"
			none = 0,

			normal = 1000,
			going_away = 1001,
			protocol_error = 1002,

			unknown_data = 1003,

			/// Indicates a received close frame has no close code
			//no_code         = 1005, // TODO

			/// Indicates the connection was closed without receiving a close frame
			no_close = 1006,

			bad_payload = 1007,
			policy_error = 1008,
			too_big = 1009,
			needs_extension = 1010,
			internal_error = 1011,

			service_restart = 1012,
			try_again_later = 1013,

			reserved1 = 1004,
			no_status = 1005, // illegal on wire
			abnormal = 1006, // illegal on wire
			reserved2 = 1015,

			last = 5000 // satisfy warnings
		};
	}
}

#if defined(_MSC_VER)
#pragma pack(pop)
#endif


#endif	//	CYX_HTTP_WEBSOCKET_H
