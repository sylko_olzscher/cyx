/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_RESPONSE_H
#define CYX_HTTP_RESPONSE_H

#include <cyx/http/http.h>
#include <cyx/http/uri.hpp>
#include <cyx/http/content_type.hpp>
#include <cyy/intrinsics/version.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/crypto/md5.h>
#include <utility>
#include <initializer_list>
#include <boost/uuid/uuid.hpp>

namespace cyx
{
	//	258EAFA5-E914-47DA-95CA-C5AB0DC85B11
	constexpr boost::uuids::uuid MAGIC_WEBSOCKET_KEY =
	{ 0x25, 0x8E, 0xAF, 0xA5
		, 0xE9, 0x14
		, 0x47, 0xDA
		, 0x95, 0xCA
		, 0xC5, 0xAB, 0x0D, 0xC8, 0x5B, 0x11
	};


	//	forward declaration(s):
	struct authentification;

	/**
     * HTTP response
     */
    struct response
	{

		response();
		response(codes::status);
		response(codes::status, std::string&&);

        cyy::version		version_;
		codes::status		status_;
		cyy::param_map_t	params_;
		std::string			body_;
	};


    /**
     * @brief reset HTTP response
     */
    void clear(response&);

	/**
	 * @brief Set "X-Powered-By" header attribute
	 */
	void powered_by(cyy::param_map_t& h, std::string const& engine);
	void powered_by(cyy::param_map_t& h);

	/**
	 * @brief Set "Content-Length" header attribute
	 */
	void content_length(cyy::param_map_t&, std::size_t);

	/**
	 * @brief Set "Content-Type" header attribute
	 */
	void content_type(cyy::param_map_t& h, std::string const& type);
	void content_type(cyy::param_map_t& h, mime_content_type const& type);

	/**
	 * @brief Set "Last-Modified" header attribute
	 */
	void last_modified(cyy::param_map_t& h, std::chrono::system_clock::time_point const& modified);

	/**
	 * set "Location" header attribute
	 */
	void location(cyy::param_map_t& h, std::string const& loc);

	/**
	 * @brief Set "ETag" header attribute
	 */
	void etag(cyy::param_map_t& h, cyy::crypto::digest_md5::value_type const&);

	/**
	 * @brief Switch to websocket protocol
	 */
	void accept_websocket(cyy::param_map_t& h
		, std::string const& key
		, std::string const& protocol);

	/**
	 * Calculate websocket hash
	 */
	std::string calculate_websocket_hash(std::string const& key);

	/**
	 * Basic authentification
	 */
	void basic_auth(cyy::param_map_t& h, std::string const& realm);

	/**
	 * example:
	 * @code
	 WWW-Authenticate: Digest
		realm="http-auth@example.org",
		qop="auth, auth-int",
		algorithm=SHA-256,
		nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v",
		opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"
	* @endcode
	*/
	void digest_auth(cyy::param_map_t& h
		, std::string const& realm
		, std::string const& algo
		, std::string const& nonce
		, std::string const opaque);

	/**
	 * Set connection type "keep-alive"
	 */
	void keep_alive(cyy::param_map_t& h);

	/**
	 * Set allowed methods OPTIONS, GET, HEAD, POST
	 */
	void allow(cyy::param_map_t& h);

	/**
	 * Set allowed methodsas specified
	 */
	void allow(cyy::param_map_t& h, std::initializer_list<methods::method>);

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::response>(cyx::response&, cyx::response&);

}

#endif	//	CYX_HTTP_RESPONSE_H


