/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_HEADER_PARSER_H
#define CYX_HTTP_HEADER_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/parser/basics_parser.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/object.h>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/parser/numeric_parser.h>
#include <cyy/parser/token_parser.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx
{

    template <typename Iterator>
    struct header_parser : boost::spirit::qi::grammar<Iterator, cyy::param_t()>
    {
        header_parser();

        boost::spirit::qi::rule<Iterator, cyy::param_t() >	r_dnt, r_uireq, r_pair, r_start;
		header_field_strict<Iterator>	r_key;
        boost::spirit::qi::rule<Iterator, boost::fusion::vector<std::string, cyy::object>()> r_param, r_length, r_ws_version;
        boost::spirit::qi::rule<Iterator, cyy::object()>	r_value;
        boost::spirit::qi::rule<Iterator, std::string()>    r_string;
		cyy::io::numeric_parser<Iterator> 	r_int;
		//	read a comma separated vector of literals
		boost::spirit::qi::rule<Iterator, std::vector<std::string>() >		r_list;
		//	accepts all typical variable names, starting with a alphabetic character
		//	followed by alphanumeric characters, dots and - signs.
        cyy::io::directive_parser<Iterator>    r_token;
		boost::spirit::qi::rule<Iterator>	r_sep, r_def;
		boost::spirit::qi::rule<Iterator, boost::fusion::vector<std::string, std::vector<std::string>>()>  r_ws_protocols, r_encodings, r_connection;

    };

    bool get_http_header(cyy::buffer_t const&, cyy::param_t& param);

}	//	cyx


#endif	//	CYX_HTTP_HEADER_PARSER_H

