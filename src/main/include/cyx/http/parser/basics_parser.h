/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_BASICS_PARSER_H
#define CYX_HTTP_BASICS_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx
{
	/**
	 *	Parse a hexadecimal string like %AE as ASCII character
	 */
	template <typename Iterator>
	struct escape_parser : boost::spirit::qi::grammar< Iterator, char() >
	{
		escape_parser();

		boost::spirit::qi::rule<Iterator, char() >	r_start;
		boost::spirit::qi::uint_parser<char, 16, 2, 2>		r_hex;
	};

	//	<CR> <NL> sequence
    template <typename Iterator>
    struct line_separator : boost::spirit::qi::grammar< Iterator >
    {
		line_separator();

        boost::spirit::qi::rule<Iterator>			r_start;
    };

	/**
	 * white spaces: SPACE and TAB (no CR and LF)
	 * Doesn't synthesize an attribute
	 */
	template <typename Iterator>
	struct white_space : boost::spirit::qi::grammar< Iterator >
	{
		white_space();
		boost::spirit::qi::rule<Iterator>			r_start;
	};

	/**
	 * All ASCII codes from 0x20 (SPACE) to 0x7e (~)
	 * Synthesize a single character
	 */
	template <typename Iterator>
	struct printable : boost::spirit::qi::grammar< Iterator, char()>
	{
		printable();
		boost::spirit::qi::rule<Iterator, char()>	r_start;
	};

	/**
	 * Quoted (special) characters. Starting with a backslash '\'
	 * Supported values are \n, \r, \v, \b, \t
	 */
	template <typename Iterator>
	struct quoted_pair : boost::spirit::qi::grammar< Iterator, char()>
	{
		quoted_pair();
		boost::spirit::qi::rule<Iterator, char()>	r_start, r_char;
	};

	/**
	 * HTTP header field. Accepts all printable characters except the ':' character.
	 * There is no space allowed between the token and the ':' character.
	 * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
	 */
	template <typename Iterator>
	struct header_field : boost::spirit::qi::grammar< Iterator, std::string()>
	{
		header_field();
		boost::spirit::qi::rule<Iterator, std::string()>	r_start;
		printable<Iterator>	r_printable;
	};

	/**
	 * HTTP header field. Accepts all printable characters except the ':' character
	 * and other separators.
	 * There is no space allowed between the token and the ':' character.
	 * @code
	 separators = "(" | ")" | "<" | ">" | "@"
	 | "," | ";" | ":" | "\" | <">
	 | "/" | "[" | "]" | "?" | "="
	 | "{" | "}" | SP | HT
	 * @endcode
	 */
	template <typename Iterator>
	struct header_field_strict : boost::spirit::qi::grammar< Iterator, std::string()>
	{
		header_field_strict();
		boost::spirit::qi::rule<Iterator, std::string()>	r_start;
		printable<Iterator>	r_printable;
	};

	//	UTF-8 encoded file name
	//	decodes strings starting with utf-8''...
	//	utf-8 encoded file name (filename*=utf-8''%e2%82%ac%20rates)
	template <typename Iterator>
	struct utf8_filename : boost::spirit::qi::grammar< Iterator, std::string() >
	{
		utf8_filename();

		boost::spirit::qi::rule<Iterator, std::string()>	r_start, r_name;
		escape_parser<Iterator>	r_esc_;
	};

	//	quoted string (filename="EURO rates";)
	template <typename Iterator>
	struct quoted_string : boost::spirit::qi::grammar< Iterator, std::string() >
	{
		quoted_string();

		boost::spirit::qi::rule<Iterator, std::string()>	r_start, r_name;
		escape_parser<Iterator>	r_esc_;
	};

	/**	comment
	 *	syntax:
	 * @code
	 comment        = "(" *( ctext | quoted-pair | comment ) ")"
	 ctext          = <any TEXT excluding "(" and ")">
	 * @endcode
	 */

	template <typename Iterator>
	struct comment : boost::spirit::qi::grammar< Iterator, std::string() >
	{
		comment();
		boost::spirit::qi::rule<Iterator, std::string()>	r_start, r_string, r_ctext;
		//boost::spirit::qi::rule<Iterator, char()>	r_ctext;
		printable<Iterator>	r_printable;
		quoted_pair<Iterator>	r_qp;	//	characters starting with backslash '\'
	};



}	//	cyx


#endif	//	CYX_HTTP_BASICS_PARSER_H
