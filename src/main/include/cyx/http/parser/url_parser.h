/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
/**	@file url_parser.hpp
*
*	Parsing an URL path like
*	@code
/over/there/index.dtb?type=animal&name=narwhal#nose
           \___/ \_/ \______________________/ \__/
             |    |            |                |
             |    |          query          fragment
\____________|____|/
        |    |    |
      path   |    |
             |    |
 interpretable as filename
                  |
                  |
    interpretable as extension
*	@endcode
*
*	The query is an optional part separated with a question mark, which contains additional
*	identification information which is not hierarchical in nature.
*/

#ifndef CYX_HTTP_URL_PARSER_H
#define CYX_HTTP_URL_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/uri.hpp>
#include <cyx/http/parser/path_parser.h>
#include <cyy/intrinsics/buffer.hpp>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx
{

    template <typename Iterator>
    struct url_parser : boost::spirit::qi::grammar<Iterator, uri()>
    {
        url_parser();

        boost::spirit::qi::rule<Iterator, uri() >			r_start;
        boost::spirit::qi::rule<Iterator, path_t() >		path_;
        boost::spirit::qi::rule<Iterator, std::string() >	r_frag;
        query_parser<Iterator>								r_query;
        path_parser<Iterator>								r_path_;
        uri_char_parser<Iterator>							r_char_;
    };

    template <typename Iterator>
    struct url_parser_string : boost::spirit::qi::grammar< Iterator, uri() >
    {
        url_parser_string();

        boost::spirit::qi::rule<Iterator, uri() >			r_start;
        url_parser< Iterator >	r_url;
    };

    /**
     * @brief get_http_version
     * @param inp input buffer
     * @param u result
     * @return true if parsing completed without errors
     */
    bool get_http_version(cyy::buffer_t const& inp, uri& u);


}	//	cyx


#endif	//	CYX_HTTP_URL_PARSER_H
