/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_PATH_PARSER_H
#define CYX_HTTP_PATH_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/uri.hpp>
#include <cyx/http/path.h>
#include <cyx/http/parser/basics_parser.h>
#include <cyy/parser/token_parser.h>

namespace cyx
{

    /**
     * Parse a valid URI character
     */
    template <typename Iterator>
    struct uri_char_parser
        : boost::spirit::qi::grammar< Iterator, char() >
    {
        uri_char_parser();

         boost::spirit::qi::rule<Iterator, char() >	r_start, r_char;
         // all valid characters in an URI string
         escape_parser<Iterator>					r_esc;
    };

    /**
     * Parse the query segement of a path into a vector of strings
     */
    template <typename Iterator>
    struct query_parser
        : boost::spirit::qi::grammar<Iterator, query_t()>
    {
        query_parser();
        boost::spirit::qi::rule<Iterator, query_t()>		r_start;
        boost::spirit::qi::rule<Iterator, opt_string_t()>	r_pair;
        boost::spirit::qi::rule<Iterator, std::string()>	r_val;
		cyy::io::identifier_parser<Iterator>		r_key;
        escape_parser<Iterator>			r_esc;
    };

    /**
     *	Converts a given string into a path_segment. Delimiters are
     *	slash (/) and backslash (\).
     */
    template <typename Iterator>
    struct path_parser
        : boost::spirit::qi::grammar< Iterator, path_t() >
    {
        path_parser();
        boost::spirit::qi::rule<Iterator, path_t() >		r_start, path_, segments_;
        boost::spirit::qi::rule<Iterator, std::string() >	segment_;
        boost::spirit::qi::rule<Iterator>					separator_;
        uri_char_parser<Iterator>							uri_char_;
    };

    template <typename Iterator>
    struct path_string_parser
        : boost::spirit::qi::grammar< Iterator, path_t() >
    {
        path_string_parser();
        boost::spirit::qi::rule<Iterator, path_t() >	r_start;
        path_parser< Iterator >	path_p;
    };

    path_t parse_path(std::string const& str);

}	//	cyx


#endif	//	CYX_HTTP_PATH_PARSER_H
