/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_AUTH_PARSER_H
#define CYX_HTTP_AUTH_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/auth_type.hpp>
#include <cyx/http/parser/basics_parser.h>
#include <cyx/http/parser/url_parser.h>
#include <cyy/parser/token_parser.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx
{
	template <typename Iterator>
	struct auth_parser : boost::spirit::qi::grammar<Iterator, authentification()>
	{
		auth_parser();

		boost::spirit::qi::rule<Iterator, authentification()>	r_start;
		boost::spirit::qi::rule<Iterator, std::string()>		r_basic;
		boost::spirit::qi::rule<Iterator, std::string()>		r_username
			, r_realm
			, r_nonce
			, r_algo
			, r_response
			, r_opaque
			, r_cnonce
			;
		boost::spirit::qi::rule<Iterator, uri()>	r_uri;
		url_parser_string<Iterator>	r_uri_string;
		cyy::io::directive_parser<Iterator>    r_token;
		quoted_string<Iterator>	r_quote;
		boost::spirit::qi::rule<Iterator, unsigned()>	r_nc;
		boost::spirit::qi::rule<Iterator, authentification::qop_enum()>			r_qop;

	};

	std::pair<authentification, bool>	parse_auth(std::string const&);

}	//	cyx


#endif	//	CYX_HTTP_AUTH_PARSER_H
