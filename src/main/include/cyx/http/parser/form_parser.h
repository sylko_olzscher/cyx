/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_FORM_PARSER_H
#define CYX_HTTP_FORM_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/uri.hpp>
#include <cyx/http/parser/path_parser.h>
//#include <cyx/http/parser/basics_parser.h>
#include <cyy/parser/token_parser.h>
#include <cyy/intrinsics/buffer.hpp>


namespace cyx
{

	/**
	 *	The query string syntax
	 *	is not generically defined, but is commonly organized as a sequence of <key>=<value> pairs
	 *	separated by a semicolon or separated by an ampersand, for example:
	 *	@code
	 Semicolon: key1=value1;key2=value2;key3=value3
	 Ampersand: key1=value1&key2=value2&key3=value3
	 *	@endcode
	 *
	 *	 Within a query component, the characters ";", "/", "?", ":", "@",
	 *   "&", "=", "+", ",", and "$" are reserved.
	 *
	 * x-www-form-urlencoded encoded content can start with a & (ampersand)
	 */
	template <typename Iterator>
    struct form_parser : boost::spirit::qi::grammar< Iterator, query_t() >
    {
		form_parser();

        boost::spirit::qi::rule<Iterator, query_t() >		r_start;
		boost::spirit::qi::rule<Iterator, opt_string_t()>	r_pair;
		boost::spirit::qi::rule<Iterator, std::string()>	r_value;
		boost::spirit::qi::rule<Iterator, void()>			r_sep;
		cyy::io::identifier_parser<Iterator>	r_key;	//	boost::spirit::qi::char_("a-zA-Z_") >> *boost::spirit::qi::char_("a-zA-Z_.0-9")
		escape_parser<Iterator>	r_esc_;
	};


    bool parse_form(cyy::buffer_t const& input, query_t& result);

}	//	cyx


#endif	//	CYX_HTTP_FORM_PARSER_H
