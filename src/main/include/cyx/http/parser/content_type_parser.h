/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_CONTENT_TYPE_PARSER_H
#define CYX_HTTP_CONTENT_TYPE_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/content_type.hpp>
#include <cyy/intrinsics/buffer.hpp>
#include <cyx/http/parser/basics_parser.h>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx	
{
	template <typename Iterator>
	struct mime_content_type_parser : boost::spirit::qi::grammar<Iterator, mime_content_type()>
	{
		mime_content_type_parser();

		boost::spirit::qi::rule<Iterator, mime_content_type()> content_type_header;
		boost::spirit::qi::rule<Iterator, param_t()> r_phrase;
		boost::spirit::qi::rule<Iterator, std::string()> part, sub_part, r_token, r_attr, value, extension_token;
		boost::spirit::qi::rule<Iterator> ws_;
		line_separator<Iterator>	r_line_sep;
		white_space<Iterator>	r_white_space;
		quoted_pair<Iterator>	r_qp;	//	all quoted symbols like \t
		comment<Iterator>	r_comment;
	};

	bool get_http_mime_type(std::string const& inp, mime_content_type&);
	bool get_http_mime_type(cyy::buffer_t const& inp, mime_content_type&);


}	//	m2m
#endif	//	CYX_HTTP_CONTENT_TYPE_PARSER_H
