/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_CONTENT_DISPOSITION_PARSER_H
#define CYX_HTTP_CONTENT_DISPOSITION_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/content_disposition.hpp>
#include <cyy/intrinsics/buffer.hpp>
#include <cyx/http/parser/basics_parser.h>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>

namespace cyx	
{
	template <typename Iterator>
	struct content_disposition_parser : boost::spirit::qi::grammar<Iterator, content_disposition()>
	{
		content_disposition_parser();

		boost::spirit::qi::rule<Iterator, content_disposition()> r_start;
		boost::spirit::qi::rule<Iterator, param_t()> r_phrase;
		boost::spirit::qi::rule<Iterator, std::string()> r_token, r_value;
		boost::spirit::qi::rule<Iterator> ws_;
		quoted_string<Iterator>	r_quoted_string;
		white_space<Iterator>	r_ws;
		utf8_filename<Iterator>	r_ut8_filename;
	};

	bool parse_content_disposition(std::string const& inp, content_disposition&);
	bool parse_content_disposition(cyy::buffer_t const& inp, content_disposition&);


}	//	cyx
#endif	//	CYX_HTTP_CONTENT_DISPOSITION_PARSER_H
