/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_COOKIE_PARSER_H
#define CYX_HTTP_COOKIE_PARSER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/http.h>
#include <map>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <cyx/http/parser/basics_parser.h>


namespace cyx	
{

	/**
 	 *	Cookie parser.
	 */
	template <typename Iterator>
	struct cookie_parser
		: boost::spirit::qi::grammar<Iterator, std::multimap<std::string, std::string>()>
	{
		typedef std::multimap<std::string, std::string>	pairs_type;
		typedef std::pair<std::string, std::string>	pair_type;

		cookie_parser();
		boost::spirit::qi::rule<Iterator, pairs_type()>		cookie_;
		boost::spirit::qi::rule<Iterator, pair_type()>		pair_;
		boost::spirit::qi::rule<Iterator, std::string()>	key_, value_, r_token;
		quoted_string<Iterator>		r_quoted_string;
	};

}	//	cyx

#endif	//	CYX_HTTP_COOKIE_PARSER_H


