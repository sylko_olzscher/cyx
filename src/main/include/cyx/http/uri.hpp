/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_URI_HPP
#define CYX_HTTP_URI_HPP

#include <cyx/http/http.h>
#include <cyx/http/path.h>
//#include <optional>   //  since C++17
#include <boost/optional.hpp>
#include <vector>
#include <algorithm>
#include <utility>

namespace cyx
{
    typedef std::pair<std::string, boost::optional<std::string>>    opt_string_t;

    /**
     * @brief define a vector of pairs of strings. The second string is optional.
	 * Used for x-www-form-urlencoded encoded (forms) too.
     */
    typedef std::vector<opt_string_t>   query_t;

    /**
     * @brief lookup
     * @return optional string of the specified key
     */
    inline boost::optional<std::string> lookup(query_t const& q, std::string const& key)
    {
        auto pos = std::find_if(q.begin(), q.end()
            , [&key](opt_string_t const& p){
            return p.first == key;
        });

        return (pos != q.end())
            ? pos->second
            : boost::optional<std::string>()
            ;
    }

	inline std::string to_str(query_t const& q)
	{
		bool flag = false;
		std::string s(1, '?');
		for (auto const& e : q)
		{
			if (flag)
			{
				s += '&';
			}
			else
			{
				flag = true;
			}

			s += e.first;
			s += '=';

			if (e.second)
			{
				s += *e.second;
			}
		}
		return s;
	}

    /**
     *	@code
     URI = scheme ":" hier-part [ "?" query ] [ "#" fragment ]
     *	@endcode
     *
     *	@code
       foo://example.com:8042/over/there?name=ferret#nose
       \_/   \______________/\_________/ \_________/ \__/
        |           |            |            |        |
     scheme     authority       path        query   fragment
        |   _____________________|__
       / \ /                        \
       urn:example:animal:ferret:nose
     *	@endcode
     *
     *	This URI representation is incomplete and doesn't contain the scheme (protocol) and the authority
     *	information. But since these data aren't available for the HTTP parser they are omitted.
     */
    struct uri
    {
        uri()
            : path_()
            , query_()
            , fragment_()
        {}

        /**
         *	Copy constructor
         */
        uri(uri const& r)
            : path_(r.path_)
            , query_(r.query_)
            , fragment_(r.fragment_)
        {}

        /**
         * assignment
         */
        uri& operator=(uri const& r)
        {
            if (this != &r)
            {
                path_ = r.path_;
                query_ = r.query_;
                fragment_ = r.fragment_;
            }
            return *this;
        }

        mutable path_t		path_;		//!<	URL path, interpretable as filename
        query_t             query_;		//!<	attribute value pairs
        std::string			fragment_;

    };
}

namespace std
{
    template <>
    inline void swap<cyx::uri>(cyx::uri& rl, cyx::uri& rr)
    {
        std::swap(rl.path_, rr.path_);
        std::swap(rl.query_, rr.query_);
        std::swap(rl.fragment_, rr.fragment_);
    }

}

#endif	//	CYX_HTTP_URI_HPP
