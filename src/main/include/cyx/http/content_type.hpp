/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_CONTENT_TYPE_H
#define CYX_HTTP_CONTENT_TYPE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200) 

#include <cyx/http/http.h>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include <boost/algorithm/string/predicate.hpp>

namespace cyx	
{

	//	parameter types
	typedef std::pair<std::string, std::string> param_t;
	typedef std::vector < param_t >				param_container_t;

	inline std::string lookup_param(param_container_t const& phrases, std::string const& name) 
	{
		auto pos = std::find_if(phrases.begin(), phrases.end(), [&name](param_t const& p) 
		{
			return (boost::algorithm::iequals(p.first, name));
		});

		return (pos != phrases.end())
			? pos->second
			: ""
			;
	}

	//	MIME Content-Type

	/**
	 * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
	 *
	 * example: 
	 * @code
	 * Content-Type: text/html; charset=ISO-8859-4
	 * @endcode
	 */
	struct mime_content_type
	{
		std::string			type_;
		std::string 		sub_type_;
		param_container_t	phrases_;

		mime_content_type()
			: type_()
			, sub_type_()
			, phrases_()
		{}

		mime_content_type(std::string const& type
			, std::string const& subtype
			, cyx::param_container_t const& phrases)
			: type_(type)
			, sub_type_(subtype)
			, phrases_(phrases)
		{}
	};

	inline mime_content_type make_mime_type(std::string const& type, std::string const& subtype)
	{
		const cyx::param_container_t tmp;
		return mime_content_type(type, subtype, tmp);
	}

	inline bool is_matching(mime_content_type const& mct
		, std::string const& type
		, std::string const& subtype)	
	{

		return boost::algorithm::iequals(mct.type_, type)
			&& boost::algorithm::iequals(mct.sub_type_, subtype)
			;

	}

	inline std::string to_str(mime_content_type const& mct)	
	{

		//	serialize content type
		std::stringstream ss;
		ss
			<< mct.type_
			<< '/'
			<< mct.sub_type_
			;

		std::for_each(mct.phrases_.begin(), mct.phrases_.end(), [&ss](param_t const& p){
			ss
				<< ';'
				<< p.first
				<< '='
				<< p.second
				;
		});

		return ss.str();
	}

	/*
	 *	The Content-Type field for multipart entities requires one parameter, 
	 *	"boundary", which is used to specify the encapsulation boundary. 
	 *	The encapsulation boundary is defined as a line consisting entirely 
	 *	of two hyphen characters ("-", decimal code 45) followed by the boundary 
	 *	parameter value from the Content-Type header field.
	 */
	inline std::string get_boundary(param_container_t const& phrases)	
	{
		return std::string("--") + lookup_param(phrases, "boundary");
	}

}	//	cyx

#endif	//	CYX_HTTP_CONTENT_TYPE_H
