/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_TYPE_CODES_H
#define CYX_TYPE_CODES_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/intrinsics/type_codes.h>

namespace cyx 
{
	namespace types
	{
		typedef enum : cyy::code_t
		{
			HTTP_TYPES = cyy::types::CYY_CUSTOM_TYPES,	//!<	start M2M data types here
			
			URL_PATH,		//!<	path_t
			QUERY_PATH,		//!<	vector of optional strings
			HTTP_METHOD,	//!<	a HTTP  method (enum)
			HTTP_STATUS,	//!<	a HTTP  status (enum)
			COOKIE,			//!<	cookies v1/2
			MIME_TYPE,

			//	
			//	the following type codes are reused in different context
			//
			SHARED_SESSION,	//!<	allows to send a shared session to a task
			WEAK_SESSION,	//!<	allows to store a weak session pointer in a database

			OTHER_TYPES,	//!<	start other data types here
		}	type_code;
	}
}	//	cyx

#endif // CYX_TYPE_CODES_H
