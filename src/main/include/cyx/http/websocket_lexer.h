/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_WEBSOCKET_LEXER_H
#define CYX_HTTP_WEBSOCKET_LEXER_H

#include <cyx/http/request.h>
#include <cyx/http/lexer_states.h>
#include <cyx/http/websocket.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/crypto/scrambler.hpp>

namespace cyx
{
	class websocket_lexer
	{
		using scrambler = cyy::crypto::scrambler<char, 4>;

	public:
		websocket_lexer(std::function<void(cyy::vector_t&&)> cb, logger_type&, cyy::buffer_t& buffer);

		/**
		 * Put next available character into state machine
		 *
		 * @return next lexer state.
		 */
		void parse(char c);

		void set_version(int);
		void set_key(std::string const&);

	private:
		void msg_complete();

	private:
		enum ws_enum
		{
			ws_open,
			ws_length_16bit,
			ws_length_64bit,
			ws_mask,
			ws_data,

		}	state_;

		/**
		 * completion callback
		 */
		std::function<void(cyy::vector_t&&)> cb_;

		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;

		cyy::buffer_t&	buffer_;	//!<	hold temporary data

		bool	mask_;
		std::uint64_t	size_;
		ws::opcode	code_;

		/**
		 * descramble masked data
		 */
		scrambler	scrambler_;

	};
}
#endif // CYX_HTTP_WEBSOCKET_LEXER_H
