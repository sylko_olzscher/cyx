/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_SERIALIZER_H
#define CYX_HTTP_SERIALIZER_H

#include <cyx/http/response.h>
#include <cyy/intrinsics/sets_fwd.h>
#include <cyy/store/store.h>
#include <functional>
#include <algorithm>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

namespace cyy 
{
	class vm_dispatcher;
	namespace store
	{
		class store;
	}
}

namespace cyx
{
	/**
	 * Microsoft IE and Edge don't display short error messages. Disable the "advanced" option "Show friendly HTTP error messages"
	 * to get the expected output.
	 */
	class serializer
	{
	public:
		serializer(cyy::vm_dispatcher& vm, cyy::store::store&);
		serializer(const serializer& other) = delete;
		~serializer();
		serializer& operator=(const serializer& other) = delete;
		
		/**
		 * Send buffer content over the wire
		 */
		boost::system::error_code flush(boost::asio::ip::tcp::socket&);
		boost::system::error_code flush(boost::asio::ssl::stream<boost::asio::ip::tcp::socket>&);

	private:
		void stock(codes::status);
		void stock_404();
		void write(response const&);
		void ws_text(std::string const&);
		void ws_binary(cyy::buffer_t const&);

	private:
		cyy::store::store& db_;
		boost::asio::streambuf buffer_;
		std::ostream ostream_;
		
	};
}
#endif // CYX_HTTP_SERIALIZER_H
