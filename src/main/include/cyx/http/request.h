/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_REQUEST_H
#define CYX_HTTP_REQUEST_H

#include <cyx/http/http.h>
#include <cyx/http/uri.hpp>
#include <cyy/intrinsics/version.h>
#include <cyy/intrinsics/sets.hpp>
#include <utility>

namespace cyx
{



	/**
     * HTTP request
     */
    struct request
	{

        request();

        methods::method method_;
        cyy::version    version_;
        uri uri_;
		cyy::param_map_t	params_;	//	HTTP header attributes
		query_t				form_;		//	"application/x-www-form-urlencoded" 
	};


    /**
     * @brief reset HTTP request
     */
    void clear(request&);

	/**
	 * @return true if Connection == Upgrade and Upgrade == websocket
	 */
	bool is_websocket(cyy::param_map_t const&);

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::request>(cyx::request&, cyx::request&);

}

#endif	//	CYX_HTTP_REQUEST_H


