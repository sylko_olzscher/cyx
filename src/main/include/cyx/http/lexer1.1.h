/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_LEXER_H
#define CYX_HTTP_LEXER_H

#include <cyx/http/request.h>
#include <cyx/http/multi_part.h>
#include <cyx/http/lexer_states.h>
#include <cyx/http/multi_part_lexer.h>
#include <cyx/http/websocket_lexer.h>
#include <cyy/intrinsics/sets_fwd.h>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/scheduler/logging/log.h>
#include <cyy/io/match.h>
#include <cyy/util/read_sets.h>
#include <functional>
#include <algorithm>

namespace cyy
{
	class vm_dispatcher;
}

namespace cyx
{
	class lexer
	{

	public:
		lexer(std::function<void(cyy::vector_t&&)> cb, logger_type&);
		lexer(cyy::vm_dispatcher& vm, std::function<void(cyy::vector_t&&)> cb, logger_type&);
		lexer(const lexer& other) = delete;
		virtual ~lexer();
		lexer& operator=(const lexer& other) = delete;
		
		template < typename I >
		void read(I first, I last)
		{

			//	loop over input buffer
			std::for_each(first, last, [this](const char c)	{
				this->parse(c);
			});

		}

		/**
		 * @return the internal parser state name
		 */
		std::string state() const;

		
	private:
		/**
		 * internal lexer state
		 */
		http::lexer_state_enum	state_;
		
	private:
		
        /**
         *	The Request-Line begins with a method token, followed by the
         *	Request-URI and the protocol version, and ending with CRLF. The
         *	elements are separated by SP characters. No CR or LF is allowed
         *	except in the final CRLF sequence.
         *	@code
         Request-Line   = Method SP Request-URI SP HTTP-Version CRLF
         *	@endcode
         */
        void parse(char c);
		
        void parse_method();
        void parse_url();
        void parse_version();
        void parse_header();
		void parse_form();

		http::lexer_state_enum calculate_post_state();
		
		void invoke_get();
		void invoke_head();
// 		void invoke_post();
		void invoke_delete();
// 		void invoke_put();	//	update
 		void invoke_connect();
		void invoke_options();
		void invoke_propfind();

	private:
		/**
		 * completion callback
		 */
		std::function<void(cyy::vector_t&&)> cb_;
		
	private:
		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;
		
		/**
		 *	matching 
		 */
		const cyy::match_ws		is_ws_;
		const cyy::match_cr		is_cr_;
		const cyy::match_nl		is_nl_;

		/**
		 * Contains all HTTP header data
		 */
        request request_;

		std::uint32_t content_size_;
		cyy::reader<cyy::param_map_t>	reader_;

		cyy::buffer_t	buffer_;	//!<	hold temporary data

		/**
		 * Parser for multi part data
		 */
		multi_part_lexer	form_lexer_;
		
		/**
		 * Parser for websocket data
		 */
		websocket_lexer	ws_lexer_;

		
	};
}
#endif // CYX_HTTP_LEXER_H
