/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_LEXER_STATES_H
#define CYX_HTTP_LEXER_STATES_H

#include <cstdint>

namespace cyx
{
	namespace http
	{
		enum lexer_state_enum : std::uint32_t
		{
			method_start,
			method_char,
			method_done,
			uri_char,
			uri_done,
			version_char,
			version_cr,
			version_done,
			header_char,
			header_cr,
			header_nl,
			header_line_done,
			headers_cr,

			//	content
			content_xml,
			content_json,
			content_urlencoded,
			multipart,

			//	websockets
			ws,
		};

		/*
		*	@return lexer state name
		*/
		inline std::string name(lexer_state_enum state)
		{
			switch (state)
			{
			case method_start:	return "START";
			case method_char:	return "METHOD";
			case method_done:	return "METHOD-DONE";
			case uri_char:		return "URI";
			case uri_done:		return "URI-DONE";
			case version_char:	return "VERSION";
			case version_cr:	return "VERSION<CR>";
			case version_done:	return "VERSION-DONE";
			case header_char:	return "HEADER";
			case header_cr:	return "HEADER<CR>";
			case header_nl:	return "HEADER<NL>";
			case header_line_done:	return "HEADER_LINE_DONE";
			case headers_cr:	return "HEADERS<CR>";

					//	content
			case content_xml:	return "CONTENT-XML";
			case content_json:	return "CONTENT-JSON";
			case content_urlencoded:	return "CONTENT-URLENCODED";
			case multipart:		return "MULTIPART";

					//	websockets
			case ws:	return "WEBSOCKET";

			default:
				break;
			}
			return "";
		}

	}
}
#endif // CYX_HTTP_LEXER_STATES_H
