/*
 * Copyright Sylko Olzscher 2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_TIDY_HPP
#define CYX_HTTP_TIDY_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/scheduler/logging/log.h>
#include <cyy/io/format/boost_format.h>
#include <boost/uuid/uuid.hpp>

namespace cyx
{
	namespace http
	{
		/**
		 * Helper class to keep session list clean.
		 *
		 * @tparam T session type
		 */
		template < typename T >
		class tidy
		{
		public:
			tidy(logger_type& l, boost::uuids::uuid tag)
				: logger_(l)
				, tag_(tag)
			{}

			void operator()(T* ptr) const
			{
				BOOST_ASSERT_MSG(ptr != nullptr, "invalid session pointer");
// 				BOOST_ASSERT_MSG(ptr->vm().tag() == tag_, "wrong session tag");

				delete ptr;

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(tag_)
					<< " deleted");
			}

		private:
			logger_type& logger_;
			const boost::uuids::uuid tag_;
		};
	}
}	//	cyx


#endif	//	CYX_HTTP_TIDY_HPP
