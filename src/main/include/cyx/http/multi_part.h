/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_MULTI_PART_H
#define CYX_HTTP_MULTI_PART_H

#include <cyx/http/http.h>
#include <cyx/http/uri.hpp>
#include <cyx/http/content_type.hpp>
#include <cyy/intrinsics/buffer.hpp>

namespace cyx
{



	/**
     * part of a multipart/form-data
     */
    struct multi_part
	{

        multi_part();

		std::size_t				size_;
		cyy::buffer_t			data_;
		param_container_t		meta_;
		mime_content_type		type_;
		
	};


    /**
     * @brief reset multi_part
     */
    void clear(multi_part&);

}	//	cyx

namespace std
{
    template <>
    void swap<cyx::multi_part>(cyx::multi_part&, cyx::multi_part&);

}

#endif	//	CYX_HTTP_MULTI_PART_H


