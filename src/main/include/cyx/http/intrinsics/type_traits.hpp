/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_TYPE_TRAITS_H
#define CYX_HTTP_TYPE_TRAITS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/intrinsics/traits/type_traits.hpp>
#include <cyx/http/type_codes.h>
#include <cyx/http/uri.hpp>
#include <cyx/http/content_type.hpp>


namespace cyy
{
	// 	cyx::types::URL_PATH - cyx::path_t
	template <>
	struct type_traits< cyx::path_t >
	{
		typedef cyx::path_t type;
		typedef std::integral_constant<cyy::code_t, cyx::types::URL_PATH>	code_;
		typedef type::value_type subtype;	// std::string
	};
	
	template <>
	struct reverse_type < cyx::types::URL_PATH >
	{
		typedef cyx::path_t type;
		static_assert(type_traits<type>::code_::value == cyx::types::URL_PATH, "assignment error");
	};
	

	// 	cyx::types::QUERY_PATH - cyx::query_t
	template <>
	struct type_traits< cyx::query_t >
	{
		typedef cyx::query_t type;
		typedef std::integral_constant<cyy::code_t, cyx::types::QUERY_PATH>	code_;
		typedef type::value_type subtype;	// optional string
	};

	template <>
	struct reverse_type < cyx::types::QUERY_PATH >
	{
		typedef cyx::query_t type;
		static_assert(type_traits<type>::code_::value == cyx::types::QUERY_PATH, "assignment error");
	};

	//	HTTP_METHOD - cyx::methods::method
	template <>
	struct type_traits< cyx::methods::method >
	{
		typedef cyx::methods::method type;
		typedef std::integral_constant<cyy::code_t, cyx::types::HTTP_METHOD>	code_;
		typedef std::uint32_t  subtype;	// enum base type
	};

	template <>
	struct reverse_type < cyx::types::HTTP_METHOD >
	{
		typedef cyx::methods::method type;
		static_assert(type_traits<type>::code_::value == cyx::types::HTTP_METHOD, "assignment error");
	};

	//	HTTP_STATUS - cyx::codes::status
	template <>
	struct type_traits< cyx::codes::status >
	{
		typedef cyx::codes::status type;
		typedef std::integral_constant<cyy::code_t, cyx::types::HTTP_STATUS>	code_;
		typedef std::uint32_t  subtype;	// enum base type
	};

	template <>
	struct reverse_type < cyx::types::HTTP_STATUS >
	{
		typedef cyx::codes::status type;
		static_assert(type_traits<type>::code_::value == cyx::types::HTTP_STATUS, "assignment error");
	};

	//	MIME_TYPE - cyx::codes::status
	template <>
	struct type_traits< cyx::mime_content_type >
	{
		typedef cyx::mime_content_type type;
		typedef std::integral_constant<cyy::code_t, cyx::types::MIME_TYPE>	code_;
		typedef null  subtype;	// struct, complex
	};

	template <>
	struct reverse_type < cyx::types::MIME_TYPE >
	{
		typedef cyx::mime_content_type type;
		static_assert(type_traits<type>::code_::value == cyx::types::MIME_TYPE, "assignment error");
	};

}	//	cyy


#endif	//	CYX_HTTP_TYPE_TRAITS_H

