/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_IO_H
#define CYX_HTTP_IO_H

#include <cyy/io/io_callback.h>

namespace cyx
{
	namespace io
	{
		bool custom(std::ostream&, cyy::object const&);
	}
}

#endif	//	CYX_HTTP_IO_H
