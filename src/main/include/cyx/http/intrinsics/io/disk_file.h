/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_DISK_FILE_H
#define CYX_HTTP_DISK_FILE_H

#include <cyx/http/response.h>
#include <cyy/intrinsics/bytes.h>
#include <cyy/io/parser/bom_parser.h>
#include <boost/filesystem.hpp>
#include <cstdint>
#include <chrono>
#include <utility>

namespace cyx
{
	namespace io
	{
		/**
		 * @brief read content of a file into a string buffer.
		 */
		std::pair<cyy::io::bom::code, std::uint64_t> read_file(std::string&, boost::filesystem::path const&);

		/**
		 * @return file size in bytes
		 */
		cyy::bytes size(boost::filesystem::path const&);

		/**
		 * @return date of last file modification
		 */
		std::chrono::system_clock::time_point modification_time(boost::filesystem::path const&);
	}
}

#endif	//	CYX_HTTP_DISK_FILE_H
