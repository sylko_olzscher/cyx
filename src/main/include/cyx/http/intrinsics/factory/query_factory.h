/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_QUERY_FACTORY_H
#define CYX_HTTP_QUERY_FACTORY_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/uri.hpp>
#include <cyy/object.h>

namespace cyy
{
	
	object factory(cyx::query_t const&);

	/**
	 *	Generate an empty query
	 */
	object query_factory();
}

#endif	//	CYX_HTTP_QUERY_FACTORY_H
