/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_METHOD_FACTORY_H
#define CYX_HTTP_METHOD_FACTORY_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/http.h>
#include <cyy/object.h>
#include <initializer_list>

namespace cyy
{
	
	object factory(cyx::methods::method);

	/**
	 * create a vector of HTTP method objects
	 */
	object factory(std::initializer_list<cyx::methods::method>);

	/*
	 * Accept the following literals:
	 * <ul>
	 * <li>HEAD</li>
	 * <li>GET</li>
	 * <li>POST</li>
	 * <li>PUT</li>
	 * <li>DELETE</li>
	 * <li>TRACE</li>
	 * <li>OPTIONS</li>
	 * <li>CONNECT</li>
	 * <li>COPY</li>
	 * <li>MOVE</li>
	 * <li>LOCK</li>
	 * <li>UNLOCK</li>
	 * <li>MKCOL</li>
	 * <li>PROPFIND</li>
	 * <li>PROPPATCH</li>
	 * </ul>
	*/
	object method_factory(std::string const&);
	object method_factory(std::uint32_t);

}

#endif	//	CYX_HTTP_METHOD_FACTORY_H
