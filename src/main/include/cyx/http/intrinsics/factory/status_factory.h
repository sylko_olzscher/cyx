/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_STATUS_FACTORY_H
#define CYX_HTTP_STATUS_FACTORY_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/http.h>
#include <cyy/object.h>

namespace cyy
{
	
	object factory(cyx::codes::status);

	/**
	 * Accept the following literals:
	 * <ul>
	 * <li>Continue</li>
	 * <li>SwitchingProtocols = 101</li>
	 * <li>Processing = 102</li>
	 * <li>OK</li>
	 * <li>Created</li>
	 * <li>Accepted</li>
	 * <li>NonAuthoritativeInformation</li>
	 * <li>NoContent</li>
	 * <li>ResetContent</li>
	 * <li>PartialContent</li>
	 * <li>MultiStatus</li>
	 * <li>AlreadyReported</li>
	 * <li>IMUsed</li>
	 * <li>MultipleChoices</li>
	 * <li>MovedPermanently</li>
	 * <li>MovedTemporarily</li>
	 * <li>SeeOther</li>
	 * <li>NotModified</li>
	 * <li>UseProxy</li>
	 * <li>Unused</li>
	 * <li>TemporaryRedirect</li>
	 * <li>BadRequest</li>
	 * <li>Unauthorized</li>
	 * <li>PaymentRequired</li>
	 * <li>Forbidden</li>
	 * <li>NotFound</li>
	 * <li>MethodNotAllowed</li>
	 * <li>NotAcceptable</li>
	 * <li>ProxyAuthRequired</li>
	 * <li>RequestTimeout</li>
	 * <li>Conflict</li>
	 * <li>Gone</li>
	 * <li>LengthRequired</li>
	 * <li>PreconditionFailed</li>
	 * <li>RequestEntityTooLarge</li>
	 * <li>RequestUriTooLong</li>
	 * <li>UnsupportedMediaType</li>
	 * <li>RequestedRangeNotSatisfiable</li>
	 * <li>ExpectationFailed</li>
	 * <li>InternalServerError</li>
	 * <li>NotImplemented</li>
	 * <li>BadGateway</li>
	 * <li>ServiceUnavailable</li>
	 * <li>GatewayTimeout</li>
	 * <li>HttpVersionNotSupported</li>
	 * </ul>
	 */
	object status_factory(std::string const&);
	object status_factory(std::uint32_t);

}

#endif	//	CYX_HTTP_STATUS_FACTORY_H
