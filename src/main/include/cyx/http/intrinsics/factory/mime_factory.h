/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_MIME_FACTORY_H
#define CYX_HTTP_MIME_FACTORY_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyx/http/content_type.hpp>
#include <cyy/object.h>

namespace cyy
{
	
	object factory(cyx::mime_content_type const&);
	object factory(std::string const&, std::string const&, cyx::param_container_t const&);
	object mime_factory();


}

#endif	//	CYX_HTTP_MIME_FACTORY_H
