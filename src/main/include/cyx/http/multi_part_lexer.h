/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_MULTI_PART_LEXER_H
#define CYX_HTTP_MULTI_PART_LEXER_H

#include <cyx/http/request.h>
#include <cyx/http/multi_part.h>
#include <cyx/http/lexer_states.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/io/match.h>

namespace cyy
{
	class vm_dispatcher;
}

namespace cyx
{
	/** 
	 * parser for HTTP uploads and forms
	 *	@see http://tools.ietf.org/html/rfc1867
	 */
	class multi_part_lexer
	{

	public:
		multi_part_lexer(std::function<void(cyy::vector_t&&)> cb
			, logger_type&
			, std::uint32_t&
			, request&);

		/**
		 * Put next available character into state machine
		 *
		 * @return next lexer state. 
		 */
		http::lexer_state_enum parse(char c);

		/**
		 * Start new upload/form
		 */
		void reset(std::string const& boundary);

	private:
		/**
		* The parameters "filename" and "filename*" differ only in that "filename*" uses the encoding
		* defined in RFC 5987. When both "filename" and "filename*" are present in a single header field value,
		* "filename*" is preferred over "filename" when both are present and understood.
		*/
		std::string lookup_filename(param_container_t const& phrases);

	private:
		//	chunk states (multipart)
		enum chunk_enum
		{
			chunk_boundary,
			chunk_boundary_cr,
			chunk_boundary_nl,
			chunk_boundary_end,
			chunk_header,
			chunk_header_nl,
			chunk_data_start,
			chunk_data,
			chunk_esc1,
			chunk_esc2,
			chunk_esc3,

		}	state_;

		/**
		 * completion callback
		 */
		std::function<void(cyy::vector_t&&)> cb_;

		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;

		/**
		 * Contains upload data
		 */
		multi_part	upload_;

		/**
		 *	matching
		 */
		const cyy::match_ws		is_ws_;
		const cyy::match_cr		is_cr_;
		const cyy::match_nl		is_nl_;

		/**
		 * Each multipart entry is enveloped into a unique
		 * boundary string.
		 */
		std::string boundary_;

		/**
		 * buffer for incoming boundary and attribute data
		 */
		std::string chunck_;

		/**
		 * Specified content size
		 */
		std::uint32_t&	content_size_;

		/**
		 * Contains all HTTP header data
		 */
		request&	request_;

		/**
		 * Totally bytes already uploaded
		 */
		std::uint32_t	upload_size_;	

		/**
		 * calculation of upload progress
		 */
		std::uint32_t progress_;
	};

}
#endif // CYX_HTTP_MULTI_PART_LEXER_H
