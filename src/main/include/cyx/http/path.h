/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_PATH_H
#define CYX_HTTP_PATH_H

#include <string>
#include <vector>
#include <initializer_list>
#include <boost/filesystem.hpp>

namespace cyx
{
	/**
	 *	Define a path as a vector of segments (string)
	 */
	typedef std::vector< std::string >	path_t;

    //	transformation
    boost::filesystem::path to_path(path_t const&);

	/**
	 * Converts also string through implicit conversion from string to boosts filesystem path
	 */
    path_t to_path(boost::filesystem::path const&);
	path_t to_path(std::initializer_list<std::string>);

	/**
	 * Join multiple paths to one path
	 */
	path_t join(std::initializer_list<path_t>);

    std::string to_str(path_t const&);

	/**
	 * @return true if path contains only filename favicon.ico
	 */
	bool is_favicon(path_t const&);

	/**
	 * Returns true if path is empty.
	 *
	 * @return true if last element contains a dot (like typical filenames)
	 */
	bool is_directory(path_t const&);

	/**
	 * Returns true if both path share the same root
	 *
	 * @return true if both path share the same root
	 */
	bool starts_with(path_t const& inp, path_t const& test);
	bool starts_with(path_t const& inp, std::initializer_list<std::string> test);


	/**
	 * Compare a path with a list of strings. The comparison is case sensitive.
	 */
	bool is_equal(path_t const&, std::initializer_list<std::string>);
}

#endif	//	CYX_HTTP_PATH_H
