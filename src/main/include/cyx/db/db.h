/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_DB_H
#define CYX_HTTP_DB_H

#include <boost/filesystem.hpp>
#include <cyx/http/content_type.hpp>

namespace cyy 
{
	namespace store 
	{
		class store;
	}
}


namespace cyx
{
	/**
 	 * Create all required tables. Could be called only
	 * once.
	 */
	void init(cyy::store::store&);

	/**
	 * Insert a basic set of MIME types
	 */
	void set_mime_default(cyy::store::store&);

	/**
	 * Load MIME definitions from XML configuration file
	 */
	bool load_mime_from_xml(cyy::store::store&, boost::filesystem::path const&);

	/**
	 * Store MIME definitions to XML configuration file
	 */
	bool store_mime_to_xml(cyy::store::store&, boost::filesystem::path const&);

	/**
	 * Query mime type of specified file extension
	 */
	std::string query_mime(cyy::store::store&, std::string const&);
		
}	//	cyx

#endif	//	CYX_HTTP_DB_H

