/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_H
#define CYX_DOCSCRIPT_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200) 

#include <cstdint>
#include <string>

namespace cyx 
{
	//
	//	define a UTF-32 string
	//
	//using u32_string = std::u32string;
	using u32_string = std::basic_string<std::uint32_t>;

	//
	//	define a pair of UTF-32 strings
	//
	using u32_string_pair_t = std::pair<u32_string, u32_string>;

}	//	cyx

#endif	//	CYX_DOCSCRIPT_H
