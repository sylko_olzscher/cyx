/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_LIBRARY_H
#define CYX_DOCSCRIPT_LIBRARY_H

//#include <cyx/docscript/docscript.h>
//#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <initializer_list>

namespace cyx	
{
	namespace docscript
	{
		enum function_type {
			NONE_,	//	undefined
			NL_,
			WS_,
			ENV_,	//	environment specific
		};

		/**
		 *	helper class to provide function meta data
		 */
		struct function
			: std::enable_shared_from_this<function>
		{
			function();
			function(std::string const&, std::size_t, function_type);
			function(function const&);

			std::shared_ptr<function const> get_ptr() const;

			bool is_undefined() const;
			bool is_ws() const;
			bool is_nl() const;

			const std::string name_;	//!<	function name
			const std::size_t rvs_;	//!< return values
			const function_type	type_;
		};

		using function_map = std::map<std::string, std::shared_ptr<function const>>;

		void insert(function_map&, std::shared_ptr<function const>, std::initializer_list<std::string> names);
		std::shared_ptr<function const> lookup(function_map const&, std::string const& name);

	}
}	//	cyx

#endif	//	CYX_DOCSCRIPT_LIBRARY_H
