/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_STATISTICS_H
#define CYX_DOCSCRIPT_STATISTICS_H

#include <cstdint>
#include <map>

namespace cyx	
{
	//
	//	Container to hold statistical data 
	//
	using frequency_t = std::map<std::uint32_t, std::size_t>;

	/**
	 * @returns the total size of symbols
	 */
	std::size_t calculate_size(frequency_t const& stat);


	/**
	 * @returns Shannon entropy 
	 */
	double calculate_entropy(frequency_t const& stat);

}	//	cyx

#endif	//	CYX_DOCSCRIPT_STATISTICS_H




