/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_DOCSCRIPT_VERBATIM_FILTER_H
#define CYX_DOCSCRIPT_VERBATIM_FILTER_H

#include <cyx/docscript/docscript.h>
#include <cstdint>
namespace cyx	
{
	namespace filter 
	{
		class verbatim
		{
		public:
			verbatim();
			void put(std::uint32_t c);
			std::string get_result();

		private:
			u32_string	result_;
		};
	}

}	//	cyx

#endif	//	CYX_DOCSCRIPT_VERBATIM_FILTER_H




