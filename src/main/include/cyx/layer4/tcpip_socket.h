/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_TCPIP_SOCKET_H
#define CYX_TCPIP_SOCKET_H

#include <boost/asio.hpp>


namespace cyx
{
    using socket_t = boost::asio::ip::tcp::socket;
}

#endif	//	CYX_TCPIP_SOCKET_H
