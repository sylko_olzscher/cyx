/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_SSL_SOCKET_H
#define CYX_SSL_SOCKET_H

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

//	To generate certificates see
//	http://stackoverflow.com/questions/6452756/exception-running-boost-asio-ssl-example
//	openssl genrsa -des3 -out server.key 1024
//	The following command requires some input. The Common Name must be the server name
//	openssl req -new -key server.key -out server.csr
//	openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt
//	cp server.key server.key.secure
//	openssl rsa - in server.key.secure - out server.key
//	The following command is going to take a long time
//	openssl dhparam -out dh2048.pem 2048
//	context_.use_certificate_chain_file("server.crt"); 
//	context_.use_private_key_file("server.key", boost::asio::ssl::context::pem);
//	context_.use_tmp_dh_file("dh2048.pem");

namespace cyx
{
    using socket_t = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;
    
    /**
     * @return TCP/IP socket
     */
    inline socket_t::lowest_layer_type& socket(socket_t& s)
    {
        return s.lowest_layer();
    }    
}

#endif	//	CYX_SSL_SOCKET_H
