/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_SMTP_CLIENT_H
#define CYX_SMTP_CLIENT_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200) 

#include <boost/asio.hpp>
#include <cyx/cyx.h>
#include <cyx/smtp/smtp.h>
#include <vector>

namespace cyx 
{
	namespace smtp	
	{

		class client
		{
		public:
			client(boost::asio::io_service&
				, std::string const& host
				, ip_port_t port
				, std::string const& user
				, std::string const& pwd);

			void send(std::string const& from
				, std::vector<std::string> const& to
				, std::string const& subject
				, std::string const& message);

			void set_sender_mail(std::string const& s);

		private:
			void write_line(std::string const&);
			void read_line(std::string&);

		private:
			boost::asio::ip::tcp::socket	socket_;
			boost::asio::ip::tcp::resolver	resolver_;
			boost::asio::streambuf request_;
			const std::string	local_host_;
			const std::string	remote_host_;
			const ip_port_t		remote_port_;
			const std::string	user_;
			const std::string	pwd_;

		};

	}	//	smtp
}	//	cyx

#endif	//	CYX_SMTP_CLIENT_H
