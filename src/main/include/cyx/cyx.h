/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_H
#define CYX_H

#include <cstdint>
#include <string>

namespace cyx
{
	//	io specific typedefs
	typedef std::uint16_t	ip_port_t;

	namespace ports
	{

		//	extract of some well known ports
		enum default_port : std::uint16_t
		{
			SSH = 22,		//!<	Secure Shell (SSH)
			TELNET = 23,	//!<	Telnet protocol�unencrypted text communications
			SMTP = 25,		//!<	Simple Mail Transfer Protocol (SMTP)
			TIME = 27,		//!<	TIME protocol
			DNS = 53,		//!<	Domain Name System (DNS)
			HTTP = 80,		//!<	Hypertext Transfer Protocol (HTTP)
			POP3 = 110,		//!<	Post Office Protocol v3 (POP3)
			IMAP = 143,		//!<	Internet Message Access Protocol (IMAP)�management of email messages
			HTTPS = 443,	//!<	HTTPS (Hypertext Transfer Protocol over SSL/TLS)
			SYSLOG = 514,	//!<	Syslog�used for system logging
			RPC = 530,		//!<	Remote procedure call

			//	-- private --
			HTTP_ALT = 8080,	//!<	HTTP Alternate, This port is a popular alternative to port 80 for offering web services. "8080" was chosen since it is "two 80's", and also because it is above the restricted well known service port range
			IPT = 26862,		//!<	IP-Telemetry
			ADMIN = 7701,		//!<	administration port
		};
	}
}

#endif	//	CYX_H
