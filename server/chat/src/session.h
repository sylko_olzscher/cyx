/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_CHAT_SESSION_H
#define CYX_CHAT_SESSION_H

#include <memory>
#include <vector>
#include <boost/uuid/uuid.hpp>
#include <cyx/layer4/ssl_socket.h>
#include <cyy/intrinsics/sets_fwd.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/vm/context.h>
#include <cyy/store/store.h>

//
//
// some forward declaration (to save compile time because of to many includes)
namespace cyy
{
	class ruler;
    class vm_dispatcher;
	namespace store
	{
		class store;
	}
}

namespace cyx
{

	struct chat_session;	//!<	the implementation class

	/**
	 * Implementation of a chat client
	 */
	class session : public std::enable_shared_from_this<session>
	{
	public:
		typedef std::shared_ptr<session>	shared_type;
		typedef std::weak_ptr<session>		weak_type;

	public:
		session(boost::asio::ssl::context& ctx
			, cyy::ruler&
			, logger_type& logger
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, boost::filesystem::path const& htdocs);
		virtual ~session();

		/**
		 * expose VM dispatcher to open
		 * an asynchronous communication path
		 */
		cyy::vm_dispatcher& vm();
        
		/**
		 * Inject and execute a programm (thread safe).
		 */
// 		boost::system::error_code run(cyy::vector_t const&);

		/**
		 * Execute the specified instructions asynchonously.
		 */
// 		void run_async(cyy::vector_t&&);

		/**
		 * @return session tag
		 */
// 		boost::uuids::uuid tag() const;

		/**
		 * Start session to read from socket
		 */
		void start();
        
        /**
         * @return the underlying socket
         */
        socket_t::lowest_layer_type& get_socket();   

	private:
#if __GNUC__ > 6 || defined(_MSC_VER) 	
        std::unique_ptr<chat_session>	impl_;
#else
        std::shared_ptr<chat_session>	impl_;
#endif
	};


}	//	cyx

#endif	//	CYX_CHAT_SESSION_H


