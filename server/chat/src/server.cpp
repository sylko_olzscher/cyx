/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "server.h"
#include <cyy/scheduler/ruler.h>
#include <cyy/util/read_sets.h>
#include <cyy/vm/code_generator.h>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/store/store.h>
#include <iostream>
#include <boost/uuid/uuid_io.hpp>

namespace cyx
{
	server::server(cyy::ruler& scheduler
		, logger_type& logger
		, cyy::store::store& db
		, boost::uuids::uuid tag
		
        , std::string const& ssl_pwd
        , std::string const& ssl_certificate
        , std::string const& ssl_private_key
        , std::string const& ssl_dh
		
		, std::string const& root
		, std::string const& css
		, std::string const& js)
	: ssl_srv(scheduler.get_io_service()
        , [this](boost::asio::ssl::context& ctx){
            return create(ctx);
        }
        , ssl_pwd
        , ssl_certificate
        , ssl_private_key
        , ssl_dh)
	, scheduler_(scheduler)
	, logger_(logger)
	, db_(db)
	, uuid_gen_()
	, connections_()
	, htdocs_(root)
	, css_file_(htdocs_ / css)
	, js_file_(htdocs_ / js)
	{		
		CYY_LOG_INFO(logger_, "document-root: "	<< htdocs_);
		CYY_LOG_INFO(logger_, "css-file: "	<< css_file_);
		CYY_LOG_INFO(logger_, "js-file: "	<< js_file_);
        
		CYY_LOG_INFO(logger_, "SSL certificate: "	<< ssl_certificate);		
		CYY_LOG_INFO(logger_, "SSL private key: "	<< ssl_private_key);		
		CYY_LOG_INFO(logger_, "Diffie-Hellman parameters: "	<< ssl_dh);		
	}

	server::~server()
	{
		CYY_LOG_INFO(logger_, "server exit...");
	}

// 	void server::on_connect(socket_t&& s)
// 	{
// 		CYY_LOG_INFO(logger_, "on_connect("
// 					<< socket(s).remote_endpoint()
// 					<< ')');
// 
// 		
// 		connect(http::make_session(std::move(s)
// 			, scheduler_
// 			, logger_
// 			, db_
// 			, uuid_gen_()
// 			, connections_
// 			, htdocs_));
// 	}

    session::shared_type server::create(boost::asio::ssl::context& ctx)
    {   
        session::shared_type sp = chat::make_session(ctx
			, scheduler_
			, logger_
			, db_
			, uuid_gen_()
			, connections_
			, htdocs_);
        
        //connect(sp); 
        
        return sp;
    }

	bool server::run(cyy::tuple_t const& config)
	{
		//
		//	parse configuration
		//	std::pair<object, bool>
		//	"node":{"address": "::1", "service":"7001"},
		//
		//const auto node = cyy::tuple_reader(config);//.get_object("server");

		//
		//	node address and service (port)
		//
		cyy::tuple_reader reader(config);
		const std::string address = reader.get<std::string>("address", "0.0.0.0");
		const std::string service = reader.get<std::string>("service", "8080");

		CYY_LOG_INFO(logger_, "startup server on "
					<< address
					<< ':'
					<< service);

		// call base class
		return ssl_srv::run(address, service);
	}

	void server::stop()
	{
		//	call base class
		CYY_LOG_INFO(logger_, "close TCP/IP acceptor");
		//shutdown();

		//	call base class
		ssl_srv::shutdown();

		//	stop all connections
		stop_clients();

	}

	void server::on_connect(session::shared_type sp)
	{
		CYY_LOG_INFO(logger_, "on_connect("
			<< sp->get_socket().remote_endpoint()
			<< ')');

		connect(sp);
	}

	void server::on_error(boost::system::error_code const& ec)
	{
		CYY_LOG_ERROR(logger_, ec.message());
	}
	
	void server::connect(session::shared_type sp)
	{
		BOOST_ASSERT(sp);
		if (sp && connections_.emplace(sp->vm().tag(), sp).second)
		{
			sp->start();
		}
	}
	
	std::size_t server::stop_clients()
	{
		CYY_LOG_INFO(logger_, "stop "
			<< connections_.size()
			<< " client(s)");
		
		std::size_t counter { 0 };

		//	-- this is far from production ready. --
		//	Put this in a task and implement thread safe
		//	access to connections map.
		while (!connections_.empty())
		{
			auto sp = connections_.begin()->second.lock();
			if (sp)
			{
				CYY_LOG_TRACE(logger_, "stop client "
					<< sp->vm().tag());
				
				//	sync call
				sp->vm().run(cyy::generate_invoke("dispatcher.exec.sync"));
				sp->vm().run(cyy::generate_invoke("io.close"));
				std::this_thread::sleep_for(std::chrono::seconds(1));
				counter++;
			}
		}

		//		connections_.clear();
		return counter;
	}
	
	tidy::tidy(logger_type& l, server::connections_t& c)
	: logger_(l)
	, connections_(c)
	{}
	
	void tidy::operator()(session* ptr) const
	{
		auto tag = ptr->vm().tag();
		
		CYY_LOG_INFO(logger_, "session ["
		<< tag
		<< "] removed from client list");
		
		if (connections_.erase(tag) != 0)
		{
			CYY_LOG_INFO(logger_, connections_.size()
			<< " sessions online");
		}
		else 
		{
			CYY_LOG_ERROR(logger_, "session ["
			<< tag
			<< "] not found in client list");
		}
		delete ptr;
	}
	
	namespace chat 
	{
		session::shared_type make_session(boost::asio::ssl::context& ctx
            , cyy::ruler& scheduler
			, logger_type& logger
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, server::connections_t& connections
			, boost::filesystem::path const& htdocs)
		{
			return std::shared_ptr<session>(new session(ctx, scheduler, logger, db, tag, htdocs), tidy(logger, connections));
		}
	}	
}
