/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_CHAT_CONTROLLER_H
#define CYX_CHAT_CONTROLLER_H

#include <string>
#include <cyy/scheduler/logging/log.h>
#include <boost/config.hpp>

namespace cyy
{
	// 	class object;
	class ruler;
	class object_reader;
}

namespace cyx
{
	class controller
	{
	public:
		/**
		 * @param pool_size thread pool size
		 * @param json_path path to JSON configuration file
		 */
		controller(std::size_t pool_size, std::string const& json_path);
		controller(controller&&);

		/**
		 * Start controller loop.
		 * The controller loop controls startup and shutdown of the server.
		 *
		 * @return EXIT_FAILURE in case of an error, otherwise EXIT_SUCCESS.
		 */
		int run(bool console);

		/**
		 * Create a JSON configuration file with default values based in the
		 * host machine.
		 *
		 * @param json_path path to JSON configuration file
		 * @return EXIT_FAILURE in case of an error, otherwise EXIT_SUCCESS.
		 */
		int create_config(std::string const& type);
		int create_config_xml();

		/**
		 * Show all available IP adresses of this node.
		 * @return EXIT_FAILURE in case of an error, otherwise EXIT_SUCCESS.
		 */
		int show_ip() const;

		/**
		 * Show content of the configuration file
		 * @return EXIT_FAILURE in case of an error, otherwise EXIT_SUCCESS.
		 */
		int show_config() const;

		/**
		 * Generate SSL keys.
		 * @return EXIT_FAILURE in case of an error, otherwise EXIT_SUCCESS.
		 */
		int generate() const;

#if defined(BOOST_OS_WINDOWS_AVAILABLE)
		/**
		* run as windows service
		*/
		static int run_as_service(controller&&, std::string const&);
		virtual void control_handler(DWORD);

#endif

	private:
		bool run(cyy::ruler&
			, logger_type&
			, cyy::object_reader const& reader);

	private:
		const std::size_t pool_size_;
		const std::string json_path_;
	};
}

#endif	//	CYX_CHAT_CONTROLLER_H
