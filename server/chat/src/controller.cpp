/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "controller.h"
#include "server.h"
#include <cyx/constants.h>
#include <cyx/db/db.h>
#include <cyy/scheduler/ruler.h>
#include <cyy/scheduler/signal_handler.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/json/json_io.h>
#include <cyy/io.h>
#include <cyy/io/stream_io.h>
#include <cyy/io/io_callback.h>
#include <cyy/util/read_sets.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/store/store.h>
#include <cyy/crypto/certificate.h>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>
#if defined(BOOST_OS_WINDOWS_AVAILABLE)
#include <cyy/scm/service.hpp>
#endif

namespace cyx
{
	controller::controller(std::size_t pool_size, std::string const& json_path)
		: pool_size_(pool_size)
		, json_path_(json_path)
	{}

	controller::controller(controller&& other)
		: pool_size_(std::move(other.pool_size_))
		, json_path_(std::move(other.json_path_))
	{}

	int controller::run(bool console)
	{
		try
		{
			//
			//	controller loop
			//
			bool shutdown {false};
			while (!shutdown)
			{
				//
				//	read/parse config file
				//
				const auto config = cyy::read_config(json_path_);
				if (config.second)
				{
					BOOST_ASSERT_MSG(cyy::get_code(config.first) == cyy::types::CYY_TUPLE, "invalid config data");

					//
					//	store start time
					//
					const std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

					//
					//	initialize a logging system
					//
					const boost::filesystem::path tmp = boost::filesystem::temp_directory_path();

					cyy::object_reader reader(config.first);
					const std::string log_dir = reader.get<std::string>("log-dir", tmp.string());

					BOOST_ASSERT_MSG(boost::filesystem::exists(log_dir), "invalid logging directory");
					BOOST_ASSERT_MSG(boost::filesystem::is_directory(log_dir), "logging directory does not exists");

					const boost::filesystem::path log_path = boost::filesystem::path(log_dir) / "chat-server.log";

					//
					//	initialize scheduler and logging system
					//
					cyy::ruler	task_scheduler(pool_size_);
					logger_type logger(log_path, task_scheduler.get_io_service());

					//	use "tail -f /tmp/junction_node.log" or less +F /tmp/junction_node.log" on linux systems
					logger.set_console_log(console);

					CYY_LOG_INFO(logger, "---=== startup ===---");
					CYY_LOG_INFO(logger, "pool size: " << pool_size_);
					CYY_LOG_INFO(logger, "load: " << cyy::to_literal(config.first));

					//
					//	start server
					//
					shutdown = run(task_scheduler, logger, reader);

					//
					//	print last messages
					//
					const auto duration = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - now);
					CYY_LOG_INFO(logger, "---=== stop scheduler after "
								<< duration.count()
								<< " seconds ===---");

					//
					//	stop task scheduler and logging system
					//
					CYY_LOG_INFO(logger, "stop scheduler... " << logger.get_pending_counter());
					task_scheduler.stop();
					logger.stop();
					task_scheduler.get_scheduler().stop();

				}
				else
				{
					std::cerr
							<< "***error: configuration file ["
							<< json_path_
							<< "] not found"
							<< std::endl;

					std::cout
							<< "use option -D to generate a configuration file"
							<< std::endl;

					shutdown = true;
				}
			}

			return EXIT_SUCCESS;
		}
		catch (std::exception& ex)	{

			//
			//	print error message
			//	and exit
			//
			std::cerr
					<< ex.what()
					<< std::endl
					;
		}

		return EXIT_FAILURE;
	}

	bool controller::run(cyy::ruler& scheduler
		, logger_type& logger
		, cyy::object_reader const& reader)
	{
		//
		//	get tag
		//
		const std::string str_tag = reader.get<std::string>("tag", "");
		const boost::uuids::uuid tag = (str_tag.empty())
			? boost::uuids::random_generator()()
			: boost::uuids::string_generator()(str_tag)
									;
		CYY_LOG_INFO(logger, "tag: " << tag);		

		//
		//	create in-memory database
		//
		cyy::store::store	db;
		init(db);

		//
		//	get MIME configuration
		//
		const boost::filesystem::path mime_cfg = reader.get_string("mime-config");
		if (boost::filesystem::exists(mime_cfg))
		{
			CYY_LOG_INFO(logger, "load MIME configuration file ["
				<< mime_cfg
				<< "]");

			//	load MIME configiration
			load_mime_from_xml(db, mime_cfg);

		}
		else
		{
			CYY_LOG_ERROR(logger, "MIME file "
				<< mime_cfg
				<< " not found - create default configuration");

			set_mime_default(db);
		}

		CYY_LOG_INFO(logger, "MIME table contains "
			<< db.size("TMime")
			<< " entries");
        
		const std::string ssl_certificate = reader["ssl"].get_string("ssl-certificate");
		const std::string ssl_private_key = reader["ssl"].get_string("ssl-private-key");
		const std::string ssl_dh = reader["ssl"].get_string("ssl-dh");

		if (!boost::filesystem::exists(ssl_certificate))
		{
			CYY_LOG_FATAL(logger, "SSL certificat "
				<< ssl_certificate
				<< " not found");
		}
		if (!boost::filesystem::exists(ssl_private_key))
		{
			CYY_LOG_FATAL(logger, "SSL private key "
				<< ssl_private_key
				<< " not found");
		}
		if (!boost::filesystem::exists(ssl_dh))
		{
			CYY_LOG_FATAL(logger, "Diffie�Hellman key "
				<< ssl_dh
				<< " not found");
		}

		//
		//	start server
		//	prepare initial socket
		//
		try
		{

			server srv(scheduler
				, logger
				, db
				, tag

				, reader["ssl"].get_string("ssl-pwd")
				, ssl_certificate
				, ssl_private_key
				, ssl_dh

				, reader["chat"].get_string("document-root")
				, reader["chat"].get_string("css-file")
				, reader["chat"].get_string("js-file"));

			srv.run(reader.get("server", cyy::tuple_t()));

			//
			//	wait for system signals
			//
			bool shutdown = false;
			cyy::signal_mgr signal;
			switch (signal.wait())
			{
	#ifdef _WIN32
			case CTRL_BREAK_EVENT:
	#else
			case SIGHUP:
	#endif
				CYY_LOG_INFO(logger, "SIGHUP received");
				break;
			default:
				shutdown = true;
				break;
			}

			//
			//	shutdown server
			//
			CYY_LOG_INFO(logger, "start shutdown");
			srv.stop();
			return shutdown;

		}
		catch (std::exception const& ex)
		{
			CYY_LOG_FATAL(logger, "error during startup: "
				<< ex.what());
		}


		//
		//	shutdown
		//
		return true;
	}

	int controller::create_config(std::string const& type)
	{
		if (boost::algorithm::iequals(type, "XML"))
		{
			return create_config_xml();
		}

		/*
		{
			"log-dir": "/tmp",
			"tag": "d274f673-c99f-43eb-a5e8-efdc314766f4",
			"server": {
				"address": "0.0.0.0",
				"service": "7701"
			}
		}
		*/
		const boost::filesystem::path tmp = boost::filesystem::temp_directory_path();

		std::fstream fout(json_path_, std::ios::trunc | std::ios::out);
		if (fout.is_open())
		{

			cyy::tuple_t json_data;
			json_data.push_back(cyy::set_factory( "log-dir", tmp.string()));
			json_data.push_back(cyy::set_factory("tag", boost::uuids::random_generator()()));	//	unused
			json_data.push_back(cyy::factory("generated", cyy::time_point_factory()));	//	unused
			json_data.push_back(cyy::set_factory("mime-config", (boost::filesystem::current_path() / "config" / "mime.xml").string()));
			json_data.push_back(cyy::set_factory( "favicon", std::string()));

			cyy::tuple_t server_data;
			server_data.push_back(cyy::set_factory( "address", "0.0.0.0"));
			//	standard port 80 requires higher privileges
			server_data.push_back(cyy::set_factory( "service", "8080"));
			json_data.push_back(cyy::set_factory("server", server_data));

			cyy::tuple_t ssl_data;
			ssl_data.push_back(cyy::set_factory("ssl-pwd", "secret"));
			ssl_data.push_back(cyy::set_factory("ssl-certificate", (boost::filesystem::current_path() / "config" / "server.crt").string()));
			//	only required to create self signed certificate
			ssl_data.push_back(cyy::set_factory("ssl-certificate-req", (boost::filesystem::current_path() / "config" / "server.csr").string()));
			ssl_data.push_back(cyy::set_factory( "ssl-private-key", (boost::filesystem::current_path() / "config" / "server.key").string()));
			ssl_data.push_back(cyy::set_factory( "ssl-dh", (boost::filesystem::current_path() / "config" / "dh2048.pem").string()));    //   file to obtain the temporary Diffie-Hellman  
            json_data.push_back(cyy::set_factory("ssl", ssl_data));
           
			//	optional data for certificate request
			cyy::tuple_t req_data;
			req_data.push_back(cyy::set_factory("country", "CH"));
			req_data.push_back(cyy::set_factory("province", "Lucerne"));
			req_data.push_back(cyy::set_factory("city", "Obernau"));
			req_data.push_back(cyy::set_factory("organization", "Pirx Ltd."));
			//	Host + Domain Name. It looks like "www.company.com" or "company.com". 
			req_data.push_back(cyy::set_factory("common", "localhost"));
			json_data.push_back(cyy::set_factory("request", req_data));

			cyy::tuple_t chat_data;
#if defined(BOOST_NO_CXX11_CONSTEXPR)
			const boost::filesystem::path cwd = boost::filesystem::path(CYX_SOURCE_DIRECTORY) / "/server/chat/htdocs";
#else
			const boost::filesystem::path cwd = boost::filesystem::path(cyx::constants::CYX_SOURCE_DIRECTORY) / "/server/chat/htdocs";
#endif
			chat_data.push_back(cyy::set_factory("document-root", cwd.string()));
			//chat_data.push_back(cyy::set_factory("document-root", "/srv/www/htdocs"));

			chat_data.push_back(cyy::set_factory( "css-file", "/css/main.css"));
			chat_data.push_back(cyy::set_factory( "js-file", "/js/main.js"));			
			chat_data.push_back(cyy::factory("index-file", cyy::vector_factory({ "index.html", "index.htm" })));
			json_data.push_back(cyy::set_factory( "chat", chat_data));
			
			cyy::serialize_json(fout, cyy::factory(json_data), cyy::io::custom_callback());

			//
			//	ToDo: generate favicon
			//
			
			std::cerr
					<< "***info: config file ["
					<< json_path_
					<< "] ready"
					<< std::endl;

			return EXIT_SUCCESS;
		}

		std::cerr
				<< "***error: cannot open file ["
				<< json_path_
				<< "]"
				<< std::endl;
		return EXIT_FAILURE;

	}

	int controller::create_config_xml()
	{
		const auto cfg = cyy::read_config(json_path_);
		if (cfg.second)
		{
			BOOST_ASSERT_MSG(cyy::primary_type_code_test<cyy::types::CYY_TUPLE>(cfg.first), "invalid config data");

			cyy::object_reader reader(cfg.first);
			const boost::filesystem::path mime_file = reader.get_string("mime-config");

			const auto dir = mime_file.parent_path();
			if (!boost::filesystem::exists(dir))
			{
				std::cerr
					<< "***warning: directory "
					<< dir
					<< " doesn't exist - try to create this"
					<< std::endl
					;

				if (!boost::filesystem::create_directories(dir))
				{
					std::cerr
						<< "***error: cannot create directory "
						<< dir
						<< std::endl
						;
					return EXIT_FAILURE;

				}
			}

			std::cout
				<< "***info: XML output file is: "
				<< mime_file
				<< std::endl
				;

			cyy::store::store	db;
			cyx::init(db);
			cyx::set_mime_default(db);
			cyx::store_mime_to_xml(db, mime_file);

			return EXIT_SUCCESS;
		}

		std::cerr
			<< "***error: configuration file ["
			<< json_path_
			<< "] not found"
			<< std::endl;

		return EXIT_FAILURE;
	}

	int controller::show_ip() const
	{
		const std::string host = boost::asio::ip::host_name();
		std::cout
				<< "host name: "
				<< host
				<< std::endl
				;

		try
		{
			boost::asio::io_service io_service;
			boost::asio::ip::tcp::resolver resolver(io_service);
			boost::asio::ip::tcp::resolver::query query(host, "");
			boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);
			boost::asio::ip::tcp::resolver::iterator end; // End marker.
			while (iter != end)
			{
				boost::asio::ip::tcp::endpoint ep = *iter++;
				std::cout
						<< (ep.address().is_v4() ? "IPv4: " : "IPv6: ")
						<< ep
						<< std::endl
						;
			}
			return EXIT_SUCCESS;
		}
		catch (std::exception const& ex)
		{
			std::cerr
					<< "***Error: "
					<< ex.what()
					<< std::endl
					;
		}
		return EXIT_FAILURE;
	}

	int controller::show_config() const
	{
		const auto cfg = cyy::read_config(json_path_);
		if (cfg.second)
		{
			BOOST_ASSERT_MSG(cyy::primary_type_code_test<cyy::types::CYY_TUPLE>(cfg.first), "invalid config data");

			cyy::print(std::cout, cfg.first, cyy::io::custom_callback());

			std::cout
					<< std::endl
					<< "salt   : "
	#if defined(BOOST_NO_CXX11_CONSTEXPR)
					<< SALT_STRING
	#else
					<< cyx::constants::SALT_STRING
	#endif
					<< std::endl
					;

			return EXIT_SUCCESS;
		}

		std::cerr
				<< "***Error: couldn't read "
				<< json_path_
				<< std::endl
				;
		return EXIT_FAILURE;

	}

	int controller::generate() const
	{
		const auto cfg = cyy::read_config(json_path_);
		if (cfg.second)
		{
			BOOST_ASSERT_MSG(cyy::primary_type_code_test<cyy::types::CYY_TUPLE>(cfg.first), "invalid config data");

			cyy::object_reader reader(cfg.first);

			const std::string ssl_pwd = reader["ssl"].get_string("ssl-pwd");
			const std::string ssl_certificat_req = reader["ssl"].get_string("ssl-certificate-req", (boost::filesystem::current_path() / "config" / "server.csr").string());
			const std::string ssl_certificate = reader["ssl"].get_string("ssl-certificate", (boost::filesystem::current_path() / "config" / "server.crt").string());
			const std::string ssl_private_key = reader["ssl"].get_string("ssl-private-key", (boost::filesystem::current_path() / "config" / "server.key").string());
			const std::string ssl_dh = reader["ssl"].get_string("ssl-dh", (boost::filesystem::current_path() / "config" / "dh2048.pem").string());

			//
			//	openssl genrsa -des3 -out server.key 1024
			//

			//	http://stackoverflow.com/questions/5927164/how-to-generate-rsa-private-key-using-openssl

			cyy::crypto::RSA_ptr pk = cyy::crypto::generate_RSA_private_key(1024, RSA_3);

			//
			//	write RSA private key
			//
			std::cout
				<< "write RSA private key "
				<< ssl_private_key
				<< std::endl;
			cyy::crypto::write_RSA_key(ssl_private_key, pk.get());

			const std::string req_country = reader["request"].get_string("country", "CH");
			const std::string req_province = reader["request"].get_string("province", "Lucerne");
			const std::string req_city = reader["request"].get_string("city", "Obernau");
			const std::string req_org = reader["request"].get_string("organization", "Pirx Ltd.");
			const std::string req_common = reader["request"].get_string("common", "localhost");

			std::cout
				<< "write certificate request "
				<< ssl_certificat_req
				<< std::endl;
			cyy::crypto::gen_X509_request(pk.release()
				, ssl_certificat_req
				, req_country
				, req_province
				, req_city
				, req_org
				, req_common);

			//using RSA_ptr = std::unique_ptr<RSA, decltype(&::RSA_free)>;
			//using BN_ptr = std::unique_ptr<BIGNUM, decltype(&::BN_free)>;
			//using BIO_FILE_ptr = std::unique_ptr<BIO, decltype(&::BIO_free)>;

			//RSA_ptr rsa(RSA_new(), ::RSA_free);
			//BN_ptr bn(BN_new(), ::BN_free);

			////int rc = ::BN_set_word(bn.get(), RSA_F4);
			//int rc = ::BN_set_word(bn.get(), RSA_3);
			//if (rc != 1)	return EXIT_FAILURE;

			//
			//	generate RSA private key
			//
			//rc = ::RSA_generate_key_ex(rsa.get(), 1024, bn.get(), NULL);
			//if (rc != 1)	return EXIT_FAILURE;

			// Convert RSA to PKEY
			//EVP_KEY_ptr pkey(EVP_PKEY_new(), ::EVP_PKEY_free);
			//rc = EVP_PKEY_set1_RSA(pkey.get(), rsa.get());


			//BIO_FILE_ptr key1(BIO_new_file(ssl_private_key.c_str(), "w"), ::BIO_free);
			//rc = ::PEM_write_bio_RSAPrivateKey(key1.get(), rsa.get(), NULL, NULL, 0, NULL, NULL);
			//if (rc != 1)	return EXIT_FAILURE;

			//	http://www.codepool.biz/how-to-use-openssl-to-generate-x-509-certificate-request.html

			return EXIT_SUCCESS;
		}
		std::cerr
			<< "***Error: couldn't read "
			<< json_path_
			<< std::endl
			;
		return EXIT_FAILURE;
	}

#if defined(BOOST_OS_WINDOWS_AVAILABLE)
	int controller::run_as_service(controller&& ctrl, std::string const& srv_name)
	{
		//	define service type
		typedef service< controller >	service_type;

		//	create service
		service_type srv(std::move(ctrl), srv_name);

		::OutputDebugString("start server ");
		::OutputDebugString(srv_name.c_str());

		//	starts dispatcher and calls service main() function 
		switch (srv.run())
		{
		case ERROR_SERVICE_ALREADY_RUNNING:
			//	An instance of the service is already running.
			::OutputDebugString("An instance of the [Chat Server] is already running.");
			break;
		case ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
			//
			//	The service process could not connect to the service controller.
			//	Typical error message, when running in console mode.
			//
			::OutputDebugString("***Error 1063: The [Chat Server] process could not connect to the service controller.");
			std::cerr
				<< "***Error 1063: The [Chat Server] process could not connect to the service controller."
				<< std::endl
				;
			break;
		case ERROR_SERVICE_NOT_IN_EXE:
			//	The executable program that this service is configured to run in does not implement the service.
			::OutputDebugString("The [Chat Server] is configured to run in does not implement the service.");
			//std::cerr
			//	<< "The  Chat Server is configured to run in does not implement the service."
			//	<< std::endl
			//	;
			break;
		default:
			::OutputDebugString("[Chat Server] dispatcher stopped");
			break;
		}

		return EXIT_SUCCESS;
	}

	void controller::control_handler(DWORD sig)
	{
		//	forward signal to shutdown manager
		cyy::forward_signal(sig);
	}

#endif

}	//	cyx
