/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "session.h"
#include <cyx/constants.h>
#include <cyx/http/lexer1.1.h>
#include <cyx/http/serializer1.1.h>
#include <cyx/http/intrinsics/io/io.h>
#include <cyx/http/intrinsics/factory/status_factory.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/scheduler/factory.hpp>
#include <cyy/io/io_set.h>
#include <cyy/io/hex_dump.hpp>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/util/read_sets.h>
#include <array>
#include <boost/uuid/uuid_io.hpp>
#include <boost/core/ignore_unused.hpp>

namespace cyx
{

	//	--- HTTP session implementation
	struct chat_session
	{

		/**
		 * define a receive buffer type
		 */
	#if defined(BOOST_NO_CXX11_CONSTEXPR)
		typedef std::array< char, BUFFER_SIZE >	buffer_type;
	#else
		typedef std::array< char, constants::BUFFER_SIZE >	buffer_type;
	#endif

		/**
		 * Pointer to session interface
		 */
		session* session_;

		/**
		 * The TCP/IP socket (secured)
		 */
		socket_t socket_;

		/**
		 * Hold received data
		 */
		buffer_type	read_buffer_;

		/**
		 * internal VM
		 */
		cyy::vm_dispatcher vm_;

		/**
		 * HTTP/1.1 parser
		 */
		lexer 	parser_;

		/**
		 * Serialization engine for HTTP/1.1 protocol
		 */
		serializer	serializer_;

		/**
		 * managing all session tasks
		 */
		cyy::scheduler::factory	task_manager_;

		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;

		/**
		 * document root
		 */
		const boost::filesystem::path  htdocs_;
		const path_t	get_content_;


		chat_session(session* client
			, boost::asio::ssl::context& ctx
			, cyy::ruler& scheduler
			, logger_type& logger
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, boost::filesystem::path const& htdocs)
		: session_(client)
		, socket_(scheduler.get_io_service(), ctx)
		, read_buffer_()
		, vm_(tag, socket_.get_io_service())
		, parser_([this](cyy::vector_t&& prg) {

			std::for_each(prg.begin(), prg.end(), [this](cyy::object obj) {
				CYY_LOG_TRACE(logger_, "param: " << cyy::to_literal(obj, cyx::io::custom));
			});
			CYY_LOG_TRACE(logger_, "execute "
						<< prg.size()
						<< " instructions");

			//
			//	parser callback
			//
			vm_.exec(std::move(prg));

		}, logger)
		, serializer_(vm_, db)
		, task_manager_(scheduler)
		, logger_(logger)
		, htdocs_(htdocs)
		, get_content_({"get", "snippet"})
		{

			//
			//	This is the adapter between the read and write operation of the socket.
			//	The serializer contains a buffer which serializes data but needs
			//	an (open) socket to send them.
			//
			vm_.async_run(cyy::register_function("io.flush", 0, [this](cyy::context& ctx) {
 				const boost::system::error_code ec = serializer_.flush(socket_);
 				ctx.set_register(ec);
			}));

			//
			//	Close the TCP/IP socket
			//
			vm_.async_run(cyy::register_function("io.close", 0, [this](cyy::context& ctx) {

				CYY_LOG_INFO(logger_, "session "
                    << cyy::uuid_short_format(vm_.tag())
                    << " - io.close");

				boost::system::error_code ec;
				//	end SSL connection
				socket_.shutdown(ec);
				//	ends TCP/IP connection
 				socket_.lowest_layer().shutdown(boost::asio::ip::tcp::socket::shutdown_both);
// 				socket_.close(ec);
				ctx.set_register(ec);

			}));

			//
			//	"http.get"
			//
			vm_.async_run(cyy::register_function("http.get", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	[v1.1,"/favicon.ico",?,"",%(("Accept":"*/*"),("Accept-Encoding":"gzip, deflate, sdch"),("Accept-Language":"de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4"),...)]
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag()) 
					<< " - http.get: "
					<< cyy::io::to_literal(frame, cyx::io::custom ));

				//	get params
				cyy::vector_reader	reader(frame);
				const cyy::version v = reader.get(0, cyy::version());
				const path_t p = reader.get(1, path_t());
				boost::ignore_unused(v);

				if (is_favicon(p))
				{
					//	image/x-icon
					ctx.invoke("io.send.favicon");
					ctx.invoke("io.flush");
				}
				else
				{
					if (is_directory(p))
					{
						if (starts_with(p, get_content_))
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / "/posts/text002.txt")
							));
						}
						else
						{ 
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / to_path(p) / "index.html")
							));
						}
					}
					else
					{
						if ((p.size() == 1) && (p.at(0) == "time.html"))
						{
							std::string msg("<html><body>hello, world at "
								+ cyy::json_format(std::chrono::system_clock::now())
								+ "</body></html>");

							std::stringstream ss;
							ss
								<< "HTTP/1.1 200 OK"
								<< "\r\n"
								<< "Content-Type: text/html"
								<< "\r\n"
								<< "Content-Length: "
								<< msg.size()
								<< "\r\n"
								<< "\r\n"
								<< msg
								;
							const std::string response(ss.str());
							boost::asio::write(socket_, boost::asio::buffer(response.data(), response.size()));

						}
						else
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / to_path(p))
							));
						}
					}
					ctx.invoke("io.flush");
				}

			}));

			//
			//	"http.head"
			//
			vm_.async_run(cyy::register_function("http.head", 2, [this](cyy::context& ctx) {

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.head");

				const std::string response("HTTP/1.1 200 OK\r\n\r\n<html><body>welcome</body></html>\r\n");
				socket_.write_some(boost::asio::buffer(response.data(), response.size()));

			}));

			//
			//	"http.post.xml"
			//
			vm_.async_run(cyy::register_function("http.post.xml", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.xml"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	"http.post.json"
			//
			vm_.async_run(cyy::register_function("http.post.json", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.json"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	"http.connect"
			//	working as relay
			//
			vm_.async_run(cyy::register_function("http.connect", 2, [this](cyy::context& ctx) {

				//
				//	example:
				//	CONNECT  126mx03.mxmail.netease.com:25 HTTP / 1.0<CR><NL><CR><NL>
				//

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.connect");

				ctx.inject(cyy::generate_invoke("io.stock.response", cyy::status_factory(codes::MethodNotAllowed)));
				ctx.invoke("io.flush");

			}));

			//
			//	http.post.form
			//
			vm_.async_run(cyy::register_function("http.post.form", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.form"
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

		}

		~chat_session()
		{

			CYY_LOG_DEBUG(logger_,
				"session "
				<< cyy::uuid_short_format(vm_.tag())
				<< " terminated");
		}

		/**
		 * Start and continue reading from socket
		 */
		void start()
		{

			//	hold a reference
			session::shared_type safe_sp = session_->shared_from_this();

			BOOST_ASSERT_MSG(socket_.lowest_layer().is_open(), "socket closed");
			BOOST_ASSERT_MSG(safe_sp, "no session");

			socket_.async_read_some(boost::asio::buffer(read_buffer_, sizeof(buffer_type)-1),
			[this, safe_sp](boost::system::error_code const & ec, std::size_t bytes_transferred) {

				if (!ec)
				{
					BOOST_ASSERT_MSG(bytes_transferred < read_buffer_.size(), "buffer overflow");
					
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " received "
						<< bytes_transferred 
						<< " bytes");

					//std::ostringstream ss;
					//cyy::io::hex_dump hd;
					//hd(ss, &this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);
					//CYY_LOG_TRACE(logger_, '\n' << ss.str());
					
					//
					//	run parser and implicit VM
					//
					this->parser_.read(&this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);

					//
					//	+-----                                    -----+
					//	The parser may produce code that closes the socket.
					//	So we must be aware of this case.
					//	To close a socket, VM must work in sync-mode. Otherwise
					//	there would be no guaranty that this session is still valid.
					//	+-----                                    -----+
					//
					if (socket_.lowest_layer().is_open())
					{
						this->start();
					}
				}
				else
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - closed");
					
					//	close socket explicitely
					vm_.invoke("io.close");
					vm_.halt();

				}
			});
		}

	};

	//	--- HTTP1/1 session

	session::session(boost::asio::ssl::context& ctx
		, cyy::ruler& scheduler
		, logger_type& logger
		, cyy::store::store& db
		, boost::uuids::uuid tag
		, boost::filesystem::path const& htdocs)
		//	make_unique<> requires a c++14 complient compiler
#if __GNUC__ > 6 || defined(_MSC_VER) 	
		: impl_(std::make_unique<chat_session>(this	
#else
		: impl_(std::make_shared<chat_session>(this	
#endif
		, ctx
		, scheduler
		, logger
		, db
		, tag
		, htdocs))	
	{}

	session::~session()
	{
		//	delete HTTP1/1 session explicit
		impl_.reset();
	}

	cyy::vm_dispatcher& session::vm()
	{
		return impl_->vm_;
	}
	
// 	boost::system::error_code session::run(cyy::vector_t const& prg)
// 	{
// 		return impl_->vm_.run(cyy::vector_t(prg));
// 	}
// 
// 	void session::run_async(cyy::vector_t&& prg)
// 	{
// 		impl_->vm_.async_run(std::move(prg));
// 	}
// 
// 	boost::uuids::uuid session::tag() const
// 	{
// 		return impl_->vm_.tag();
// 	}

	void session::start()
	{
		auto self = this->shared_from_this();
		impl_->socket_.async_handshake(boost::asio::ssl::stream_base::server, [self](boost::system::error_code ec) {

			if (!ec)
			{
				CYY_LOG_TRACE(self->impl_->logger_, "session "
					<< cyy::uuid_short_format(self->impl_->vm_.tag())
					<< " - starts reading from "
					<< self->get_socket().remote_endpoint());
				//
				//
				//	Start session to read from socket
				self->impl_->start();
			}
			else
			{
				CYY_LOG_FATAL(self->impl_->logger_, "session "
					<< cyy::uuid_short_format(self->impl_->vm_.tag())
					<< " - "
					<< ec
					<< ": "
					<< ec.message());
			}
		});
	}

    socket_t::lowest_layer_type& session::get_socket()
    {
        return socket(impl_->socket_);
    }
    
}	//	cyx

