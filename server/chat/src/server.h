/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_CHAT_SERVER_H
#define CYX_CHAT_SERVER_H

#include "session.h"
#include "../../../lib/shared/ssl_server.hpp"
#include <cyy/scheduler/logging/log.h>
#include <memory>
#include <boost/uuid/random_generator.hpp>
//#include <boost/uuid/uuid.hpp>

namespace cyy
{
	class object;
	class ruler;
	namespace store
	{
		class store;
	}
}

namespace cyx
{

	class server : public ssl_srv<session::shared_type>
	{
	public:
		typedef std::map<boost::uuids::uuid, session::weak_type>	connections_t;
		
	public:
		server() = delete;
		server(server const&) = delete;

		/**
		 * @param scheduler thread scheduler
		 * @param account administrator account
		 * @param pwd_hash password hash
		 * @param monitor maximal timeout for login and other requests
		 */
		server(cyy::ruler& scheduler
			, logger_type& logger
			, cyy::store::store& db
			, boost::uuids::uuid tag

			, std::string const& ssl_pwd
			, std::string const& ssl_certificate
			, std::string const& ssl_private_key
			, std::string const& ssl_dh
			
			, std::string const& root
			, std::string const& css
			, std::string const& js);

		virtual ~server();

		/**
		 * Startup TCP/IP server
		 */
		bool run(cyy::tuple_t const& cfg);

		/**
		 * Shutdown TCP/IP server
		 */
		void stop();

	protected:
 		virtual void on_connect(session::shared_type sp);
		virtual void on_error(boost::system::error_code const& ec);
        session::shared_type create(boost::asio::ssl::context&);
		
		void connect(session::shared_type sp);
		std::size_t stop_clients();

	private:
		cyy::ruler& 	scheduler_;
		logger_type&	logger_;
		cyy::store::store& db_;
		boost::uuids::random_generator	uuid_gen_;	//	basic_random_generator<mt19937>
		connections_t	connections_;
		
		const boost::filesystem::path	htdocs_;
		const boost::filesystem::path	css_file_;
		const boost::filesystem::path	js_file_;
	};
	
	/**
	 * helper class to keep session list clean
	 */
	class tidy
	{
	public:
		tidy(logger_type&, server::connections_t&);
		void operator()(session* ptr) const;
		
	private:
		logger_type& logger_;
		server::connections_t& connections_;
	};
	
	namespace chat 
	{
		/**
		 * session factory
		 */
		session::shared_type make_session(boost::asio::ssl::context&
            , cyy::ruler&
			, logger_type& logger
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, server::connections_t&
			, boost::filesystem::path const&);
	}
		
}

#endif	//	CYX_CHAT_SERVER_H

