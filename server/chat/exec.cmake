# top level files
set (chat_server)

set (chat_server_cpp

	server/chat/src/main.cpp
	server/chat/src/controller.cpp
    server/chat/src/server.cpp
    server/chat/src/session.cpp
	
)

set (chat_server_h

	server/chat/src/controller.h
	server/chat/src/server.h
	server/chat/src/session.h
	
	lib/shared/ssl_server.hpp

	src/main/include/cyx/version_info.h
	src/main/include/cyx/constants.h
	src/main/include/cyx/layer4/ssl_socket.h

	src/main/include/cyx/http/session/tidy.hpp
)

set (chat_server_res
	server/chat/htdocs/index.html
# 	server/chat/htdocs/css/chat.css
)

set (chat_server_setup
# 	src/main/templates/create_chat_srv.bat.in
# 	src/main/templates/delete_chat_srv.bat.in
# 	src/main/templates/restart_chat_srv.bat.in
# 	src/main/templates/chat.windows.cgf.in
)

source_group("resources" FILES ${chat_server_res})
source_group("setup" FILES ${chat_server_setup})

# define the main program
set (chat_server
  ${chat_server_cpp}
  ${chat_server_h}
  ${chat_server_res}
  ${chat_server_setup}
)
