/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_SERVER_H
#define CYX_HTTP_SERVER_H

#include "session.h"
#include "../../../lib/shared/tcpip_server.h"
#include <cyy/store/store.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/scheduler/registry.h>
#include <memory>
#include <boost/uuid/random_generator.hpp>

#include <boost/uuid/uuid.hpp>

namespace cyy
{
	class object;
	class ruler;
}

namespace cyx
{

	class server : public tcpip_srv
	{
	public:
		typedef std::map<boost::uuids::uuid, session::weak_type>	connections_t;
		
	public:
		server() = delete;
		server(server const&) = delete;

		/**
		* @param scheduler thread scheduler
		* @param account administrator account
		* @param pwd_hash password hash
		* @param monitor maximal timeout for login and other requests
		*/
		server(logger_type& logger
			, cyy::ruler& scheduler
			, cyy::store::store&
			, boost::uuids::uuid tag
			, std::string const&
			, std::vector<std::string> const&
			, std::string const&
			, std::string const& realm);

		virtual ~server();

		/**
		* Startup TCP/IP server
		*/
		bool run(std::string const&, std::string const&);

		/**
		* Shutdown TCP/IP server
		*/
		void stop();

	protected:
		virtual void on_connect(socket_t&& s);
		virtual void on_error(boost::system::error_code const& ec);
		
		void connect(session::shared_type sp);
// 		std::size_t stop_clients();

	private:
// 		cyy::ruler& 	scheduler_;
		logger_type&	logger_;
		cyy::scheduler::registry	registry_;
		cyy::store::store&	db_;
		const boost::filesystem::path htdocs_;
		const boost::filesystem::path storage_;
		const std::string realm_;
		boost::uuids::random_generator	uuid_gen_;	//	basic_random_generator<mt19937>
// 		connections_t	connections_;
	};
		
	namespace http 
	{
		/**
		 * session factory
		 */
		session::shared_type make_session(boost::asio::ip::tcp::socket&&
// 			, cyy::ruler&
			, logger_type& logger
			, cyy::scheduler::registry&
			, cyy::store::store&
			, boost::uuids::uuid tag
			, boost::filesystem::path const&
// 			, server::connections_t&
			, boost::filesystem::path const&
			, std::string const&);
	}
		
}

#endif	//	CYX_HTTP_SERVER_H

