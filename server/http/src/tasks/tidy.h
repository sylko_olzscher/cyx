/*
 * Copyright Sylko Olzscher 2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_PLOG_TASK_TIDY_H
#define CYX_PLOG_TASK_TIDY_H

#include "../session.h"
#include <cyy/scheduler/broker.h>
#include <cyy/scheduler/logging/log.h>

namespace cyx
{
	class task_tidy
	{
	public:
		typedef std::map<boost::uuids::uuid, session::weak_type>	connections_t;

	public:
		using msg_0 = std::tuple<std::string, cyy::vector_t>;
		using msg_1 = std::tuple<session::shared_type>;
		using msg_2 = std::tuple<boost::uuids::uuid>;
		using msg_3 = std::tuple<>;
		using msg_type = std::tuple<msg_0, msg_1, msg_2, msg_3>;

	public:
		task_tidy(cyy::broker& b
		, logger_type&);

		/**
		 * slot 0
		 * Forward function call
		 */
		void process(std::string const& fun, cyy::vector_t const& data);

		/**
		 * slot 1
		 * Server event - on_connect(). Insert new session.
		 */
		void process(session::shared_type);

		/**
		 * slot 2
		 * Remove from session list
		 */
		void process(boost::uuids::uuid);

		/**
		 * slot 3
		 * Close all sessions.
		 */
		void process();

		void stop();
		void run(int workload);

	private:
		cyy::broker& broker_;
		logger_type& logger_;
		connections_t connections_;
	};

}	//	cyx

#endif	//	CYX_PLOG_TASK_TIDY_H


