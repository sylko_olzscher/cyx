/*
 * Copyright Sylko Olzscher 2015-2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "session.h"
#include <cyx/constants.h>
#include <cyx/http/intrinsics/factory/status_factory.h>
#include <cyx/http/intrinsics/factory/method_factory.h>
#include <cyx/http/lexer1.1.h>
#include <cyx/http/serializer1.1.h>
#include <cyx/http/intrinsics/io/io.h>
#include <cyx/http/parser/auth_parser.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/vm/code_generator.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/intrinsics/factory/boost_factory.h>
#include <cyy/scheduler/factory.hpp>
#include <cyy/io/io_set.h>
#include <cyy/io/hex_dump.hpp>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/intrinsics/algorithm.h>
#include <cyy/store/store.h>
#include <boost/uuid/uuid_io.hpp>
#include <boost/core/ignore_unused.hpp>

namespace cyx
{

	//	--- HTTP session implementation
	struct http_session
	{

		/**
		 * define a receive buffer type
		 */
#if defined(BOOST_NO_CXX11_CONSTEXPR)
		typedef std::array< char, BUFFER_SIZE >	buffer_type;
#else
		typedef std::array< char, constants::BUFFER_SIZE >	buffer_type;
#endif

		/**
		 * Pointer to session interface
		 */
		session* session_;

		/**
		 * The TCP/IP socket (unsecured)
		 */
		socket_t socket_;

		/**
		 * Hold received data
		 */
		buffer_type	read_buffer_;

		/**
		 * internal VM
		 */
		cyy::vm_dispatcher vm_;

		/**
		 * HTTP/1.1 parser
		 */
		lexer 	parser_;

		/**
		 * Serialization engine for HTTP/1.1 protocol
		 */
		serializer	serializer_;

		/**
		 * managing all session tasks
		 */
// 		cyy::scheduler::factory	task_manager_;
		/**
		 * managing all session tasks
		 */
		cyy::scheduler::registry&	reg_;

		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;

		/**
		 * document root, storage root
		 */
		const boost::filesystem::path htdocs_;
		const boost::filesystem::path storage_;

		const std::string realm_;
		/**
		 * session upgrade state
		 */
		enum upgrade_state
		{
			STATE_BASIC_HTTP,
			STATE_WEBSOCKET,
		} state_;

		/*
		 * Timer for testing purposes
		 */
		boost::asio::steady_timer timer_;

		/**
		 * shutdown state. There is a difference between a closed session
		 * and a system shutdown. If this flag is true, the complete system
		 * shuts down and no further action is required.
		 */
		bool shutdown_;

		http_session(session* client
			, socket_t&& s
// 			, cyy::ruler& scheduler
			, logger_type& logger
			, cyy::scheduler::registry& reg
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, boost::filesystem::path const& htdocs
			, boost::filesystem::path const& storage
			, std::string const& realm)
		: session_(client)
			, socket_(std::move(s))
			, read_buffer_()
			, vm_(tag, socket_.get_io_service())
			, parser_(vm_, [this](cyy::vector_t&& prg) {

				//std::for_each(prg.begin(), prg.end(), [this](cyy::object obj) {
				//	CYY_LOG_TRACE(logger_, "param: " << cyy::to_literal(obj, cyx::io::custom));
				//});
				//CYY_LOG_TRACE(logger_, "execute "
				//			<< prg.size()
				//			<< " instructions");

				//
				//	parser callback
				//
				vm_.exec(std::move(prg));

			}, logger)
			, serializer_(vm_, db)
			, reg_(reg)
			, logger_(logger)
			, htdocs_(htdocs)
			, storage_(storage)
			, realm_(realm)
			, state_(STATE_BASIC_HTTP)
			, timer_(reg_.ruler_.get_io_service())
			, shutdown_(false)
		{

			//
			//	This is the adapter between the read and write operation of the socket.
			//	The serializer contains a buffer which serializes data but needs
			//	an (open) socket to send them.
			//
			vm_.async_run(cyy::register_function("io.flush", 0, [this](cyy::context& ctx) {
				const boost::system::error_code ec = serializer_.flush(socket_);
				ctx.set_register(ec);
			}));

			//
			//	Close the TCP/IP socket
			//
			vm_.async_run(cyy::register_function("io.close", 0, [this](cyy::context& ctx) {

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - io.close");

				boost::system::error_code ec;
				socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
				socket_.close(ec);
				ctx.set_register(ec);

			}));

			//
			//	set shutdown flag
			//
			vm_.async_run(cyy::register_function("session.shutdown", 0, [this](cyy::context& ctx) {

				BOOST_ASSERT_MSG(!shutdown_, "session already in shutdown mode");
				shutdown_ = true;

				CYY_LOG_INFO(logger_, "session."
					<< cyy::uuid_short_format(vm_.tag())
					<< ".session.shutdown ");

			}));

			//
			//	"http.get"
			//
			vm_.async_run(cyy::register_function("http.get", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	[v1.1,"/favicon.ico",?,"",%(("Accept":"*/*"),("Accept-Encoding":"gzip, deflate, sdch"),("Accept-Language":"de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4"),...)]
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.get: "
					<< cyy::io::to_literal(frame, cyx::io::custom ));

				//	get params
				cyy::vector_reader	reader(frame);
				const cyy::version v = reader.get(0, cyy::version());
				const path_t p = reader.get(1, path_t());
				boost::ignore_unused(v);

				if (is_favicon(p))
				{
					ctx.invoke("io.send.favicon");
					ctx.invoke("io.flush");
				}
				else
				{
					if (is_directory(p))
					{
						ctx.inject(cyy::generate_invoke("io.send.file"
							, cyy::factory(htdocs_ / to_path(p) / "index.html")
						));
					}
					else
					{
						if (starts_with(p, { "storage" }))
						{
							CYY_LOG_INFO(logger_, "session "
								<< cyy::uuid_short_format(vm_.tag())
								<< " - download "
								<< (storage_.parent_path() / to_path(p)));

							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(storage_.parent_path() / to_path(p))));

							ctx.inject(cyy::generate_invoke("file.delete"
								, cyy::factory(storage_.parent_path() / to_path(p))));
							
						}
						else if (is_equal(p, { "feed.rss" }))
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / "rss.xml")
							));

						}
						else if (is_equal(p, { "files.json" }))
						{
							//	deliver a list of files from the storage directory
							this->send_file_list(ctx);
						}
						else if (is_equal(p, { "basic-auth.html" }))
						{
							const std::string auth = reader[4].get_string("Authorization");
							if (auth.empty() || boost::algorithm::equals(auth, "Basic Og=="))
							{
								//	send basic authorization request
								ctx.inject(cyy::generate_invoke("io.auth.basic"
									, cyy::factory(realm_)
								));
							}
							else
							{
								const std::pair<authentification, bool>	r = parse_auth(auth);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - credentials: "
									<< r.first.credentials_);

								//	queen DRIP YELP bestbuy 0 VISA
								if (boost::algorithm::equals(r.first.credentials_, "guest:qDYb0V")
                                    || boost::algorithm::equals(r.first.credentials_, "user:c57s"))
								{ 
									ctx.inject(cyy::generate_invoke("io.send.file"
										, cyy::factory(htdocs_ / to_path(p))
									));
								}
								else
								{
									//	send basic authorization request
									ctx.inject(cyy::generate_invoke("io.auth.basic"
										, cyy::factory(realm_)
									));
								}
							}
						}
						else if (is_equal(p, { "cv.download.html" }) || is_equal(p, { "david.download.html" }))
						{
							const std::string auth = reader[4].get_string("Authorization");
							if (auth.empty() || boost::algorithm::equals(auth, "Basic Og=="))
							{
								//	send basic authorization request
								ctx.inject(cyy::generate_invoke("io.auth.basic"
									, cyy::factory(realm_)
								));
							}
							else
							{
								const std::pair<authentification, bool>	r = parse_auth(auth);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - credentials: "
									<< r.first.credentials_);
								//	queen DRIP YELP bestbuy 0 VISA
								if (boost::algorithm::equals(r.first.credentials_, "guest:qDYb0V")
                                    || boost::algorithm::equals(r.first.credentials_, "user:c57s")
                                )
								{
									ctx.inject(cyy::generate_invoke("io.send.file"
										, cyy::factory(htdocs_ / to_path(p))
									));
								}
								else
								{
									//	send basic authorization request
									ctx.inject(cyy::generate_invoke("io.auth.basic"
										, cyy::factory(realm_)
										, cyy::string_factory("unknown account")
									));
								}
							}
						}
						else if (is_equal(p, { "digest-auth.html" }))
						{
							//https://en.wikipedia.org/wiki/Digest_access_authentication
							const std::string auth = reader[4].get_string("Authorization");
							if (auth.empty())
							{
								//	send basic authorization request
								ctx.inject(cyy::generate_invoke("io.auth.digest"
									, cyy::factory(realm_)
									, cyy::string_factory("MD5")
									, cyy::string_factory("7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v")
									, cyy::string_factory("FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS")
								));
							}
							else
							{
								const std::pair<authentification, bool>	r = parse_auth(auth);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - user: "
									<< r.first.user_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - realm: "
									<< r.first.realm_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - nonce: "
									<< r.first.nonce_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - response: "
									<< r.first.response_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - opaque: "
									<< r.first.opaque_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - qop: "
									<< r.first.qop_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - nc: "
									<< r.first.nc_);
								CYY_LOG_INFO(logger_, "session "
									<< cyy::uuid_short_format(vm_.tag())
									<< " - cnonce: "
									<< r.first.cnonce_);

								ctx.inject(cyy::generate_invoke("io.send.file"
									, cyy::factory(htdocs_ / to_path(p))
								));
							}
						}
						else
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / to_path(p))
							));
						}
					}
					ctx.invoke("io.flush");
				}

			}));

			//
			//	"http.head"
			//
			vm_.async_run(cyy::register_function("http.head", 2, [this](cyy::context& ctx) {

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.head");

				const std::string response("HTTP/1.1 200 OK\r\n\r\n<html><body>welcome</body></html>\r\n");
				socket_.write_some(boost::asio::buffer(response.data(), response.size()));

			}));

			//
			//	"http.post.xml"
			//
			vm_.async_run(cyy::register_function("http.post.xml", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.xml"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	"http.post.json"
			//
			vm_.async_run(cyy::register_function("http.post.json", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.json"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	http.post.form
			//
			vm_.async_run(cyy::register_function("http.post.form", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.form"
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

			//
			//	http.upload.progress
			//
			vm_.async_run(cyy::register_function("http.upload.progress", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	["filesToUpload[]",4630idx,4873idx,95idx]
				//CYY_LOG_INFO(logger_, "session "
				//<< cyy::uuid_short_format(vm_.tag())
				//	<< " - http.upload.progress"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				cyy::vector_reader	reader(frame);
				const std::uint32_t progress = reader.get_numeral<std::uint32_t>(2, 0);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.upload.progress "
					<< progress
					<< "%");


			}));

			//
			//	http.upload.data
			//
			vm_.async_run(cyy::register_function("http.upload.data", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	synopsis: 
				//	* variable name (id)
				//	* file name
				//	* MIME type
				//	* data
				//	* URL
				//	["filesToUpload[]","sml_2016_08_01T15_15_17_00000967_00008edd_gex.gwf.ch_26862.xml",text/xml,'\x3c\x3...]
				//CYY_LOG_INFO(logger_, "session "
				//	<< vm_.tag()
				//	<< " - http.upload.data"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//	get params
				cyy::vector_reader	reader(frame);
				const std::string var = reader.get_string(0);
				const std::string file = reader.get_string(1);
				const cyy::buffer_t& data = reader.get(3, cyy::buffer_t());

				if (file.empty())
				{
					CYY_LOG_INFO(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.data - received "
						<< var
						<< " with "
						<< data.size()
						<< " bytes");
				}
				else
				{
					CYY_LOG_INFO(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.data - file "
						<< file
						<< " with "
						<< data.size()
						<< " bytes");

					//storage_
					std::fstream fout((storage_ / file).string(), std::ios::trunc | std::ios::out);
					if (fout.is_open())
					{
						fout.write(data.data(), data.size());
					}
					else
					{
						CYY_LOG_ERROR(logger_, "session "
							<< cyy::uuid_short_format(vm_.tag())
							<< " - http.upload.data - cannot open "
							<< (storage_ / file).string());
					}
				}

				if (data.size() < 64)
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.data - "
						<< var
						<< " = "
						<< std::string(data.begin(), data.end()));
				}

			}));

			//
			//	"http.upload.complete"
			//
			vm_.async_run(cyy::register_function("http.upload.complete", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	[true,85037u32]
				//CYY_LOG_INFO(logger_, "session "
				//	<< vm_.tag()
				//	<< " - http.upload.complete "
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//	get params
				cyy::vector_reader	reader(frame);
				const auto size = reader.get_numeral<std::uint32_t>(1, 0);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.upload.complete - with "
					<< size
					<< " bytes");

				//	send HTTP 303: see other
				ctx.inject(cyy::generate_invoke("io.stock.303", cyy::string_factory("/"))).invoke("io.flush");
			}));

			//
			//	http.open.websocket
			//
			vm_.async_run(cyy::register_function("http.open.websocket", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//
				//	example:
				//	[v1.1,/echo/,?,"",%(("Accept-Encoding":"gzip, deflate, sdch"),("Accept-Language":"de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4"),("Cache-Control":"no-cache"),("Connection":"Upgrade"),("DNT":true),("Host":"localhost:8080"),("Origin":"http://localhost:8080"),("Pragma":"no-cache"),("Sec-WebSocket-Extensions":"permessage-deflate; client_max_window_bits"),("Sec-WebSocket-Key":"L3jfmDvL071963wiye1FMA=="),("Sec-WebSocket-Protocol":"soap, xmpp"),("Sec-WebSocket-Version":"13"),("Upgrade":"websocket"),("User-Agent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36"))]
				//
				//CYY_LOG_INFO(logger_, "session "
				//	<< vm_.tag()
				//	<< " - http.open.websocket"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	extract the following parameters:
				//
				//	("Sec-WebSocket-Extensions":"permessage-deflate; client_max_window_bits"), 
				//	("Sec-WebSocket-Key":"L3jfmDvL071963wiye1FMA=="), 
				//	("Sec-WebSocket-Protocol":"soap, xmpp"), 
				//	("Sec-WebSocket-Version":"13"), 
				//
				cyy::vector_reader	reader(frame);
				const auto ws_key = reader[4].get_string("Sec-WebSocket-Key");
				const std::vector<std::string> ws_protocols = reader[4].get_vector<std::string>("Sec-WebSocket-Protocol", "");			
				const auto ws_version = reader[4].get_numeral<int>("Sec-WebSocket-Version", 0);

				CYY_LOG_TRACE(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.open.websocket key: "
					<< ws_key);

				CYY_LOG_TRACE(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.open.websocket version: "
					<< ws_version);

				std::string ps;
				for (auto p : ws_protocols)
				{
					ps += p;
					ps += ' ';
				}
				CYY_LOG_TRACE(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.open.websocket protocols: "
					<< ps);

				//
				//	accept incoming websocket open request
				//
				ctx.inject(cyy::generate_invoke("io.upgrade.websocket"
					, cyy::find(frame.at(4), "Sec-WebSocket-Key")
					, cyy::string_factory("soap")
				));

				//
				//	update session upgrade state
				//
				state_ = STATE_WEBSOCKET;

				ctx.invoke("io.flush");

				//
				//	send server timestamp
				//
				start_timer();

			}));

			//
			//	ws.data.text
			//
			vm_.async_run(cyy::register_function("ws.data.text", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//CYY_LOG_TRACE(logger_, "session "
				//	<< vm_.tag()
				//	<< " - ws.data.text"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	get text
				//
				const auto text = cyy::string_cast(frame.at(0), "");

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.data.text: "
					<< text);

				//
				//	echo text
				//
				ctx.inject(cyy::generate_invoke("io.ws.text"
					, cyy::factory("> " + text)
				));

				//
				//	flush
				//
				ctx.invoke("io.flush");

				//
				//	send ping
				//
				ctx.invoke("io.ws.ping");
				ctx.invoke("io.flush");

			}));
			
			//
			//	ws.data.binary
			//
			vm_.async_run(cyy::register_function("ws.data.binary", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.data.binary"
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

			//
			//	ws.ping
			//
			vm_.async_run(cyy::register_function("ws.ping", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.ping"
					<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	send pong reply
				//
				ctx.invoke("io.ws.pong");
				ctx.invoke("io.flush");

			}));

			//
			//	ws.pong
			//
			vm_.async_run(cyy::register_function("ws.pong", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.pong "
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

			//
			//	ws.close
			//
			vm_.async_run(cyy::register_function("ws.close", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.close "
					<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	cancel timer
				//
				timer_.cancel();

				//
				//	update session state
				//
				state_ = STATE_BASIC_HTTP;

			}));

			//
			//	file.delete
			//
			vm_.async_run(cyy::register_function("file.delete", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - file.delete "
					<< cyy::io::to_literal(frame, cyx::io::custom));

				const auto p = cyy::value_cast(frame.at(0), boost::filesystem::temp_directory_path());
				boost::filesystem::remove(p);

			}));

			//
			//	"http.options"
			//
			vm_.async_run(cyy::register_function("http.options", 4, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	 [v1.1,/vfs/,?,"",%(("Connection":["Keep-Alive"]),("Host":"localhost:8080"),("User-Agent":"Microsoft-WebDAV-MiniRedir/10.0.15063"),("translate":"f"))]
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< ".options "
					<< cyy::io::to_literal(frame, cyx::io::custom));

				ctx.inject(cyy::generate_invoke("io.options"
					, cyy::factory({methods::Copy, methods::Delete, methods::Get, methods::Head, methods::Lock, methods::MkCol, methods::Move, methods::Options, methods::Post, methods::Propfind, methods::Proppatch, methods::Put, methods::Unlock})
				));
				ctx.invoke("io.flush");

			}));

			vm_.async_run(cyy::register_function("http.propfind", 3, [this](cyy::context& ctx) {
				const cyy::vector_t frame = ctx.stack_frame(true);

				//	 [v1.1,/vfs/,?,"",%(("Connection":["Keep-Alive"]),("Content-Length":0i32),("Depth":"0"),("Host":"localhost:8080"),("User-Agent":"Microsoft-WebDAV-MiniRedir/10.0.15063"),("translate":"f"))]
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< ".propfind "
					<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	see https://msdn.microsoft.com/en-us/library/aa142960(v=exchg.65).aspx
				//
				//	HTTP/1.1 207 Multi-Status
				//	Content-Type: text/xml
				//	
				//	<?xml version="1.0"?>
				//	<a:multistatus xmlns:a="DAV:">
				//	 <a:response>
				//	   <a:href>http://www.contoso.com/public/docs/</a:href>
				//	   <a:propstat>
				//	    <a:prop>
				//	       <a:supportedlock>
				//	          <a:lockentry>
				//	             <a:lockscope><a:local/></a:lockscope>
				//	             <a:locktype>
				//	                <a:transaction><a:local/></a:transaction>
				//	             </a:locktype>
				//	          </a:lockentry>
				//	          <a:lockentry>
				//	             <a:lockscope><a:shared/></a:lockscope>
				//	             <a:locktype><a:write/></a:locktype>
				//	          </a:lockentry>
				//	       </a:supportedlock>
				//	    </a:prop>
				//	    <a:status>HTTP/1.1 200 OK</a:status>
				//	   </a:propstat>
				//	 </a:response>
				//	</a:multistatus>
			}));

		}

		~http_session()
		{

			CYY_LOG_DEBUG(logger_,
				"session "
				<< cyy::uuid_short_format(vm_.tag())
				<< " terminated");
		}

		/**
		 * Start and continue reading from socket
		 */
		void start()
		{

			//	hold a reference
			session::shared_type safe_sp = session_->shared_from_this();

			BOOST_ASSERT_MSG(socket_.is_open(), "socket closed");
			BOOST_ASSERT_MSG(safe_sp, "no session");

			socket_.async_read_some(boost::asio::buffer(read_buffer_, sizeof(buffer_type)-1),
			[this, safe_sp](boost::system::error_code const & ec, std::size_t bytes_transferred) {

				if (!ec)
				{
					BOOST_ASSERT_MSG(bytes_transferred < read_buffer_.size(), "buffer overflow");
					
					//CYY_LOG_TRACE(logger_, bytes_transferred << " bytes received");
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - parse "
						<< bytes_transferred
						<< " bytes in state "
						<< this->parser_.state());

#ifdef _DEBUG
					//std::ostringstream ss;
					//cyy::io::hex_dump hd;
					//hd(ss, &this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);
					//CYY_LOG_TRACE(logger_, '\n' << ss.str());
#endif
					
					//
					//	run parser and VM implicitely
					//
					this->parser_.read(&this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);

					//
					//	+-----                                    -----+
					//	The parser may produce code that closes the socket.
					//	So we must be aware of this case.
					//	To close a socket, VM must work in sync-mode. Otherwise
					//	there would be no guaranty that this session is still valid.
					//	+-----                                    -----+
					//
					if (socket_.is_open())
					{
						this->start();
					}
				}
				else
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(this->vm_.tag())
						<< (shutdown_ ? " server " : " client ")
						<< " closed socket");

					timer_.cancel();

					if (shutdown_)
					{
						//	controlled by server

						if (state_ == STATE_WEBSOCKET)
						{
							vm_.squeeze(cyy::generate_invoke("io.ws.close"));
						}
						//	close socket explicitely
						boost::system::error_code ec;
						socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
						socket_.close(ec);

					}
					else
					{
						//
						//	update shutdown state
						//
						shutdown_ = true;

						//
						//	remove from session list
						//
						reg_.send("session.admin", 2, this->vm_.tag());

					}

					//
					//	halt VM
					//
					if (vm_.halt(4096))
					{

						CYY_LOG_TRACE(logger_, "session "
							<< cyy::uuid_short_format(this->vm_.tag())
							<< " - halted");
					}
					else
					{
						CYY_LOG_ERROR(logger_, "session "
							<< cyy::uuid_short_format(this->vm_.tag())
							<< " - halted with "
							<< vm_.pending_requests()
							<< " pending requests");
					}
				}
			});
		}

		void on_timer(boost::system::error_code const& ec) 
		{
			if (ec != boost::asio::error::operation_aborted)
			{
				start_timer();
				const auto now = std::chrono::system_clock::now();
				vm_.async_run(cyy::generate_invoke("io.ws.text", cyy::factory(cyy::json_format(now))));
				vm_.invoke("io.flush");

			}
		}

		void start_timer()
		{
			timer_.expires_from_now(std::chrono::seconds(1));
			timer_.async_wait(boost::bind(&http_session::on_timer, this, boost::asio::placeholders::error));

		}

		/**
		 * Get a list of all files available for download in storage directory.
		 */
		void send_file_list(cyy::context& ctx)
		{
			cyy::param_map_t	files;
			std::for_each(boost::filesystem::directory_iterator(storage_)
				, boost::filesystem::directory_iterator()
				, [&files](boost::filesystem::path const& p)
			{
				if (boost::filesystem::is_regular_file(p))
				{
					//typedef std::map<std::string, object>	param_map_t;
					files[p.filename().string()] = cyy::factory(p);
				}
			});

			//	build a JSON string
			ctx.inject(cyy::generate_invoke("io.send.json", cyy::factory(files)));
			

		}
	};

	//	--- HTTP1/1 session

	session::session(boost::asio::ip::tcp::socket&& s
		, logger_type& logger
		, cyy::scheduler::registry& reg
		, cyy::store::store& db
		, boost::uuids::uuid tag
		, boost::filesystem::path const& htdocs
		, boost::filesystem::path const& storage
		, std::string const& realm)
		//	requires a c++14 complient compiler
#if __GNUC__ > 6 || defined(_MSC_VER) 	
    : impl_(std::make_unique<http_session>(this
#else
    : impl_(std::make_shared<http_session>(this
#endif
		, std::move(s)
// 		, scheduler
		, logger
		, reg
		, db
		, tag
		, htdocs
		, storage
		, realm))
	{}

	session::~session()
	{
		//	delete HTTP1/1 session explicit
		impl_.reset();
	}

	cyy::vm_dispatcher& session::vm()
	{
		return impl_->vm_;
	}
	
	void session::start()
	{
		//
		//
		//	Start session to read from socket
		impl_->start();
	}
	

}	//	cyx

namespace cyy
{
	object factory(cyx::session::shared_type s)
	{
		return object(core::make_value(s));
	}

	object factory(cyx::session::weak_type w)
	{
		return object(core::make_value(w));
	}

	object weak_session_ptr_factory(cyx::session::shared_type sp)
	{
		cyx::session::weak_type wp = sp;
		return factory(wp);
	}
}
