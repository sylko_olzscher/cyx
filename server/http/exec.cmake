# top level files
set (http_server)

set (http_server_cpp

	server/http/src/main.cpp
	server/http/src/controller.cpp
	server/http/src/server.cpp
 	server/http/src/session.cpp
 	
	lib/shared/tcpip_server.cpp
	
)

set (http_server_h

	server/http/src/controller.h
	server/http/src/server.h
	server/http/src/session.h
	
	lib/shared/tcpip_server.h

	src/main/include/cyx/version_info.h
	src/main/include/cyx/constants.h
	src/main/include/cyx/layer4/tcpip_socket.h

	src/main/include/cyx/http/session/tidy.hpp
)

set (http_server_res
	server/http/htdocs/index.html
	server/http/htdocs/rss.xml
	server/http/htdocs/index.html
	server/http/htdocs/basic-auth.html
	server/http/htdocs/digest-auth.html
	server/http/htdocs/cv.html
	server/http/htdocs/cv.download.html
	server/http/htdocs/images/img-01.png
	server/http/htdocs/images/img-02.png
	server/http/htdocs/images/img-03.png
	server/http/htdocs/images/img-04.png
	server/http/htdocs/images/img-05.png
	server/http/htdocs/images/img-06.png
	server/http/htdocs/images/img-07.png
	server/http/htdocs/docs/parallels-2016.pdf
	server/http/htdocs/docs/PKI-certificate.pdf
	server/http/htdocs/docs/windows-server-2016.pdf
)

set (http_server_setup
	src/main/templates/create_http_srv.bat.in
	src/main/templates/delete_http_srv.bat.in
	src/main/templates/restart_http_srv.bat.in
	src/main/templates/http.windows.cgf.in
)

set (http_server_tasks
	server/http/src/tasks/tidy.h
	server/http/src/tasks/tidy.cpp
)


source_group("resources" FILES ${http_server_res})
source_group("setup" FILES ${http_server_setup})
source_group("tasks" FILES ${plog_server_tasks})

# define the main program
set (http_server
  ${http_server_cpp}
  ${http_server_h}
  ${http_server_res}
  ${http_server_setup}
  ${http_server_tasks}
)
