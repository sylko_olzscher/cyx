﻿

<h2 class="blog-post-title">Covariance and Contravariance in C++ Standard Library</h2>
<p class="blog-post-meta">Sumant Tambe</p>
<p><span class="glyphicon glyphicon-time"></span> Posted on November 14, 2015 at 10:00 PM</p>

<a href="https://en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science)">Covariance and Contravariance</a> are concepts that come up often as you go deeper into generic programming. While designing a language that supports parametric polymorphism (e.g., templates in C++, generics in Java, C#), the language designer has a choice between Invariance, Covariance, and Contravariance when dealing with generic types. C++'s choice is "invariance". Let's look at an example. <br />
<pre class="brush: cpp">
struct Vehicle {};
struct Car : Vehicle {};

std::vector&lt;Vehicle *&gt; vehicles;
std::vector&lt;Car *&gt; cars;

vehicles = cars; // Does not compile
</pre>

The above program does not compile because C++ templates are invariant. Of course, each time a C++ template is instantiated, the compiler creates a brand new type that uniquely represents that instantiation. Any other type to the same template creates another unique type that has nothing to do with the earlier one. Any two unrelated user-defined types in C++ can't be assigned to each-other by default. You have to provide a copy-constructor or an assignment operator. 
<br/>
<br/>
However, the fun starts when you realize that it's just a choice and there are other valid choices. In fact, C++ makes a different choice for pointers and references. For example, it's common knowledge that pointer of type Car is assignable to pointer of type Vehicle. That's because Car is a subtype of Vehicle. More accurately, the Car struct inherits from the Vehicle struct and the compiler allows us to use Car pointers in places where Vehicle pointer is expected. I.e., subtyping is activated through inheritance. Later in the post we will use subtyping without using inheritance.
<br/><br/>
If you think about pointers as a shortcut for the Pointer template below, it becomes apparent that the language has some special rules for them. Don't let the special * syntax confuse you. It is just a shortcut to avoid the ceremony below.
<pre class="brush: cpp">
template &lt;class T&gt;
using Pointer = T *;

Pointer&lt;Vehicle&gt; vehicle;
Pointer&lt;Car&gt; car;

vehicle = car; // Works!
</pre>

So what choices are available? The question we want to ask ourselves is, <u>"What relationship do I expect between instantiations of a template with two different types that happen to have a subtype relationship?"</u>
<ul>
<li>The first choice is <strong>no relationship</strong>. I.e., the template instantiations completely ignore the relationship between parameter types. This is C++ default. It's called invariance. (a.k.a. C++ templates are invariant)</li>
<li>The second choice is <strong>covariant</strong>. I.e., the template instantiations have the same subtype relationship as the parameter types. This is seen in C++ pointers and also in std::shared_ptr, std::unique_ptr because they want to behave as much like pointers as possible. You have write special code to enable that because the language does not give it to you by default.</li>
<li>The third choice is <strong>contravariance</strong>. I.e., the template instantiations have the opposite subtype relationship to that of the parameter types. I.e., TEMPLATE&lt;base&gt; is subtype of TEMPLATE&lt;derived&gt;. We'll come back to contravariance in much more detail later in the post.</li>
</ul>

All C++ standard library containers are invariant (even if they contain pointers).

<h2>Covariance</h2>

As said earlier, with covariance, the templated type maintains the relationship between argument types. I.e., if argument types are unrelated, the templated types shall be unrelated. If <i>derived</i> is a sub-type of  <i>base</i> (expressed as inheritance) then TEMPLATE&lt;derived&gt; shall be sub-type of TEMPLATE&lt;base&gt;. I.e., any place where TEMPLATE&lt;base&gt; is expected, TEMPLATE&lt;derived&gt; can be substituted and everything will work just fine. The other way around is not allowed.
<br/><br/>
There are some common examples of covariance in C++ standard library.
<pre class="brush: cpp">
std::shared_ptr&lt;Vehicle&gt; shptr_vehicle;
std::shared_ptr&lt;Car&gt; shptr_car;
shptr_vehicle = shptr_car; // Works
shptr_car = shptr_vehicle' // Does not work.

std::unique_ptr&lt;Vehicle&gt; unique_vehicle;
std::unique_ptr&lt;Car&gt; unique_car;
unique_vehicle = std::move(unique_car); // Works
unique_car = std::move(unique_vehicle); // Does not work
</pre>

One (formal) way to think about covariance is that "the type is allowed to get <strong>bigger</strong> upon assignment". I.e., Vehicle is broader/bigger type than Car. 

Here's a quick rundown of some of the commonly used C++ standard library types and their covariance/contravariance properties.
<br/><br/>
<table width="80%" border="1">
<tr>
<th>Type</th><th>Covariant</th><th>Contravariant</th>
</tr>
<tr><td>STL containers</td><td>No</td><td>No</td></tr>
<tr><td>std::initializer_list&lt;T *&gt;</td><td>No</td><td>No</td></tr>
<tr><td>std::future&lt;T&gt;</td><td>No</td><td>No</td></tr>
<tr><td>boost::optional&lt;T&gt;</td><td>No (see note below)</td><td>No</td></tr>
<tr><td>std::shared_ptr&lt;T&gt;</td><td>Yes</td><td>No</td></tr>
<tr><td>std::unique_ptr&lt;T&gt;</td><td>Yes</td><td>No</td></tr>
<tr><td>std::pair&lt;T *, U *&gt;</td><td>Yes</td><td>No</td></tr>
<tr><td>std::tuple&lt;T *, U *&gt;</td><td>Yes</td><td>No</td></tr>
<tr><td>std::atomic&lt;T *&gt;</td><td>Yes</td><td>No</td></tr>
<tr><td>std::function&lt;R *(T *)&gt;</td><td>Yes (in return)</td><td>Yes (in arguments)</td></tr>
</table>
<br/>
The boost::optional&lt;T&gt; appears to be covariant but it really isn't because it slices the object underneath. The same thing happens with std::pair and std::tuple. Therefore, they behave covariantly correctly only when the parameter type itself behaves covariantly. 
<br/><br/>
Finally, Combining one covariant type with another (e.g., std::shared_ptr&lt;std::tuple&lt;T *&gt;&gt;) does not necessarily preserve covariance because it is not built into the language. It is often implemented as a <strong>single-level</strong> direct convertibility. I.e., std::tuple&lt;Car *&gt; * is not directly convertible to std::tuple&lt;Vehicle *&gt; *. It would have been if the language itself enforced subtyping between std::tuple&lt;Car*&gt; and std::tuple&lt;Vehicle *&gt; but it does not. On the other hand, std::tuple&lt;std::shared_ptr&lt;T&gt;&gt; behaves covariantly.

<br/><br/>
By "single-level direct convertibility", I mean the following conversion of U* to T*. Convertibility is poor man's test for subtyping in C++. 

<br/><br/>
A covariant SmartPointer might be implemented as follows. <br/><br/>
<pre class="brush: cpp">
template &lt;class T&gt;
class SmartPointer
{
public:
    template &lt;typename U&gt;
    SmartPointer(U* p) : p_(p) {}

    template &lt;typename U&gt;
    SmartPointer(const SmartPointer&lt;U&gt;& sp,
                 typename std::enable_if&lt;std::is_convertible&lt;U*, T*&gt;::value, void&gt;::type * = 0) 
      : p_(sp.p_) {}

    template &lt;typename U&gt;
    typename std::enable_if&lt;std::is_convertible&lt;U*, T*&gt;::value, SmartPointer&lt;T&gt;&&gt;::type 
    operator=(const SmartPointer&lt;U&gt; & sp)
    {
        p_ = sp.p_;
        return *this;
    }

   T* p_;
};
</pre>

<h2>Contravariance</h2>

Contravariance, as it turns out, is quite counter-intuitive and messes up with your brain. But it is a very valid choice when it comes to selecting how generic types behave. Before we deal with contravariance, lets quickly revisit a very old C++ feature: covariant return types. 
<br/><br/>
Consider the following class hierarchy.
<br/>
<pre class="brush: cpp">
class VehicleFactory {
  public:
    virtual Vehicle * create() const { return new Vehicle(); }
    virtual ~VehicleFactory() {}
};

class CarFactory : public VehicleFactory {
public:
    virtual Car * create() const override { return new Car(); }
};
</pre>
Note that the return value of VehicleFactory::create function is Vehicle * where as CarFactory::create is Car *. This is allowed. The CarFactory::create function overrides its parent's virtual function. This feature is called overriding with covariant return types.
<br/><br/>
What happens when you change the raw pointers to std::shared_ptr? Is it still a valid program?....
<br/><br/>
As it turns out, it's not. std::shared_ptr (or any simulated covariant type for that matter) can't fool the compiler into believing that the two functions have covariant return types. The compiler rejects the code because as far as it knows, only the pointer types (and references too) have built-in covariance and nothing else. 
<br/><br/>
Lets look a these two factories from the substitutability perspective. The client of VehicleFactory (which has no knowledge of CarFactory) can use VehicleFactory safely even if the create function gets dispatched to CarFactory at run-time. After all, the create function return something that can be treated like a vehicle. No concrete details about Car are necessary for the client to work correctly. That's just classic Object-oriented programming.
<br/><br/> 
Covariance appears to work fine for return types of overridden functions. How about the argument? Is there some sort of variance possible? Does C++ support it? Does it make sense outside C++? <br/><br/>

Let's change the create function to accept Iron * as raw material. Obviously, the CarFactory::create must also accept an argument of type Iron *. It is supposed to work and it does. That's old hat. 
<br/><br/>
What if CarFactory is so advanced that it takes any Metal and creates a Car? Consider the following.
<pre class="brush: cpp">
struct Vehicle {};
struct Car : Vehicle {};

struct Metal {};
struct Iron : Metal {};

class VehicleFactory {
  public:
    virtual Vehicle * create(Iron *) const { return new Vehicle(); }
    virtual ~VehicleFactory() {}
};

class CarFactory : public VehicleFactory {
public:
    virtual Car * create(Metal *) const override { return new Car(); }
};
</pre>
The above program is illegal C++. The CarFactory::create does not override anything in its base class and therefore due to the override keyword compiler rejects the code. Without override, the program compiles but you are looking at two completely separate functions marked virtual but really they won't do what you expect. 
<br/></br/>
More interesting question is whether it makes sense to override a function in a way that the argument in the derived function is broader/larger than that of the bases's?...
<br/><br/>
<strong>Welcome to Contravariance...</strong><br/><br/>
It totally does make sense and this language feature is called <strong>contravariant argument types</strong>. From the perspective of the client of VehicleFactory, the client needs to provide some Iron. The CarFactory not only accepts Iron but any Metal to make a Car. So the Client works just fine. 
<br/><br/>
Note the reversed relationship in the argument types. The derived create function accepts the broader type because it must do at least as much as the base's function is able to do. This reverse relationship is the crux of contravariance.

<br/><br/>
C++ does not have built-in support for contravariant argument types. So that's how it ends for C++? Of course not!

<a name="function_contravariance"></a>
<h2>Covariant Return Types and Contravariant Argument Types in std::function</h2>
OK, the heading gives it away so lets get right down to an example.
<pre class="brush: cpp">
template &lt;class T&gt;
using Sink = std::function&lt;void (T *)&gt;;

Sink&lt;Vehicle&gt; vehicle_sink = [](Vehicle *){ std::cout &lt;&lt; "Got some vehicle\n"; };
Sink&lt;Car&gt; car_sink = vehicle_sink; // Works!
car_sink(new Car());

vehicle_sink = car_sink; // Fails to compile
</pre>
Sink is a function type that accepts any pointer of type T and return nothing. car_sink is a function that accepts only cars and vehicle_sink is a function that accepts any vehicle. Intuitively, it makes sense that if the client needs a car_sink, a vehicle_sink will work just fine because it is more general. Therefore, substitutability works in the reverse direction of parameter types. As a result, Sink is contravariant in its argument type.
<br/><br/>
std::function is covariant in return type too. 
<pre class="brush: cpp">
std::function&lt;Car * (Metal *)&gt; car_factory = 
  [](Metal *){ std::cout &lt;&lt; "Got some Metal\n"; return new Car(); };

std::function&lt;Vehicle * (Iron *)&gt; vehicle_factory = car_factory;

Vehicle * some_vehicle = vehicle_factory(new Iron()); // Works
</pre>

Covariance and Contravariance of std::function works with smart pointers too. I.e., std::function taking a shared_ptr of base type is convertible to std::function taking a shared_ptr of derived type.
<pre class="brush: cpp">

std::cout &lt;&lt; std::is_convertible&lt;std::function&lt;void (std::shared_ptr&lt;Vehicle&gt;)&gt;, 
                                 std::function&lt;void (std::shared_ptr&lt;Car&gt;)&gt;&gt;::value 
          << "\n"; // prints 1.

</pre>
<br/>
<strong>Sink of a Sink is a Source!</strong><br/><br/>
I hope the examples so far have helped build an intuition behind covariance and contravariance. So far it looks like types that appear in argument position should behave contravariantly and types that appear in return position, should behave covariantly. It's a good intuition only until it breaks!
<pre class="brush: cpp">
template &lt;class T&gt;
using Source = std::function&lt;void (Sink&lt;T&gt;)&gt;;

Source&lt;Car&gt; source_car = [](Sink&lt;Car&gt; sink_car){ sink_car(new Car()); };

source_car([](Car *){ std::cout << "Got a Car!!\n"; });

Source&lt;Vehicle&gt; source_vehicle = source_car; // covariance!

</pre>

Type T occurs at argument position in Source. So is Source contravariant in T?...
<br/><br/>
It's not! It's still covariant in T. 
<br/><br/>
However, Source&lt;T&gt; is contravriant in Sink&lt;T&gt; though.... Afterall, Source is a <strong>Sink of a Sink&lt;T&gt;</strong>! 
<br/><br/>
OK, still with me?
<br/><br/>
Let's get this *&%$# straight!
<br/><br/>
Source&lt;Car&gt; does not really take Car as an argument. It takes Sink&lt;Car&gt; as an argument. The only thing you can really do with it is sink/pass a car into it. Therefore, the lambda passes a new car pointer to sink_car. Again on the next line, calling source_car you have to pass a Sink&lt;Car&gt;. That of course is a lambda that accepts Car pointer as input and simply prints a happy message. 
<br/><br/>
Source&lt;Car&gt; indeed works like a factory of Cars. It does not "return" it. It uses a callback to give you your new car. It's equivalent to returning a new Car. After all, the direction of dataflow is outward. From Callee to the Caller. As the data is flowing outwards, it's covariant. 
<br/><br/>
More formally, type of Source is (T->())->(). A function that takes a callback as an input and returns nothing (i.e., read () as void). As T appears on the left hand side of even number of arrows, it's covariant with respect to the entire type. As simple as that!
<br/><br/>

<h2>Generalizing with Multiple Arguments and Currying</h2>
The covariance and contravariance of std::function works seamlessly with multiple argument functions as well as when they are <a href="http://cpptruths.blogspot.com/2014/03/fun-with-lambdas-c14-style-part-1.html">curried</a>.
<pre class="brush: cpp">
struct Metal {};
struct Iron : Metal {};
struct Copper : Metal {};

// multiple contravariant position arguments
std::function&lt;Vehicle * (Iron *, Copper *)&gt; vehicle_ic; 
std::function&lt;Car * (Metal *, Metal *)&gt; car_mm = [](Metal *, Metal *) { return new Car(); };
vehicle_ic = car_mm;
vehicle_ic(new Iron(), new Copper());

// Curried versions
std::function&lt;std::function&lt;Vehicle * (Copper *)&gt; (Iron *)&gt; curried_vehicle;
std::function&lt;std::function&lt;Car * (Metal *)&gt; (Metal *)&gt; curried_car;
curried_car = [](Metal *m) { 
  return std::function&lt;Car * (Metal *)&gt;([m](Metal *) { return new Car(); }); 
};  
curried_vehicle = curried_car;
curried_vehicle(new Iron())(new Copper());

</pre>

The car_mm function can be substituted where vehicle_ic is expected because it accepts wider types and returns narrower types (subtypes). The difference is that these are two argument functions. Each argument type must be at least the same as what's expected by the client or broader. 
<br/><br/>
As every multi-argument function can be represented in curried form, we don't want to throw way our nice co-/contra-variant capabilities of the function-type while currying. Of course, it does not as can be seen from the next example. 
<br/><br/>
The curried_vehicle function accepts a single argument and returns a std::function. curried_car is a subtype of curried_vehicle only if it accepts equal-or-broader type and returns equal-or-narrower type. Clearly, curried_car accepts Metal*, which is broader than Iron*. On the return side, it must return a function-type that is a subtype of the return type of curried_vehicle. Applying the rules of function subtyping again, we see that the returned function type is also a proper subtype. Hence currying is oblivious to co-/contra-variance of argument/return types.
<br/><br/>
So that's it for now on co-/contra-variance. CIAO until next time! 
<br/><br/>
<a href="http://melpon.org/wandbox/permlink/N88zRhrGxpxz040b">Live code</a> tested on latest gcc, clang, and vs2015.
<br/><br/>
For comments see <a href="https://www.reddit.com/r/cpp/comments/3styua/covariance_and_contravariance_in_c_standard/">reddit/r/cpp</a> and <a href="https://news.ycombinator.com/item?id=10567180">Hacker News</a>.
