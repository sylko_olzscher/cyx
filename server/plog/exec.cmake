# top level files
set (plog_server)

set (plog_server_cpp

	server/plog/src/main.cpp
	server/plog/src/controller.cpp
	server/plog/src/server.cpp
 	server/plog/src/session.cpp
 	
	lib/shared/tcpip_server.cpp
	
)

set (plog_server_h

	server/plog/src/controller.h
	server/plog/src/server.h
	server/plog/src/session.h
	
	lib/shared/tcpip_server.h

	src/main/include/cyx/version_info.h
	src/main/include/cyx/constants.h
	src/main/include/cyx/layer4/tcpip_socket.h

	src/main/include/cyx/http/session/tidy.hpp
)

set (plog_server_res
	server/plog/htdocs/index.html
	server/plog/htdocs/admin/index.html
	server/plog/htdocs/css/plog.css
)

set (plog_compiler

	tools/compiler/src/driver.cpp
	tools/compiler/src/reader.cpp
	tools/compiler/src/generator.cpp
	tools/compiler/src/driver.h
	tools/compiler/src/reader.h
	tools/compiler/src/generator.h	
)


set (plog_server_setup
	src/main/templates/create_plog_srv.bat.in
	src/main/templates/delete_plog_srv.bat.in
	src/main/templates/restart_plog_srv.bat.in
	src/main/templates/plog.windows.cgf.in
)

set (plog_server_tasks
	server/plog/src/tasks/tidy.h
	server/plog/src/tasks/tidy.cpp
)

source_group("resources" FILES ${plog_server_res})
source_group("setup" FILES ${plog_server_setup})
source_group("compiler" FILES ${plog_compiler})
source_group("tasks" FILES ${plog_server_tasks})

# define the main program
set (plog_server
  ${plog_server_cpp}
  ${plog_server_h}
  ${plog_server_res}
  ${plog_compiler}
  ${plog_server_setup}
  ${plog_server_tasks}
)
