/*
 * Copyright Sylko Olzscher 2015
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "session.h"
#include "../../../tools/compiler/src/driver.h"
#include <cyx/layer4/tcpip_socket.h>
#include <cyx/constants.h>
#include <cyx/http/lexer1.1.h>
#include <cyx/http/serializer1.1.h>
#include <cyx/http/intrinsics/io/io.h>
#include <cyx/http/intrinsics/factory/status_factory.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/vm/code_generator.h>
#include <cyy/vm/log_processor.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/scheduler/factory.hpp>
#include <cyy/io/io_set.h>
#include <cyy/io/hex_dump.hpp>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/util/read_sets.h>
#include <array>
#include <boost/uuid/uuid_io.hpp>
#include <boost/core/ignore_unused.hpp>

namespace cyx
{

	//	--- HTTP session implementation
	struct http_session
	{

		/**
		 * define a receive buffer type
		 */
	#if defined(BOOST_NO_CXX11_CONSTEXPR)
		typedef std::array< char, BUFFER_SIZE >	buffer_type;
	#else
		typedef std::array< char, constants::BUFFER_SIZE >	buffer_type;
	#endif

		/**
		 * Pointer to session interface
		 */
		session* session_;

		/**
		 * The TCP/IP socket (unsecured)
		 */
		socket_t socket_;

		/**
		 * Hold received data
		 */
		buffer_type	read_buffer_;

		/**
		 * internal VM
		 */
		cyy::vm_dispatcher vm_;

		/**
		 * HTTP/1.1 parser
		 */
		lexer 	parser_;

		/**
		 * Serialization engine for HTTP/1.1 protocol
		 */
		serializer	serializer_;

		/**
		 * managing all session tasks
		 */
		cyy::scheduler::registry&	reg_;

		/**
		 * threadsafe logger class
		 */
		logger_type&	logger_;

		/**
		 * document root
		 */
		const boost::filesystem::path  htdocs_, storage_;
		const path_t	get_content_;

		/**
		 * shutdown state. There is a difference between a closed session
		 * and a system shutdown. If this flag is true, the complete system
		 * shuts down and no further action is required.
		 */
		bool shutdown_;

		/*
		 * docscript compiler driver
		 */
		driver	driver_;	// (std::vector< std::string >const& inc, int verbose)

		http_session(session* client
			, boost::asio::ip::tcp::socket&& s
			, logger_type& logger
			, cyy::scheduler::registry& reg
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, boost::filesystem::path const& htdocs
			, boost::filesystem::path const& storage
			, std::vector<std::string> const& includes
			, int verbose)
		: session_(client)
			, socket_(std::move(s))
			, read_buffer_()
			, vm_(tag, socket_.get_io_service())
			, parser_([this](cyy::vector_t&& prg) {

#ifdef _DEBUG
				//std::for_each(prg.begin(), prg.end(), [this](cyy::object obj) {
				//	CYY_LOG_TRACE(logger_, "param: " << cyy::to_literal(obj, cyx::io::custom));
				//});
#endif
            
				//CYY_LOG_INFO(logger_, "session ["
				//	<< cyy::uuid_short_format(vm_.tag())
				//	<< "] executes "
				//	<< prg.size()
				//	<< " instructions");

				//
				//	parser callback
				//
				vm_.exec(std::move(prg));

			}, logger)
			, serializer_(vm_, db)
			, reg_(reg)
			, logger_(logger)
			, htdocs_(htdocs)
			, storage_(storage)
			, get_content_({"get", "snippet"})
			, shutdown_(false)
			, driver_(includes, verbose)
		{

			//
			//	register logging functions
			//
			cyy::register_logger(logger_, vm_);

			//
			//	This is the adapter between the read and write operation of the socket.
			//	The serializer contains a buffer which serializes data but needs
			//	an (open) socket to send them.
			//
			vm_.async_run(cyy::register_function("io.flush", 0, [this](cyy::context& ctx) {
				const boost::system::error_code ec = serializer_.flush(socket_);
				ctx.set_register(ec);
			}));

			//
			//	Close the TCP/IP socket
			//
			vm_.async_run(cyy::register_function("io.close", 0, [this](cyy::context& ctx) {

				boost::system::error_code ec;
				socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
				socket_.close(ec);
				ctx.set_register(ec);

				CYY_LOG_INFO(logger_, "session "
                    << cyy::uuid_short_format(vm_.tag())
                    << " - io.close: "
                    << ec.message());
                
			}));

			//
			//	set shutdown flag
			//
			vm_.async_run(cyy::register_function("session.shutdown", 0, [this](cyy::context& ctx) {

				BOOST_ASSERT_MSG(!shutdown_, "session already in shutdown mode");
				shutdown_ = true;

				CYY_LOG_INFO(logger_, "session."
					<< cyy::uuid_short_format(vm_.tag())
					<< ".session.shutdown ");

			}));

			//
			//	"http.get"
			//
			vm_.async_run(cyy::register_function("http.get", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	[v1.1,"/favicon.ico",?,"",%(("Accept":"*/*"),("Accept-Encoding":"gzip, deflate, sdch"),("Accept-Language":"de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4"),...)]
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.get: "
					<< cyy::io::to_literal(frame, cyx::io::custom ));

				//	get params
				cyy::vector_reader	reader(frame);
				const cyy::version v = reader.get(0, cyy::version());
				const path_t p = reader.get(1, path_t());
				boost::ignore_unused(v);
				
				if (is_favicon(p))
				{
					//	image/x-icon
					ctx.invoke("io.send.favicon").invoke("io.flush");
				}
				else
				{
					if (is_directory(p))
					{
						if (starts_with(p, get_content_))
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / "/posts/text002.txt")
							));
						}
						else if (starts_with(p, {"admin"}))
						{
							const std::string auth = reader[4].get_string("Authorization");
							if (auth.empty())
							{
								//	send basic authorization request
								ctx.inject(cyy::generate_invoke("io.auth.basic"
									, cyy::string_factory("dashboard")
								));
							}
							else
							{
								ctx.inject(cyy::generate_invoke("io.send.file"
									, cyy::factory(htdocs_ / to_path(p) / "index.html")
								));

							}
						}
						else
						{ 
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / to_path(p) / "index.html")
							));
						}
					}
					else
					{
						if ((p.size() == 1) && (p.at(0) == "index.json"))
						{
							//	generate blog index: This is a list of all available posts
							deliver_index(ctx);
						}
						else if ((p.size() == 1) && (p.at(0) == "time.html"))
						{
							std::string msg("<html><body>hello, world at "
								+ cyy::json_format(std::chrono::system_clock::now())
								+ "</body></html>");

							std::stringstream ss;
							ss
								<< "HTTP/1.1 200 OK"
								<< "\r\n"
								<< "Content-Type: text/html"
								<< "\r\n"
								<< "Content-Length: "
								<< msg.size()
								<< "\r\n"
								<< "\r\n"
								<< msg
								;
							const std::string response(ss.str());
							boost::asio::write(socket_, boost::asio::buffer(response.data(), response.size()));

						}
						else
						{
							ctx.inject(cyy::generate_invoke("io.send.file"
								, cyy::factory(htdocs_ / to_path(p))
							));
						}
					}
					ctx.invoke("io.flush");
				}

			}));

			//
			//	"http.head"
			//
			vm_.async_run(cyy::register_function("http.head", 2, [this](cyy::context& ctx) {

				CYY_LOG_INFO(logger_, "session "
					<<cyy::uuid_short_format(vm_.tag())
					<< " - http.head");

				const std::string response("HTTP/1.1 200 OK\r\n\r\n<html><body>welcome</body></html>\r\n");
				socket_.write_some(boost::asio::buffer(response.data(), response.size()));

			}));

			//
			//	"http.post.xml"
			//
			vm_.async_run(cyy::register_function("http.post.xml", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.xml"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	"http.post.json"
			//
			vm_.async_run(cyy::register_function("http.post.json", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.json"
					<< cyy::io::to_literal(frame, cyx::io::custom ));

			}));

			//
			//	"http.connect"
			//	working as relay
			//
			vm_.async_run(cyy::register_function("http.connect", 2, [this](cyy::context& ctx) {

				//
				//	example:
				//	CONNECT  126mx03.mxmail.netease.com:25 HTTP / 1.0<CR><NL><CR><NL>
				//

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.connect");

				ctx.inject(cyy::generate_invoke("io.stock.response", cyy::status_factory(codes::MethodNotAllowed)));
				ctx.invoke("io.flush");

			}));

			//
			//	http.post.form
			//
			vm_.async_run(cyy::register_function("http.post.form", 2, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.post.form"
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

			//
			//	http.open.websocket
			//
			vm_.async_run(cyy::register_function("http.open.websocket", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//
				//	example:
				//	[v1.1,/ws/devices/,?,"",%(("Accept-Encoding":["gzip","deflate","sdch","br"]),("Accept-Language":"de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4"),("Cache-Control":"no-cache"),("Connection":["Upgrade"]),("DNT":true),("Host":"localhost:8080"),("Origin":"http://localhost:8080"),("Pragma":"no-cache"),("Sec-WebSocket-Extensions":"permessage-deflate; client_max_window_bits"),("Sec-WebSocket-Key":"EZ3OgUQoVh12seu1tQl60g=="),("Sec-WebSocket-Protocol":["soap","xmpp"]),("Sec-WebSocket-Version":13i32),("Upgrade":"websocket"),("User-Agent":"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"))]
				//

				//
				//	extract the following parameters:
				//
				//	("Sec-WebSocket-Extensions":"permessage-deflate; client_max_window_bits"), 
				//	("Sec-WebSocket-Key":"L3jfmDvL071963wiye1FMA=="), 
				//	("Sec-WebSocket-Protocol":"soap, xmpp"), 
				//	("Sec-WebSocket-Version":"13"), 
				//
				cyy::vector_reader	reader(frame);
				const cyx::path_t p = reader.get(1, cyx::path_t());
				const auto ws_key = reader[4].get_string("Sec-WebSocket-Key");
				const std::vector<std::string> ws_protocols = reader[4].get_vector<std::string>("Sec-WebSocket-Protocol", "");
				const auto ws_version = reader[4].get_numeral<int>("Sec-WebSocket-Version", 0);

				boost::ignore_unused(ws_version);
				
				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.open.websocket: "
					<< cyx::to_str(p));

				//CYY_LOG_TRACE(logger_, "session "
				//	<< cyy::uuid_short_format(vm_.tag())
				//	<< " - http.open.websocket key: "
				//	<< ws_key);

				//CYY_LOG_TRACE(logger_, "session "
				//	<< cyy::uuid_short_format(vm_.tag())
				//	<< " - http.open.websocket version: "
				//	<< ws_version);

				std::string ps;
				for (auto p : ws_protocols)
				{
					ps += p;
					ps += ' ';
				}
				CYY_LOG_TRACE(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.open.websocket protocols: "
					<< ps);

				//
				//	accept incoming websocket open request
				//
				ctx.inject(cyy::generate_invoke("io.upgrade.websocket"
					, cyy::find(frame.at(4), "Sec-WebSocket-Key")
					, cyy::string_factory("dsync")
				));

				ctx.invoke("io.flush");

				//
				//	give feedback
				//
				cyy::param_map_t load;

				//
				//	send server time
				//
				load["now"] = cyy::time_point_factory();
				ctx.inject(cyy::generate_invoke("io.ws.json", cyy::factory(load)));
				ctx.invoke("io.flush");

			}));

			//
			//	ws.data.text
			//
			vm_.async_run(cyy::register_function("ws.data.text", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//CYY_LOG_TRACE(logger_, "session "
				//	<< vm_.tag()
				//	<< " - ws.data.text"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//
				//	get text
				//
				const auto text = cyy::string_cast(frame.at(0), "");

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.data.text: "
					<< text);

				//
				//	echo text
				//
				//ctx.inject(cyy::generate_invoke("io.ws.text"
				//	, cyy::factory("> " + text)
				//));

				//
				//	flush
				//
				//ctx.invoke("io.flush");

				//
				//	send ping
				//
				ctx.invoke("io.ws.ping");
				ctx.invoke("io.flush");

			}));

			//
			//	ws.pong
			//
			vm_.async_run(cyy::register_function("ws.pong", 1, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - ws.pong "
					<< cyy::io::to_literal(frame, cyx::io::custom));

			}));

			//
			//	http.upload.data
			//
			vm_.async_run(cyy::register_function("http.upload.data", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	synopsis: 
				//	* variable name (id)
				//	* file name
				//	* MIME type
				//	* data
				//	* URL
				//	["filesToUpload[]","sml_2016_08_01T15_15_17_00000967_00008edd_gex.gwf.ch_26862.xml",text/xml,'\x3c\x3...]
				//CYY_LOG_DEBUG(logger_, "session "
				//	<< vm_.tag()
				//	<< " - http.upload.data"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//	get params
				cyy::vector_reader	reader(frame);
				const std::string var = reader.get_string(0);
				const boost::filesystem::path file = reader.get_string(1);	//	IE delivers a full path with arent directory
				const cyy::buffer_t& data = reader.get(3, cyy::buffer_t());

				//auto storage = boost::filesystem::temp_directory_path();

				if (file.empty())
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.data - received ["
						<< var
						<< "] with "
						<< data.size()
						<< " bytes");
				}
				else
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.data - file "
						<< (storage_ / file.filename())
						<< " with "
						<< data.size()
						<< " bytes");

					//
					//	write into temporary file
					//
					std::fstream fout((storage_ / file.filename()).string(), std::ios::trunc | std::ios::out);
					if (fout.is_open())
					{
						fout.write(data.data(), data.size());
						fout.flush();
						fout.close();

						//
						//	generate some temporary file names for intermediate files
						//
						boost::filesystem::path body = boost::filesystem::temp_directory_path() / (boost::filesystem::path(file).filename().string() + ".bin");

						CYY_LOG_INFO(logger_, "session "
							<< cyy::uuid_short_format(vm_.tag())
							<< " - http.upload.data - start compiler with "
							<< (storage_ / file.filename()));

						//
						//	start compiler
						//
                        driver_.run(storage_ / file.filename()
                                      , body
                                      , boost::filesystem::temp_directory_path() / "a.html"
                                      , true);
					}
					else
					{
						CYY_LOG_ERROR(logger_, "session "
							<< cyy::uuid_short_format(vm_.tag())
							<< " - http.upload.data - cannot open "
							<< (storage_ / file).string());
					}
				}

			}));

			//
			//	"http.upload.complete"
			//
			vm_.async_run(cyy::register_function("http.upload.complete", 5, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	 [true,3228u32,/upload/files/,?,""]
				//CYY_LOG_INFO(logger_, "session "
				//	<< vm_.tag()
				//	<< " - http.upload.complete "
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				//	get params
				cyy::vector_reader	reader(frame);
				const auto size = reader.get_numeral<std::uint32_t>(1, 0);

				CYY_LOG_INFO(logger_, "session "
					<< cyy::uuid_short_format(vm_.tag())
					<< " - http.upload.complete - with "
					<< size
					<< " bytes");
			}));

			//
			//	http.upload.progress
			//
			vm_.async_run(cyy::register_function("http.upload.progress", 3, [this](cyy::context& ctx) {

				const cyy::vector_t frame = ctx.stack_frame(true);

				//	["filesToUpload[]",4630idx,4873idx,95idx]
				//CYY_LOG_INFO(logger_, "session "
				//<< cyy::uuid_short_format(vm_.tag())
				//	<< " - http.upload.progress"
				//	<< cyy::io::to_literal(frame, cyx::io::custom));

				cyy::vector_reader	reader(frame);
				const std::uint32_t progress = reader.get_numeral<std::uint32_t>(2, 0);

				if ((progress % 10) == 0)
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(vm_.tag())
						<< " - http.upload.progress "
						<< progress
						<< "%");
				}

			}));

		}

		~http_session()
		{

			CYY_LOG_DEBUG(logger_,
				"session "
				<< cyy::uuid_short_format(vm_.tag())
				<< " - terminated");
		}

		/**
		 * Start and continue reading from socket
		 */
		void start()
		{

			//	hold a reference
			session::shared_type safe_sp = session_->shared_from_this();

			BOOST_ASSERT_MSG(socket_.is_open(), "socket closed");
			BOOST_ASSERT_MSG(safe_sp, "no session");

			socket_.async_read_some(boost::asio::buffer(read_buffer_, sizeof(buffer_type)-1),
			[this, safe_sp](boost::system::error_code const & ec, std::size_t bytes_transferred) {

				if (!ec)
				{
					BOOST_ASSERT_MSG(bytes_transferred < read_buffer_.size(), "buffer overflow");
					
                    CYY_LOG_TRACE(logger_, "session ["
                        << cyy::uuid_short_format(vm_.tag())
                        << "] received " 
                        << bytes_transferred 
                        << " byte(s)");

					//std::ostringstream ss;
					//cyy::io::hex_dump hd;
					//hd(ss, &this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);
					//CYY_LOG_TRACE(logger_, '\n' << ss.str());
					
					//
					//	run parser and VM implicitely
					//
					this->parser_.read(&this->read_buffer_[0], &this->read_buffer_[bytes_transferred]);

					//
					//	+-----                                    -----+
					//	The parser may produce code that closes the socket.
					//	So we must be aware of this case.
					//	To close a socket, VM must work in sync-mode. Otherwise
					//	there would be no guaranty that this session is still valid.
					//	+-----                                    -----+
					//
					if (socket_.is_open())
					{
						this->start();
					}
				}
				else
				{
					CYY_LOG_TRACE(logger_, "session "
						<< cyy::uuid_short_format(this->vm_.tag())
						<< (shutdown_ ? " server " : " client ")
						<< " closed socket");

					if (shutdown_)
					{

						//	controlled by server
						//	close socket explicitely
						boost::system::error_code ec;
						socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
						socket_.close(ec);

					}
					else
					{
						//
						//	update shutdown state
						//
						shutdown_ = true;

						//
						//	remove from session list
						//
						reg_.send("session.admin", 2, this->vm_.tag());

					}

					//
					//	halt VM
					//
					if (vm_.halt(4096))
					{

						CYY_LOG_TRACE(logger_, "session "
							<< cyy::uuid_short_format(this->vm_.tag())
							<< " - halted");
					}
					else
					{
						CYY_LOG_ERROR(logger_, "session "
							<< cyy::uuid_short_format(this->vm_.tag())
							<< " - halted with "
							<< vm_.pending_requests()
							<< " pending requests");
					}

				}
			});
		}

		void deliver_index(cyy::context& ctx)
		{
			const boost::filesystem::path idx_file(htdocs_ / "posts/index.json");

			ctx.inject(cyy::generate_invoke("io.send.file"
				, cyy::factory(idx_file)
			));
		}

	};

	//	--- HTTP1/1 session

	session::session(boost::asio::ip::tcp::socket&& s
		, logger_type& logger
		, cyy::scheduler::registry& reg
		, cyy::store::store& db
		, boost::uuids::uuid tag
		, boost::filesystem::path const& htdocs
		, boost::filesystem::path const& storage
		, std::vector<std::string> const& includes
		, int verbose)
		//	make_unique<> requires a c++14 complient compiler
#if __GNUC__ > 6 || defined(_MSC_VER) 	
	: impl_(std::make_unique<http_session>(this	
#else
	: impl_(std::make_shared<http_session>(this	
#endif
		, std::move(s)
		, logger
		, reg
		, db
		, tag
		, htdocs
		, storage
		, includes
		, verbose))
	{}

	session::~session()
	{
		//	delete HTTP1/1 session explicit
		impl_.reset();
	}

	void session::start()
	{
		//
		//
		//	Start session to read from socket
		impl_->start();
	}

	cyy::vm_dispatcher& session::vm()
	{
		return impl_->vm_;
	}

}	//	cyx

namespace cyy
{
	object factory(cyx::session::shared_type s)
	{
		return object(core::make_value(s));
	}

	object factory(cyx::session::weak_type w)
	{
		return object(core::make_value(w));
	}

	object weak_session_ptr_factory(cyx::session::shared_type sp)
	{
		cyx::session::weak_type wp = sp;
		return factory(wp);
	}
}
