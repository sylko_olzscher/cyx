/*
 * Copyright Sylko Olzscher 2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "tidy.h"
//#include <noddy/db/session.h>
#include <cyy/io/format/digest_format.h>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/vm/code_generator.h>
#include <cyy/scheduler/factory.hpp>
#include <cyy/vm/vm_dispatcher.h>
#include <boost/uuid/uuid_io.hpp>

namespace cyx
{
	task_tidy::task_tidy(cyy::broker& b
	, logger_type& l)
	: broker_(b)
	, logger_(l)
	, connections_()
	{
		CYY_LOG_INFO(logger_,  "task."
			<< broker_.get_tag()
			<< ".tidy - created");
			
	}
		

	void task_tidy::process(std::string const& fun, cyy::vector_t const& data)
	{
		const boost::uuids::uuid tag = cyy::uuid_cast(data[0]);
		auto pos = connections_.find(tag);
		if (pos != connections_.end())
		{
			//	get session pointer
			auto sp = pos->second.lock();
			if (sp)
			{
				sp->vm().exec(cyy::generate_invoke(fun, data));
			}
			else
			{
				CYY_LOG_ERROR(logger_, "task."
					<< broker_.get_tag()
					<< ".tidy - client "
					<< cyy::uuid_short_format(tag)
					<< " not longer valid");

				//
				//	remove from connection list
				//
				connections_.erase(pos);
			}
		}
		else
		{
			CYY_LOG_ERROR(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - client "
				<< cyy::uuid_short_format(tag)
				<< " not found");
		}
	}

	void task_tidy::process(session::shared_type sp)
	{
		if (sp && connections_.emplace(sp->vm().tag(), sp).second)
		{
			CYY_LOG_INFO(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - "
				<< connections_.size()
				<< " sessions online");

			sp->start();
		}
		else
		{
			CYY_LOG_ERROR(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - cannot start client");

		}
	}

	void task_tidy::process(boost::uuids::uuid tag)
	{
		CYY_LOG_INFO(logger_, "task."
			<< broker_.get_tag()
			<< ".tidy - shutdown session "
			<< cyy::uuid_short_format(tag));

		//	shutdown
		if (connections_.erase(tag) != 0)
		{
			CYY_LOG_INFO(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - "
				<< connections_.size()
				<< " session(s) online");
		}
		else 
		{
			CYY_LOG_ERROR(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - session ["
				<< tag
				<< "] not found in client list");
		}
	}

	void task_tidy::process()
	{
		//	stop all (active) sessions
		std::list<session::shared_type>	s;
		std::for_each(connections_.begin(), connections_.end(), [&s](connections_t::value_type const& c) {

			auto sp = c.second.lock();
			if (sp)
			{
				s.push_back(sp);
			}
		});


		CYY_LOG_INFO(logger_, "task."
			<< broker_.get_tag()
			<< ".tidy - close "
			<< s.size()
			<< "/"
			<< connections_.size()
			<< " session(s)");

		for (auto sp : s)
		{
			const auto tag = sp->vm().tag();

			CYY_LOG_INFO(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - close session "
				<< cyy::uuid_short_format(tag));

			//
			//	set shutdown flag, close connection and halt VM
			//
			sp->vm().invoke("session.shutdown").invoke("io.close").halt();

			//
			//	remove from connection list
			//
			BOOST_ASSERT_MSG(!connections_.empty(), "no connections");
			connections_.erase(tag);
		}

		CYY_LOG_INFO(logger_, "task."
			<< broker_.get_tag()
			<< ".tidy - "
			<< s.size()
			<< " session(s) closed");
	}

	void task_tidy::stop()
	{
		if (!connections_.empty())
		{
			CYY_LOG_FATAL(logger_, "task."
				<< broker_.get_tag()
				<< ".tidy - open "
				<< connections_.size()
				<< " session(s)");

			//
			//	wait a second
			//
			std::this_thread::sleep_for(std::chrono::seconds(1));
			
			
			//
			//	clear connection table
			//
			connections_.clear();
		}

		CYY_LOG_INFO(logger_, "task."
			<< broker_.get_tag()
			<< ".tidy - stop");
	}

	void task_tidy::run(int workload)
	{
		CYY_LOG_INFO(logger_, "task."
			<< broker_.get_tag()
			<< ".tidy - run");

	}

}	//	cyx
