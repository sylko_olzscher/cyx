/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "server.h"
#include "session.h"
#include "tasks/tidy.h"
#include <cyx/http/session/tidy.hpp>
#include <cyy/scheduler/ruler.h>
#include <cyy/util/read_sets.h>
#include <cyy/vm/code_generator.h>
#include <cyy/store/store.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/vm/vm_dispatcher.h>
#include <boost/uuid/uuid_io.hpp>

namespace cyx
{
	server::server(logger_type& logger
		, cyy::ruler& scheduler
		, cyy::store::store& db
		, boost::uuids::uuid tag
		, std::string const& root
		, std::string const& css
		, std::string const& js
		, boost::filesystem::path file_storage
		, std::vector<std::string> includes
		, int verbose
		, std::string const& realm)
	: tcpip_srv(scheduler.get_io_service())
		, logger_(logger)
		, registry_(scheduler)
		, db_(db)
		, uuid_gen_()
// 		, connections_()
		, htdocs_(root)
		, css_file_(htdocs_ / css)
		, js_file_(htdocs_ / js)
		, file_storage_(file_storage)
		, includes_(includes)
		, verbose_(verbose)
		, realm_(realm)
	{		
		CYY_LOG_INFO(logger_, "document-root: "	<< htdocs_);
		CYY_LOG_INFO(logger_, "css-file: "	<< css_file_);
		CYY_LOG_INFO(logger_, "js-file: "	<< js_file_);
		CYY_LOG_INFO(logger_, "file-storage: " << file_storage_);
		CYY_LOG_INFO(logger_, "realm: " << realm_);

		//
		//	start static tasks to manage lifetime of sessions
		//
		registry_.start<task_tidy>("session.admin", logger_);
	}

	server::~server()
	{
		CYY_LOG_INFO(logger_, "server exit...");
	}

	void server::on_connect(socket_t&& s)
	{
		CYY_LOG_INFO(logger_, "on_connect("
			<< s.remote_endpoint()
			<< ')');

		
		connect(http::make_session(std::move(s)
			, logger_
			, registry_
			, db_
			, uuid_gen_()
// 			, connections_
			, htdocs_
			, file_storage_
			, includes_
			, verbose_));
	}

	bool server::run(cyy::tuple_t const& config)
	{
		//
		//	node address and service (port)
		//
		cyy::tuple_reader reader(config);
		const std::string address = reader.get<std::string>("address", "0.0.0.0");
		const std::string service = reader.get<std::string>("service", "8080");

		CYY_LOG_INFO(logger_, "startup server on "
					<< address
					<< ':'
					<< service);

		// call base class
		return tcpip_srv::run(address, service);
	}

	void server::stop()
	{
		//	call base class
		CYY_LOG_INFO(logger_, "close TCP/IP acceptor");
		tcpip_srv::shutdown();

		//	stop all connections
		registry_.deliver("session.admin", 3);

	}

	void server::on_error(boost::system::error_code const& ec)
	{
		CYY_LOG_ERROR(logger_, ec.message());
	}
	
	void server::connect(session::shared_type sp)
	{
		BOOST_ASSERT(sp);

		//	send START message to tidy task
		registry_.send("session.admin", 1, cyy::factory(sp));
	}
			
	namespace http 
	{
		session::shared_type make_session(boost::asio::ip::tcp::socket&& s
			, logger_type& logger
			, cyy::scheduler::registry& reg
			, cyy::store::store& db
			, boost::uuids::uuid tag
// 			, server::connections_t& connections
			, boost::filesystem::path const& htdocs
			, boost::filesystem::path const& storage
			, std::vector<std::string> const& includes
			, int verbose)
		{
			using tidy_t = tidy<session>;
			return std::shared_ptr<session>(new session(std::move(s), logger, reg, db, tag, htdocs, storage, includes, verbose), tidy_t(logger, tag));
		}
	}	
}
