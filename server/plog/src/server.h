/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_PLOG_SERVER_H
#define CYX_PLOG_SERVER_H

#include "session.h"
#include "../../../lib/shared/tcpip_server.h"
#include <cyy/scheduler/logging/log.h>
#include <cyy/scheduler/registry.h>
#include <memory>
#include <boost/uuid/random_generator.hpp>

#include <boost/uuid/uuid.hpp>

namespace cyy
{
	class object;
	class ruler;
	namespace store
	{
		class store;
	}
}

namespace cyx
{

	class server : public tcpip_srv
	{
	public:
		typedef std::map<boost::uuids::uuid, session::weak_type>	connections_t;
		
	public:
		server() = delete;
		server(server const&) = delete;

		/**
		 * @param scheduler thread scheduler
		 * @param account administrator account
		 * @param pwd_hash password hash
		 * @param monitor maximal timeout for login and other requests
		 */
		server(logger_type& logger
			, cyy::ruler& scheduler
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, std::string const& root
			, std::string const& css
			, std::string const& js
			, boost::filesystem::path file_storage
			, std::vector<std::string> includes
			, int verbose
			, std::string const& realm);

		virtual ~server();

		/**
		* Startup TCP/IP server
		*/
		bool run(cyy::tuple_t const& cfg);

		/**
		* Shutdown TCP/IP server
		*/
		void stop();

	protected:
		virtual void on_connect(socket_t&& s);
		virtual void on_error(boost::system::error_code const& ec);
		
		void connect(session::shared_type sp);

	private:
		logger_type&	logger_;
		cyy::scheduler::registry	registry_;
		cyy::store::store& db_;
		boost::uuids::random_generator	uuid_gen_;	//	basic_random_generator<mt19937>
// 		connections_t	connections_;
		
		const boost::filesystem::path	htdocs_;
		const boost::filesystem::path	css_file_;
		const boost::filesystem::path	js_file_;
		const boost::filesystem::path	file_storage_;
		const std::vector<std::string>	includes_;
		const int verbose_;
		const std::string realm_;
	};
		
	namespace http 
	{
		/**
		 * session factory
		 */
		session::shared_type make_session(boost::asio::ip::tcp::socket&&
			, logger_type& logger
			, cyy::scheduler::registry&
			, cyy::store::store& db
			, boost::uuids::uuid tag
// 			, server::connections_t&
			, boost::filesystem::path const&
			, boost::filesystem::path const& file_storage
			, std::vector<std::string> const& includes
			, int verbose);
	}
		
}

#endif	//	CYX_PLOG_SERVER_H

