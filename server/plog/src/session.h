/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYX_HTTP_SESSION_H
#define CYX_HTTP_SESSION_H

#include <memory>
#include <vector>
#include <boost/uuid/uuid.hpp>
#include <boost/system/error_code.hpp>
#include <cyy/intrinsics/sets_fwd.h>
#include <cyy/scheduler/logging/log.h>
#include <cyy/scheduler/registry.h>
#include <cyy/vm/context.h>
#include <cyy/store/store.h>

//
//
// some forward declarations (to save compile time because of to many includes)
namespace cyy
{
	class ruler;
	class vm_dispatcher;
	namespace store
	{
		class store;
	}
}

namespace cyx
{

	struct http_session;	//!<	the implementation class

	/**
	 * Implementation of an HTTP client
	 */
	class session : public std::enable_shared_from_this<session>
	{
	public:
		typedef std::shared_ptr<session>	shared_type;
		typedef std::weak_ptr<session>		weak_type;

	public:
		session(boost::asio::ip::tcp::socket&&
			, logger_type& logger
			, cyy::scheduler::registry&
			, cyy::store::store& db
			, boost::uuids::uuid tag
			, boost::filesystem::path const& htdocs
			, boost::filesystem::path const& storage
			, std::vector<std::string> const& includes
			, int verbose);
		virtual ~session();

		/**
		 * Start session to read from socket
		 */
		void start();

		/**
		 * expose VM dispatcher to open
		 * an asynchronous communication path
		 */
		cyy::vm_dispatcher& vm();

    private:
#if __GNUC__ > 6 || defined(_MSC_VER)
		std::unique_ptr<http_session>	impl_;
#else
		std::shared_ptr<http_session>	impl_;
#endif
	};


}	//	cyx

#include <cyx/http/type_codes.h>

namespace cyy
{
	// 	noddy::types::SHARED_SESSION - noddy::session::shared_type
	template <>
	struct type_traits< cyx::session::shared_type >
	{
		typedef cyx::session::shared_type type;
		typedef std::integral_constant<cyy::code_t, cyx::types::SHARED_SESSION>	code_;
		typedef cyx::session subtype;
	};

	template <>
	struct reverse_type < cyx::types::SHARED_SESSION >
	{
		typedef cyx::session::shared_type type;
		static_assert(type_traits<type>::code_::value == cyx::types::SHARED_SESSION, "assignment error");
	};

	// 	noddy::types::WEAK_SESSION - noddy::session::shared_type
	template <>
	struct type_traits< cyx::session::weak_type >
	{
		typedef cyx::session::weak_type type;
		typedef std::integral_constant<cyy::code_t, cyx::types::WEAK_SESSION>	code_;
		typedef cyx::session subtype;
	};

	template <>
	struct reverse_type < cyx::types::WEAK_SESSION >
	{
		typedef cyx::session::weak_type type;
		static_assert(type_traits<type>::code_::value == cyx::types::WEAK_SESSION, "assignment error");
	};

	//
	//	factory
	//

	object factory(cyx::session::shared_type);
	object weak_session_ptr_factory(cyx::session::shared_type);
	object factory(cyx::session::weak_type);

}
#endif	//	CYX_HTTP_SESSION_H


